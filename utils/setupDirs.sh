workDirRoot=/nomad/nomadlab/work-local

rawDataRoot=/nomad/nomadlab/raw-data
parsedRoot=/nomad/nomadlab/parsed
normalizedRoot=/nomad/nomadlab/normalized

mkdir -p "$workDirRoot"
mkdir -p "$rawDataRoot"
mkdir -p "$parsedRoot"
mkdir -p "$normalizedRoot"
sudo ln -s "$workDirRoot" /work-local
sudo ln -s "$rawDataRoot" /raw-data
sudo ln -s "$parsedRoot" /parsed
sudo ln -s "$normalizedRoot" /parsed
