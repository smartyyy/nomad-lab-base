#!/bin/bash

cd /nomad/nomadlab/user-data/shared/tutorialsNew/demos
git pull --rebase
/labEnv3/bin/python collect.py > ~/collected-demos.json
scp ~/collected-demos.json labtest-nomad.esc:/nomad/nomadlab/servers/labtest-nomad/config/www-root/userapi/demos/index.json
