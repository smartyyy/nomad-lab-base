# stats db

mkdir -p /nomad/nomadlab/stats/local/dbdata

docker run -d -v /nomad/nomadlab/stats/local/dbdata:/var/lib/postgresql/data --name parsingstatsdb -e POSTGRES_USER=parsing_stats -e POSTGRES_PASSWORD='pippo' -p 5435:5432 postgres
