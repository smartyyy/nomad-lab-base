#!/bin/env python
import re, logging

def extractArchiveIds(v, prefix = 'R'):
    """extracts all the archiveids from a string, and normalizes them to start with prefix"""
    archiveRe = re.compile(r"(?<![A-Za-z0-9])[NRS]([-_a-z0-9A-Z]{28})(?![A-Za-z0-9])")
    return [prefix + sha for sha in archiveRe.findall(v)]

def loadKnown(f):
    "loads the known archive gids from the file f to a set"
    res = set()
    while True:
        line = f.readline()
        if not line: break
        archiveGids = extractArchiveIds(line)
        if len(archiveGids) != 1:
            logging.warn("unexpected number of archive gids ({gids}) in {line} of {f}".format(gids=archiveGids, line=repr(line), f=f.name))
        res.update(archiveGids)
    return res

def printUnknown(known, fIn, fOut, archiveGidsOnly = False, prefix = 'R'):
    while True:
        line = fIn.readline()
        if not line: break
        archiveGids = extractArchiveIds(line)
        if len(archiveGids) != 1:
            logging.warn("unexpected number of archive gids ({gids}) in {line} of {f}".format(gids=archiveGids, line=repr(line), f = fIn.name))
        knowsArchive = False
        for a in archiveGids:
            if a in known:
                knowsArchive = True
        if not knowsArchive:
            if (archiveGidsOnly):
                for a in extractArchiveIds(line, prefix=prefix):
                    fOut.write(a)
                    fOut.write("\n")
            else:
                fOut.write(line)

if __name__ == "__main__":
    import sys, argparse
    parser = argparse.ArgumentParser(description='prints the missing archives to a file')
    parser.add_argument('--known', type=str, nargs=1, default = [], action='append',
                    help='file containing the konwn archive gids')
    parser.add_argument('--archive-gids-only', default = False,  action='store_true',
                        help = 'output only the extracted archive gids')
    parser.add_argument('--gid-prefix', type=str, nargs=1, default = ['R'], action='store',
                        help='prefix of the gid, relevant only with --archive-gids-only')
    parser.add_argument('infile', type=str, nargs='*',
                    help='file containing the lines to filter')
    args = parser.parse_args()
    known = set()
    for k in args.known:
        with open(k[0]) as kn:
            known.update(loadKnown(kn))
    for f in args.infile:
        with open(f) as fIn:
            printUnknown(known, fIn, sys.stdout, archiveGidsOnly = args.archive_gids_only, prefix = args.gid_prefix[0])
    if not args.infile:
        printUnknown(known, sys.stdin, sys.stdout, archiveGidsOnly = args.archive_gids_only, prefix = args.gid_prefix[0])
