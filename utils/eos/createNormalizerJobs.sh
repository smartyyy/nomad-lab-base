bounds=("{a..m}" "{n..z}")
for inFile in $@ ; do
bName=$( basename "$inFile" )
for i in $(seq 0 1 1); do
cat > submit${i}-$bName <<EOF
#!/bin/bash
## run in /bin/bash
#SBATCH -J n${bName}
## execute job from the current working directory
## this is default slurm behavior
#SBATCH -D ./
#SBATCH --mem 110G
## do not send mail
#SBATCH --mail-type=NONE
#SBATCH --nodes=1
#SBATCH -t 24:00:00
#SBATCH --qos=shm
#source /u/fawzi/bin/scala.sh
#module load git
#module load jdk/1.7
source /u/fawzi/.bashrc

# 
#
#-Dnomad_lab.integrated_pipeline.inFile="${inFile}" \

SBT_OPTS="-Xmx1024M -Xss3M -XX:+UseConcMarkSweepGC"
CMD="java $SBT_OPTS -Dnomad_lab.configurationToUse=eos_prod \
-Deos_prod.nomad_lab.replacements.rootNamespace=prod-034 \
-Deos_prod.nomad_lab.parser_worker_rabbitmq.numberOfWorkers=1 \
-Djava.library.path=/u/fawzi/lib \
-Deos_prod.nomad_lab.parsing_stats.jdbcUrl=\\"\\" \
-Dnomad_lab.integrated_pipeline.treeparserMinutesOfRuntime=530 \
-Dnomad_lab.parser_worker_rabbitmq.rabbitMQHost=labdev-nomad.esc.rzg.mpg.de \
  -jar /u/fawzi/bin/nomadTool-assembly-1.10.1-31-gcfbfb6c2-dirty.jar"

for f in ${inFile}${bounds[$i]} ; do
  (
  if [[ -e "\$f" ]] ; then
  for n in \$( cat \$f ); do
  #\$CMD normalize --archive-uri nmd://\$n --normalizers SectionSystemNormalizer,MetaIndexRebuildNormalizer,FhiDosNormalizer,FhiAimsBasisNormalizer,SpringerNormalizer,SystemTypeNormalizer,PrototypesNormalizer,MetaIndexRebuildNormalizer &> out-\${n}.txt
  #\$CMD normalize --archive-uri nmd://\$n --normalizers CalculationInfoNormalizer,MetaIndexRebuildNormalizer &> out-\${n}.txt
  #\$CMD normalize --archive-uri nmd://\$n --normalizers SystemTypeNormalizer,PrototypesNormalizer,MetaIndexRebuildNormalizer &> out-\${n}.txt
  \$CMD normalize --archive-uri nmd://\$n --normalizers SectionSystemNormalizer,MetaIndexRebuildNormalizer,SymmetryNormalizer,MetaIndexRebuildNormalizer,CalculationInfoNormalizer,FhiDosNormalizer,FhiAimsBasisNormalizer,SpringerNormalizer,SystemTypeNormalizer,PrototypesNormalizer,MetaIndexRebuildNormalizer &> out-\${n}.txt
  done
  fi
) &
done
wait
 
EOF
done
done
