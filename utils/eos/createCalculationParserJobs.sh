for inFile in $@ ; do
bName=$( basename "$inFile" )
cat > submit-$bName <<EOF
#!/bin/bash
## run in /bin/bash
#SBATCH -J n${bName}
## execute job from the current working directory
## this is default slurm behavior
#SBATCH -D ./
#SBATCH --mem 110G
## do not send mail
#SBATCH --mail-type=NONE
#SBATCH --nodes=1
#SBATCH -t 10:00:00
#SBATCH --qos=shm
#source /u/fawzi/bin/scala.sh
#module load git
#module load jdk/1.7
source /u/fawzi/.bashrc

# 
#
#-Dnomad_lab.integrated_pipeline.inFile="${inFile}" \

SBT_OPTS="-Xmx1024M -Xss3M -XX:+UseConcMarkSweepGC"
CMD="java $SBT_OPTS -Dnomad_lab.configurationToUse=eos_prod \
-Deos_prod.nomad_lab.replacements.rootNamespace=prod-032 \
-Deos_prod.nomad_lab.parser_worker_rabbitmq.numberOfWorkers=1 \
-Djava.library.path=/u/fawzi/lib \
-Deos_prod.nomad_lab.parsing_stats.jdbcUrl=\\"\\" \
-Dnomad_lab.integrated_pipeline.treeparserMinutesOfRuntime=530 \
-Dnomad_lab.parser_worker_rabbitmq.rabbitMQHost=labdev-nomad.esc.rzg.mpg.de \
  -jar /u/fawzi/bin/nomadCalculationParserWorker-assembly-1.9.0.jar"
for i in 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16; do
  \$CMD &> out-${bName}-\${i}.txt &
done
wait
 
EOF
done
