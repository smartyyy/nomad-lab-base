#!/bin/bash
# createIndex.sh directory [extension] 

find $1 -name \*.${2-h5} -exec basename '{}' ';' | sed 's/\.[^.]*$//'
