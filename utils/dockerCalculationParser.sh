#!/usr/bin/env bash
# Starts a calculation parser

docker run -d --user $UID:$(getent group docker | cut -d: -f3) --volumes-from parserVolContainer eu.nomad-laboratory/nomadcalculationparserworker
