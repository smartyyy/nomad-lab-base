#!/bin/bash
baseDir=$(dirname $(readlink -f $0))
codeStats=$baseDir/collectCodeStats.sh
cd $baseDir/..
$codeStats
git submodule foreach $codeStats
