#!/bin/bash
pwd
if [[ -e build.sbt ]]; then
    cloc $(git ls-files | grep -vE '^parsers/|python-common|nomad-meta-info|webservice/src/main/resources/frontend/(node_modules|scripts)|py')
else
    cloc $(git ls-files)
fi
git shortlog -s -n
git rev-list --all --count
echo
