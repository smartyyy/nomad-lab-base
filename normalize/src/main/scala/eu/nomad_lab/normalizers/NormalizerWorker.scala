/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.normalizers
import eu.nomad_lab.query
import eu.nomad_lab.ref
import eu.nomad_lab.resolve
import eu.nomad_lab.LocalEnv
import eu.nomad_lab.meta
//import eu.nomad_lab.archive
import eu.nomad_lab.h5
import eu.nomad_lab.h5.ArchiveSetH5
import eu.nomad_lab.h5.EmitJsonVisitor
import eu.nomad_lab.parsers.BackendH5
import eu.nomad_lab.parsers.GenIndexBackend
import eu.nomad_lab.parsers.JsonParseEventsWriterBackend
import eu.nomad_lab.h5.ArchiveH5
import eu.nomad_lab.query._
import java.nio.file.Paths
import java.nio.file.Files
import eu.nomad_lab.ref.NomadUri
import eu.nomad_lab.resolve.ResolvedRef
import eu.nomad_lab.resolve.FailedResolution
import eu.nomad_lab.resolve.Resolver
import eu.nomad_lab.resolve.Archive
import scala.util.control.NonFatal
//import eu.nomad_lab.normalizers.AllNormalizers
import eu.nomad_lab.normalize
import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.QueueMessage.{ NormalizedResult, NormalizerRequest }
import eu.nomad_lab.normalize.NormalizerResult
import java.nio.file.Paths
import java.nio.file.Files
import java.io._

object NormalizerWorker extends StrictLogging {
  //  val sectionCompleters: Map[String, SectionCompleter] = Map(),
  //  val archiveOperations: Map[String, ArchiveOperation] = Map()){
  def normalizerHandler(incomingMessage: NormalizerRequest, normalizerList: Seq[String]): NormalizedResult = {
    var message: Option[NormalizerRequest] = Some(incomingMessage)
    var archiveUri: Option[String] = None
    val t0 = new java.util.Date
    var didExist = false
    var created = false
    var errorMessage: Option[String] = None
    var normalizedFileUri: Option[String] = None
    var normalizedFilePath: Option[String] = None
    var nResult: NormalizerResult.NormalizerResult = NormalizerResult.NormalizerFailure
    var archivePath: Option[String] = None

    archiveUri = Some(incomingMessage.treeUri)
    //    normalizedFilePath = Some(incomingMessage.treeFilePath)
    archiveUri match {
      case None =>
        errorMessage = Some("archive uri is required for command normalization")
        nResult = NormalizerResult.NormalizerFailure
        logger.warn(s"NormalizerResult: $nResult when normalizing ${normalizedFilePath}, URI: ${archiveUri}")
        return NormalizedResult(
          didExist = didExist,
          created = created,
          errorMessage = errorMessage,
          normalizedFileUri = archiveUri,
          normalizedFilePath = normalizedFilePath,
          nResult = nResult
        )

      case Some(uri) =>
        val nUri = NomadUri(uri)
        val aGid: String = nUri.toRef match {
          case ref.ArchiveRef(aGid) =>
            aGid
          case r =>
            logger.warn(s"NormalizerResult: $nResult when normalizing ${normalizedFilePath}, URI: ${archiveUri}")
            logger.warn(s"Error: expected an archive reference, but got $r from $uri")
            errorMessage = Some("Error: expected an archive reference, but got $r from $uri")
            return NormalizedResult(
              didExist = didExist,
              created = created,
              errorMessage = errorMessage,
              normalizedFileUri = archiveUri,
              normalizedFilePath = normalizedFilePath,
              nResult = nResult
            )

        }
        var resolvedRef: Option[ResolvedRef] = None
        val archiveSet = ArchiveSetH5.toNormalizeSet
        val archive = archivePath match {
          case None =>
            val rr = Resolver.resolveInArchiveSet(archiveSet, nUri.toRef)
            resolvedRef = Some(rr)
            logger.warn { s"what is this: ${rr} and ${resolvedRef}  +++ ${didExist}" }
            rr match {
              case resolve.Archive(_, arch) => arch
              case v =>
                logger.warn(s"NormalizerResult: $nResult when normalizing ${normalizedFilePath}, URI: ${archiveUri}")
                logger.warn(s"Error: expected an archive reference, but got $v when resolving $uri")
                errorMessage = Some(s"Error: expected an archive reference, but got $v when resolving $uri")
                return NormalizedResult(
                  didExist = didExist,
                  created = created,
                  errorMessage = errorMessage,
                  normalizedFileUri = archiveUri,
                  normalizedFilePath = normalizedFilePath,
                  nResult = nResult
                )

            }

          case Some(path) =>
            didExist = Files.exists(Paths.get(path))
            logger.warn(s"DSB $path , $path.toString")
            normalizedFilePath = Some(path)
            if (didExist) {
              nResult = NormalizerResult.NormalizerSkipped
              logger.warn(s"NormalizerResult: $nResult when normalizing ${normalizedFilePath}, URI: ${archiveUri}")
              logger.warn(s"the file ${normalizedFilePath} already exists and will not be overwritten")
              return NormalizedResult(
                didExist = didExist,
                created = created,
                errorMessage = errorMessage,
                normalizedFileUri = archiveUri,
                normalizedFilePath = normalizedFilePath,
                nResult = nResult
              )
            } else {
              normalizedFilePath = Some(path.toString)
              created = true
              val f = h5.FileH5.open(Paths.get(path))
              val arch = f.openArchive(aGid)
              f.release()
              arch
            }
        }
        logger.warn(s"DSB , print archive ${archive} and ${archiveSet}")

        try {
          val knownNormalizers: Map[String, normalize.NormalizerGenerator] = AllNormalizers.knownNormalizers
          val unknownNormalizers: Seq[String] = normalizerList.filter { x: String => !knownNormalizers.contains(x) }
          if (!unknownNormalizers.isEmpty) {
            errorMessage = Some(s"Error: unknown normalizers ${unknownNormalizers.mkString("[", ",", "]")}, known normalizers: ${knownNormalizers.keys.mkString("[", ",", "]")}")
          } else {
            // val backendH5 = new BackendH5(archiveSet)
            for (normalizerName <- normalizerList) {
              val tmpFile = Files.createTempFile("normalizer", ".json", LocalEnv.filePermissionsAttributes)
              logger.info(s"Normalizing the archive ${archiveUri} using the ${normalizerName} normalizer")
              logger.info(s"tmpFile: $tmpFile")
              val backendExt = new JsonParseEventsWriterBackend(meta.KnownMetaInfoEnvs.all, new java.io.FileWriter(tmpFile.toFile))
              val backendInt = new GenIndexBackend(backendExt)
              val normalizer = knownNormalizers(normalizerName)
              val evaluator = new query.DirectArchiveScanner(
                archiveSet = archiveSet,
                archive = archive,
                scanOp = new normalize.DirectNormalizer(
                generator = normalizer,
                backend = backendInt
              )
              )
              //              tmpFile.close
              evaluator.run()
              evaluator.cleanup()
            }
          }
        } catch {
          case NonFatal(e) => {
            errorMessage = Some(s"error normalizing $archive with ${normalizerList.mkString("[", ",", "]")}")
            nResult = NormalizerResult.NormalizerFailure
          }
        } finally {
          resolvedRef match {
            case Some(r) =>
              r.giveBack()
              nResult = NormalizerResult.NormalizerSuccess
              logger.warn(s"NormalizerResult: $nResult when normalizing ${normalizedFilePath}, URI: ${archiveUri}")
              created = true

            case None =>
              archive.release()
            //              publish(incomingMessage)
          }
        }
    }

    NormalizedResult(
      didExist = didExist,
      created = created,
      errorMessage = errorMessage,
      normalizedFileUri = archiveUri,
      normalizedFilePath = normalizedFilePath,
      nResult = nResult
    )
    //    publish(a)
    //NormalizedResult(
    //      didExist = didExist,
    //      created = created,
    //      errorMessage = errorMessage,
    //      normalizedFileUri = archiveUri,
    //      normalizedFilePath = normalizedFilePath,
    //      nResult = nResult
    //    ))

  }
}
