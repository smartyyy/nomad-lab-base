/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.elasticsearch

import com.sksamuel.elastic4s.http.ElasticDsl._
import com.sksamuel.elastic4s.http.HttpClient
import com.sksamuel.elastic4s.{ ElasticsearchClientUri, IndexAndType }
import com.typesafe.config.{ Config, ConfigFactory }
import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.LocalEnv
import eu.nomad_lab.meta.{ KnownMetaInfoEnvs, MetaInfoEnv }
import org.{ json4s => jn }

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.{ Await, Future }
import scala.concurrent.duration._
import scala.util.control.NonFatal

object ESManager {
  class Settings(config: Config, section: String) {
    // validate vs. reference.conf
    config.checkValid(ConfigFactory.defaultReference(), "simple-lib")

    val clientUri: String = config.getString(s"$section.clientUri")
    val indexNameData: String = config.getString(s"$section.indexNameData")
    val indexNameTopics: String = config.getString(s"$section.indexNameTopics")
    val typeNameData: String = config.getString(s"$section.typeNameData")
    val typeNameTopics: String = config.getString(s"$section.typeNameTopics")
    val nCachedCalculations: Int = config.getInt(s"$section.nCachedCalculations")
    val verbose: Boolean = config.getBoolean(s"$section.verbose")
    val waitTime: Duration = Duration(config.getString(s"$section.waitTime"))

    def toJson: jn.JValue = {
      import org.json4s.JsonDSL._
      ("clientUri" -> clientUri) ~
        ("indexNameData" -> indexNameData) ~
        ("indexNameTopics" -> indexNameTopics) ~
        ("typeNameData" -> typeNameData) ~
        ("typeNameTopics" -> typeNameTopics) ~
        ("nCachedCalculations" -> nCachedCalculations) ~
        ("waitTime" -> waitTime.toUnit(SECONDS)) ~
        ("verbose" -> verbose)
    }
  }

  def defaultSettings(metaData: MetaInfoEnv): Settings = {
    val section = metaData match {
      case KnownMetaInfoEnvs.repo => "nomad_lab.repo.elastic"
      case KnownMetaInfoEnvs.archive => "nomad_lab.archive.elastic"
      case _ => throw new IllegalArgumentException(s"no ES config known for meta-data block $metaData")
    }
    new Settings(LocalEnv.defaultConfig, section)
  }

  def apply(
    metaData: MetaInfoEnv,
    settings: Option[Settings] = None
  ): ESManager = {
    val mySettings = settings.getOrElse(defaultSettings(metaData))
    new ESManager(
      metaData = metaData,
      client = HttpClient(ElasticsearchClientUri(mySettings.clientUri)),
      settings = mySettings
    )
  }
}

/**
 * Manages the connection to elastic search
 *
 * Owns the client and will close it on stop
 */
class ESManager(
    val metaData: MetaInfoEnv,
    val client: HttpClient,
    val settings: ESManager.Settings
) extends StrictLogging {

  val connector = new ConnectorElasticSearch(metaData, this)

  /**
   * Stops the elastic client
   *
   * Required for a clean shut down of the VM
   * (kills background threads that keep the VM running))
   */
  def stop(): Unit = {
    client.close()
  }

  /**
   * Returns the last calc_id in the index
   */
  def findLastId(index: String, typeName: String, field: String): Future[Long] = {
    val query = search(IndexAndType(index, typeName)).storedFields(field).
      sortByFieldDesc(field).limit(1)
    client.execute(query).map { response =>
      response.hits.hits.headOption match {
        case None =>
          -1L
        case Some(h) =>
          h.storedField(field).value match {
            case l: java.lang.Long => l.toLong
            case i: java.lang.Integer => i.toLong
          }
      }
    }
  }

  def getIndexMapping(indexName: String, typeName: String): Map[String, Any] = {
    try {
      val request = getMapping(IndexAndType(indexName, typeName))
      val result = client.execute(request)
      Await.result(result, 10.seconds).head.mappings(typeName)
    } catch {
      case NonFatal(e) =>
        logger.warn("could not fetch index mapping", e)
        Map()
    }
  }
}
