/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.elasticsearch

import com.sksamuel.elastic4s.Indexes
import com.sksamuel.elastic4s.http.ElasticDsl
import com.sksamuel.elastic4s.http.ElasticDsl.{ matchAllQuery, search }
import com.sksamuel.elastic4s.searches.SearchDefinition
import com.sksamuel.elastic4s.searches.queries.matches.MatchQueryDefinition
import com.sksamuel.elastic4s.searches.queries.term.{ TermQueryDefinition, TermsQueryDefinition }
import com.sksamuel.elastic4s.searches.queries.{ BoolQueryDefinition, QueryDefinition, RangeQueryDefinition }
import eu.nomad_lab.jsonapi
import eu.nomad_lab.meta.MetaInfoEnv
import eu.nomad_lab.query.QueryQuantifier.QueryQuantifier
import eu.nomad_lab.query._
import org.{ json4s => jn }

object ConnectorElasticSearch {
  private val errorTitle = "Query conversion failure"

  object FieldUsage extends Enumeration {
    val Search = Value
    val Aggregate = Value
    val SourceFilter = Value
  }

  /**
   * Gives the ElasticSearch index field belonging to a given meta-data name.
   * @param metaName the meta data name to resolve
   * @return the name of the ElasticSearch field for this metadata
   * @throws ApiCallException if a meta-data name is unknown
   */
  def metaNameToField(metaName: String)(implicit metaData: MetaInfoEnv): String = {
    val fieldName = metaData.pathViaSections(metaName).drop(1).replace("/", ".")
    if (fieldName.isEmpty) throw ApiCallException(s"unknown metadata $metaName")
    fieldName
  }

  /**
   * Gives the ElasticSearch index field belonging to a given meta-data name as a sequence of
   * path segments.
   * @param metaName the meta data name to resolve
   * @return the path segments leading to the target field
   * @throws ApiCallException if a meta-data name is unknown
   */
  def metaNameToPathSegments(metaName: String)(implicit metaData: MetaInfoEnv): Seq[String] = {
    val path = metaData.pathViaSections(metaName).drop(1).split("/")
    if (path.isEmpty) throw ApiCallException(s"unknown metadata $metaName")
    path
  }
}

class ConnectorElasticSearch protected[elasticsearch] (val metaInfo: MetaInfoEnv, private val es: ESManager) {
  import ConnectorElasticSearch.{ FieldUsage, errorTitle }

  private def analyzeMapping(rawMapping: Map[String, Any]): Map[String, String] = {
    rawMapping.flatMap { x =>
      x._2 match {
        case y: Map[_, _] =>
          val z = y.asInstanceOf[Map[String, Any]]
          if (z.contains("type"))
            Map(x._1 -> z("type").asInstanceOf[String])
          else if (z.contains("properties"))
            analyzeMapping(z("properties").asInstanceOf[Map[String, Any]]) + (x._1 -> "nested")
          else
            Map[String, String]()
        case _ => Map[String, String]()
      }
    }
  }
  private val allFields = analyzeMapping(es.getIndexMapping(
    es.settings.indexNameData, es.settings.typeNameData
  ))

  def transformToQuery(queryString: Option[String])(implicit postProcessing: Option[QueryExpression => QueryExpression] = None): SearchDefinition = {
    queryString match {
      case Some(string) =>
        val abstractRepresentation = QueryExpression.parseExpression(string)
        val normalizedRepresentation = postProcessing match {
          case None => abstractRepresentation
          case Some(f) => f(abstractRepresentation)
        }
        toESQuery(normalizedRepresentation) match {
          case Left(errors) => throw ApiCallException(errors.head.title + ": " + errors.head.detail)
          case Right(baseQuery) => search(Indexes(Seq(es.settings.indexNameData))).query(baseQuery).size(0)
        }
      case None => search(Indexes(Seq(es.settings.indexNameData))).query(matchAllQuery()).size(0)
    }
  }

  /**
   * Gives the ElasticSearch index field belonging to a given meta-data name. In contrast to the
   * static version of this function, the existence of the field in the ES index is also verified.
   * @param metaName the meta data name to resolve
   * @return the name of the ElasticSearch field for this metadata
   * @throws ApiCallException if a meta-data name is unknown or not an indexed field
   */
  def metaNameToField(metaName: String): String = {
    val fieldName = metaInfo.pathViaSections(metaName).drop(1).replace("/", ".")
    if (fieldName.isEmpty) throw ApiCallException(s"unknown metadata $metaName")
    if (!allFields.contains(metaName)) throw ApiCallException(s"metadata $metaName is not indexed")
    fieldName
  }

  /**
   * Gives the ElasticSearch index field belonging to a given meta-data name as a sequence of
   * path segments. In contrast to the static version of this function, the existence of the field
   * in the ES index is also verified.
   * @param metaName the meta data name to resolve
   * @return the path segments leading to the target field\
   * @throws ApiCallException if a meta-data name is unknown or not an indexed field
   */
  def metaNameToPathSegments(metaName: String): Seq[String] = {
    metaNameToField(metaName).split(".")
  }

  /**
   * Checks if the listed fields exist in the ElasticSearch index and are indexed appropriately
   * for the requested task.
   * @param fields the field names to check (just metadata-names, not fully qualified paths)
   * @param purpose the type of operation the fields will be used for
   * @throws ApiCallException if a field doesn't exist or is indexed inappropriately for the task
   */
  def validateFields(fields: Seq[String], purpose: FieldUsage.Value): Unit = {
    val testCondition: (String, String) => Unit = purpose match {
      case FieldUsage.SourceFilter => (_, _) =>
      case FieldUsage.Search => (fieldType, name) => {
        if (fieldType == "nested")
          throw ApiCallException(s"$name is a nested field and cannot be searched for")
      }
      case FieldUsage.Aggregate => (fieldType, name) => {
        if (fieldType == "nested")
          throw ApiCallException(s"$name is a nested field and cannot be used in aggregations")
        else if (fieldType == "text")
          throw ApiCallException(s"$name is a text field and cannot be used in aggregations")
      }
    }
    fields.foreach { x =>
      allFields.get(x) match {
        case Some(y) => testCondition(y, x)
        case None => throw ApiCallException(s"field $x does not exist in index")
      }
    }
  }

  def toESQuery(expr: QueryExpression): Either[Seq[jsonapi.Error], QueryDefinition] = {
    expr match {
      case and: AndConjunction => convertAndConjunction(and)
      case term: AtomicTerm => convertAtomicTerm(term) match {
        case Left(error) => Left(Seq(error))
        case Right(x) => Right(x)

      }
    }
  }

  private def convertAndConjunction(and: AndConjunction): Either[Seq[jsonapi.Error], QueryDefinition] = {
    var errors: Seq[jsonapi.Error] = Seq()
    val query: Option[QueryDefinition] = and.terms.size match {
      case 0 =>
        errors = errors :+ jsonapi.Error(errorTitle, "AND has no elements", and.toString)
        None
      case _ =>
        val elements = and.terms.map(value => toESQuery(value))
        val subErrors = elements.filter(x => x.isLeft).flatMap(x => x.left.get)
        val subConditions = elements.filter(x => x.isRight).map(x => x.right.get)
        subErrors.size match {
          case 0 => Some(BoolQueryDefinition(must = subConditions))
          case _ => errors = errors ++ subErrors; None
        }
    }
    errors.size match {
      case 0 if query.isDefined => Right(query.get)
      case _ => Left(errors)
    }
  }

  private def convertAtomicTerm(term: AtomicTerm): Either[jsonapi.Error, QueryDefinition] = {
    if (metaInfo.pathViaSections(term.metaName) == "")
      Left(jsonapi.Error(errorTitle, s"unknown meta-data ${term.metaName}", term.toString))
    else if (!allFields.contains(term.metaName))
      Left(jsonapi.Error(errorTitle, s"metadata ${term.metaName} is not indexed", term.toString))
    else if (allFields(term.metaName) == "nested")
      Left(jsonapi.Error(errorTitle, s"metadata ${term.metaName} is a nested field", term.toString))
    else if (term.compareOp.isEmpty && term.values.nonEmpty)
      Left(jsonapi.Error(errorTitle, "empty set of values", term.toString))
    else {
      val field = metaNameToField(term.metaName)
      val values = term.values.map(_.values)
      val baseQuery = term.compareOp match {
        case Some(CompareOp.OpEqual) => transformOpEqualTerm(field, term.quantifier, values)
        case Some(CompareOp.OpMatch) => transformOpMatchTerm(field, term.quantifier, values)
        case Some(CompareOp.OpBigger) => transformOpBiggerTerm(field, term.quantifier, values)
        case Some(CompareOp.OpSmaller) => transformOpSmallerTerm(field, term.quantifier, values)
        case (None) =>
          Left(jsonapi.Error(errorTitle, s"existence test not yet implemented", term.toString))
        case (Some(x)) =>
          Left(jsonapi.Error(errorTitle, s"operator $x not yet implemented", term.toString))
      }
      if (term.invert && baseQuery.isRight) {
        Right(BoolQueryDefinition(not = Seq(baseQuery.right.get)))
      } else {
        baseQuery
      }
    }
  }

  private def transformOpEqualTerm(field: String, quantifier: QueryQuantifier,
    values: Seq[jn.JValue#Values]): Either[jsonapi.Error, QueryDefinition] = {
    quantifier match {
      case QueryQuantifier.AllTarget =>
        values.size match {
          case 1 => Right(TermQueryDefinition(field, values.head))
          case _ => Right(BoolQueryDefinition(
            must = values.map(value => TermQueryDefinition(field, value))
          ))
        }
      case QueryQuantifier.All => Right(BoolQueryDefinition(must =
        values.map { value => TermQueryDefinition(field, value) } :+
          TermQueryDefinition(field + "_count", values.length)))
      case QueryQuantifier.Any =>
        values.size match {
          case _ =>
            val allowed = values.map(x => x)
            Right(TermsQueryDefinition(field, allowed)(ElasticDsl.BuildableTermsNoOp))
        }
      case x => Left(jsonapi.Error(errorTitle, s"equality does not yet support quantifier $x"))
    }
  }

  private def transformOpMatchTerm(field: String, quantifier: QueryQuantifier,
    values: Seq[jn.JValue#Values]): Either[jsonapi.Error, QueryDefinition] = {
    quantifier match {
      case (QueryQuantifier.Any) =>
        values.size match {
          case 1 => Right(MatchQueryDefinition(field, values.head))
          case _ => Right(BoolQueryDefinition(
            must = values.map(value => MatchQueryDefinition(field, value))
          ))
        }
      case x => Left(jsonapi.Error(errorTitle, s"match does not yet support quantifier $x"))
    }
  }

  private def transformOpBiggerTerm(field: String, quantifier: QueryQuantifier,
    values: Seq[jn.JValue#Values]): Either[jsonapi.Error, QueryDefinition] = {
    quantifier match {
      case (QueryQuantifier.Any) =>
        values.size match {
          case 1 => Right(RangeQueryDefinition(field, gt = Some(values.head)))
          case _ => Right(BoolQueryDefinition(
            should = values.map(value => RangeQueryDefinition(field, gt = Some(value)))
          ))
        }
      case x => Left(jsonapi.Error(errorTitle, s"bigger does not yet support quantifier $x"))
    }
  }

  private def transformOpSmallerTerm(field: String, quantifier: QueryQuantifier,
    values: Seq[jn.JValue#Values]): Either[jsonapi.Error, QueryDefinition] = {
    quantifier match {
      case (QueryQuantifier.Any) =>
        values.size match {
          case 1 => Right(RangeQueryDefinition(field, lt = Some(values.head)))
          case _ => Right(BoolQueryDefinition(
            should = values.map(value => RangeQueryDefinition(field, lt = Some(value)))
          ))
        }
      case x => Left(jsonapi.Error(errorTitle, s"smaller does not yet support quantifier $x"))
    }
  }

}
