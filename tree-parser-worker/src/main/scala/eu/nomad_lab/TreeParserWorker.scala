/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab
import eu.nomad_lab.normalizers.NormalizerWorker.normalizerHandler
import com.typesafe.config.{ Config, ConfigFactory }
import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.QueueMessage.{ CalculationParserRequest, TreeParserRequest, NormalizerRequest }
import eu.nomad_lab.parsing_queue.TreeParser.TreeParserException
import eu.nomad_lab.parsing_queue.TreeParser

import eu.nomad_lab.h5.ArchiveSetH5
import eu.nomad_lab.h5.FileH5
import eu.nomad_lab.h5.EmitJsonVisitor
import eu.nomad_lab.parsers.BackendH5
import eu.nomad_lab.h5.ArchiveH5
import java.nio.file.Paths
import java.nio.file.Files
import eu.nomad_lab.ref.NomadUri
import eu.nomad_lab.resolve.ResolvedRef
import eu.nomad_lab.resolve.FailedResolution
import eu.nomad_lab.resolve.Resolver
import eu.nomad_lab.resolve.Archive
import scala.util.control.NonFatal
import eu.nomad_lab.QueueMessage.{ NormalizedResult, NormalizerRequest }

import eu.nomad_lab.parsing_stats.ParsingStatsDb // ??
import org.json4s.JsonAST.JValue
import com.rabbitmq.client._
import java.io._
import scala.util.control.NonFatal

import eu.{ nomad_lab => lab }

object TreeParserWorker extends StrictLogging {

  /**
   * The settings required to get the read and write queue
   */
  class Settings(config: Config, worker: String) {
    // validate vs. reference.conf
    config.checkValid(ConfigFactory.defaultReference(), "simple-lib")
    //    var repl_aux: Map[String, String] = replacements
    //    repl_aux += ("whichWorker" -> whichWorker)
    //    replacements = settings.repl_aux
    val whichWorker = worker //config.getString("nomad_lab.parser_worker_rabbitmq.whichWorker")
    val (readExchange, readQueue, writeQueue, writeToExchange) = whichWorker match {
      case "TreeParserWorker" => (
        config.getString("nomad_lab.parser_worker_rabbitmq.treeParserExchange"),
        config.getString("nomad_lab.parser_worker_rabbitmq.treeParserQueue"),
        config.getString("nomad_lab.parser_worker_rabbitmq.singleParserQueue"),
        config.getString("nomad_lab.parser_worker_rabbitmq.singleParserExchange")
      )
      case "NormalizerWorker" => (
        config.getString("nomad_lab.parser_worker_rabbitmq.toBeNormalizedExchange"),
        config.getString("nomad_lab.parser_worker_rabbitmq.toBeNormalizedQueue"),
        config.getString("nomad_lab.parser_worker_rabbitmq.normalizerCompletedQueue"),
        config.getString("nomad_lab.parser_worker_rabbitmq.normalizerCompletedExchange")
      )

      case _ => throw new Exception(s"Worker not found. Expected: TreeParserWorker, NormalizerWorker")
    }
    val rabbitMQHost = config.getString("nomad_lab.parser_worker_rabbitmq.rabbitMQHost")

    def toJValue: JValue = {
      import org.json4s.JsonDSL._;
      (("rabbitMQHost" -> rabbitMQHost) ~
        ("readQueue" -> readQueue) ~
        ("writeToExchange" -> writeToExchange) ~
        ("writeQueue" -> writeQueue))
    }
  }

  //  ????
  val statsDb: Option[ParsingStatsDb] = {
    try {
      val db = ParsingStatsDb()
      Some(db)
    } catch {
      case NonFatal(e) =>
        logger.warn("Failed to initialize parsing stats db", e)
        None
    }
  }
  // ??
  //  val whichWorker: String = "TreeParserWorker"

  //  var repl = LocalEnv.defaultSettings.replacements
  //  val settings = new Settings(LocalEnv.defaultConfig)
  //
  //  val rabbitMQConnectionFactory: ConnectionFactory = new ConnectionFactory
  //  rabbitMQConnectionFactory.setHost(settings.rabbitMQHost)
  //  val rabbitMQConnection: Connection = rabbitMQConnectionFactory.newConnection

  /**
   * Main function of TreeParserWorker
   *
   * @param args
   */
  val usage = """
  nomadTool [--help]
        /* meta info related options: */
    worker
      [which worker will consume the RabbitMQ (either TreeParserWorker or NormalizerWorker) <path to nomadmetainfo.json to load>]
"""
  def main(args: Array[String]): Unit = {
    if (args.length == 0) {
      println(usage)
      return
    }
    var list: List[String] = args.toList
    if (list.isEmpty) {
      println(usage)
      return
    }
    var whichWorker: String = "TreeParserWorker"
    var verbose: Boolean = false

    while (!list.isEmpty) {
      val arg = list.head
      list = list.tail
      logger.warn("DSBWarning" + arg.toString)
      arg match {
        case "--help" | "-h" =>
          println(usage)
          return
        case "--worker" =>
          if (list.isEmpty) {
            println(s"Error: missing the worker to execute the RabbitMQ --worker. $usage")
            return
          }
          while (!list.isEmpty) {
            whichWorker = list.head
            list = list.tail
          }
        case _ =>
          println(s"Error: unexpected argument $arg in worker command. $usage")
          return
      }
    }

    var repl = LocalEnv.defaultSettings.replacements
    val settings = new Settings(LocalEnv.defaultConfig, whichWorker)

    val rabbitMQConnectionFactory: ConnectionFactory = new ConnectionFactory
    rabbitMQConnectionFactory.setHost(settings.rabbitMQHost)
    val rabbitMQConnection: Connection = rabbitMQConnectionFactory.newConnection

    readFromQueue(settings, rabbitMQConnection)

    ////    For sample trial without the read queue initialization
    //    val tempMessage =  QueueMessage.TreeParserQueueMessage(
    //      treeUri = "file:///home/kariryaa/NoMad/nomad-lab-base/tree-parser-worker/fhi.zip",
    //      treeFilePath = "/home/kariryaa/NoMad/nomad-lab-base/tree-parser-worker/fhi.zip",
    //      treeType = TreeType.Zip
    //    )
    //    findParserAndWriteToQueue(tempMessage)

  }

  /**
   * Read messages from the TreeParserQueue
   */
  def readFromQueue(settings: Settings, rabbitMQConnection: Connection): Unit = {

    val channel: Channel = rabbitMQConnection.createChannel
    channel.exchangeDeclare(settings.readExchange, "fanout")
    //channel.queueDeclare(String queue, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object> arguments)
    channel.queueDeclare(settings.readQueue, true, false, false, null)
    channel.queueBind(settings.readQueue, settings.readExchange, "")
    logger.info(s"Reading from Queue: ${settings.readQueue}")

    // Fair dispatch: don't dispatch a new message to a worker until it has processed and acknowledged the previous one
    val prefetchCount = 1
    channel.basicQos(prefetchCount) //Sets prefetch for each consumer. For channel based prefetch use  channel.basicQos(prefetchCount, global = true)

    val consumer: Consumer = new DefaultConsumer((channel)) {
      @throws(classOf[IOException])
      override def handleDelivery(consumerTag: String, envelope: Envelope, properties: AMQP.BasicProperties, body: Array[Byte]) {
        try {
          settings.whichWorker match {
            case "TreeParserWorker" => {
              val message: TreeParserRequest = eu.nomad_lab.JsonSupport.readUtf8[TreeParserRequest](body)
              println(" [x] Received '" + message + "'")
              findParserAndWriteToQueue(message, settings, rabbitMQConnection)
            }
            case "NormalizerWorker" => {
              val message: NormalizerRequest = eu.nomad_lab.JsonSupport.readUtf8[NormalizerRequest](body)
              println(" [x] Received '" + message + "'")
              normalizeAndWriteToQueue(message, settings, rabbitMQConnection)
            }
            case _ => throw new Exception(s"Worker not found. Expected: TreeParserWorker, NormalizerWorker")
          }
        } finally {
          println(" [x] Done")
          channel.basicAck(envelope.getDeliveryTag(), false)
        }
      }
    }

    channel.basicConsume(settings.readQueue, false, consumer)
    ()
  }

  val archiveRe = "nmd://(R[-_a-zA-Z0-9]{28})(?:/.*)?$".r

  /**
   * Find the parsable files and parsers. Write this information to singleParserExchange
   */
  def findParserAndWriteToQueue(incomingMessage: TreeParserRequest, settings: Settings, rabbitMQConnection: Connection): Unit = {
    val prodchannel: Channel = rabbitMQConnection.createChannel
    prodchannel.exchangeDeclare(settings.writeToExchange, "fanout")
    //channel.queueDeclare(String queue, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object> arguments)
    prodchannel.queueDeclare(settings.writeQueue, true, false, false, null)
    prodchannel.queueBind(settings.writeQueue, settings.writeToExchange, "")
    def publish(parserAssociation: TreeParser.ParserAssociation) = {
      val message: CalculationParserRequest = TreeParser.createCalculationParserRequest(parserAssociation)
      val msgBytes = JsonSupport.writeUtf8(message)
      logger.info(s"Message: $message, bytes Array size: ${msgBytes.length}")
      try {
        prodchannel.basicPublish(settings.writeToExchange, "", null, msgBytes)
      } catch {
        case e: Exception =>
          logger.error(s"Error while publishing to the exchange. Trying again in sometime. Message: ${e.getMessage}")
          try {
            Thread.sleep(1000)
            prodchannel.basicPublish(settings.writeToExchange, "", null, msgBytes)
          } catch {
            case e: Exception =>
              logger.error(s"Retry failed. Exiting now. Message: ${e.getMessage}")
              System.exit(1)
          }
      }

      statsDb match {
        case Some(db) =>
          db.addCalculationAssignement(parserAssociation)
        case None => ()
      }
    }
    val archiveGid: Option[String] = incomingMessage.treeUri match {
      case archiveRe(gid) => Some(gid)
      case _ => None
    }
    archiveGid match {
      case Some(aGid) =>
        statsDb match {
          case Some(db) =>
            db.registerTreeScanStart(
              rootNamespace = lab.LocalEnv.defaultSettings.rootNamespace,
              treeUri = incomingMessage.treeUri
            )
          case None => ()
        }
      case None => ()
    }
    try {
      logger.info(s"Writing to Exchange: ${settings.writeToExchange}")
      val treeParser = new TreeParser(parsers.AllParsers.defaultParserCollection)

      treeParser.findParserAndPublish(publish, incomingMessage)
    } finally {
      archiveGid match {
        case Some(aGid) =>
          statsDb match {
            case Some(db) =>
              db.registerTreeScanEnd(
                rootNamespace = lab.LocalEnv.defaultSettings.rootNamespace,
                treeUri = incomingMessage.treeUri
              )
            case None => ()
          }
        case None => ()
      }
    }
    prodchannel.close
  }

  def normalizeAndWriteToQueue(incomingMessage: NormalizerRequest, settings: Settings, rabbitMQConnection: Connection): Unit = {
    var archivePath: Option[String] = None
    var normalizerList: Seq[String] = Seq("SectionSystemNormalizer")
    var verbose: Boolean = false

    val prodchannel: Channel = rabbitMQConnection.createChannel
    prodchannel.exchangeDeclare(settings.writeToExchange, "fanout")
    prodchannel.queueDeclare(settings.writeQueue, true, false, false, null)
    prodchannel.queueBind(settings.writeQueue, settings.writeToExchange, "")
    def publish(incomingMessage: NormalizedResult) = {
      logger.warn(s"DSB out ${incomingMessage}")
      val msgBytes = JsonSupport.writeUtf8(incomingMessage)
      logger.info(s"Message: $incomingMessage, bytes Array size: ${msgBytes.length}")
      try {
        prodchannel.basicPublish(settings.writeToExchange, "", null, msgBytes)
      } catch {
        case e: Exception =>
          logger.error(s"Error while publishing to the exchange. Trying again in sometime. Message: ${e.getMessage}")
          try {
            Thread.sleep(1000)
            prodchannel.basicPublish(settings.writeToExchange, "", null, msgBytes)
          } catch {
            case e: Exception =>
              logger.error(s"Retry failed. Exiting now. Message: ${e.getMessage}")
              System.exit(1)
          }
      }

    }
    val message: NormalizedResult = normalizerHandler(incomingMessage, normalizerList)
    publish(message)
  }
}
