NoMaD Lab Base Layer
====================

Main repository of the base layer of the [NoMaD Laboratory](http://nomad-lab.eu/).

See the file Setup.markdown for quick setup/run information.

The official version lives at
    git@gitlab.mpcdf.mpg.de:nomad-lab/nomad-lab-base.git
you can browse the code and submit issues at
    https://gitlab.mpcdf.mpg.de/nomad-lab/nomad-lab-base
For more information see the [wiki](https://gitlab.rzg.mpg.de/nomad-lab/nomad-lab-base/wikis/home).
