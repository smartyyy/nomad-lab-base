/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.integrated_pipeline

import java.util.concurrent.LinkedBlockingQueue
import akka.actor._
import com.rabbitmq.client.{ DefaultConsumer, Envelope, _ }
import com.typesafe.config.{ Config, ConfigFactory }
import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.QueueMessage.{ CalculationParserRequest, CalculationParserResult, TreeParserRequest, ToBeNormalizedQueueMessage }
import eu.nomad_lab.parsing_queue.TreeParser
import eu.nomad_lab.parsing_queue.TreeParser.ParserAssociation
import eu.nomad_lab.parsing_stats.ParsingStatsDb
import eu.nomad_lab.ref.NomadUri
//import org.json4s.JsonAST.JValue
import org.{ json4s => jn }
import eu.{ nomad_lab => lab }
import eu.nomad_lab.parsers
import eu.nomad_lab.JsonSupport
import eu.nomad_lab.parsing_queue.CalculationParser
import eu.nomad_lab.LocalEnv
import scala.collection.mutable
import scala.util.control.NonFatal
import scala.util.matching.Regex
import scala.concurrent.duration._
import java.lang.Thread

class ParserWorker(
    val parserNr: Int,
    val queue: LinkedBlockingQueue[Option[CalculationParserRequest]],
    val updateParsingStats: (CalculationParser.StatsValues) => Unit,
    val parsingManager: ActorRef
) extends Runnable with StrictLogging {

  class Settings(config: Config) {
    // validate vs. reference.conf
    config.checkValid(ConfigFactory.defaultReference(), "simple-lib")

    val uncompressRoot = config.getString("nomad_lab.parser_worker_rabbitmq.uncompressRoot")
    val parsedJsonPath = config.getString("nomad_lab.parser_worker_rabbitmq.parsedJsonPath")
    val parsedCalculationH5Path = config.getString("nomad_lab.parser_worker_rabbitmq.parsedCalculationH5Path")

    def toJValue: jn.JValue = {
      import org.json4s.JsonDSL._
      (("uncompressRoot" -> uncompressRoot) ~
        ("parsedJsonPath" -> parsedJsonPath))
    }
  }

  var repl = LocalEnv.defaultSettings.replacements
  val settings = new Settings(LocalEnv.defaultConfig)
  val calculationParser = new CalculationParser(
    ucRoot = settings.uncompressRoot.toString + "-" + parserNr.toString,
    backendGenerator = CalculationParser.jsonH5Generator,
    parserCollection = parsers.AllParsers.defaultParserCollection,
    replacements = LocalEnv.defaultSettings.replacements //,
  //statsFinishedCallback = Some(updateParsingStats)
  )
  def run(): Unit = {
    logger.info(s"Parser worker $parserNr ready to read now!")
    var workToDo: Boolean = true
    while (workToDo) {
      queue.take() match {
        case Some(message) =>
          logger.info(s"Parser $parserNr pulled message from the queue. Parsing now")
          parsingManager ! TaskStarted(message, new java.util.Date)
          val result = calculationParser.handleParseRequest(message)
          logger.info(s"Parser $parserNr: Parsing Done")
          parsingManager ! TaskFinished(result)
        case None =>
          queue.put(None)
          parsingManager ! WorkerStopped(parserNr.toString)
          logger.info(s"Parser $parserNr: Stopping Now! Received an empty message!")
          workToDo = false
      }
    }
    logger.info(s"Parser $parserNr stopped.")

  }

}
