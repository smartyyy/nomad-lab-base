/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.integrated_pipeline

import java.util.concurrent.LinkedBlockingQueue
import akka.actor._
import com.typesafe.config.{ Config, ConfigFactory }
import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.QueueMessage.CalculationParserRequest
import org.{ json4s => jn }
import eu.nomad_lab.parsers
import eu.nomad_lab.LocalEnv

object PipelineManager extends App with StrictLogging {

  val archiveRe = "nmd://(R[-_a-zA-Z0-9]{28})(?:/.*)?$".r

  val capacity = 10
  // Declare a blocking queue (ugly global, should be moved to tree parser)
  val blockingQueue: LinkedBlockingQueue[Option[CalculationParserRequest]] = new LinkedBlockingQueue[Option[CalculationParserRequest]](capacity)

  def defineSystem(nrOfWorkers: Int) {
    val system = ActorSystem("Calculation-Actor-System")
    val statsCollector = system.actorOf(Props[StatsCollector], name = "StatsCollector")
  }

  class Settings(config: Config) {
    // validate vs. reference.conf
    config.checkValid(ConfigFactory.defaultReference(), "simple-lib")
    val readQueue = config.getString("nomad_lab.parser_worker_rabbitmq.treeParserQueue")
    val readExchange = config.getString("nomad_lab.parser_worker_rabbitmq.treeParserExchange")
    val rabbitMQHost = config.getString("nomad_lab.parser_worker_rabbitmq.rabbitMQHost")
    val toBeNormalizedQueue = config.getString("nomad_lab.parser_worker_rabbitmq.toBeNormalizedQueue")
    val toBeNormalizedExchange = config.getString("nomad_lab.parser_worker_rabbitmq.toBeNormalizedExchange")

    val nrOfWorkers = config.getInt("nomad_lab.parser_worker_rabbitmq.numberOfWorkers")
    val treeparserMinutesOfRuntime: Int = config.getInt("nomad_lab.integrated_pipeline.treeparserMinutesOfRuntime")
    def toJValue: jn.JValue = {
      import org.json4s.JsonDSL._
      (("rabbitMQHost" -> rabbitMQHost) ~
        ("readQueue" -> readQueue) ~
        ("readExchange" -> readExchange) ~
        ("toBeNormalizedQueue" -> toBeNormalizedQueue) ~
        ("toBeNormalizedExchange" -> toBeNormalizedExchange) ~
        ("nrOfWorkers" -> nrOfWorkers) ~
        ("treeparserMinutesOfRuntime" -> treeparserMinutesOfRuntime))

    }
  }
  var repl = LocalEnv.defaultSettings.replacements
  val settings = new Settings(LocalEnv.defaultConfig)
  val nWorkers = {
    val w = Runtime.getRuntime().availableProcessors() / 2
    if (w < 2)
      2
    else
      w
  }
  //Define our actor system
  defineSystem(nWorkers)
}

object PipelineParserActor {
  case class ParseMessage()
}
