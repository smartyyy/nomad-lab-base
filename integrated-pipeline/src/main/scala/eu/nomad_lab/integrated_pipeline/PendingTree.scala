/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.integrated_pipeline

import eu.nomad_lab.QueueMessage.TreeParserRequest
import eu.nomad_lab.QueueMessage.CalculationParserRequest
import eu.nomad_lab.QueueMessage.TreeParserRequest

class PendingTree(
    val treeParserRequest: TreeParserRequest,
    var treeParsingFinished: Boolean = false,
    var mainUriToParse: Set[String] = Set(),
    var mainUriUnaknowledged: Set[String] = Set(),
    var pendingParsing: Map[String, CalculationParserRequest] = Map(),
    var parserStats: Map[String, ParserStats] = Map()
) {
  def isDone: Boolean = {
    treeParsingFinished && mainUriToParse.isEmpty
  }

  def addMainUriToParse(mainUri: String): Unit = {
    if (treeParsingFinished)
      throw new Exception(s"addMainUriToParse $mainUri after treeParsingFinished")
    if (mainUriUnaknowledged(mainUri))
      mainUriUnaknowledged -= mainUri
    else
      mainUriToParse += mainUri
  }

  def startedParsing(parseRequest: CalculationParserRequest): Unit = {
    val mainUri = parseRequest.mainFileUri
    if (mainUriToParse(mainUri))
      mainUriToParse -= mainUri
    else
      mainUriUnaknowledged += mainUri
    pendingParsing += (mainUri -> parseRequest)
  }

  def finishedParsing(mainFileUri: String): Unit = {
    pendingParsing.get(mainFileUri) match {
      // to do
      case Some(pendingParsing) =>
      case None =>
    }
  }
}
