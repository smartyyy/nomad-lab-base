/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.integrated_pipeline

import akka.actor._
import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.QueueMessage.{ CalculationParserRequest, CalculationParserResult, TreeParserRequest, ToBeNormalizedQueueMessage }
import eu.nomad_lab.parsing_queue.TreeParser
import eu.nomad_lab.parsing_queue.TreeParser.ParserAssociation
import eu.nomad_lab.parsers
import scala.util.control.NonFatal
import scala.concurrent.duration._

import TreeParserRunningStatus._

abstract class TreeParserActor extends Actor with StrictLogging {
  val statsCollector: ActorRef = context.parent
  var runningStatus: TreeParserRunningStatus.Value = TreeParserRunningStatus.NotStarted
  val parserCollection = parsers.AllParsers.defaultParserCollection
  val treeParser = new TreeParser(
    parserCollection = parserCollection
  )
  var endOps: Map[String, () => Unit] = Map()

  if (PipelineManager.settings.treeparserMinutesOfRuntime > 0) {
    import context._
    context.system.scheduler.scheduleOnce(PipelineManager.settings.treeparserMinutesOfRuntime.minutes, context.self, StopFetching)
  }

  /**
   * reads from the tree parser queue
   */
  def readFromTreeParserQueue(): Unit

  /**
   * stops reading from the tree parser queue at the next possibility
   */
  def stopReadingFromTreeParserQueue(): Unit

  /**
   * Find the parsable files and parsers. Write this information to the local blockingQueue
   */
  def findParserAndWriteToQueue(incomingMessage: TreeParserRequest, finished: () => Unit): Unit = {
    val treeUri = incomingMessage.treeUri
    endOps.get(treeUri) match {
      case None => endOps += (treeUri -> finished)
      case Some(op) =>
        logger.warn(s"Skipping parse request $incomingMessage because the same tree uri is already being processed")
        finished()
        return ()
    }
    def publish(parserAssociation: TreeParser.ParserAssociation) = {
      val message: CalculationParserRequest = TreeParser.createCalculationParserRequest(parserAssociation)
      statsCollector ! parserAssociation
      logger.info(s"Created CalculationParserRequest: $message!! Putting on the Blocking Queue")
      PipelineManager.blockingQueue.put(Some(message))
    }
    var cleanEnd: Boolean = false
    try {
      statsCollector ! TreeScanStart(incomingMessage)
      treeParser.findParserAndPublish(publish, incomingMessage)
      cleanEnd = true
    } catch {
      case NonFatal(e) =>
        logger.error(s"failure in treeparser", e)
        runningStatus = TreeParserRunningStatus.Stopped
        context.parent ! Die
    } finally {
      logger.info(s"Published to Blocking queue")
      statsCollector ! TreeScanEnd(incomingMessage, cleanEnd)
    }
  }

  def receive = {
    case StartTreeParser =>
      runningStatus match {
        case NotStarted =>
          runningStatus = Fetching
          readFromTreeParserQueue()
        case Fetching =>
          logger.warn(s"StartTreeParser called when treeParser is $runningStatus")
        case FetchingStopped =>
        case Stopped => ()

        // runningStatus to do
      }
    case TreeParserGroupProcessed(uri) =>
      endOps.get(uri) match {
        case None => throw new Exception(s"No finish op for $uri")
        case Some(op) =>
          endOps -= uri
          op()
      }
    case StopFetching =>
      runningStatus match {
        case NotStarted =>
          runningStatus = Stopped
        case Fetching =>
          stopReadingFromTreeParserQueue()
          runningStatus = FetchingStopped
        case FetchingStopped => ()
        case Stopped => ()
      }
    case Die =>
      runningStatus match {
        case NotStarted =>
          runningStatus = Stopped
          logger.info(s"Stopping ${self.path.name} (never started)")
        case Fetching =>
          stopReadingFromTreeParserQueue()
          PipelineManager.blockingQueue.put(None)
          runningStatus = Stopped
          logger.info(s"Stopping ${self.path.name}")
        case FetchingStopped =>
          PipelineManager.blockingQueue.put(None)
          runningStatus = Stopped
          logger.info(s"Stopping ${self.path.name}")
        case Stopped => ()
      }
  }
}
