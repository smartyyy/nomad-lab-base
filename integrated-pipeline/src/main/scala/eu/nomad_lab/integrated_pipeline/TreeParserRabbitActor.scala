/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.integrated_pipeline

import java.io.IOException
//import akka.actor._
import com.rabbitmq.client.{ DefaultConsumer, Envelope, _ }
import eu.nomad_lab.QueueMessage.{ TreeParserRequest, ToBeNormalizedQueueMessage }
import eu.nomad_lab.parsing_queue.TreeParser
import eu.nomad_lab.ref.NomadUri
import eu.nomad_lab.JsonSupport
import eu.nomad_lab.LocalEnv
//import scala.concurrent.duration._
import java.lang.Thread
import TreeParserRunningStatus._

class TreeParserRabbitActor extends TreeParserActor {
  val settings: PipelineManager.Settings = new PipelineManager.Settings(LocalEnv.defaultConfig)
  val rabbitMQConnectionFactory: ConnectionFactory = new ConnectionFactory
  rabbitMQConnectionFactory.setHost(settings.rabbitMQHost)
  val rabbitMQConnection: Connection = rabbitMQConnectionFactory.newConnection
  var channelObj: Option[Channel] = None
  var consumerTag: Option[String] = None

  /**
   * Read messages from the TreeParserQueue
   */
  def readFromTreeParserQueue(): Unit = {
    if (!channelObj.isEmpty)
      throw new TreeParser.FetchException(s"channelObj did exist in readFromTreeParserQueue")
    val channel: Channel = rabbitMQConnection.createChannel
    channelObj = Some(channel)
    channel.exchangeDeclare(settings.readExchange, "fanout")
    //channel.queueDeclare(String queue, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object> arguments)
    channel.queueDeclare(settings.readQueue, true, false, false, null)
    channel.queueBind(settings.readQueue, settings.readExchange, "")
    logger.info(s"Reading from Queue: ${settings.readQueue}")
    // Fair dispatch: don't dispatch a new message to a worker until it has acknowledged the previous one
    val prefetchCount = 1
    channel.basicQos(prefetchCount) //Sets prefetch for each consumer. For channel based prefetch use  channel.basicQos(prefetchCount, global = true)
    val consumer: Consumer = new DefaultConsumer((channel)) {
      @throws(classOf[IOException])
      override def handleDelivery(consumerTag: String, envelope: Envelope, properties: AMQP.BasicProperties, body: Array[Byte]) {
        val message: TreeParserRequest = JsonSupport.readUtf8[TreeParserRequest](body)
        logger.info(" [x] Received '" + message + "'")
        runningStatus match {
          case NotStarted =>
          case Fetching =>
          case FetchingStopped =>
          case Stopped => // to do
            def finishedOp(): Unit = {
              runningStatus match {
                case Stopped => // to do
                case NotStarted =>
                case Fetching =>
                case FetchingStopped =>
                  //if (!isStopping) {
                  logger.info(" [x] Done")
                  statsCollector ! TreeParserGroupFinished(message, true)
                  val treeUri = message.treeUri
                  val archiveGid = NomadUri(treeUri).archiveGid
                  var repl = LocalEnv.defaultSettings.replacements
                  repl += ("treeUri" -> treeUri)
                  repl += ("archiveGid" -> archiveGid)
                  repl += ("archiveGidPrefix" -> archiveGid.take(3))
                  val mergedH5Path = LocalEnv.defaultConfig.getString("nomad_lab.parser_worker_rabbitmq.parsedSingleH5Path")
                  val msg = ToBeNormalizedQueueMessage(
                    mergedH5Path = LocalEnv.makeReplacements(repl, mergedH5Path),
                    archiveUri = treeUri
                  )
                  writeToNormalizationQueue(msg)
                  //Send acknowledgement to the broker once the message has been consumed.
                  channel.basicAck(envelope.getDeliveryTag(), false)

              }
            }

            findParserAndWriteToQueue(message, finishedOp)
        }
      }
    }
    //basicConsume(String queue, boolean autoAck, Consumer callback)
    consumerTag match {
      case Some(t) => throw new Exception(s"consumerTag was already set in readFromTreeParserQueue")
      case None =>
        consumerTag = Some(channel.basicConsume(settings.readQueue, false, consumer))
    }
  }

  def stopReadingFromTreeParserQueue(): Unit = {
    channelObj match {
      case Some(channel) =>
        consumerTag match {
          case Some(tag) =>
            logger.info(s"stopping fetching")
            channel.basicCancel(tag)
          // set channelObj and consumerTag to None?
          case None => ()
        }
      case None => ()
    }
  }

  def writeToNormalizationQueue(message: ToBeNormalizedQueueMessage): Unit = {
    val prodchannel: Channel = rabbitMQConnection.createChannel
    prodchannel.exchangeDeclare(settings.toBeNormalizedExchange, "fanout")
    prodchannel.queueDeclare(settings.toBeNormalizedQueue, true, false, false, null)
    prodchannel.queueBind(settings.toBeNormalizedQueue, settings.toBeNormalizedQueue, "")
    val msgBytes = JsonSupport.writeUtf8(message)
    logger.info(s"Message: $message, bytes Array size: ${msgBytes.length}")
    try {
      prodchannel.basicPublish(settings.toBeNormalizedExchange, "", null, msgBytes)
    } catch {
      case e: Exception =>
        logger.error(s"Error while publishing to the exchange. Trying again in sometime. Message: ${e.getMessage}")
        try {
          Thread.sleep(1000)
          prodchannel.basicPublish(settings.toBeNormalizedExchange, "", null, msgBytes)
        } catch {
          case e: Exception =>
            logger.error(s"Retry failed. Exiting now. Message: ${e.getMessage}")
            System.exit(1)
        }
    }
  }
}
