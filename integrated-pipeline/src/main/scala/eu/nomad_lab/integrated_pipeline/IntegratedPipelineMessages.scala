/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.integrated_pipeline
import eu.nomad_lab.QueueMessage.{ CalculationParserRequest, CalculationParserResult, TreeParserRequest, ToBeNormalizedQueueMessage }

case class TaskFinished(result: CalculationParserResult)

case object StartTreeParser

case object Die

case object StopFetching

case object MasterStatusCheck

/**
 * called by tree parser on the stats collector when the tree is acknowledged
 */
case class TreeParserGroupFinished(
  parseRequest: TreeParserRequest,
  cleanEnd: Boolean
)

/**
 * called by StatsCollector on the TreeParser when full processing of the tree is done
 */
case class TreeParserGroupProcessed(
  treeUri: String
)

case class MasterStatusReport(
  nrActiveWorkers: Int,
  nrFilesParsed: Int,
  lastRequestFinishedTime: java.util.Date
)

case class TaskStarted(
  request: CalculationParserRequest,
  lastRequestReceivedTime: java.util.Date
)

case class TreeScanStart(message: TreeParserRequest)

case class TreeScanEnd(message: TreeParserRequest, cleanEnd: Boolean)

case class WorkerStopped(workerName: String)
