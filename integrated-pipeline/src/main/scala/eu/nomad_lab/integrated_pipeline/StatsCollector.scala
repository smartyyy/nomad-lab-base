/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.integrated_pipeline

import akka.actor._
import eu.nomad_lab.parsing_queue.TreeParser.ParserAssociation
import eu.nomad_lab.parsing_stats.ParsingStatsDb
import eu.nomad_lab.JsonSupport
import eu.nomad_lab.parsing_queue.CalculationParser
import eu.nomad_lab.LocalEnv
import scala.util.control.NonFatal
import com.typesafe.scalalogging.StrictLogging

class StatsCollector extends Actor with StrictLogging {
  var pendingTrees: Map[String, PendingTree] = Map()

  val treeParserActor = {
    val inFilePath = LocalEnv.defaultConfig.getString("nomad_lab.integrated_pipeline.inFile")
    if (inFilePath.isEmpty)
      context.actorOf(Props[TreeParserRabbitActor], name = "TreeParserActor")
    else
      context.actorOf(Props[TreeParserFileActor], name = "TreeParserActor")
  }

  val calculationParserMaster = context.actorOf(Props[CalculationParserMaster], name = "CalculationParserMaster")

  treeParserActor ! StartTreeParser

  val statsDb: Option[ParsingStatsDb] = {
    try {
      val db = ParsingStatsDb()
      Some(db)
    } catch {
      case NonFatal(e) =>
        logger.warn("Failed to initialize parsing stats db", e)
        None
    }
  }

  def receive = {
    case TaskFinished(result) =>
      logger.debug(s"Writing Results to the ParsingDB: ${JsonSupport.writePrettyStr(result)}")
      statsDb match {
        case Some(db) => db.setParsingResult(result)
        case None => ()
      }
    case parserAssociation: ParserAssociation =>
      logger.debug(s"parserAssociation added to db")
      val treeUri = parserAssociation.incomingMessage.treeUri
      pendingTrees.get(treeUri) match {
        case None =>
          throw new Exception(s"StatsCollector not pending tree for $treeUri when processing parserAssociation $parserAssociation")
        case Some(pTree) =>
          pTree.addMainUriToParse(parserAssociation.mainFileUri)
          statsDb match {
            case Some(db) =>
              db.addCalculationAssignement(parserAssociation)
            case None => ()
          }
      }
    case stats: CalculationParser.StatsValues =>
      statsDb match {
        case None => ()
        case Some(db) =>
          db.updateParsingStats(stats)
      }
    case TreeScanStart(message) =>
      val treeUri = message.treeUri
      pendingTrees.get(treeUri) match {
        case None =>
          pendingTrees += (treeUri -> new PendingTree(message))
        case Some(pTree) =>
          throw new Exception(s"starting tree ${treeUri} for $message, when $pTree is already in progress")
      }
      statsDb match {
        case Some(db) =>
          db.registerTreeScanStart(
            rootNamespace = LocalEnv.defaultSettings.rootNamespace,
            treeUri = treeUri
          )
        case None => ()
      }
    case TreeScanEnd(treeParserRequest, cleanEnd) =>
      val treeUri = treeParserRequest.treeUri
      pendingTrees.get(treeUri) match {
        case None =>
          throw new Exception(s"finished scan for tree ${treeUri} which is not listed as pending")
          pendingTrees += (treeUri -> new PendingTree(treeParserRequest))
        case Some(pTree) =>
          pTree.treeParsingFinished = true
          if (pTree.pendingParsing.isEmpty) {

            pendingTrees -= treeUri
            treeParserActor ! TreeParserGroupFinished(treeParserRequest, cleanEnd)
          }
      }
      statsDb match {
        case Some(db) =>
          db.registerTreeScanEnd(
            rootNamespace = LocalEnv.defaultSettings.rootNamespace,
            treeUri = treeUri
          )
        case None => ()
      }
    case TreeParserGroupProcessed(uri) =>
      treeParserActor ! TreeParserGroupProcessed(uri)
    case TreeParserGroupFinished(treeParserRequest, cleanEnd) =>
    case Die =>
      //context.stop(context.self)
      logger.info("...stopping everything")
      context.system.shutdown()
      logger.info("it is a lonely world")
      java.lang.System.exit(0)
  }
}
