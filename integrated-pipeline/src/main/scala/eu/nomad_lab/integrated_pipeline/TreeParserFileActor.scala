/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.integrated_pipeline

//import akka.actor._
import eu.nomad_lab.QueueMessage.{ TreeParserRequest, ToBeNormalizedQueueMessage }
import eu.nomad_lab.LocalEnv
import TreeParserRunningStatus._

class TreeParserFileActor extends TreeParserActor {
  val inFilePath = LocalEnv.defaultConfig.getString("nomad_lab.integrated_pipeline.inFile")
  val inF = scala.io.Source.fromFile(inFilePath)(scala.io.Codec.UTF8).getLines()

  def readFromTreeParserQueue(): Unit = {
    logger.info(s"Finished reading file $inFilePath")
    runningStatus match {
      case NotStarted =>
        runningStatus = Stopped
        logger.info(s"Stopping ${self.path.name}")
      case Fetching =>
        if (inF.hasNext) {
          val line = inF.next
          val message = TreeParserRequest.fromPath(java.nio.file.Paths.get(line))
          findParserAndWriteToQueue(message, { () => () })
          // blockingQueue.put(Some(inF.next))
          self ! StartTreeParser
        } else {
          runningStatus = FetchingStopped
          // if nothing in progress => stop
          logger.info(s"No more data for ${self.path.name}")
        }
      case FetchingStopped => ()
      case Stopped => ()
    }
  }

  def stopReadingFromTreeParserQueue(): Unit = {
    runningStatus match {
      case NotStarted =>
        runningStatus = Stopped
        logger.info(s"Stopping ${self.path.name}")
      case Fetching =>
        runningStatus = FetchingStopped
        logger.info(s"No more data for ${self.path.name}")
      case FetchingStopped => ()
      case Stopped => ()
    }
  }

}
