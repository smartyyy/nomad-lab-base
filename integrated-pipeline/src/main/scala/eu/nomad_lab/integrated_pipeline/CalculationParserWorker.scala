/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.integrated_pipeline

import akka.actor._
import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.QueueMessage.CalculationParserRequest
import eu.nomad_lab.parsers
import eu.nomad_lab.JsonSupport
import eu.nomad_lab.parsing_queue.CalculationParser
import eu.nomad_lab.LocalEnv
import scala.collection.mutable
import scala.concurrent.duration._
import java.lang.Thread

/**
 * actor that keeps all the parser threads
 */
class CalculationParserMaster() extends Actor with StrictLogging {
  val maxWorker: Int = LocalEnv.defaultConfig.getInt("nomad_lab.parser_worker_rabbitmq.numberOfWorkers")
  val statsCollector: ActorRef = context.parent
  var lastRequestFinishedTime: Option[java.util.Date] = None
  var lastRequest: Option[CalculationParserRequest] = None
  val workersStatus: mutable.Map[String, TaskStarted] = mutable.Map()
  var nrFilesParsed = 0
  var activeWorkers = 0
  def updateParsingStats(stats: CalculationParser.StatsValues): Unit = {
    statsCollector ! stats
  }
  import context._

  val workers = for (i <- 1 to maxWorker) yield {
    val worker = new ParserWorker(i, PipelineManager.blockingQueue, updateParsingStats: (CalculationParser.StatsValues) => Unit, context.self)
    val thread = new Thread(worker, "calculationParser-" + i.toString)
    thread.start()
    (worker -> thread)
  }
  activeWorkers = maxWorker
  logger.info(s"Created ${workers.size} workers.")

  def receive = {
    case Die =>
      //statsCollector ! TreeParserGroupProcessed
      logger.info("Manager will die...")
      for ((worker, thread) <- workers)
        if (thread.isAlive())
          logger.warn(s"Stopping manager while worker thread ${thread.getName} is still alive")
      context.stop(self)
    case TaskStarted(request, lastRequestReceivedTime) =>
      workersStatus += sender().path.name -> TaskStarted(request, lastRequestReceivedTime)
      lastRequest = Some(request)
      logger.info(s"Received message from ParserActor: Parsing Started. ${JsonSupport.writePrettyStr(request)}, $lastRequestReceivedTime")
    case TaskFinished(result) =>
      workersStatus -= sender().path.name
      lastRequestFinishedTime = Some(new java.util.Date)
      nrFilesParsed += 1
      statsCollector ! TaskFinished(result)
      logger.info(s"Received message from ParserActor: Parsing Done. $result, $lastRequestFinishedTime")
    // write normalization queue
    case MasterStatusCheck =>
      sender() ! MasterStatusReport(workersStatus.size, nrFilesParsed, lastRequestFinishedTime.getOrElse(new java.util.Date))
    case WorkerStopped(w) =>
      activeWorkers -= 1
      logger.info(s"Manager detected worker stop, active workers: $activeWorkers")
      if (activeWorkers == 0) {
        logger.info(s"Manager detected that all workers stopped, perparing to kill")
        context.system.scheduler.scheduleOnce(30.seconds, statsCollector, Die)
        context.system.scheduler.scheduleOnce(20.seconds, self, Die)
      }
  }

}
