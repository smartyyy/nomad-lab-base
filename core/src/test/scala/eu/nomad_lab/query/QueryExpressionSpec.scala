/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.query

import org.specs2.mutable.Specification
import org.{ json4s => jn }

/**
 * Specification (fixed tests) for query strings
 */
class QueryExpressionSpec extends Specification {
  "atomic term parse" >> {
    QueryExpression.parseExpression(
      "atom_labels"
    ) must_== AtomicTerm(
        metaName = "atom_labels"
      )

    QueryExpression.parseExpression(
      "not atom_labels"
    ) must_== AtomicTerm(
        invert = true,
        metaName = "atom_labels"
      )

    QueryExpression.parseExpression(
      "not all atom_labels.length > 1"
    ) must_== AtomicTerm(
        invert = true,
        quantifier = QueryQuantifier.All,
        metaName = "atom_labels",
        metaElement = Some(MetaElement.Length),
        compareOp = Some(CompareOp.OpBigger),
        values = Seq(jn.JInt(1))
      )

    QueryExpression.parseExpression(
      "alltarget atom_labels = \"Co\",\"O\""
    ) must_== AtomicTerm(
        quantifier = QueryQuantifier.AllTarget,
        metaName = "atom_labels",
        compareOp = Some(CompareOp.OpEqual),
        values = Seq(jn.JString("Co"), jn.JString("O"))
      )

    QueryExpression.parseExpression(
      "alltarget atom_labels = Co,O"
    ) must_== AtomicTerm(
        quantifier = QueryQuantifier.AllTarget,
        metaName = "atom_labels",
        compareOp = Some(CompareOp.OpEqual),
        values = Seq(jn.JString("Co"), jn.JString("O"))
      )

    QueryExpression.parseExpression(
      "alltarget atom_labels = Co,O and not energy_total"
    ) must_== AndConjunction(Seq(
        AtomicTerm(
          quantifier = QueryQuantifier.AllTarget,
          metaName = "atom_labels",
          compareOp = Some(CompareOp.OpEqual),
          values = Seq(jn.JString("Co"), jn.JString("O"))
        ), AtomicTerm(
          invert = true,
          metaName = "energy_total"
        )
      ))
  }

}
