/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.query

import org.specs2.mutable.Specification
import org.{ json4s => jn }
import eu.nomad_lab.meta.KnownMetaInfoEnvs

/**
 * Specification (fixed tests) for query strings
 */
class CompiledSectionTermSpec extends Specification {
  "existence term" >> {
    val t1 = AtomicTerm(
      metaName = "atom_labels"
    )
    val c1 = CompiledSectionTerm(KnownMetaInfoEnvs.publicMeta, t1)
    val cc = c1.comparer.asInstanceOf[BaseComparer[Long]]
    val e = cc.evalWithIterator
    e(Iterator(0L)) must_== false
    e(Iterator(1L)) must_== true
    e(Iterator(2L)) must_== true
  }
  "non existence term" >> {
    val t1 = AtomicTerm(
      invert = true,
      metaName = "atom_labels"
    )
    val c1 = CompiledSectionTerm(KnownMetaInfoEnvs.publicMeta, t1)
    val cc = c1.comparer.asInstanceOf[BaseComparer[Long]]
    val e = cc.evalWithIterator
    e(Iterator(0L)) must_== true
    e(Iterator(1L)) must_== false
    e(Iterator(2L)) must_== false
  }
  "any check" >> {
    val t1 = AtomicTerm(
      quantifier = QueryQuantifier.Any,
      metaName = "atom_labels",
      metaElement = Some(MetaElement.Length),
      compareOp = Some(CompareOp.OpEqual),
      values = Seq(jn.JInt(1L), jn.JInt(2L))
    )
    val c1 = CompiledSectionTerm(KnownMetaInfoEnvs.publicMeta, t1)
    val cc = c1.comparer.asInstanceOf[BaseComparer[Long]]
    val e = cc.evalWithIterator
    e(Iterator(0L, 1L)) must_== true
    e(Iterator(1L, 0L)) must_== true
    e(Iterator(0L, 2L)) must_== true
    e(Iterator(3L, 2L, 1L)) must_== true
    e(Iterator(2L, 1L)) must_== true
    e(Iterator(1L, 2L)) must_== true
    e(Iterator(0L)) must_== false
    e(Iterator(1L)) must_== true
    e(Iterator(3L, 0L)) must_== false
    e(Iterator()) must_== false
  }
  "alltarget check" >> {
    val t1 = AtomicTerm(
      quantifier = QueryQuantifier.AllTarget,
      metaName = "atom_labels",
      metaElement = Some(MetaElement.Length),
      compareOp = Some(CompareOp.OpEqual),
      values = Seq(jn.JInt(1L), jn.JInt(2L))
    )
    val c1 = CompiledSectionTerm(KnownMetaInfoEnvs.publicMeta, t1)
    val cc = c1.comparer.asInstanceOf[BaseComparer[Long]]
    val e = cc.evalWithIterator
    e(Iterator(0L, 1L)) must_== false
    e(Iterator(1L, 0L)) must_== false
    e(Iterator(0L, 2L)) must_== false
    e(Iterator(3L, 2L, 1L)) must_== true
    e(Iterator(2L, 1L)) must_== true
    e(Iterator(1L, 2L)) must_== true
    e(Iterator(0L)) must_== false
    e(Iterator(1L)) must_== false
    e(Iterator(3L, 0L)) must_== false
    e(Iterator()) must_== false
  }
  "alldata check" >> {
    val t1 = AtomicTerm(
      quantifier = QueryQuantifier.AllData,
      metaName = "atom_labels",
      metaElement = Some(MetaElement.Length),
      compareOp = Some(CompareOp.OpEqual),
      values = Seq(jn.JInt(1L), jn.JInt(2L))
    )
    val c1 = CompiledSectionTerm(KnownMetaInfoEnvs.publicMeta, t1)
    val cc = c1.comparer.asInstanceOf[BaseComparer[Long]]
    val e = cc.evalWithIterator
    e(Iterator(0L, 1L)) must_== false
    e(Iterator(1L, 0L)) must_== false
    e(Iterator(0L, 2L)) must_== false
    e(Iterator(3L, 2L, 1L)) must_== false
    e(Iterator(2L, 1L)) must_== true
    e(Iterator(1L, 2L)) must_== true
    e(Iterator(0L)) must_== false
    e(Iterator(1L)) must_== true
    e(Iterator(3L, 0L)) must_== false
    e(Iterator()) must_== true
  }
  "all check" >> {
    val t1 = AtomicTerm(
      quantifier = QueryQuantifier.All,
      metaName = "atom_labels",
      metaElement = Some(MetaElement.Length),
      compareOp = Some(CompareOp.OpEqual),
      values = Seq(jn.JInt(1L), jn.JInt(2L))
    )
    val c1 = CompiledSectionTerm(KnownMetaInfoEnvs.publicMeta, t1)
    val cc = c1.comparer.asInstanceOf[BaseComparer[Long]]
    val e = cc.evalWithIterator
    e(Iterator(0L, 1L)) must_== false
    e(Iterator(1L, 0L)) must_== false
    e(Iterator(0L, 2L)) must_== false
    e(Iterator(3L, 2L, 1L)) must_== false
    e(Iterator(2L, 1L)) must_== true
    e(Iterator(1L, 2L)) must_== true
    e(Iterator(0L)) must_== false
    e(Iterator(1L)) must_== false
    e(Iterator(3L, 0L)) must_== false
    e(Iterator()) must_== false
  }
  "matching check" >> {
    val t1 = AtomicTerm(
      quantifier = QueryQuantifier.Matching,
      metaName = "atom_labels",
      metaElement = Some(MetaElement.Length),
      compareOp = Some(CompareOp.OpEqual),
      values = Seq(jn.JInt(1L), jn.JInt(2L))
    )
    val c1 = CompiledSectionTerm(KnownMetaInfoEnvs.publicMeta, t1)
    val cc = c1.comparer.asInstanceOf[BaseComparer[Long]]
    val e = cc.evalWithIterator
    e(Iterator(0L, 1L)) must_== false
    e(Iterator(1L, 0L)) must_== false
    e(Iterator(0L, 2L)) must_== false
    e(Iterator(3L, 2L, 1L)) must_== false
    e(Iterator(2L, 1L)) must_== false
    e(Iterator(1L, 2L)) must_== true
    e(Iterator(0L)) must_== false
    e(Iterator(1L)) must_== false
    e(Iterator(3L, 0L)) must_== false
    e(Iterator()) must_== false
  }

}
