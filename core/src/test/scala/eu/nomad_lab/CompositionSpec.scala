/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab

import org.specs2.mutable.Specification
import java.nio.charset.StandardCharsets
import Composition._
import util.control.NonFatal

/**
 * Specification (fixed tests) for Composition
 */
class CompositionSpec extends Specification {
  "dict<->Formula" >> {
    formulaStrToDict("SiO2") must_== Map(14 -> 1, 8 -> 2)
    formulaStrToDict("FeO3XiXoP5") must_== Map(26 -> 1, 8 -> 3, 0 -> 1, -1 -> 1, 15 -> 5)
    formulaStrToDict("C4H3COOH") must_== Map(6 -> 5, 1 -> 4, 8 -> 2)
    dictToFormulaStr(formulaStrToDict("SiO2"), reduce = false) must_== "SiO2"
    dictToFormulaStr(formulaStrToDict("FeO3XiXoP5"), reduce = false) must_== "FeP5O3XXb"
    (try {
      formulaStrToDict("SiO_4*2(H2O)")
      false
    } catch {
      case NonFatal(e) => true
    }) must_== true
    formulaStrToDict("PippoI3") must_== Map(0 -> 1, 53 -> 3)
  }

  "reweightedFormula" >> {
    dictToFormulaStr(formulaDictReweighted(formulaStrToDict("H2O"))) must_== "O34H67"
    dictToFormulaStr(formulaDictReweighted(formulaDictReweighted(formulaStrToDict("H2O")))) must_== "O34H67"
    dictToFormulaStr(formulaDictReweighted(formulaStrToDict("C200H2O"))) must_== "OC99H"
    dictToFormulaStr(formulaDictReweighted(formulaDictReweighted(formulaStrToDict("C200H2O")))) must_== "OC99H"
    dictToFormulaStr(formulaDictReweighted(formulaStrToDict("C300HO"))) must_== "OC99H"
    dictToFormulaStr(formulaDictReweighted(formulaDictReweighted(formulaStrToDict("C300HO")))) must_== "OC99H"
  }
}
