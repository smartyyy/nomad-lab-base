/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.parsers

import java.io._
import java.nio.file.Files

import org.specs2.mutable.Specification
import org.{ json4s => jn }
import ucar.ma2
import ucar.ma2.{ Array => NArray }
import org.apache.avro
import org.apache.avro.generic.{ GenericDatumReader, GenericRecord }
import org.apache.avro.file.DataFileReader
import eu.nomad_lab.JsonUtils
import org.json4s.JValue
import org.json4s.JsonAST.JObject

class AvroParseEventsWriterBackendTest extends Specification {
  "AvroParseEventsWriterBackendTest" should {
    "check that the avro test file is written correctly" in {

      // Create a simple test parser
      val echoParserGen = new SimpleExternalParserGenerator(
        name = "testParserForAvro",
        parserInfo = jn.JObject(
          ("name" -> jn.JString("testParserForAvro")) ::
            ("version" -> jn.JString("1.0")) :: Nil
        ),
        mainFileTypes = Seq("text/.*"),
        mainFileRe = """XXX testParserForAvro XXX""".r,
        cmd = Seq("/bin/cat", "${mainFilePath}"),
        resList = Seq()
      )

      val avroFile: String = "avroTest.avro"
      val avroBackend = new AvroParseEventsWriterBackend(
        metaInfoEnv = echoParserGen.parseableMetaInfo,
        outFile = new java.io.File(avroFile)
      )

      val realValue: Double = 2.3
      var references: Map[String, Long] = Map()
      references = references.+("Lady in the Water" -> 3, "Snakes on a Plane" -> 4)

      val arrayInt: Array[Int] = Array(1, 2, 3, 4, 5, 6)
      var arrayLong: Array[Long] = Array()
      val itInt = arrayInt.iterator
      while (itInt.hasNext) {
        arrayLong = arrayLong :+ itInt.next().asInstanceOf[Number].longValue()
      }
      val arrayDouble: Array[Double] = Array(23.5, 3245.1, 34.1)
      var arrayFloat: Array[Float] = Array()
      val itDouble = arrayDouble.iterator
      while (itDouble.hasNext) {
        arrayFloat = arrayFloat :+ itDouble.next().toFloat
      }
      val arrayBool: Array[Boolean] = Array(true, false, false)
      val arrayString = new ma2.ArrayObject.D2(classOf[String], 1, 3)
      arrayString.set(0, 0, "ha ha")
      arrayString.set(0, 1, "ho ho")
      arrayString.set(0, 2, "hi hi")

      val mainFileUri = Some("nmd://Rasdfghjklpqwertyuioplkjhgfde")
      val metaName: List[String] = List("firstSection", "firstValue", "firstRealValue", "firstIntArray", "firstLongArray", "firstFloatArray", "firstDoubleArray", "firstBooleanArray", "firstStringArray", "firstSection")
      val gIndex: Long = 0

      avroBackend.startedParsingSession(mainFileUri, echoParserGen.parserInfo, Some(ParseResult.ParseSuccess), jn.JNothing)
      avroBackend.openSectionWithGIndex(metaName(0), gIndex)
      avroBackend.setSectionInfo(metaName(0), gIndex, references)
      avroBackend.addValue(metaName(1), echoParserGen.parserInfo, gIndex)
      avroBackend.addRealValue(metaName(2), realValue, gIndex)
      avroBackend.addArrayValues(metaName(3), NArray.factory(arrayInt), gIndex)
      avroBackend.addArrayValues(metaName(4), NArray.factory(arrayLong), gIndex)
      avroBackend.addArrayValues(metaName(5), NArray.factory(arrayFloat), gIndex)
      avroBackend.addArrayValues(metaName(6), NArray.factory(arrayDouble), gIndex)
      avroBackend.addArrayValues(metaName(7), NArray.factory(arrayBool), gIndex)
      avroBackend.addArrayValues(metaName(8), arrayString, gIndex)
      //addArray(metaName: String, shape: Seq[Long], gIndex: Long = -1)
      //setArrayValues(metaName: String, values: NArray, offset: Option[Seq[Long]] = None, gIndex: Long = -1)
      avroBackend.closeSection(metaName(0), gIndex)
      avroBackend.finishedParsingSession(Some(ParseResult.ParseSuccess), jn.JNothing, mainFileUri, echoParserGen.parserInfo)

      val classLoader: ClassLoader = getClass().getClassLoader()
      val avroSchemaStream: java.io.InputStream = classLoader.getResourceAsStream("ParseEvents.avsc")
      val schema: avro.Schema = new avro.Schema.Parser().parse(avroSchemaStream)
      val reader = new GenericDatumReader[GenericRecord](schema)
      val myFile = new File("avroTest.avro")
      val dataFileReader = new DataFileReader[GenericRecord](myFile, reader)
      var x: GenericRecord = null

      x = dataFileReader.next()
      mainFileUri.get.compare(x.get("mainFileUri").toString) must_== 0

      x = dataFileReader.next()
      x.get("metaName").toString == metaName(0) must_== true

      x = dataFileReader.next()
      x.get("metaName").toString == metaName(0) must_== true

      x = dataFileReader.next()
      x.get("metaName").toString == metaName(1) must_== true
      org.json4s.jackson.JsonMethods.parse(x.get("value").toString) == echoParserGen.parserInfo must_== true

      x = dataFileReader.next()
      x.get("metaName").toString == metaName(2) must_== true
      x.get("value").toString.toDouble == realValue must_== true

      x = dataFileReader.next()
      x.get("metaName").toString == metaName(3) must_== true
      val y1 = x.get("flatValues").asInstanceOf[org.apache.avro.generic.GenericData.Array[Int]]
      for (i <- arrayInt.indices) {
        y1.get(i) must_== arrayInt(i)
      }

      x = dataFileReader.next()
      x.get("metaName").toString == metaName(4) must_== true
      val y2 = x.get("flatValues").asInstanceOf[org.apache.avro.generic.GenericData.Array[Long]]
      for (i <- arrayLong.indices) {
        y2.get(i) must_== arrayInt(i)
      }

      //      x = dataFileReader.next()
      //      x.get("metaName").toString == metaName(5) must_== true
      //      x.get("metaName").toString == metaName(6) must_== true
      //      x.get("metaName").toString == metaName(7) must_== true
      //      x.get("metaName").toString == metaName(8) must_== true

      //      x.get("flatValues").toString ==  5) must_== true
      //      x.get("flatValues").toString ==  6) must_== true
      //      x.get("flatValues").toString ==  7) must_== true
      //      x.get("flatValues").toString ==  8) must_== true
      //JsonUtils.parseStr(dataFileReader.next().toString) \\ "mainFileUri" == jn.JString(mainFileUri.get) must_== true
      //val gr = dataFileReader.next()
      //println("metaName: " + gr.get("metaName"))
      //println("firstSection: " + metaName)
      //gr.get("metaName") == metaName must_== false // ES SOLLTE TRUE SEIN, ABER DANN FAILED DER TEST. WARUM???????
      //println(dataFileReader.next().get("mainFileUri").toString)

      Files.delete(java.nio.file.Paths.get(avroFile))
      ok
    }
  }
}
