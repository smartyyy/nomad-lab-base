/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.parsers;
import org.specs2.mutable.Specification
import ucar.ma2

/**
 * Specification (fixed tests) for MetaInfo serialization
 */
class JsonWriterBackendSpec extends Specification {
  val a1 = new ma2.ArrayDouble.D2(3, 2)

  "a2f" >> {
    val writer = new java.io.StringWriter
    JsonWriterBackend.writeNArrayWithDtypeStr(a1, "f64", writer)
    val out = writer.toString()
    out must_== """[[0.0, 0.0],
 [0.0, 0.0],
 [0.0, 0.0]]"""
  }
}
