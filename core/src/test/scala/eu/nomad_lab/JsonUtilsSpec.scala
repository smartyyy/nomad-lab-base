/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab

import org.specs2.mutable.Specification
import org.json4s.{ JNothing, JNull, JBool, JDouble, JDecimal, JInt, JString, JArray, JObject, JValue, JField }

/**
 * Specification (fixed tests) for MetaInfo serialization
 */
class JsonUtilsSpec extends Specification {

  //implicit val formats = DefaultFormats + new eu.nomad_lab.MetaInfoRecordSerializer
  import org.json4s.JsonDSL._;
  val jObj1 =
    ("a" -> 45) ~
      ("b" -> 1.0) ~
      ("c" -> "z") ~
      ("d" -> Seq(5, 4, 2, 4)) ~
      ("e" -> false) ~
      ("f" -> true) ~
      ("g" -> JNothing) ~
      ("h" -> JNull) ~
      ("i" -> math.Pi);

  val jObj1Str = """{"a":45,"b":1.0,"c":"z","d":[5,4,2,4],"e":false,"f":true,"h":null,"i":3.141592653589793}"""

  val jObj2 = JObject(jObj1.obj.reverse)

  val jVal = JsonUtils.parseStr("""
    {
        "name": "TestProperty1",
        "description": "a meta info property to test serialization to json",
        "superNames": []
    }""")

  "jsonNormalized 1" >> {
    JsonUtils.normalizedStr(jObj1) must_== jObj1Str
    JsonUtils.normalizedStr(jObj2) must_== jObj1Str
  }

  "jsonComplexity" >> {
    JsonUtils.jsonComplexity(jObj1) must_== 14
    JsonUtils.jsonComplexity(jVal) must_== 4
  }

  "string-JValue" >> {
    val s1 = "0123456789 0123456789 \n \n\n0123456789 0123456789 "
    val s2 = "0123456789 0123456789 \n 01234567890123456789"
    val v1 = JsonUtils.normalizedStr(JsonUtils.stringToJValue(s1, maxLineLength = 10))
    val v2 = JsonUtils.normalizedStr(JsonUtils.stringToJValue(s2, maxLineLength = 10))
    val e1 = """["0123456789 ","0123456789"," \n"," \n","\n","0123456789"," 012345678","9 "]"""
    val e2 = """["0123456789 ","0123456789"," \n"," 012345678","9012345678","9"]"""

    "s->j->s" >> {
      JsonUtils.jValueToString(JString(s1), "s1", "JsonUtilsSpec") must_== Some(s1)
      JsonUtils.jValueToString(JsonUtils.stringToJValue(s1, maxLineLength = 10), "stringToJValue(s1)", "JsonUtilsSpec") must_== Some(s1)
      JsonUtils.jValueToString(JsonUtils.stringToJValue(s2, maxLineLength = 10), "s2", "JsonUtilsSpec") must_== Some(s2)
      JsonUtils.jValueToString(JsonUtils.stringToJValue("", maxLineLength = 10), "emptyString", "JsonUtilsSpec") must_== Some("")
    }

    "s->j" >> {
      JsonUtils.normalizedStr(JsonUtils.stringToJValue("")) must_== "\"\""
      JsonUtils.normalizedStr(JsonUtils.stringToJValue("x y")) must_== "\"x y\""
      JsonUtils.normalizedStr(JsonUtils.stringToJValue("", forceArray = true)) must_== "[\"\"]"
      JsonUtils.normalizedStr(JsonUtils.stringToJValue("x y", forceArray = true)) must_== "[\"x y\"]"
      v1 must_== e1
      v2 must_== e2
    }
  }

  "stringArrayIndent" >> {
    {
      val s = new java.io.StringWriter
      val w = new JsonUtils.StringArraySplitWriter(s)
      w.write("""["ba","xx"]""")
      s.toString must_== """[
  "ba",
  "xx"]"""
    }
    {
      val s = new java.io.StringWriter
      val w = new JsonUtils.StringArraySplitWriter(s)
      w.write("""[{
   "rr": ["ba","xx"]}]""")
      s.toString must_== """[{
   "rr": [
     "ba",
     "xx"]}]"""
    }
  }

}
