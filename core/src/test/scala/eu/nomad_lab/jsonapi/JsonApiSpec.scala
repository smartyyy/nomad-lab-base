/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.jsonapi

import org.specs2.mutable.Specification
import java.nio.charset.StandardCharsets
import util.control.NonFatal
import org.{ json4s => jn }

/**
 * Specification (fixed tests) for JsonApi
 */
class JsonApiSpec extends Specification {
  "Response" >> {
    val v1 = Value("type1", "id1", attributes = Map("bla" -> jn.JString("bla"), "x" -> jn.JInt(4)))
    val v2 = Value("type2", "id2", attributes = Map("tr" -> jn.JString("xx")), includedRelationships = Map("z" -> ToOne(Some(v1)), "zs" -> ToMany(Seq(v1))))
    val v2b = Value("type2", "id2b", attributes = Map("tr" -> jn.JString("xx")), includedRelationships = Map("z" -> ToOne(Some(v1)), "zs" -> ToMany(Seq(v1))))
    val v3 = Value("type2", "id1", attributes = Map("tr" -> jn.JString("xx2")), includedRelationships = Map("z" -> ToOne(None), "zs" -> ToMany(Seq())))
    val res1 = {
      val w = new java.io.StringWriter
      val rw = new ResultWriter(w)
      rw.start()
      rw.addValue(v2)
      rw.addValue(v2)
      rw.addValue(v2b)
      rw.addValue(v3)
      rw.finish()
      w.flush()
      w.close()
      w.toString
    }
    res1 must_== """{
  "data": [
    {"type":"type2","id":"id2","attributes":{"tr":"xx"},"relationships":{"z":{"data":{"type":"type1","id":"id1"}},"zs":{"data":[{"type":"type1","id":"id1"}]}}},
    {"type":"type2","id":"id2b","attributes":{"tr":"xx"},"relationships":{"z":{"data":{"type":"type1","id":"id1"}},"zs":{"data":[{"type":"type1","id":"id1"}]}}},
    {"type":"type2","id":"id1","attributes":{"tr":"xx2"},"relationships":{"z":{"data":null},"zs":{"data":[]}}}],
  "include": [
    {"type":"type1","id":"id1","attributes":{"bla":"bla","x":4}}]
}
"""

    val res2 = {
      val w = new java.io.StringWriter
      val rw = new ResultWriter(w, loneValue = true)
      rw.start()
      rw.addValue(v2)
      rw.finish()
      w.flush()
      w.close()
      w.toString
    }
    res2 must_== """{
  "data":
    {"type":"type2","id":"id2","attributes":{"tr":"xx"},"relationships":{"z":{"data":{"type":"type1","id":"id1"}},"zs":{"data":[{"type":"type1","id":"id1"}]}}},
  "include": [
    {"type":"type1","id":"id1","attributes":{"bla":"bla","x":4}}]
}
"""

    val res3 = {
      val w = new java.io.StringWriter
      val rw = new ResultWriter(w)
      rw.start()
      rw.finish(errors0 = Iterator(Error(
        "Test Internal Error",
        """An error just for testing purposes"""
      )))
      w.flush()
      w.close()
      w.toString
    }
    res3 must_== """{
  "errors":[
    {"title":"Test Internal Error","detail":"An error just for testing purposes"}]
}
"""
  }
}
