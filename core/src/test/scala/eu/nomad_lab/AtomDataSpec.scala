/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab

import org.specs2.mutable.Specification
import java.nio.charset.StandardCharsets
import AtomData._
import util.control.NonFatal

/**
 * Specification (fixed tests) for AtomData
 */
class AtomDataSpec extends Specification {
  "atomNrFromLabel" >> {
    for ((symb, atNr) <- chemicalSymbols.zipWithIndex)
      atomNrFromLabel(symb) must_== atNr
    atomNrFromLabel("Xr") must_== 0
    atomNrFromLabel("Xippo") must_== 0
  }

  "atomNrsFromLabels" >> {
    for ((atNr, ref) <- atomNrsFromLabels(chemicalSymbols).zipWithIndex)
      atNr must_== ref
    val nrs = atomNrsFromLabels(Seq("Si", "Fe", "Xippo", "Xallino", "Xinco"))
    nrs must_== Seq(14, 26, -2, 0, -1)
  }

  "labelForAtomNr" >> {
    for ((symb, atNr) <- chemicalSymbols.zipWithIndex)
      labelForAtomNr(atNr) must_== symb
    labelForAtomNr(-1) must_== "Xb"
    labelForAtomNr(-2) must_== "Xc"
    labelForAtomNr(-45) must_== "Xzcbw"
  }
}
