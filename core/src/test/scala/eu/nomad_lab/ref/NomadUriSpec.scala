/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.ref

import org.specs2.mutable.Specification
import java.nio.charset.StandardCharsets
import com.typesafe.scalalogging.StrictLogging
import scala.collection.SortedMap

/**
 * Specification (fixed tests) for MetaInfo serialization
 */
class NomadUriSpec extends Specification with StrictLogging {

  val uriSamples = Seq(
    "nmd://Rpippo_pippo-pippo3pippopippo" -> ArchiveRef("Rpippo_pippo-pippo3pippopippo"),
    "nmd://Rpippo_pippo-pippo3pippopippo/data/pippo.out" -> RawDataRef("Rpippo_pippo-pippo3pippopippo", Seq("data", "pippo.out")),
    "nmd://Npippo_pippo-pippo3pippopippo/Cpippo3pippo_pippo-pippopippo" -> CalculationRef("Rpippo_pippo-pippo3pippopippo", "Cpippo3pippo_pippo-pippopippo"),
    "nmd://Spippo_pippo-pippo3pippopippo/Cpippo3pippo_pippo-pippopippo" -> CalculationRef("Rpippo_pippo-pippo3pippopippo", "Cpippo3pippo_pippo-pippopippo"),
    "nmd://Npippo_pippo-pippo3pippopippo/Cpippo3pippo_pippo-pippopippo/section_run/section_single_configuration_calculation/energy_total" -> TableRef("Rpippo_pippo-pippo3pippopippo", "Cpippo3pippo_pippo-pippopippo", Seq("section_run", "section_single_configuration_calculation", "energy_total")),
    "nmd://Spippo_pippo-pippo3pippopippo/Cpippo3pippo_pippo-pippopippo/section_run/section_single_configuration_calculation/energy_total" -> TableRef("Rpippo_pippo-pippo3pippopippo", "Cpippo3pippo_pippo-pippopippo", Seq("section_run", "section_single_configuration_calculation", "energy_total")),
    "nmd://Npippo_pippo-pippo3pippopippo/Cpippo3pippo_pippo-pippopippo/section_run/section_single_configuration_calculation/energy_total/0c" -> RowRef("Rpippo_pippo-pippo3pippopippo", "Cpippo3pippo_pippo-pippopippo", Seq(("section_run" -> SortedMap()), ("section_single_configuration_calculation" -> SortedMap()), ("energy_total" -> SortedMap("c" -> (0: Long))))),
    "nmd://Spippo_pippo-pippo3pippopippo/Cpippo3pippo_pippo-pippopippo/section_run/section_single_configuration_calculation/energy_total/0c" -> RowRef("Rpippo_pippo-pippo3pippopippo", "Cpippo3pippo_pippo-pippopippo", Seq(("section_run" -> SortedMap()), ("section_single_configuration_calculation" -> SortedMap()), ("energy_total" -> SortedMap("c" -> (0: Long))))),
    "nmd://Npippo_pippo-pippo3pippopippo/Cpippo3pippo_pippo-pippopippo/section_run/0l/section_single_configuration_calculation/0l/energy_total/0c/0l" -> RowRef("Rpippo_pippo-pippo3pippopippo", "Cpippo3pippo_pippo-pippopippo", Seq(("section_run" -> SortedMap("l" -> (0: Long))), ("section_single_configuration_calculation" -> SortedMap("l" -> (0: Long))), ("energy_total" -> SortedMap("l" -> (0: Long), "c" -> 0)))),
    "nmd://Spippo_pippo-pippo3pippopippo/Cpippo3pippo_pippo-pippopippo/section_run/0l/section_single_configuration_calculation/0l/energy_total/0c/0l" -> RowRef("Rpippo_pippo-pippo3pippopippo", "Cpippo3pippo_pippo-pippopippo", Seq(("section_run" -> SortedMap("l" -> (0: Long))), ("section_single_configuration_calculation" -> SortedMap("l" -> (0: Long))), ("energy_total" -> SortedMap("l" -> (0: Long), "c" -> 0)))),
    "nmd://Npippo_pippo-pippo3pippopippo/Cpippo3pippo_pippo-pippopippo/section_run/0l/section_single_configuration_calculation/0l/energy_total/0l" -> RowRef("Rpippo_pippo-pippo3pippopippo", "Cpippo3pippo_pippo-pippopippo", Seq(("section_run" -> SortedMap("l" -> (0: Long))), ("section_single_configuration_calculation" -> SortedMap("l" -> (0: Long))), ("energy_total" -> SortedMap("l" -> (0: Long))))),
    "nmd://Spippo_pippo-pippo3pippopippo/Cpippo3pippo_pippo-pippopippo/section_run/0l/section_single_configuration_calculation/0l/energy_total/0l" -> RowRef("Rpippo_pippo-pippo3pippopippo", "Cpippo3pippo_pippo-pippopippo", Seq(("section_run" -> SortedMap("l" -> (0: Long))), ("section_single_configuration_calculation" -> SortedMap("l" -> (0: Long))), ("energy_total" -> SortedMap("l" -> (0: Long)))))
  )

  "nomadUriToRef" >> {
    examplesBlock {
      for (((uri, nRef), i) <- uriSamples.zipWithIndex) {
        s"uri $i (${nRef.getClass().getSimpleName()})" in {
          val pUri = NomadUri(uri)
          pUri.toUri must_== uri
          val rRef = pUri.toRef
          rRef must_== nRef
          rRef.toUriStr(pUri.objectKind) must_== uri
        }
      }
    }
  }
}

