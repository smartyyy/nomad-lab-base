/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab

import org.specs2.mutable.Specification
import java.nio.charset.StandardCharsets
import AtomData._
import util.control.NonFatal

/**
 * Specification (fixed tests) for AtomData
 */
class SemVerSpec extends Specification {
  val semVersStr: Seq[String] = Seq(
    "0.9.2", "1.0.0-alpha+001", "1.0.0-alpha", "1.0.0-alpha.1",
    "1.0.0-alpha.beta", "1.0.0-beta+exp.sha.5114f85", "1.0.0-beta",
    "1.0.0-beta.2", "1.0.0-beta.11", "1.0.0-rc.1", "1.0.0+20130313144700",
    "1.0.0", "1.9.0", "1.10.0", "1.11.1", "2.0"
  )
  "Sematic versioning" >> {
    val semVers: Seq[SemVer] = semVersStr.map { x: String => SemVer(x) }
    "recover" >> {
      for (v1 <- semVers) {
        val s1 = v1.toString
        val v2 = SemVer(s1)
        val s2 = v2.toString
        v1 must_== v2
        s1 must_== s2
      }
      semVers(0) must_== SemVer(0, 9, 2)
    }

    "ordering" >> {
      for (
        (v1, i1) <- semVers.zipWithIndex;
        (v2, i2) <- semVers.zipWithIndex
      ) {
        v1.compare(v2).signum must_== i1.compare(i2).signum
      }
      SemVer(1) must be_<(SemVer(2))
    }
  }
}
