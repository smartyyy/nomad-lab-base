/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab

import eu.nomad_lab.parsers.ParseResult.ParseResult
import eu.nomad_lab.normalize.NormalizerResult.NormalizerResult
import org.json4s.JsonAST.{ JArray, JString, JField, JObject }
import org.json4s._

/**
 * Enum of possible tree/archive type
 */
object TreeType extends Enumeration {
  type TreeType = Value
  val File, Directory, Zip, Tar, Unknown = Value
}

class InvalidTreeTypeException(
  msg: String, what: Throwable = null
) extends Exception(msg, what)

/**
 * Json serialization and deserialization support for MetaInfoRecord
 */
class TreeTypeSerializer extends CustomSerializer[TreeType.Value](format => (
  {
    case JString(str) =>
      TreeType.withName(str)
    case JNothing =>
      TreeType.Unknown
    case v =>
      throw new InvalidTreeTypeException(s"invalid TreeType ${JsonUtils.normalizedStr(v)}")
  },
  {
    case x: TreeType.Value => JString(x.toString)
  }
))

object QueueMessage {

  /**
   * Request sent to the tree parser to find the appropriate parser for the given tree/archive
   */
  case class TreeParserRequest(
    treeUri: String, // URI inside the given treeFilePath; eg. nmd://R9h5Wp_FGZdsBiSo5Id6pnie_xeIH/data
    treeFilePath: String, //Path to the archive or the root directory eg. /nomad/nomadlab/raw_data/data/R9h/R9h5Wp_FGZdsBiSo5Id6pnie_xeIH.zip
    treeType: TreeType.Value = TreeType.Unknown, // type of tree we are dealing with
    relativeTreeFilePath: Option[String] = None, // path within the archive (optional) eg. data
    maxDepth: Int = -1, // In case of directory, the maxDepth to trace
    followSymlinks: Boolean = true // In case of directory
  )

  object TreeParserRequest {
    def fromPath(path: java.nio.file.Path): TreeParserRequest = {
      val filename = path.getFileName.toString
      val archiveRe = "^(R[-_a-zA-Z0-9]{28})\\.zip$".r
      val (uri, pathInArchive) = filename match {
        case archiveRe(gid) =>
          (s"nmd://$gid/data", Some(s"$gid/data"))
        case _ =>
          ("file://" + path.toAbsolutePath().toString(), None)
      }
      TreeParserRequest(
        treeUri = uri,
        treeFilePath = path.toAbsolutePath.toString,
        treeType = if (filename.endsWith(".zip")) TreeType.Zip else TreeType.Unknown,
        relativeTreeFilePath = pathInArchive
      )
    }
  }

  case class NormalizerRequest(
    treeUri: String, // URI inside the given treeFilePath; eg. nmd://R9h5Wp_FGZdsBiSo5Id6pnie_xeIH/data
    treeFilePath: String, //Path to the archive or the root directory eg. /nomad/nomadlab/raw_data/data/R9h/R9h5Wp_FGZdsBiSo5Id6pnie_xeIH.zip
    treeType: TreeType.Value = TreeType.Unknown, // type of tree we are dealing with
    relativeTreeFilePath: Option[String] = None, // path within the archive (optional) eg. data
    maxDepth: Int = -1, // In case of directory, the maxDepth to trace
    followSymlinks: Boolean = true // In case of directory
  )

  object NormalizerRequest {
    def fromPath(path: java.nio.file.Path): NormalizerRequest = {
      val filename = path.getFileName.toString
      val archiveRe = "^(nmd://S[-_a-zA-Z0-9]{28})\\$".r
      val (uri, pathInArchive) = filename match {
        case archiveRe(gid) =>
          ("$gid", None)
        case _ =>
          ("nmd://" + path.toAbsolutePath().toString(), None)
      }
      NormalizerRequest(
        treeUri = uri, //"nmd://NXIuEEpQCu2mtTCUQJ09wvc0dmmNR", //uri,
        treeFilePath = path.toAbsolutePath.toString,
        treeType = if (filename.endsWith(".zip")) TreeType.Zip else TreeType.Unknown,
        relativeTreeFilePath = pathInArchive
      )
    }
  }

  /**
   * Request send to the calculation parser to uncompress the tree/archive and initialize the parser
   */
  case class CalculationParserRequest(
    parserName: String, //Name of the parser to use for the file; CastepParser
    mainFileUri: String, //Uri of the main file; Example:  nmd://R9h5Wp_FGZdsBiSo5Id6pnie_xeIH/data/examples/foo/Si2.castep
    relativeFilePath: String, // file path, from the tree root. Example data/examples/foo/Si2.castep
    treeUri: String, // nomad uri of the tree to parse: nmd://R9h5Wp_FGZdsBiSo5Id6pnie_xeIH
    treeFilePath: String, // Same as the treeFilePath in TreeParserQueueMessage; eg. /nomad/nomadlab/raw_data/data/R9h/R9h5Wp_FGZdsBiSo5Id6pnie_xeIH.zip
    treeType: TreeType.Value = TreeType.Unknown, // type of root tree we are dealing with
    overwrite: Boolean = false // Overwrite an existing file; eg. In case of failure of previous run
  )

  /**
   * Result of the calculation parser.
   */
  case class CalculationParserResult(
    rootNamespace: String,
    parseResult: ParseResult,
    parserId: String,
    parserInfo: JValue, // info on the parser used i.e. {"name":"CastepParser","version":"1.0"}
    calculationGid: String, // calculation gid: sha of mainFileUri, prepended with C
    parsedFileUri: Option[String], // This is build as sha of mainFileUri, prepended with P, i.e. nmd://PutioKaDl4tgPd4FnrdxPscSGKAgK
    parsedFilePath: Option[String], // Complete file path, to the parsed file /nomad/nomadlab/work/parsed/<parserId>/Put/PutioKaDl4tgPd4FnrdxPscSGKAgK.nc
    didExist: Boolean, // Did the parsed file did exist
    created: Boolean, // Was a new parsed file created
    errorMessage: Option[String] = None,
    parsingTimeSec: Double, // time needed for parsing
    parseRequest: CalculationParserRequest, //Uri of the main file; Example:  nmd://R9h5Wp_FGZdsBiSo5Id6pnie_xeIH/data/examples/foo/Si2.castep
    backendInfo: JValue // Info on the backend used (i.e. how/where the result of parsing are saved)
  )

  /**
   * Result of the calculation parser to be normalized.
   */
  case class ToBeNormalizedQueueMessage(
    mergedH5Path: String, //path to the merged h5 file, i.e., /parsed/prod-008/Rui/Ruixxxxx.h5
    archiveUri: String // URI to the archive nmd://R9h5Wp_FGZdsBiSo5Id6pnie_xeIH
  )

  /**
   * The final normalized result.
   */
  case class NormalizedResult(
    didExist: Boolean,
    created: Boolean,
    errorMessage: Option[String] = None,
    normalizedFileUri: Option[String] = None, //String,
    normalizedFilePath: Option[String] = None, //String,
    nResult: NormalizerResult

  )
}
