/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.ref
import scala.collection.SortedMap
import scala.collection.breakOut

/**
 * the (logical) depth of the object being referred
 */
object UriDepth extends Enumeration {
  type UriDepth = Value
  val Archive, Path, Calculation, Table, Row = Value
}

/**
 * The kind of root object (resource) being referred to
 */
object ObjectKind extends Enumeration {
  type ObjectKind = Value
  val RawData, ParsedData, NormalizedData = Value
}

/**
 * Common superclass to a reference to a nomad object
 */
sealed abstract class NomadRef {
  def uriDepth: UriDepth.Value

  def toUriStr(objectKind: ObjectKind.Value): String
}

/**
 * A reference to a file or directory in a raw data archive
 */
case class RawDataRef(
    archiveGid: String,
    path: Seq[String]
) extends NomadRef {
  def uriDepth: UriDepth.Value = {
    if (path.isEmpty)
      UriDepth.Archive
    else
      UriDepth.Path
  }

  def toUriStr(objectKind: ObjectKind.Value): String = {
    s"nmd://$archiveGid/${path.mkString("/")}"
  }
}

/**
 * A reference to an archive, either in raw data, parsed or normalized data
 */
case class ArchiveRef(
    archiveGid: String
) extends NomadRef {
  def uriDepth: UriDepth.Value = { UriDepth.Archive }

  def toUriStr(objectKind: ObjectKind.Value): String = {
    objectKind match {
      case ObjectKind.RawData => s"nmd://$archiveGid"
      case ObjectKind.ParsedData => s"nmd://S${archiveGid.substring(1)}"
      case ObjectKind.NormalizedData => s"nmd://N${archiveGid.substring(1)}"
    }
  }
}

/**
 * reference to a calculation either in normalized of parsed data
 */
case class CalculationRef(
    archiveGid: String,
    calculationGid: String
) extends NomadRef {
  def uriDepth: UriDepth.Value = { UriDepth.Calculation }

  def archiveRef: ArchiveRef = {
    ArchiveRef(archiveGid)
  }

  def toUriStr(objectKind: ObjectKind.Value): String = {
    objectKind match {
      case ObjectKind.RawData => throw new NomadUriException(s"calculation has no direct connection to raw data, cannot convert CalculationRef to raw data uri, use mainFileUri of the calculation if you need the raw data.")
      case ObjectKind.ParsedData => s"nmd://S${archiveGid.substring(1)}/$calculationGid"
      case ObjectKind.NormalizedData => s"nmd://N${archiveGid.substring(1)}/$calculationGid"
    }
  }
}

/**
 * reference to a table (i.e. a group of section or values using the same metadata name)
 */
case class TableRef(
    archiveGid: String,
    calculationGid: String,
    path: Seq[String]
) extends NomadRef {
  def uriDepth: UriDepth.Value = { UriDepth.Table }

  def archiveRef: ArchiveRef = {
    ArchiveRef(archiveGid)
  }

  def calculationRef: CalculationRef = {
    CalculationRef(archiveGid, calculationGid)
  }

  def metaPath: Seq[String] = path

  def toUriStr(objectKind: ObjectKind.Value): String = {
    objectKind match {
      case ObjectKind.RawData => throw new NomadUriException(s"Table has no direct connection to raw data, cannot convert TableRef to raw data uri, use mainFileUri of the calculation if you need the raw data.")
      case ObjectKind.ParsedData => s"nmd://S${archiveGid.substring(1)}/$calculationGid/${path.mkString("/")}"
      case ObjectKind.NormalizedData => s"nmd://N${archiveGid.substring(1)}/$calculationGid/${path.mkString("/")}"
    }
  }
}

/**
 * reference to a row (i.e a specific section or value entry)
 */
case class RowRef(
    archiveGid: String,
    calculationGid: String,
    indexedPath: Seq[(String, SortedMap[String, Long])],
    cIndex: Option[Long] = None
) extends NomadRef {
  def uriDepth: UriDepth.Value = { UriDepth.Row }

  def archiveRef: ArchiveRef = {
    ArchiveRef(archiveGid)
  }

  def calculationRef: CalculationRef = {
    CalculationRef(archiveGid, calculationGid)
  }

  def tableRef: TableRef = {
    TableRef(archiveGid, calculationGid, indexedPath.map {
      case (meta, _) => meta
    })
  }

  /**
   * path meta + indexes as sequence of strings
   */
  def path: Seq[String] = {
    indexedPath.flatMap {
      case (meta, indexes) =>
        val strIdxs: Seq[String] = indexes.map {
          case (k, idx) =>
            idx.toString + k
        }(breakOut)
        meta +: strIdxs
    }
  }

  /**
   * paths of meta infos
   */
  def metaPath: Seq[String] = {
    indexedPath.map(_._1)
  }

  /**
   *  returns the uri string corresponding to this reference
   */
  def toUriStr(objectKind: ObjectKind.Value): String = {
    val baseUri = calculationRef.toUriStr(objectKind)
    path.mkString("/")
    baseUri + "/" + path.mkString("/")
  }
}
