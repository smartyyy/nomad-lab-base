/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.resolve

import org.{ json4s => jn }
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.Files
import eu.nomad_lab.JsonUtils
import eu.nomad_lab.h5._
import eu.nomad_lab.ref._
import scala.collection.breakOut

/**
 * A resolved reference
 */
sealed abstract class ResolvedRef {
  def uriString: String

  def valid: Boolean = { true }

  def minJson: jn.JValue

  def giveBack(): Unit

  def objectKind: ObjectKind.Value

  override def toString: String = {
    JsonUtils.prettyStr(minJson)
  }
}

/**
 * Failed resolution of an uri
 */
case class FailedResolution(
    uriString: String,
    errorMessage: String
) extends ResolvedRef {
  override val valid: Boolean = false

  override def minJson: jn.JValue = {
    jn.JObject(
      ("uri" -> jn.JString(uriString)) ::
        ("error" -> jn.JString(errorMessage)) :: Nil
    )
  }

  override def objectKind: ObjectKind.Value = ObjectKind.NormalizedData

  override def giveBack(): Unit = {}
}

case class RawData(
    archivePath: Path,
    archiveGid: String,
    pathInArchive: Path
) extends ResolvedRef {

  override def uriString: String = "nmd://" + Paths.get(archiveGid, pathInArchive.toString).toString

  override def minJson: jn.JValue = {
    jn.JObject(
      ("uri" -> jn.JString(uriString)) ::
        ("type" -> jn.JString("raw_data")) ::
        ("inArchivePath" -> jn.JString(pathInArchive.toString)) ::
        ("downloadUrl" -> jn.JString(s"http://data.nomad-coe.eu/raw-data/data/${archiveGid.take(3)}/${archiveGid}.zip")) ::
        ("downloadSize" -> jn.JInt(Files.size(archivePath))) :: Nil
    )
  }

  override def objectKind: ObjectKind.Value = ObjectKind.RawData

  override def giveBack(): Unit = {}
}

trait ParsedRef {
  def archiveSet: ArchiveSetH5

  def objectKind: ObjectKind.Value = {
    if (archiveSet == ArchiveSetH5.normalizedSet)
      ObjectKind.NormalizedData
    else if (archiveSet == ArchiveSetH5.parsedSet)
      ObjectKind.ParsedData
    else
      ObjectKind.NormalizedData
  }
}

case class Archive(
    archiveSet: ArchiveSetH5,
    archive: ArchiveH5
) extends ResolvedRef with ParsedRef {
  override def uriString = {
    archive.toRef.toUriStr(objectKind)
  }

  def downloadUrl: String = {
    val path = archive.fileH5.path
    var downloadUrlList: List[String] = Nil
    for (i <- (path.getNameCount() - 1).to((path.getNameCount() - 4) max 0, -1))
      downloadUrlList = path.getName(i).toString() :: downloadUrlList
    downloadUrlList = "http://data.nomad-coe.eu" :: downloadUrlList
    downloadUrlList.mkString("/")
  }

  override def minJson: jn.JValue = {
    val calcs: List[jn.JString] = (for (c <- archive.calculations) yield {
      jn.JString(c.calculationGid)
    })(breakOut)

    jn.JObject(
      ("uri" -> jn.JString(uriString)) ::
        ("type" -> jn.JString("archive")) ::
        ("downloadUrl" -> jn.JString(downloadUrl)) ::
        ("calculationGids" -> jn.JArray(calcs)) :: Nil
    )
  }

  override def giveBack(): Unit = {
    archiveSet.giveBackArchive(archive)
  }
}

case class Calculation(
    archiveSet: ArchiveSetH5,
    calculation: CalculationH5
) extends ResolvedRef with ParsedRef {
  override def uriString = {
    calculation.toRef.toUriStr(objectKind)
  }

  override def minJson: jn.JValue = {
    val sectionTables: List[jn.JField] = (for (s <- calculation.subSectionTables(true)) yield {
      s.name -> jn.JObject(
        ("size" -> jn.JInt(s.length)) :: Nil
      )
    })(breakOut)
    jn.JObject(
      ("uri" -> jn.JString(uriString)) ::
        ("type" -> jn.JString("calculation")) ::
        ("subSectionTables" -> jn.JObject(sectionTables)) :: Nil
    )
  }

  override def giveBack(): Unit = {
    archiveSet.giveBackCalculation(calculation)
  }
}

case class SectionTable(
    archiveSet: ArchiveSetH5,
    sectionTable: SectionTableH5
) extends ResolvedRef with ParsedRef {
  override def uriString = {
    sectionTable.toRef.toUriStr(objectKind)
  }

  override def minJson: jn.JValue = {
    val sectionTables: List[jn.JField] = (for (s <- sectionTable.subSectionTables(true)) yield {
      s.name -> jn.JObject(
        ("size" -> jn.JInt(s.length)) :: Nil
      )
    })(breakOut)
    val valueTables: List[jn.JField] = (for (s <- sectionTable.subValueTables(true)) yield {
      s.name -> jn.JObject(
        ("dtypeStr" -> jn.JString(s.metaInfo.dtypeStr.get)) ::
          ("shape" -> {
            val listShape: List[jn.JValue] = s.metaInfo.shape match {
              case Some(seq) =>
                (seq.map {
                  case Left(i) => jn.JInt(i)
                  case Right(s) => jn.JString(s)
                })(breakOut)
              case None => Nil
            }
            jn.JArray(listShape)
          }) ::
          ("size" -> jn.JInt(s.length)) :: Nil
      )
    })(breakOut)
    jn.JObject(
      ("uri" -> jn.JString(uriString)) ::
        ("type" -> jn.JString("section_table")) ::
        ("size" -> jn.JInt(sectionTable.lengthL)) ::
        ("subSectionTables" -> jn.JObject(sectionTables)) ::
        ("subValueTables" -> jn.JObject(valueTables)) :: Nil
    )
  }

  override def giveBack(): Unit = {
    archiveSet.giveBackSectionTable(sectionTable)
  }
}

case class Section(
    archiveSet: ArchiveSetH5,
    section: SectionH5
) extends ResolvedRef with ParsedRef {

  override def uriString = {
    section.toRef.toUriStr(objectKind)
  }

  override def minJson: jn.JValue = {
    val sectionCollections: List[jn.JField] = (for (s <- section.subSectionCollections(true)) yield {
      val sTable = s.table
      sTable.name -> jn.JObject(
        ("upperCIndex" -> jn.JInt(s.upperGIndex)) ::
          ("lowerCIndexMin" -> jn.JInt(s.lowerGIndex)) ::
          ("size" -> jn.JInt(s.lengthL)) :: Nil
      )
    })(breakOut)
    val valueCollections: List[jn.JField] = (for (s <- section.valueCollections(true)) yield {
      s.table.name -> jn.JObject(
        ("dtypeStr" -> jn.JString(s.table.metaInfo.dtypeStr.get)) ::
          ("shape" -> {
            val listShape: List[jn.JValue] = s.table.metaInfo.shape match {
              case None => Nil
              case Some(s) =>
                s.map {
                  case Left(i) => jn.JInt(i)
                  case Right(s) => jn.JString(s)
                }(breakOut)
            }
            jn.JArray(listShape)
          }) ::
          ("size" -> jn.JInt(s.length)) :: Nil
      )
    })(breakOut)
    jn.JObject(
      ("uri" -> jn.JString(uriString)) ::
        ("type" -> jn.JString("section")) ::
        ("subSectionCollections" -> jn.JObject(sectionCollections)) ::
        ("valueCollections" -> jn.JObject(valueCollections)) :: Nil
    )
  }

  override def giveBack(): Unit = {
    archiveSet.giveBackSection(section)
  }
}

case class ValueTable(
    archiveSet: ArchiveSetH5,
    valueTable: ValueTableH5
) extends ResolvedRef with ParsedRef {
  override def uriString = {
    valueTable.toRef.toUriStr(objectKind)
  }

  override def minJson: jn.JValue = {
    jn.JObject(
      ("uri" -> jn.JString(uriString)) ::
        ("type" -> jn.JString("value_table")) ::
        ("dtypeStr" -> jn.JString(valueTable.metaInfo.dtypeStr.get)) ::
        ("shape" -> {
          valueTable.metaInfo.shape match {
            case None => jn.JNothing
            case Some(s) =>
              val listShape: List[jn.JValue] = valueTable.metaInfo.shape match {
                case Some(sh) =>
                  sh.map {
                    case Left(i) => jn.JInt(i)
                    case Right(s) => jn.JString(s)
                  }(breakOut)
                case None => Nil
              }
              jn.JArray(listShape)
          }
        }) ::
        ("size" -> jn.JInt(valueTable.length)) :: Nil
    )
  }

  override def giveBack(): Unit = {
    archiveSet.giveBackValueTable(valueTable)
  }
}

case class Value(
    archiveSet: ArchiveSetH5,
    value: ValueH5
) extends ResolvedRef with ParsedRef {

  override def uriString = {
    value.toRef.toUriStr(objectKind)
  }

  override def minJson: jn.JValue = {
    jn.JObject(
      ("uri" -> jn.JString(uriString)) ::
        ("type" -> jn.JString("value")) ::
        ("dtypeStr" -> jn.JString(value.table.metaInfo.dtypeStr.get)) ::
        ("value" -> value.jValue) :: Nil
    )
  }

  override def giveBack(): Unit = {
    archiveSet.giveBackValue(value)
  }
}
