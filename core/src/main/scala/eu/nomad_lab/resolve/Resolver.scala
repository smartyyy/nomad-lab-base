/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.resolve;
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.Files
import eu.nomad_lab.h5._
import eu.nomad_lab.ref._
import eu.nomad_lab.LocalEnv
import scala.util.control.NonFatal

object Resolver {
  class ResolveError(
    uriStr: String,
    msg: String
  ) extends Exception(s"$msg ($uriStr)")

  /**
   * resolves the reference in the given archive
   * might throw
   */
  def resolveInArchiveSet(archiveSet: ArchiveSetH5, nomadRef: NomadRef): ResolvedRef = {
    nomadRef match {
      case ArchiveRef(archiveGid) =>
        val archive = archiveSet.acquireArchive(archiveGid)
        Archive(archiveSet, archive)
      case CalculationRef(archiveGid, calculationGid) =>
        val calculation = archiveSet.acquireCalculation(archiveGid, calculationGid)
        Calculation(archiveSet, calculation)
      case tRef: TableRef =>
        archiveSet.acquireTableRef(tRef) match {
          case Left(sectionTable) =>
            SectionTable(archiveSet, sectionTable)
          case Right(valueTable) =>
            ValueTable(archiveSet, valueTable)
        }
      case rowRef: RowRef =>
        archiveSet.acquireRowRef(rowRef) match {
          case Left(s) =>
            Section(archiveSet, s)
          case Right(v) =>
            Value(archiveSet, v)
        }
      case RawDataRef(archiveGid, path) =>
        throw new ResolveError(nomadRef.toUriStr(ObjectKind.NormalizedData), "internal error, unexpected ref")
    }
  }

  /**
   * Returns a small json description for the given uri
   *
   * Does not throw, returns a FailedResolution object instead
   */
  def resolve(uriStr: String): ResolvedRef = {
    try {
      val uri = NomadUri(uriStr)
      uri.objectKind match {
        case ObjectKind.RawData =>
          val rawDataBasePath: Path = LocalEnv.defaultSettings.rawDataRoot.resolve("data")
          uri.toRef match {
            case ArchiveRef(archiveGid) =>
              val archivePath = rawDataBasePath.resolve(archiveGid.take(3)).resolve(archiveGid + ".zip")
              if (!Files.exists(archivePath)) {
                FailedResolution(
                  uriStr, s"Reference to non existing/missing Raw Data archive $archiveGid"
                )
              } else {
                RawData(archivePath, archiveGid, pathInArchive = Paths.get(""))
              }
            case RawDataRef(archiveGid, path) =>
              val archivePath = rawDataBasePath.resolve(archiveGid.take(3)).resolve(archiveGid + ".zip")
              if (!Files.exists(archivePath)) {
                FailedResolution(
                  uriStr, s"Reference to non existing/missing Raw Data archive $archiveGid"
                )
              } else {
                RawData(archivePath, archiveGid, pathInArchive = Paths.get(""))
              }
            case _ =>
              FailedResolution(
                uriStr, s"Invalid raw data reference type"
              )
          }
        case ObjectKind.NormalizedData | ObjectKind.ParsedData =>
          val archiveSet = uri.objectKind match {
            case ObjectKind.ParsedData =>
              ArchiveSetH5.parsedSet
            case ObjectKind.NormalizedData =>
              ArchiveSetH5.normalizedSet
            case ObjectKind.RawData =>
              throw new ResolveError(uriStr, "internal error")
          }
          resolveInArchiveSet(archiveSet, uri.toRef)
      }
    } catch {
      case NonFatal(e) =>
        val sw = new java.io.StringWriter
        e.printStackTrace(new java.io.PrintWriter(sw))
        FailedResolution(uriStr, s"resolution failed due to $sw")
    }
  }
}
