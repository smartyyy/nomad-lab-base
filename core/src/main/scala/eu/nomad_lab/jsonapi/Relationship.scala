/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.jsonapi
import org.{ json4s => jn }

/**
 * Represents a relationship in jsonapi
 */
sealed abstract class Relationship[+T] {
  def links: Map[String, Link]

  def meta: Map[String, jn.JValue]

  def values: Seq[T]

  def isScalar: Boolean

  def map[U](f: T => U): Relationship[U]
}

case class ToOne[+T](
    value: Option[T],
    override val links: Map[String, Link] = Map(),
    override val meta: Map[String, jn.JValue] = Map()
) extends Relationship[T] {
  override val values = value.toSeq

  override val isScalar = true

  override def map[U](f: T => U): Relationship[U] = {
    ToOne(value.map(f), links, meta)
  }
}

case class ToMany[+T](
    override val values: Seq[T],
    override val links: Map[String, Link] = Map(),
    override val meta: Map[String, jn.JValue] = Map()
) extends Relationship[T] {
  override val isScalar = false

  override def map[U](f: T => U): Relationship[U] = {
    ToMany(values.map(f), links, meta)
  }
}
