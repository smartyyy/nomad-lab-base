/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.jsonapi
import org.{ json4s => jn }
import scala.collection.breakOut
import java.io.Writer
import eu.nomad_lab.JsonUtils
import scala.collection.mutable
import JsonApi.mStr
import JsonApi.mDict

/**
 * an error for the json api
 *
 * title: human readbale stricng common for this type of error
 * detail: human radable string regarding this specific error
 * id: a unique identifier for this particular occurrence of the problem
 * status: the HTTP status code applicable to this problem, expressed as a string value
 * code: an application-specific error code, expressed as a string value
 */
case class Error(
    title: String,
    detail: String,
    idStr: String = "",
    status: String = "",
    code: String = "",
    source: Option[ErrorSource] = None,
    meta: Map[String, jn.JValue] = Map(),
    links: Map[String, Link] = Map()
) {
  def toJValue: jn.JValue = {
    val lnks = if (links.isEmpty)
      jn.JNothing
    else
      jn.JObject(
        links.map {
          case (k, l) =>
            k -> l.toJValue
        }(breakOut): List[(String, jn.JValue)]
      )
    jn.JObject(
      ("id" -> mStr(idStr)) ::
        ("links" -> lnks) ::
        ("status" -> mStr(status)) ::
        ("title" -> mStr(title)) ::
        ("detail" -> mStr(detail)) ::
        ("code" -> mStr(code)) ::
        ("source" -> (source match {
          case Some(s) => s.toJValue
          case None => jn.JNothing
        })) ::
        ("meta" -> mDict(meta)) :: Nil
    )
  }
}
