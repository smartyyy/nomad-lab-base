/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.meta;
import java.nio.file.Path
import java.nio.file.Paths

object MetaInfo {
  private var _knownDictionaries: Map[String, MetaDictionary] = Map()

  def loadDictFromFilePath(filePath: Path): MetaDictionary = {
    import eu.nomad_lab.JsonSupport.formats
    val absPath = filePath.toAbsolutePath
    val rPath = filePath.toRealPath()
    synchronized {
      _knownDictionaries.get(absPath.toString) match {
        case Some(d) => return d
        case None =>
          _knownDictionaries.get(rPath.toString) match {
            case Some(d) => return d
            case None =>
          }
      }
    }
    val d = MetaDictionary.fromFilePath(absPath)
    synchronized {
      _knownDictionaries += (absPath.toString -> d)
      _knownDictionaries += (rPath.toString -> d)
      d
    }

  }

  def loadFromResourcePath(resourcePath: String): MetaDictionary = {
    import eu.nomad_lab.JsonSupport.formats
    val classLoader: ClassLoader = getClass().getClassLoader();
    val filePath = Paths.get(classLoader.getResource(resourcePath).getFile())
    synchronized {
      _knownDictionaries.get(filePath.toString) match {
        case Some(d) => return d
        case None =>
      }
    }
    //      logger.info("Path is :"+filePath);
    val resolver = new ResourceDependencyResolver(classLoader)
    val d = MetaDictionary.fromInputStream(classLoader.getResourceAsStream(resourcePath), s"resources://$resourcePath")
    synchronized {
      _knownDictionaries += (filePath.toString -> d)
      d
    }
  }

}

class MetaInfo(dictionaries: Map[String, MetaDictionary]) {
  /*
  /**
   * Calculates the Gid of name, resolving all dependencies and calculating their gid
   * if required.
   *
   * nameToGidsCache will be updated with all the gids calculated.
   */
  def calculateGid(
    name: String,
    nameToGidCache: mutable.Map[String, String],
    metaInfos: Map[String, MetaInfoEntry],
    dependencies: Seq[MetaDictionary],
    context: String,
    precalculated: Boolean = false
  ): String = {

    def firstMetaFromDeps(n: String): Option[MetaInfoRecord] = {
      for (d <- dependencies) {
        val value = d.meta_info_entry.get(n)
        if (!value.isEmpty)
          return value
      }
      None
    }

    nameToGidCache.get(name) match {
      case Some(v) =>
        v
      case None =>
        if (precalculated) {
          throw new GidNotPrecalculatedError(name, context)
        } else {
          val inProgress = mutable.ListBuffer[String]()
          var hasPending: Boolean = false
          val toDo = mutable.ListBuffer[String](name)

          for (i <- 1 to 2) {
            while (!toDo.isEmpty) {
              var now: String = ""
              if (!hasPending && !inProgress.isEmpty) {
                now = inProgress.last
                inProgress.trimEnd(1)
              } else {
                now = toDo.last
                toDo.trimEnd(1)
              }
              hasPending = false
              val nowMeta = metaInfos.get(now) match {
                case Some(meta) => meta
                case None => {
                  firstMetaFromDeps(now) match {
                    case Some(meta) => meta
                    case None => throw new MetaInfoEnv.DependsOnUnknownNameException(
                      context, name, now
                    )
                  }
                }
              }
              for (superName <- nowMeta.superNames) {
                if (!nameToGidCache.contains(superName)) {
                  hasPending = true
                  if (toDo.contains(superName))
                    toDo -= superName
                  if (inProgress.contains(superName))
                    throw new MetaInfoEnv.MetaInfoCircularDepException(
                      context, name, superName, inProgress
                    )
                  toDo += superName
                }
              }
              if (!hasPending) {
                val gidNow = evalGid(nowMeta, nameToGidCache)
                nameToGidCache += (now -> gidNow)
                if (inProgress.contains(now))
                  inProgress -= now
              } else {
                if (inProgress.contains(now))
                  throw new MetaInfoEnv.MetaInfoCircularDepException(
                    context, name, now, inProgress
                  )
                inProgress += now
              }
            }
            toDo ++= inProgress
            inProgress.clear()
          }
          nameToGidCache(name)
        }
    }
  }
 */

  def metaInfoEntry(metaName: String): Option[MetaInfoEntry] = {
    for ((n, d) <- dictionaries) {
      d.meta_info_entry.get(metaName) match {
        case Some(e) => return Some(e)
        case None =>
      }
    }
    None
  }
}
