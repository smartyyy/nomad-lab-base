/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.meta
import org.json4s.{ JNothing, JNull, JBool, JDouble, JDecimal, JInt, JString, JArray, JObject, JValue, JField }
import scala.collection.breakOut
import scala.collection.mutable
import eu.nomad_lab.JsonUtils
import com.typesafe.scalalogging.StrictLogging
import scala.collection.SortedMap
import eu.nomad_lab.SemVer
import java.nio.file.StandardOpenOption
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.charset.StandardCharsets
import java.io.Reader
import java.io.InputStreamReader
import scala.util.control.NonFatal

object MetaDictRequire extends StrictLogging {

  def fromJValue(jValue: JValue)(implicit formats: org.json4s.Formats): MetaDictRequire = {
    jValue match {
      case JObject(o) =>
        var metadict_relative_path: Option[String] = None
        var metadict_expected_version: Option[String] = None
        var metadict_dependency_gid: Option[String] = None
        o.foreach {
          case JField("metadict_relative_path", value) =>
            value match {
              case JString(p) => metadict_relative_path = Some(p)
              case JNothing => ()
              case JNull => metadict_relative_path = None
              case _ => throw new JsonUtils.InvalidValueError(
                "metadict_require.metadict_relative_path", "meta_dictionary", JsonUtils.prettyStr(value), s"a string"
              )
            }
          case JField("metadict_expected_version", value) =>
            value match {
              case JString(v) =>
                metadict_expected_version = Some(v)
                SemVer.tryParse(v) match {
                  case Some(_) =>
                  case None =>
                    logger.warn(s"version string '$v' not in semantic versioning in metadict_require ${JsonUtils.normalizedStr(jValue)}") // throw instead?
                }
              case JNothing => ()
              case JNull => metadict_expected_version = None
              case _ =>
                throw new JsonUtils.InvalidValueError(
                  "metadict_require.metadict_expected_version", "meta_dictionary", JsonUtils.prettyStr(value), "A string with the expected version of the dependency"
                )
            }
          case JField("metadict_dependency_gid", value) =>
            value match {
              case JString(p) => metadict_dependency_gid = Some(p)
              case JNothing => ()
              case JNull => metadict_dependency_gid = None
              case _ => throw new JsonUtils.InvalidValueError(
                "metadict_require.metadict_dependency_gid", "meta_dictionary", JsonUtils.prettyStr(value), s"a string"
              )
            }
          case JField(k, v) =>
            throw new JsonUtils.InvalidValueError(
              "metadict_require", "meta_dictionary", JsonUtils.prettyStr(jValue), s"a known field (metadict_relative_path, metadict_relative_path) not $k"
            )
        }
        metadict_relative_path match {
          case Some(p) =>
            MetaDictRequire(
              metadict_relative_path = p,
              metadict_expected_version = metadict_expected_version,
              metadict_dependency_gid = metadict_dependency_gid
            )
          case None =>
            throw new JsonUtils.InvalidValueError(
              "metadict_require.metadict_relative_path", "meta_dictionary", JsonUtils.prettyStr(jValue), s"metadict_relative_path is required, but missing"
            )
        }
      case v =>
        throw new JsonUtils.InvalidValueError(
          "metadict_require", "meta_dictionary", JsonUtils.prettyStr(jValue), "a dictionary (object) with meta_range_kind, meta_range_minimum, meta_range_maximum, meta_range_units"
        )
    }
  }
}

case class MetaDictRequire(
    metadict_relative_path: String,
    metadict_expected_version: Option[String] = None,
    metadict_dependency_gid: Option[String] = None
) {
  def toJValue: JValue = {
    import org.json4s.JsonDSL._
    ("metadict_relative_path" -> metadict_relative_path) ~
      ("metadict_expected_version" -> metadict_expected_version) ~
      ("metadict_dependency_gid" -> metadict_dependency_gid)
  }
}

object MetaDictionary extends StrictLogging {

  def fromJValue(jValue: JValue, context: String, extraArgs: Boolean = true)(implicit formats: org.json4s.Formats): MetaDictionary = {
    val ctx = context + " meta_dictionary"
    jValue match {
      case JObject(o) =>
        var metadict_name: Option[String] = None
        var metadict_description: Option[String] = None
        var metadict_version: Option[String] = None
        var metadict_require: List[MetaDictRequire] = Nil
        var meta_info_entry: SortedMap[String, MetaInfoEntry] = SortedMap()
        var metadict_gid: Option[String] = None
        var otherKeys: List[(String, JValue)] = Nil
        o.foreach {
          case JField("type", value) =>
            value match {
              case JString(t) =>
                if (t != "meta_dictionary 2.0.0") {
                  if (t.startsWith("meta_dictionary ")) {
                    SemVer.tryParse(t.drop("meta_dictionary ".length)) match {
                      case Some(v) =>
                        if (v.major != 2) {
                          throw JsonUtils.InvalidValueError(
                            "type", ctx, JsonUtils.prettyStr(value), "the same major version (meta_dictionary 2.0)"
                          )
                        } else if (v.minor > 0) {
                          logger.warn(s"new minor version ${v.minor} of meta_dictionary,  might have issues reading")
                        }
                      case None =>
                        throw JsonUtils.InvalidValueError(
                          "type", ctx, JsonUtils.prettyStr(value), "A semantic Version number after \"meta_dictionary \" (to be more precise 2.0.0)"
                        )
                    }
                  } else {
                    throw JsonUtils.InvalidValueError(
                      "type", ctx, JsonUtils.prettyStr(value), "\"meta_dictionary 2.0.0\""
                    )
                  }
                }
              case _ =>
                throw JsonUtils.InvalidValueError(
                  "type", ctx, JsonUtils.prettyStr(value), "the string \"meta_dictionary 2.0.0\""
                )
            }
          case JField("metadict_description", value) =>
            JsonUtils.jValueToString(value, "metadict_description", ctx) match {
              case Some(s) => metadict_description = Some(s)
              case None => ()
            }
          case JField("metadict_name", value) =>
            JsonUtils.jValueToString(value, "metadict_name", ctx) match {
              case Some(s) => metadict_name = Some(s)
              case None => ()
            }
          case JField("metadict_version", value) =>
            value match {
              case JString(v) =>
                metadict_version = Some(v)
                if (SemVer.tryParse(v).isEmpty) {
                  throw new JsonUtils.InvalidValueError(
                    "metadict_version", ctx, JsonUtils.prettyStr(value), "A string with the version using [sematic versioning](https://semver.org/)"
                  )
                }
              case JNothing => ()
              case JNull => metadict_version = None
              case _ =>
                throw new JsonUtils.InvalidValueError(
                  "metadict_version", ctx, JsonUtils.prettyStr(value), "A string with the version of the dictionary"
                )
            }
          case JField("metadict_gid", value) =>
            value match {
              case JString(v) =>
                metadict_gid = Some(v)
              case JNothing => ()
              case JNull => metadict_gid = None
              case _ =>
                throw new JsonUtils.InvalidValueError(
                  "metadict_gid", ctx, JsonUtils.prettyStr(value), "A string with the gid of the dictionary (and all its dependencies)"
                )
            }
          case JField("metadict_require", value) =>
            value match {
              case JArray(a) =>
                metadict_require = a.map { v =>
                  MetaDictRequire.fromJValue(v)(formats)
                }
              case JNothing => ()
              case JNull => ()
              case _ =>
                throw new JsonUtils.InvalidValueError(
                  "metadict_require", ctx, JsonUtils.prettyStr(value), s"a list of metadict_require objects"
                )
            }
          case JField("meta_info_entry", value) =>
            value match {
              case JArray(a) =>
                val knownNames = mutable.Set[String]()
                meta_info_entry = a.map { v =>
                  val e = MetaInfoEntry.fromJValue(v, ctx, extraArgs = extraArgs)(formats)
                  if (knownNames.contains(e.meta_name))
                    throw new JsonUtils.InvalidValueError(
                      "meta_info_entry", ctx, JsonUtils.prettyStr(value), s"unique meta_name, ${e.meta_name} is duplicated"
                    )
                  knownNames += e.meta_name
                  e.meta_name -> e
                }(breakOut): SortedMap[String, MetaInfoEntry]
              case JNothing => ()
              case JNull => ()
              case _ =>
                throw new JsonUtils.InvalidValueError(
                  "meta_info_entry", ctx, JsonUtils.prettyStr(value), s"a list of meta_info_entry objects"
                )
            }
          case JField(k, v) =>
            if (extraArgs)
              otherKeys = (k -> v) :: otherKeys
            else
              throw JsonUtils.UnexpectedFieldError(
                context = ctx,
                field = k,
                value = v,
                info = s"In strict mode (extraArgs = false) only known fields (metadict_description, metadict_version, metadict_require, meta_info_entry, metadict_gid) are accepted, not $k."
              )
        }
        otherKeys = otherKeys.reverse
        val desc = metadict_description match {
          case Some(d) => d
          case None => throw new JsonUtils.InvalidValueError(
            "metadict_description", ctx, JsonUtils.prettyStr(jValue), s"metadict_description is required, but missing"
          )
        }
        val vers = metadict_version match {
          case Some(v) => v
          case None => throw new JsonUtils.InvalidValueError(
            "metadict_version", ctx, JsonUtils.prettyStr(jValue), s"metadict_version is required, but missing"
          )
        }
        val name = metadict_name match {
          case Some(n) => n
          case None => throw new JsonUtils.InvalidValueError(
            "metadict_name", ctx, JsonUtils.prettyStr(jValue), s"a string required, but missing"
          )
        }
        MetaDictionary(
          metadict_name = name,
          metadict_description = desc,
          metadict_version = vers,
          metadict_require = metadict_require,
          meta_info_entry = meta_info_entry,
          metadict_gid = metadict_gid,
          otherKeys = otherKeys
        )
      case v =>
        throw new JsonUtils.InvalidValueError(
          "-", ctx, JsonUtils.prettyStr(jValue), "a dictionary (object) with metadict_description, metadict_version, metadict_require, meta_info_entry,..."
        )
    }
  }

  /**
   * initializes a MetaDictionary from a path of the filesystem
   */
  def fromFilePath(
    filePath: String, extraArgs: Boolean
  )(implicit formats: org.json4s.Formats): MetaDictionary =
    fromFilePath(Paths.get(filePath), extraArgs)(formats)

  def fromFilePath(
    filePath: String
  )(implicit formats: org.json4s.Formats): MetaDictionary =
    fromFilePath(Paths.get(filePath), true)(formats)

  def fromFilePath(
    filePath: Path, extraArgs: Boolean = true, checkName: Boolean = true
  )(implicit formats: org.json4s.Formats): MetaDictionary = {
    val r = Files.newBufferedReader(filePath, StandardCharsets.UTF_8)
    val d = fromReader(
      reader = r,
      context = s"file(path:$filePath)",
      extraArgs = extraArgs
    )(formats)
    d.nameCheck(filePath.toString)
    d
  }

  /**
   * initializes a SimpleMetaInfoEnv with an input stream containing UTF-8 encoded json
   */
  def fromReader(
    reader: Reader, context: String, extraArgs: Boolean = true
  )(implicit formats: org.json4s.Formats): MetaDictionary = {
    val metaInfoJson = JsonUtils.parseReader(reader)
    fromJValue(metaInfoJson, context, extraArgs = extraArgs)(formats)
  }

  /**
   * intializes a SimpleMetaInfoEnv with an input stream containing UTF-8 encoded json
   */
  def fromInputStream(stream: java.io.InputStream, context: String, extraArgs: Boolean = true)(implicit formats: org.json4s.Formats): MetaDictionary = {
    val r = new InputStreamReader(stream, StandardCharsets.UTF_8)
    fromReader(r, context, extraArgs = extraArgs)(formats)
  }
}

case class MetaDictionary(
    metadict_name: String,
    metadict_description: String,
    metadict_version: String,
    metadict_require: List[MetaDictRequire],
    meta_info_entry: SortedMap[String, MetaInfoEntry],
    metadict_deprecated: Boolean = false,
    metadict_gid: Option[String] = None,
    otherKeys: List[(String, JValue)] = Nil
) extends StrictLogging {

  def toJValue(extraArgs: Boolean = true, inlineExtraArgs: Boolean = true, selfGid: Boolean = true, subGids: Boolean = true): JValue = {
    import org.json4s.JsonDSL._
    ("metadict_deprecated" -> (if (metadict_deprecated) JBool(true) else JNothing)) ~
      ("metadict_name" -> metadict_name) ~
      ("metadict_description" -> JsonUtils.stringToJValue(metadict_description)) ~
      ("metadict_version" -> metadict_version) ~
      ("metadict_require" -> JArray(metadict_require.sortBy(_.metadict_relative_path).map(r => r.toJValue))) ~
      ("meta_info_entry" -> JArray((meta_info_entry.map {
        case (k, v) =>
          v.toJValue(
            extraArgs = extraArgs,
            inlineExtraArgs = inlineExtraArgs,
            selfGid = selfGid,
            subGids = subGids
          )
      })(breakOut))) ~
      ("metadict_gid" -> metadict_gid)
  }

  def nameCheck(filePath: String) = {
    val vString: String = try {
      val v = SemVer(metadict_version)
      s"${v.major}(?:\\.${v.minor}(?:\\.${v.patch})?)?(?:-${v.label})?(?:\\+${v.build})?"
    } catch {
      case NonFatal(e) =>
        logger.warn("A valid semantic version is expected in metadict_version of $filePath, not $metadict_version. $e")
        "[0-9]+(?:\\.[0-9]+(?:\\.[0-9]+)?)?(?:-[-a-zA-Z0-9]+(?:\\.[-a-zA-Z0-9]+)*)?(?:\\+[-a-zA-Z0-9]+(?:\\.[-a-zA-Z0-9]+)*)?"
    }
    val fileName = filePath.slice(filePath.lastIndexOf('/') + 1, filePath.length)
    val fileNameRe = s"^$metadict_name(?:\\s+$vString)?\\.nomadmetainfo\\.json$$".r
    fileName match {
      case fileNameRe() =>
      case f =>
        logger.warn(s"unexpected filename '$f' for dictionary $metadict_name and version $metadict_version at $filePath, could not match $fileNameRe")
    }
  }

  def toWriter[W <: java.io.Writer](outF: W): Unit = {
    val w = new JsonUtils.StringArraySplitWriter(outF)
    JsonUtils.prettyWriter(toJValue(), w)
    w.flush()
  }

  def toPath(p: String): Unit =
    toPath(Paths.get(p), overwrite = true)

  def toPath(p: String, overwrite: Boolean): Unit =
    toPath(Paths.get(p), overwrite = overwrite)

  def toPath(p: Path): Unit =
    toPath(p, overwrite = true)

  def toPath(p: Path, overwrite: Boolean, checkName: Boolean = true): Unit = {
    if (checkName)
      nameCheck(p.toString)
    val w = if (overwrite)
      Files.newBufferedWriter(p, StandardCharsets.UTF_8, java.nio.file.StandardOpenOption.CREATE, java.nio.file.StandardOpenOption.TRUNCATE_EXISTING)
    else
      Files.newBufferedWriter(p, StandardCharsets.UTF_8, java.nio.file.StandardOpenOption.CREATE_NEW)
    try {
      toWriter(w)
    } finally {
      w.flush()
      w.close()
    }
  }

}
