/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.parsers

import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.Files
import java.nio.file.StandardCopyOption
import com.typesafe.scalalogging.StrictLogging
import eu.{ nomad_lab => lab }
import org.{ json4s => jn }
import ucar.ma2.{ Array => NArray }
import ucar.ma2

class NoopBackend(
  metaInfoEnv: lab.meta.MetaInfoEnv,
  val backendInfoOverride: jn.JValue = jn.JNothing
) extends BaseParserBackend(
  metaInfoEnv
) with ParserBackendExternal {

  override def backendInfo: jn.JValue = {
    backendInfoOverride match {
      case jn.JNothing =>
        jn.JObject(
          ("backendType" -> jn.JString(getClass().getName())) :: Nil
        )
      case _ =>
        backendInfoOverride
    }
  }

  override def cleanup(): Unit = {}

  override def startedParsingSession(
    mainFileUri: Option[String],
    parserInfo: jn.JValue,
    parserStatus: Option[ParseResult.Value] = None,
    parserErrors: jn.JValue = jn.JNothing
  ): Unit = {}

  override def finishedParsingSession(
    parserStatus: Option[ParseResult.Value],
    parserErrors: jn.JValue = jn.JNothing,
    mainFileUri: Option[String] = None,
    parserInfo: jn.JValue = jn.JNothing,
    parsingStats: Map[String, Long] = Map()
  ): Unit = {}

  override def setSectionInfo(metaName: String, gIndex: Long, references: Map[String, Long]): Unit = {}

  override def addValue(metaName: String, value: jn.JValue, gIndex: Long = -1): Unit = {
  }

  /**
   * Adds a floating point value corresponding to metaName.
   *
   * The value is added to the section the meta info metaName is in.
   * A gIndex of -1 means the latest section.
   */
  override def addRealValue(metaName: String, value: Double, gIndex: Long = -1): Unit = {}

  /**
   * Adds a new array of the given size corresponding to metaName.
   *
   * The value is added to the section the meta info metaName is in.
   * A gIndex of -1 means the latest section.
   * The array is unitialized.
   */
  override def addArray(metaName: String, shape: Seq[Long], gIndex: Long = -1): Unit = {}

  /**
   * Adds values to the last array added
   */
  override def setArrayValues(
    metaName: String,
    values: NArray,
    offset: Option[Seq[Long]] = None,
    gIndex: Long = -1
  ): Unit = {}

  /**
   * Adds an array value with the given array values
   */
  override def addArrayValues(metaName: String, values: NArray, gIndex: Long = -1): Unit = {}
}
