/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.parsers;
import ucar.nc2
import ucar.ma2.{ Array => NArray }
import ucar.ma2
import scala.collection.breakOut
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import eu.nomad_lab.JsonUtils
import eu.nomad_lab.meta.MetaInfoEnv
import eu.nomad_lab.meta.MetaInfoRecord
import org.json4s.{ JNothing, JNull, JBool, JDouble, JDecimal, JInt, JString, JArray, JObject, JValue, JField }
import eu.nomad_lab.H5Lib
import eu.nomad_lab.Base64
import scala.util.control.NonFatal
import java.nio.file.Path
import java.nio.file.Paths

object H5Backend {
  val superGIndexOfRootSections: Long = 0 // use -1?

  /** global and local indexes of a value, and its dataset */
  case class ValueIndexes(
    valueDatasetName: String,
    globalIndex: Long,
    localIndex: Long
  )

  class HdfValueException(
    msg: String,
    what: Throwable = null
  ) extends Exception(msg, what)

  /**
   * returns the gIndex of the super section
   */
  def superGIndex(indexDatasetId: Long, globalIndex: Long): Long = {
    val idxVal = H5Lib.datasetRead(
      datasetId = indexDatasetId,
      dtypeStr = "i64",
      dimsToRead = Seq(1, 1),
      offset = Seq(globalIndex, 0)
    )
    val idxValIt = idxVal.getIndexIterator
    idxValIt.getLongNext()
  }

  /**
   * Manager for sections
   */
  class H5SectionManager(
      val metaInfo: MetaInfoRecord,
      val parentSection: Option[H5SectionManager],
      val calculationGroup: Long,
      openSections0: Set[Long] = Set()
  ) extends GenericBackend.SectionManager {
    def parentSectionName: Option[String] = {
      parentSection.map(_.metaInfo.name)
    }

    var _sectionGroup: Option[Long] = None
    /**
     * Returns the group of this section in the hdf file, creating it the first time requested
     * this stays cached until one call closeSectionGroup
     */
    def sectionGroup(create: Boolean = true): Long = {
      _sectionGroup match {
        case Some(g) =>
          g
        case None =>
          val parentGroup = parentSection match {
            case Some(s) =>
              s.sectionGroup(create)
            case None =>
              calculationGroup
          }
          val newG = H5Lib.groupGet(locId = parentGroup, groupPath = metaInfo.name, create = create)
          if (newG >= 0)
            _sectionGroup = Some(newG)
          newG
      }
    }

    /**
     * closes the cached section group
     */
    def closeSectionGroup(): Unit = {
      _sectionGroup match {
        case Some(g) =>
          _sectionGroup = None
          H5Lib.groupClose(g)
        case None => ()
      }
    }

    /**
     * Returns the index dataset, creating it when requested.
     *
     *  You get ownership of the returned dataset (i.e. remember to call close on it)
     */
    def openIndexDataset(create: Boolean = true): Long = {
      val name = metaInfo.name + "-index"
      val indexDim: Long = 1
      H5Lib.datasetGet(locId = sectionGroup(create), name, "i64",
        dimensions = Seq(0: Long, indexDim),
        maxDimensions = Seq(H5Lib.unlimitedDim, indexDim),
        create = create)
    }

    /** returns the gIndex of the last section created */
    def lastSectionGIndex: Long = {
      val d = openIndexDataset(false)
      if (d >= 0) {
        val dimsId = H5Lib.datasetGetDimensionsId(d)
        val nDims = H5Lib.dimsExtents(dimsId)(0) - 1
        H5Lib.dimsClose(dimsId)
        H5Lib.datasetClose(d)
        nDims
      } else {
        -1
      }
    }

    val openSections = mutable.Set[Long]()
    openSections ++= openSections0

    /**
     * returns the gIndexes of the sections that are still open
     */
    def openSectionsGIndexes(): Iterator[Long] = openSections.iterator;

    /**
     * sets info values of an open section.
     *
     * references should be references to gIndex of the root sections this section refers to.
     */
    def setSectionInfo(gIndex: Long, references: Map[String, Long]): Unit = {
      val gIdx = if (gIndex == -1)
        lastSectionGIndex
      else
        gIndex
      val superGIndex: Long = parentSection match {
        case Some(superSection) =>
          references.get(superSection.metaInfo.name) match {
            case Some(i) =>
              if (i == -1)
                superSection.lastSectionGIndex
              else
                i
            case None =>
              throw new HdfValueException(s"setSectionInfo missing super section ${superSection.metaInfo.name} of ${metaInfo.name} in references $references")
          }
        case None => superGIndexOfRootSections
      }
      val newIndexValues = new ma2.ArrayLong.D2(1, 1)
      newIndexValues.set(0, 0, superGIndex)
      val idx = openIndexDataset(false)
      if (idx >= 0) {
        H5Lib.datasetWrite(
          idx,
          value = newIndexValues,
          offset = Seq(gIdx, 0),
          dtypeStr = "i64"
        )
        H5Lib.datasetClose(idx)
      } else {
        throw new HdfValueException(s"setSectionInfo called with invalid section index for ${metaInfo.name}")
      }
    }

    /**
     * returns the gIndex of a newly opened section
     */
    def openSection(gBackend: GenericBackend): Long = {
      val indexDatasetId = openIndexDataset(true)
      val indexDimId = H5Lib.datasetGetDimensionsId(indexDatasetId)
      val indexDims = H5Lib.dimsExtents(indexDimId)
      val globalIndex = indexDims(0)
      indexDims(0) += 1
      H5Lib.datasetResize(indexDatasetId, indexDims)
      val newIndexValues = new ma2.ArrayLong.D2(1, 1)
      newIndexValues.set(0, 0, parentSection match {
        case Some(section) => section.lastSectionGIndex
        case None => superGIndexOfRootSections
      })
      H5Lib.datasetWrite(
        indexDatasetId,
        value = newIndexValues,
        offset = Seq(globalIndex, 0),
        dtypeStr = "i64"
      )
      H5Lib.datasetClose(indexDatasetId)
      globalIndex
    }

    /**
     * section with gIndex (not supported, allow?)
     */
    def openSectionWithGIndex(gBackend: GenericBackend, gIndex: Long): Unit = {
      throw new HdfValueException(s"openSectionWithGIndex is not implemented for H5Sections, but called with ${metaInfo.name}")
    }

    /**
     * closes the given section
     */
    def closeSection(gBackend: GenericBackend, gIndex: Long) = {
      openSections -= gIndex
    }

    /**
     * Information on an open section
     */
    def sectionInfo(gIndex: Long): String = {
      if (openSections(gIndex))
        s"open section ${metaInfo.name} ($gIndex)"
      else
        s"section $gIndex in ${metaInfo.name} is closed"
    }
  }

  trait HdfScalarValue {
    def metaInfo: MetaInfoRecord;
    def sectionManager: H5SectionManager

    val openDimensions = metaInfo.shape match {
      case Some(dims) =>
        dims.zipWithIndex.filter {
          case (Right(str), idx) => true
          case _ => false
        }
      case None =>
        Seq()
    }

    /**
     * returns a dataset to store values with the given size
     * locId should be the group of the section containing the concrete meta info metaName
     * If create is false returns -1 if not existing, otherwise creates it if needed.
     */
    def valueDataset(valueDimensions: Seq[Long], create: Boolean = true): (String, Long) = {
      val openDimensionsValues = openDimensions.map {
        case (dimName, idx) =>
          if (valueDimensions.length <= idx)
            throw new HdfValueException(s"extracting openDimension tries to extract index $idx from valuesDimensions with size ${valueDimensions.length} (${valueDimensions.mkString("[", ",", "]")})")
          Base64.b64Nr(valueDimensions(idx))
      }
      val name = openDimensionsValues.foldLeft(metaInfo.name + "-v") {
        _ + "." + _
      }
      val datasetId = H5Lib.datasetGet(sectionManager.sectionGroup(create = true), name, metaInfo.dtypeStr.get,
        dimensions = (0: Long) +: valueDimensions,
        maxDimensions = (H5Lib.unlimitedDim) +: valueDimensions,
        create = create)
      (name -> datasetId)
    }

    def valueDatasetWithName(name: String): Long = {
      H5Lib.datasetOpen(sectionManager.sectionGroup(true), name)
    }

    /**
     * returns the dataset for the index for the given metaName
     * locId should be the group of the section containing the concrete meta info metaName
     * If create is false returns -1 if not existing, otherwise creates it if needed.
     */
    def indexDataset(create: Boolean = true): Long = {
      val name = metaInfo.name + "-index"
      val indexDim: Long = openDimensions.length.toLong + (if (openDimensions.isEmpty) 1 else 2)
      H5Lib.datasetGet(sectionManager.sectionGroup(true), name, "i64",
        dimensions = Seq(0: Long, indexDim),
        maxDimensions = Seq(H5Lib.unlimitedDim, indexDim),
        create = create)
    }

    /** adds a value of the given size to the metaName, returns its global and local index */
    def valueAdd(indexDatasetId: Long, valuesDatasetName: String, valuesDatasetId: Long, dimensions: Seq[Long], superSectionGindex: Long): ValueIndexes = {
      val valueDimId = H5Lib.datasetGetDimensionsId(valuesDatasetId)
      val valueDims = H5Lib.dimsExtents(valueDimId)
      H5Lib.dimsClose(valueDimId)
      if (valueDims.length != dimensions.length + 1)
        throw new HdfValueException(s"Incompatible sizes for value and dataset: ${valueDims.mkString("[", ",", "]")} vs dimensions in $metaInfo.name")
      for (i <- 0 until dimensions.length) {
        if (valueDims(i + 1) != dimensions(i))
          throw new HdfValueException(s"Incompatible sizes for value and dataset: ${valueDims.mkString("[", ",", "]")} vs dimensions in $metaInfo.name")
      }
      val localIndex = valueDims(0)
      valueDims(0) += 1
      H5Lib.datasetResize(valuesDatasetId, valueDims)
      val indexDimId = H5Lib.datasetGetDimensionsId(indexDatasetId)
      val indexDims = H5Lib.dimsExtents(indexDimId)
      H5Lib.dimsClose(indexDimId)
      val globalIndex = indexDims(0)
      indexDims(0) += 1
      H5Lib.datasetResize(indexDatasetId, indexDims)
      val newIndexValues = new ma2.ArrayLong.D2(1, indexDims(1).toInt)
      newIndexValues.set(0, 0, superSectionGindex match {
        case -1 => sectionManager.lastSectionGIndex
        case i => i
      })
      if (!openDimensions.isEmpty) {
        newIndexValues.set(0, 1, localIndex)
        var ii: Int = 2
        openDimensions.foreach {
          case (name, index) =>
            newIndexValues.set(0, ii, dimensions(index))
            ii += 1
        }
      }
      H5Lib.datasetWrite(
        indexDatasetId,
        value = newIndexValues,
        offset = Seq(globalIndex, 0),
        dtypeStr = "i64"
      )
      ValueIndexes(valuesDatasetName, globalIndex, localIndex)
    }

    /**
     * sets the value at localIndex to the given value
     */
    def valueSetLocal(valuesDatasetId: Long, localIndex: Long, value: NArray, valueOffset: Seq[Long] = Seq()): Unit = {
      val dimsId = H5Lib.datasetGetDimensionsId(valuesDatasetId)
      val valueDims = H5Lib.dimsExtents(dimsId)
      val offset = Array.fill[Long](valueDims.length)(0: Long)
      offset(0) = localIndex
      for ((off, i) <- valueOffset.zipWithIndex)
        offset(i + 1) = off
      if (offset(0) < 0) offset(0) += valueDims(0)
      val valueSizes = Array.fill[Long](offset.length)(1: Long)
      val valueShape = value.getShape
      if (valueShape.length + 1 != valueDims.length)
        throw new HdfValueException(s"Invalid shape for value ${valueShape.mkString("[", ",", "]")} vs ${valueDims.drop(1).mkString("[", ",", "]")} in ${metaInfo.name}")
      for ((s, i) <- valueShape.zipWithIndex) {
        if (s.toLong != valueDims(i + 1))
          throw new HdfValueException(s"Invalid shape for value ${valueShape.mkString("[", ",", "]")} vs ${valueDims.drop(1).mkString("[", ",", "]")} in ${metaInfo.name}")
        valueSizes(i + 1) = s.toLong
      }
      H5Lib.datasetWrite(valuesDatasetId, value, offset = offset, dtypeStr = metaInfo.dtypeStr.get, valueSizes = valueSizes)
    }

    /**
     * gets the value at the given local index
     */
    def valueGetLocal(valuesDatasetId: Long, localIndex: Long, valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq()): NArray = {
      val dimsId = H5Lib.datasetGetDimensionsId(valuesDatasetId)
      val valueDims = H5Lib.dimsExtents(dimsId)
      val offset = Array.fill[Long](valueDims.length)(0: Long)
      offset(0) = localIndex
      for ((off, i) <- valueOffset.zipWithIndex)
        offset(i + 1) = off
      valueDims(0) = 1

      val array = H5Lib.createNArray(metaInfo.dtypeStr.get, valueDims.drop(1).map(_.toInt))
      H5Lib.datasetRead(
        datasetId = valuesDatasetId,
        dtypeStr = metaInfo.dtypeStr match {
          case Some(t) => t
          case None => throw new HdfValueException(s"meta info ${metaInfo.name} expected to be a meta info with dtypeStr, not ${JsonUtils.normalizedStr(metaInfo.toJValue())}")
        },
        dimsToRead = valueDims,
        offset = offset,
        value = Some(array)
      )
    }

    /**
     * resolves a global index
     */
    def resolveGlobalIndex(indexDatasetId: Long, globalIndex: Long): ValueIndexes = {
      val idxVal = H5Lib.datasetRead(
        datasetId = indexDatasetId,
        dtypeStr = "i64",
        dimsToRead = Seq(1: Long, if (openDimensions.isEmpty) 1 else (2 + openDimensions.length.toLong)),
        offset = Seq(globalIndex, 0)
      )
      val idxValIt = idxVal.getIndexIterator
      val superGIndex = idxValIt.getLongNext()
      var localIndex: Long = -1
      var name: String = ""
      if (openDimensions.isEmpty) {
        localIndex = globalIndex
        name = metaInfo.name + "-v"
      } else {
        localIndex = idxValIt.getLongNext()
        val openDimensionsValues = openDimensions.map {
          case (dimName, idx) =>
            Base64.b64Nr(idxValIt.getLongNext())
        }
        name = openDimensionsValues.foldLeft(metaInfo.name + "-v") {
          _ + "." + _
        }
      }
      ValueIndexes(name, globalIndex = globalIndex, localIndex = localIndex)
    }

    /** adds an array value */
    def valueAddArray(values: NArray, gIndex: Long): Unit = {
      val shapeDims = values.getShape.map(_.toLong).toSeq
      metaInfo.shape.getOrElse(Seq()).length match {
        case 0 =>
          if (shapeDims.length != 0 && shapeDims != Seq(1L))
            throw new HdfValueException(s"Incompatible sizes for value and dataset: ${shapeDims.mkString("[", ",", "]")} vs scalar in $metaInfo.name")
        case n =>
          if (shapeDims.length != n)
            throw new HdfValueException(s"Incompatible sizes for value and dataset: ${shapeDims.mkString("[", ",", "]")} vs ${metaInfo.shape.get.mkString("[", ",", "]")} in $metaInfo.name")
      }
      val (valDatasetName, valDatasetId) = valueDataset(values.getShape.map(_.toLong).toSeq, create = true)
      val idxDatasetId = indexDataset(create = true)
      val valueIndex = valueAdd(idxDatasetId, valuesDatasetName = valDatasetName, valuesDatasetId = valDatasetId, dimensions = values.getShape.map(_.toLong).toSeq, superSectionGindex = gIndex)
      valueSetLocal(valDatasetId, localIndex = valueIndex.localIndex, value = values)
      H5Lib.datasetClose(valDatasetId)
      H5Lib.datasetClose(idxDatasetId)
    }
  }

  trait HdfValue extends HdfScalarValue {

    /** adds an array value */
    def addArrayValues(values: NArray, gIndex: Long): Unit = valueAddArray(values, gIndex)

    def addArray(shape: Seq[Long], gIndex: Long): Unit = {
      val (valDatasetName, valDatasetId) = valueDataset(shape, create = true)
      val idxDatasetId = indexDataset(create = true)
      val valueIndex = valueAdd(idxDatasetId, valuesDatasetName = valDatasetName, valuesDatasetId = valDatasetId, dimensions = shape, superSectionGindex = gIndex)
      H5Lib.datasetClose(valDatasetId)
      H5Lib.datasetClose(idxDatasetId)

    }

    def setArrayValues(values: NArray, offset: Option[Seq[Long]], gIndex: Long = -1): Unit = {
      if (sectionManager.lastSectionGIndex != gIndex)
        throw new HdfValueException(s"setArrayValues on non last gIndex $gIndex of ${metaInfo.name} not implemented")
      val (valDatasetName, valDatasetId) = valueDataset(values.getShape.map(_.toLong).toSeq, create = true)
      valueSetLocal(valDatasetId, localIndex = -1, value = values, valueOffset = offset.getOrElse(Seq()))
      H5Lib.datasetClose(valDatasetId)
    }

  }

  /**
   * storing for boolean values
   */
  final class H5MetaDataManager_b(
      metaInfo: MetaInfoRecord,
      val sectionManager: H5SectionManager
  ) extends GenericBackend.MetaDataManager_b(metaInfo) with HdfScalarValue {

    def dispatch_b(value: Boolean, gIndex: Long): Unit = {
      val valueArray = new ma2.ArrayBoolean.D1(1)
      valueArray.set(0, value)
      valueAddArray(valueArray, gIndex)
    }
  }

  /**
   * storing for integer values
   */
  final class H5MetaDataManager_i(
      metaInfo: MetaInfoRecord,
      val sectionManager: H5SectionManager
  ) extends GenericBackend.MetaDataManager_i(metaInfo) with HdfScalarValue {

    def dispatch_i(value: Long, gIndex: Long): Unit = {
      val valueArray: NArray = metaInfo.dtypeStr.get match {
        case "i" | "i32" =>
          val dArray = new ma2.ArrayInt.D1(1)
          dArray.set(0, value.toInt)
          dArray
        case "i64" | "r" =>
          val dArray = new ma2.ArrayLong.D1(1)
          dArray.set(0, value)
          dArray
      }
      valueAddArray(valueArray, gIndex)
    }
  }

  /**
   * storing for floating point values
   */
  final class H5MetaDataManager_f(
      metaInfo: MetaInfoRecord,
      val sectionManager: H5SectionManager
  ) extends GenericBackend.MetaDataManager_f(metaInfo) with HdfScalarValue {

    def dispatch_f(value: Double, gIndex: Long): Unit = {
      val valueArray: NArray = metaInfo.dtypeStr.get match {
        case "f" | "f64" =>
          val dArray = new ma2.ArrayDouble.D1(1)
          dArray.set(0, value)
          dArray
        case "i64" =>
          val dArray = new ma2.ArrayFloat.D1(1)
          dArray.set(0, value.toFloat)
          dArray
      }
      valueAddArray(valueArray, gIndex)
    }
  }

  /**
   * storing for byte arrays (blobs)
   */
  final class H5MetaDataManager_B64(
      metaInfo: MetaInfoRecord,
      val sectionManager: H5SectionManager
  ) extends GenericBackend.MetaDataManager_B64(metaInfo) with HdfScalarValue {

    def dispatch_B64(value: String, gIndex: Long): Unit = {
      val arr = new ma2.ArrayString.D1(1)
      arr.set(0, value)
      valueAddArray(arr, gIndex)
    }
  }

  /**
   * storing for string values
   */
  final class H5MetaDataManager_C(
      metaInfo: MetaInfoRecord,
      val sectionManager: H5SectionManager
  ) extends GenericBackend.MetaDataManager_C(metaInfo) with HdfScalarValue {

    def dispatch_C(value: String, gIndex: Long): Unit = {
      val arr = new ma2.ArrayString.D1(1)
      arr.set(0, value)
      valueAddArray(arr, gIndex)
    }
  }

  /**
   * storing for json dictionaries
   */
  final class H5MetaDataManager_D(
      metaInfo: MetaInfoRecord,
      val sectionManager: H5SectionManager
  ) extends GenericBackend.MetaDataManager_D(metaInfo) with HdfScalarValue {

    def dispatch_D(value: JObject, gIndex: Long): Unit = {
      val arr = new ma2.ArrayObject.D1(classOf[org.json4s.JsonAST.JObject], 1)
      arr.set(0, value)
      valueAddArray(arr, gIndex)
    }
  }

  /**
   * Storing for arrays of ints
   */
  class H5MetaDataManager_Ai32(
      metaInfo: MetaInfoRecord,
      val sectionManager: H5SectionManager
  ) extends GenericBackend.ArrayMetaDataManager_i32(metaInfo) with HdfValue {
  }

  /**
   * Storing for arrays of longs
   */
  class H5MetaDataManager_Ai64(
      metaInfo: MetaInfoRecord,
      val sectionManager: H5SectionManager
  ) extends GenericBackend.ArrayMetaDataManager_i64(metaInfo) with HdfValue {
  }

  /**
   * Storing for arrays of floats
   */
  class H5MetaDataManager_Af32(
      metaInfo: MetaInfoRecord,
      val sectionManager: H5SectionManager
  ) extends GenericBackend.ArrayMetaDataManager_f32(metaInfo) with HdfValue {
  }

  /**
   * Storing for arrays of doubles
   */
  class H5MetaDataManager_Af64(
      metaInfo: MetaInfoRecord,
      val sectionManager: H5SectionManager
  ) extends GenericBackend.ArrayMetaDataManager_f64(metaInfo) with HdfValue {
  }

  /**
   * Storing for arrays of bytes
   */
  class H5MetaDataManager_Ab(
      metaInfo: MetaInfoRecord,
      val sectionManager: H5SectionManager
  ) extends GenericBackend.ArrayMetaDataManager_b(metaInfo) with HdfValue {
  }

  /**
   * Storing for arrays of byte arrays (generic binary data, blobs)
   */
  class H5MetaDataManager_AB64(
      metaInfo: MetaInfoRecord,
      val sectionManager: H5SectionManager
  ) extends GenericBackend.ArrayMetaDataManager_B64(metaInfo) with HdfValue {
  }

  /**
   * Storing for arrays of unicode strings
   */
  class H5MetaDataManager_AC(
      metaInfo: MetaInfoRecord,
      val sectionManager: H5SectionManager
  ) extends GenericBackend.ArrayMetaDataManager_C(metaInfo) with HdfValue {
  }

  /**
   * Storing for arrays of json dictionary
   */
  class H5MetaDataManager_AD(
      metaInfo: MetaInfoRecord,
      val sectionManager: H5SectionManager
  ) extends GenericBackend.ArrayMetaDataManager_D(metaInfo) with HdfValue {
  }

  /**
   * Some meta info is invalid
   */
  class InvalidMetaInfoException(
    metaInfo: MetaInfoRecord, msg: String
  ) extends Exception(s"${metaInfo.name} is invalid: $msg, metaInfo: ${JsonUtils.prettyStr(metaInfo.toJValue())}") {}

  /**
   * Error when compiling the meta info data
   */
  class MetaCompilationException(
    metaInfo: MetaInfoRecord, msg: String
  ) extends Exception(s"Error while compiling meta info ${metaInfo.name}: $msg, metaInfo: ${JsonUtils.prettyStr(metaInfo.toJValue())}") {}

  /**
   * manager for the given meta info
   */
  def h5DataManager(metaInfo: MetaInfoRecord, sectionManager: H5SectionManager): GenericBackend.MetaDataManager = {
    if (metaInfo.kindStr == "type_dimension")
      return new GenericBackend.DummyMetaDataManager(metaInfo, sectionManager)
    if (metaInfo.kindStr != "type_document_content")
      throw new MetaCompilationException(metaInfo, "caching data manager can be instantiated only for conrete data (kindStr = type_document_content)")
    val scalar = metaInfo.shape match {
      case Some(shape) =>
        if (shape.isEmpty)
          true
        else
          false
      case None =>
        throw new InvalidMetaInfoException(metaInfo, "concrete meta info should have a shape ([] for scalar values)")
    }
    val dtypeStr = metaInfo.dtypeStr match {
      case Some(s) =>
        s
      case None =>
        throw new InvalidMetaInfoException(metaInfo, "concrete meta info should have a specific dtypeStr")
    }
    if (scalar) {
      dtypeStr match {
        case "f" | "f64" | "f32" => new H5MetaDataManager_f(metaInfo, sectionManager)
        case "i" | "i64" | "i32" | "r" => new H5MetaDataManager_i(metaInfo, sectionManager)
        case "b" => new H5MetaDataManager_b(metaInfo, sectionManager)
        case "B" => new H5MetaDataManager_B64(metaInfo, sectionManager)
        case "C" => new H5MetaDataManager_C(metaInfo, sectionManager)
        case "D" => new H5MetaDataManager_D(metaInfo, sectionManager)
        case _ =>
          throw new InvalidMetaInfoException(metaInfo, "Unknown dtypeStr, known types: f,f32,f64,i,i32,i64,r,b,B,C,D")
      }
    } else {
      dtypeStr match {
        case "f" | "f64" => new H5MetaDataManager_Af64(metaInfo, sectionManager)
        case "f32" => new H5MetaDataManager_Af32(metaInfo, sectionManager)
        case "i" | "i32" => new H5MetaDataManager_Ai32(metaInfo, sectionManager)
        case "i64" | "r" => new H5MetaDataManager_Ai64(metaInfo, sectionManager)
        case "b" => new H5MetaDataManager_Ab(metaInfo, sectionManager)
        case "B" => new H5MetaDataManager_AB64(metaInfo, sectionManager)
        case "C" => new H5MetaDataManager_AC(metaInfo, sectionManager)
        case "D" => new H5MetaDataManager_AD(metaInfo, sectionManager)
        case _ =>
          throw new InvalidMetaInfoException(metaInfo, "Unknown dtypeStr, known types: f,f32,f64,i,i32,i64,r,b,B,C,D")
      }
    }
  }

  /**
   * Instantiate the given section manager ensuring all its dependencies are also instantiated
   */
  def instantiateSectionManager(metaEnv: MetaInfoEnv, metaInfo: MetaInfoRecord, sectionManagers: mutable.Map[String, H5SectionManager], calculationGroup: Long, toIgnore: Set[String] = Set()): Option[H5SectionManager] = {
    if (toIgnore(metaInfo.name))
      return None
    sectionManagers.get(metaInfo.name) match {
      case Some(null) =>
        throw new MetaCompilationException(metaInfo, "circular dependency")
      case Some(s) =>
        Some(s)
      case None =>
        sectionManagers(metaInfo.name) = null
        val superSectionNames = GenericBackend.firstSuperSections(metaEnv, metaInfo.name)
        val parentSection: Option[H5SectionManager] = superSectionNames.length match {
          case 0 => None
          case 1 =>
            metaEnv.metaInfoRecordForName(superSectionNames(0)) match {
              case Some(superMetaInfo) =>
                instantiateSectionManager(metaEnv, superMetaInfo, sectionManagers, calculationGroup) match {
                  case Some(superManager) => Some(superManager)
                  case None => return None // should be ignored
                }
              case None =>
                throw new MetaCompilationException(metaInfo, s"missing dependency ${superSectionNames.mkString(",")}")
            }
          case n =>
            throw new MetaCompilationException(metaInfo, s"expected just one direct superSection, not ${superSectionNames.mkString(",")}")
        }
        val newSection = new H5SectionManager(
          metaInfo = metaInfo,
          parentSection = parentSection,
          calculationGroup = calculationGroup
        )
        sectionManagers(metaInfo.name) = newSection
        Some(newSection)
    }
  }

  /** creates a json writer that writes to the given path */
  def createBackend(
    parser: OptimizedParser,
    fileToWrite: Path,
    repl: Map[String, String]
  ): H5Backend = {
    val metaInfoEnv: MetaInfoEnv = parser.parseableMetaInfo
    val h5File = H5Backend.H5File.create(fileToWrite, Paths.get("/", repl("archiveGid"), repl("calculationGid")))
    apply(metaInfoEnv, h5File, closeFileOnFinishedParsing = true)
  }

  /**
   * Using the given factory methods to intantiate the section and data managers
   */
  def apply(
    metaEnv: MetaInfoEnv,
    h5File: H5File,
    toIgnore: Set[String] = Set(),
    closeFileOnFinishedParsing: Boolean = true
  ): H5Backend = {
    val calculationGroup = h5File.groupHandle
    val allNames: Set[String] = metaEnv.allNames.toSet
    // sections
    val sManagers = mutable.Map[String, H5SectionManager]()
    val sectionManagers: Map[String, GenericBackend.SectionManager] = allNames.flatMap { (name: String) =>
      val metaInfo = metaEnv.metaInfoRecordForName(name, true, true).get
      if (metaInfo.kindStr == "type_section") {
        val sectionManager = instantiateSectionManager(metaEnv, metaInfo, sManagers, calculationGroup, toIgnore) match {
          case Some(m) => m
          case None => new GenericBackend.DummySectionManager(
            metaInfo = metaInfo,
            parentSectionName = GenericBackend.firstSuperSections(metaEnv, metaInfo.name).headOption
          )
        }
        Some(metaInfo.name -> sectionManager)
      } else {
        None
      }
    }(breakOut)
    // concrete data
    val metaDataManagers: Map[String, GenericBackend.MetaDataManager] = allNames.flatMap { (name: String) =>
      val metaInfo = metaEnv.metaInfoRecordForName(name, true, true).get
      if (metaInfo.kindStr == "type_document_content" || metaInfo.kindStr == "type_dimension") {
        val superSectionNames = GenericBackend.firstSuperSections(metaEnv, name)
        if (superSectionNames.size != 1)
          throw new InvalidMetaInfoException(metaInfo, s"multiple direct super sections: ${superSectionNames.mkString(", ")}")
        val dataManager = if (metaInfo.kindStr == "type_document_content" && !toIgnore(metaInfo.name)) {
          sManagers.get(superSectionNames(0)) match {
            case Some(sectionManager) =>
              h5DataManager(metaInfo, sectionManager)
            case None =>
              sectionManagers.get(superSectionNames(0)) match {
                case None =>
                  throw new InvalidMetaInfoException(metaInfo, s"could not find super section ${superSectionNames(0)} of ${metaInfo.name}")
                case Some(superSection) =>
                  new GenericBackend.DummyMetaDataManager(metaInfo, superSection)
              }
          }
        } else {
          sectionManagers.get(superSectionNames(0)) match {
            case None =>
              throw new InvalidMetaInfoException(metaInfo, s"could not find super section ${superSectionNames(0)} of ${metaInfo.name}")
            case Some(superSection) =>
              new GenericBackend.DummyMetaDataManager(metaInfo, superSection)
          }
        }
        Some(name -> dataManager)
      } else {
        None
      }
    }(breakOut)

    new H5Backend(
      metaInfoEnv = metaEnv,
      h5SectionManagers = sManagers.toMap,
      sectionManagers = sectionManagers,
      metaDataManagers = metaDataManagers,
      h5File = h5File,
      closeFileOnFinishedParsing = closeFileOnFinishedParsing
    )
  }

  object H5File {
    /**
     * creates a new file (overwriting an existing file if present)
     */
    def create(
      filePath: Path,
      mainGroupPath: java.nio.file.Path = Paths.get("/"),
      fileType: Option[String] = Some("nomad_info_data_h5_1_0")
    ): H5File = {
      val h5FileHandle = H5Lib.fileCreate(filePath.toString())
      val absPath = java.nio.file.Paths.get("/", mainGroupPath.toString())
      var groupHandle: Long = h5FileHandle
      val it = mainGroupPath.iterator()
      while (it.hasNext() && groupHandle >= 0) {
        val pNow = it.next()
        val newGroup = H5Lib.groupGet(groupHandle, pNow.toString(), create = true)
        if (groupHandle != h5FileHandle)
          H5Lib.groupClose(groupHandle)
        groupHandle = newGroup
      }
      fileType match {
        case Some(typeStr) =>
          val sDim = H5Lib.dimsCreate(Array(1: Long))
          val typeAttrId = H5Lib.attributeCreate(h5FileHandle, attributeName = "type", attributeType = "C", dimensionsId = sDim)
          H5Lib.attributeWriteStr(typeAttrId, values = Array(typeStr))
          H5Lib.dimsClose(sDim)
          H5Lib.attributeClose(typeAttrId)
        case None => ()
      }
      new H5File(filePath = filePath, mainGroupPath = absPath, h5FileHandle = h5FileHandle, groupHandle = groupHandle)
    }

    /**
     * opens an existing hdf file
     */
    def open(filePath: Path, mainGroupPath: Path, write: Boolean = false): H5File = {
      val h5FileHandle = H5Lib.fileOpen(filePath.toString(), write = write)
      val absPath = java.nio.file.Paths.get("/", mainGroupPath.toString())
      var groupHandle: Long = h5FileHandle
      val it = mainGroupPath.iterator()
      while (it.hasNext() && groupHandle >= 0) {
        val pNow = it.next()
        val newGroup = H5Lib.groupGet(groupHandle, pNow.toString(), create = write)
        if (groupHandle != h5FileHandle)
          H5Lib.groupClose(groupHandle)
        groupHandle = newGroup
      }
      new H5File(filePath = filePath, mainGroupPath = absPath, h5FileHandle = h5FileHandle, groupHandle = groupHandle)
    }
  }

  /**
   * represents an hdf file
   */
  class H5File(
      val filePath: java.nio.file.Path,
      val mainGroupPath: java.nio.file.Path,
      val h5FileHandle: Long,
      val groupHandle: Long
  ) {
    def close(): Unit = {
      if (groupHandle != h5FileHandle && groupHandle >= 0)
        H5Lib.groupClose(groupHandle)
      if (h5FileHandle >= 0)
        H5Lib.fileClose(h5FileHandle)
    }

    def valid: Boolean = {
      groupHandle >= 0
    }
  }
}

/**
 * Backend that caches values
 */
class H5Backend(
    metaInfoEnv: MetaInfoEnv,
    val h5SectionManagers: Map[String, H5Backend.H5SectionManager],
    val sectionManagers: Map[String, GenericBackend.SectionManager],
    val metaDataManagers: Map[String, GenericBackend.MetaDataManager],
    val h5File: H5Backend.H5File,
    val closeFileOnFinishedParsing: Boolean = true
) extends GenericBackend(metaInfoEnv) with ParserBackendInternal {

  /**
   * closes the handles of all section managers (i.e. release all cached hdf handles)
   */
  def closeManagersH5Handles(): Unit = {
    h5SectionManagers.foreach(_._2.closeSectionGroup())
  }

  /** closes hdf handles and file */
  def close(): Unit = {
    closeManagersH5Handles()
    h5File.close()
  }

  def setSingleStrAttrib(locId: Long, name: String, value: String): Unit = {
    val sDim = H5Lib.dimsCreate(Array(1: Long))
    val attrId = H5Lib.attributeCreate(locId, attributeName = name, attributeType = "C", dimensionsId = sDim)
    H5Lib.attributeWriteStr(attrId, values = Array(value))
    H5Lib.dimsClose(sDim)
    H5Lib.attributeClose(attrId)
  }

  def setMultipleStrAttrib(locId: Long, name: String, value: Array[String]): Unit = {
    val sDim = H5Lib.dimsCreate(Array(value.length.toLong), Array(H5Lib.unlimitedDim))
    val attrId = H5Lib.attributeCreate(locId, attributeName = name, attributeType = "C", dimensionsId = sDim)
    H5Lib.attributeWriteStr(attrId, values = value)
    H5Lib.dimsClose(sDim)
    H5Lib.attributeClose(attrId)
  }

  /**
   * Started a parsing session
   */
  override def startedParsingSession(
    mainFileUri: Option[String],
    parserInfo: JValue,
    parserStatus: Option[ParseResult.Value] = None,
    parserErrors: JValue = JNothing
  ): Unit = {
    mainFileUri match {
      case Some(uri) =>
        setSingleStrAttrib(h5File.groupHandle, "mainFileUri", uri)
      case None => ()
    }
    parserInfo match {
      case JNothing => ()
      case v =>
        setSingleStrAttrib(h5File.groupHandle, "parserInfo", JsonUtils.normalizedStr(v))
    }
    parserStatus match {
      case None => ()
      case Some(s) =>
        setSingleStrAttrib(h5File.groupHandle, "parserStatus", s.toString())
    }
    parserErrors match {
      case JNothing => ()
      case v =>
        setSingleStrAttrib(h5File.groupHandle, "parserErrors", JsonUtils.normalizedStr(v))
    }
    super.startedParsingSession(mainFileUri, parserInfo, parserStatus, parserErrors)
  }

  override def onFinishedParsingSession(
    oldParsingSession: GenericBackend.ParsingSession,
    newParsingSession: GenericBackend.ParsingSession
  ): Unit = {
    if (oldParsingSession.mainFileUri.isEmpty) {
      newParsingSession.mainFileUri match {
        case Some(uri) =>
          setSingleStrAttrib(h5File.groupHandle, "mainFileUri", uri)
        case None => ()
      }
    }
    if (oldParsingSession.parserInfo.toSome.isEmpty) {
      newParsingSession.parserInfo match {
        case JNothing => ()
        case v =>
          setSingleStrAttrib(h5File.groupHandle, "parserInfo", JsonUtils.normalizedStr(v))
      }
    }
    if (oldParsingSession.parserStatus.isEmpty) {
      newParsingSession.parserStatus match {
        case None => ()
        case Some(s) =>
          setSingleStrAttrib(h5File.groupHandle, "parserStatus", s.toString())
      }
    }
    if (oldParsingSession.parserErrors.toSome.isEmpty) {
      newParsingSession.parserErrors match {
        case JNothing => ()
        case v =>
          setSingleStrAttrib(h5File.groupHandle, "parserErrors", JsonUtils.normalizedStr(v))
      }
    }
    if (closeFileOnFinishedParsing)
      close()
    else
      closeManagersH5Handles() // do nothing?
  }
}
