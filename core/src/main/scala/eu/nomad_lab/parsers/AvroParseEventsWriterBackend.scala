/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.parsers;

import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.JsonUtils
import eu.nomad_lab.meta.MetaInfoEnv
import org.apache.avro
import org.apache.avro.generic.{ GenericData, GenericRecord }
import org.json4s.{ JNothing, JValue }
import ucar.ma2.{ Array => NArray }
import scala.collection.JavaConverters._
import collection.JavaConversions._

object AvroParseEventsWriterBackend {
  class AvroError(
    msg: String
  ) extends Exception(msg)

}

class AvroParseEventsWriterBackend(
    metaInfoEnv: MetaInfoEnv,
    val outFile: java.io.File
) extends BaseParserBackend(metaInfoEnv) with ParserBackendExternal with StrictLogging {

  var mainFileUri: Option[String] = None
  var parserInfo: JValue = JNothing
  var parserStatus: Option[ParseResult.Value] = None
  var parserErrors: JValue = JNothing

  val classLoader: ClassLoader = getClass().getClassLoader()
  val avroSchemaStream: java.io.InputStream = classLoader.getResourceAsStream("ParseEvents.avsc")
  val schema: avro.Schema = new avro.Schema.Parser().parse(avroSchemaStream)
  val records: Map[String, avro.Schema] = {
    schema.getTypes().asScala.map { (s: avro.Schema) => s.getName() -> s }.toMap
  }
  var datumWriter: avro.io.DatumWriter[GenericRecord] = new avro.generic.GenericDatumWriter[GenericRecord](schema)
  var dataFileWriter: avro.file.DataFileWriter[GenericRecord] = new avro.file.DataFileWriter[GenericRecord](datumWriter)

  override def startedParsingSession(
    mainFileUri: Option[String],
    parserInfo: JValue,
    parserStatus: Option[ParseResult.Value] = None,
    parserErrors: JValue = JNothing
  ): Unit = {
    this.mainFileUri = mainFileUri
    this.parserInfo = parserInfo
    this.parserStatus = parserStatus
    this.parserErrors = parserErrors

    //logger.info(s"========== Beginning of startedParsingSession ============")
    dataFileWriter.create(schema, outFile)
    val record: GenericData.Record = new GenericData.Record(records("StartedParsingSession"))
    this.mainFileUri match {
      case None => ()
      case Some(uri) => record.put("mainFileUri", uri)
    }
    this.parserInfo match {
      case JNothing => ()
      case _ => record.put("parserInfo", JsonUtils.normalizedStr(this.parserInfo))
    }
    this.parserStatus match {
      case None => ()
      case Some(s) => record.put("parsingStatus", s.toString())
    }
    this.parserErrors match {
      case JNothing => ()
      case _ => record.put("parsingErrors", this.parserErrors)
    }
    dataFileWriter.append(record)
    //logger.info(s"========== End of startedParsingSession ============")
  }

  override def finishedParsingSession(
    parserStatus: Option[ParseResult.Value],
    parserErrors: JValue = JNothing,
    mainFileUri: Option[String] = None,
    parserInfo: JValue = JNothing,
    parsingStats: Map[String, Long] = Map()
  ): Unit = {
    this.parserStatus = parserStatus
    this.parserErrors = parserErrors
    this.mainFileUri = mainFileUri
    this.parserInfo = parserInfo

    //logger.info(s"========== Beginning of finishedParsingSession ============")
    val record: GenericData.Record = new GenericData.Record(records("FinishedParsingSession"))
    this.parserStatus match {
      case None => ()
      case Some(s) => record.put("parsingStatus", s.toString())
    }
    this.parserErrors match {
      case JNothing => ()
      case _ => record.put("parsingErrors", this.parserErrors)
    }
    this.mainFileUri match {
      case None => ()
      case Some(uri) => record.put("mainFileUri", uri)
    }
    this.parserInfo match {
      case JNothing => ()
      case _ => record.put("parserInfo", JsonUtils.normalizedStr(this.parserInfo))
    }
    dataFileWriter.append(record)
    dataFileWriter.close()
    //logger.info(s"========== End of finishedParsingSession ============")
  }

  /**
   * sets info values of an open section.
   *
   * references should be references to gIndex of the root sections this section refers to.
   */
  //override
  def setSectionInfo(metaName: String, gIndex: Long, references: Map[String, Long]): Unit = {
    //logger.info(s"========== Beginning of setSectionInfo ============")
    val record: GenericData.Record = new GenericData.Record(records("SetSectionInfo"))
    record.put("metaName", metaName)
    record.put("gIndex", gIndex)
    record.put("references", mapAsJavaMap(references).asInstanceOf[java.util.Map[java.lang.String, java.lang.Long]])
    dataFileWriter.append(record)
    //logger.info(s"========== End of setSectionInfo ============")
  }

  /**
   * closes a section
   *
   * after this no other value can be added to the section.
   * metaName is the name of the meta info, gIndex the index of the section
   */
  override def closeSection(metaName: String, gIndex: Long): Unit = {
    //logger.info(s"========== Beginning of closeSection ============")
    val record: GenericData.Record = new GenericData.Record(records("CloseSection"))
    record.put("metaName", metaName)
    record.put("gIndex", gIndex)
    dataFileWriter.append(record)
    //logger.info(s"========== End of closeSection ============")
  }

  /**
   * Adds a json value corresponding to metaName.
   *
   * The value is added to the section the meta info metaName is in.
   * A gIndex of -1 means the latest section.
   */
  override def addValue(metaName: String, value: JValue, gIndex: Long = -1): Unit = {
    //logger.info(s"========== Beginning of addValue ============")
    val record: GenericData.Record = new GenericData.Record(records("AddValue"))
    record.put("metaName", metaName)
    record.put("value", JsonUtils.normalizedStr(value))
    record.put("gIndex", gIndex)
    dataFileWriter.append(record)
    //logger.info(s"========== End of addValue ============")
  }

  /**
   * Adds a floating point value corresponding to metaName.
   *
   * The value is added to the section the meta info metaName is in.
   * A gIndex of -1 means the latest section.
   */
  override def addRealValue(metaName: String, value: Double, gIndex: Long = -1): Unit = {
    //logger.info(s"========== Beginning of addRealValue ============")
    val record: GenericData.Record = new GenericData.Record(records("AddRealValue"))
    record.put("metaName", metaName)
    record.put("value", value)
    record.put("gIndex", gIndex)
    dataFileWriter.append(record)
    //logger.info(s"========== End of addRealValue ============")
  }

  /**
   * Adds a new array value of the given size corresponding to metaName.
   *
   * The value is added to the section the meta info metaName is in.
   * A gIndex of -1 means the latest section.
   * The array is unitialized.
   */
  override def addArray(metaName: String, shape: Seq[Long], gIndex: Long = -1): Unit = {
    logger.info(s"========== Beginning of addArray ============")
    val record: GenericData.Record = new GenericData.Record(records("AddArrayValue"))
    record.put("metaName", metaName)
    record.put("shape", shape.toArray)
    record.put("gIndex", gIndex)
    dataFileWriter.append(record)
    logger.info(s"========== End of addArray ============")
  }

  /**
   * Adds values to the last array added
   */
  override def setArrayValues(
    metaName: String,
    values: NArray,
    offset: Option[Seq[Long]] = None,
    gIndex: Long = -1
  ): Unit = {
    logger.info(s"========== Beginning of setArrayValues ============")
    val storage = values.getStorage()
    //val shape: Array[Long] = values.getShape().map(_.toLong)  // WHAT WAS THAT NEEDED FOR?
    val record: GenericData.Record = storage match {
      case a: Array[Int] => new GenericData.Record(records("SetArrayValues_i32"))
      case a: Array[Long] => new GenericData.Record(records("SetArrayValues_i64"))
      case a: Array[Float] => new GenericData.Record(records("SetArrayValues_f32"))
      case a: Array[Double] => new GenericData.Record(records("SetArrayValues_f64"))
      // SetArrayValues_b ?
      // SetArrayValues_B ?
      // SetArrayValues_C ?
      // SetArrayValues_D ?
    }
    record.put("metaName", metaName)
    record.put("flatValues", values)
    record.put("offset", offset) // .toArray ???
    record.put("gIndex", gIndex)
    dataFileWriter.append(record)
    logger.info(s"========== End of setArrayValues ============")
  }

  /**
   * Adds an array value with the given array values
   */
  override def addArrayValues(
    metaName: String,
    values: NArray,
    gIndex: Long = -1
  ): Unit = {
    //logger.info(s"========== Beginning of addArrayValues ============")
    val storage = values.getStorage()
    //logger.info(s"values.getSize : ${values.getSize}")
    //logger.info(s"values.getStorage() = ${storage}")
    var record: GenericData.Record = null

    storage match {
      case a: Array[Int] =>
        record = new GenericData.Record(records("AddArrayValues_i32"))
        record.put("flatValues", java.util.Arrays.asList(a: _*))
      case a: Array[Long] =>
        record = new GenericData.Record(records("AddArrayValues_i64"))
        record.put("flatValues", java.util.Arrays.asList(a: _*))
      case a: Array[Float] =>
        record = new GenericData.Record(records("AddArrayValues_f32"))
        record.put("flatValues", java.util.Arrays.asList(a: _*))
      case a: Array[Double] =>
        record = new GenericData.Record(records("AddArrayValues_f64"))
        record.put("flatValues", java.util.Arrays.asList(a: _*))
      case a: Array[Boolean] =>
        record = new GenericData.Record(records("AddArrayValues_b"))
        record.put("flatValues", java.util.Arrays.asList(a: _*))
      //case obj: Object =>
      case obj: Array[Object] =>
        record = new GenericData.Record(records("AddArrayValues_D"))
        var dt: List[String] = List()
        val it = obj.toList.iterator
        while (it.hasNext) {
          dt = dt.:+(it.next().toString)
        }
        record.put("flatValues", java.util.Arrays.asList(dt: _*))
      case v =>
        throw new AvroParseEventsWriterBackend.AvroError(s"Unhandled storage type, ${v}, in addArrayValues")
    }
    record.put("metaName", metaName)
    record.put("gIndex", gIndex)
    val shape: Array[Long] = values.getShape().map(_.toLong)
    //shape.foreach(s => logger.info(s"Shape: ${s}"))
    //logger.info(s"shape $shape ${shape.mkString("[", ",", "]")}")
    record.put("valuesShape", java.util.Arrays.asList(shape: _*))
    dataFileWriter.append(record)
    //logger.info(s"========== End of addArrayValues ============")
  }

  /**
   * Informs tha backend that a section with the given gIndex should be opened
   *
   * The index is assumed to be unused, it is an error to reopen an existing section.
   */
  override def openSectionWithGIndex(metaName: String, gIndex: Long): Unit = {
    //logger.info(s"========== Beginning of openSectionWithGIndex ============")
    val record: GenericData.Record = new GenericData.Record(records("OpenSectionWithGIndex"))
    record.put("metaName", metaName)
    record.put("gIndex", gIndex)
    dataFileWriter.append(record)
    //logger.info(s"========== End of openSectionWithGIndex ============")
  }
}

