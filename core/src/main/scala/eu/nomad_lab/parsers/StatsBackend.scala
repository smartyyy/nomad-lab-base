/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.parsers;
import ucar.ma2.{ Array => NArray }
import scala.collection.mutable
import eu.nomad_lab.meta.MetaInfoEnv
import org.json4s.{ JNothing, JNull, JBool, JDouble, JDecimal, JInt, JString, JArray, JObject, JValue, JField }

object StatsBackend {

  object StatsBackendBase {
    /**
     * Internal error in the stats collection
     */
    class StatsException(msg: String) extends Exception(msg) {}

    object StatsStatus extends Enumeration {
      type StatsStatus = Value
      val ShouldStart, Collecting, Finished = Value
    }
  }

  /**
   * Backend that collects statistics on the metadata
   */
  abstract class StatsBackendBase(
      val statsUpdateCallback: Option[(StatsBackendBase) => Unit] = None,
      val statsFinishedCallback: Option[(StatsBackendBase) => Unit] = None
  ) extends ParserBackendBase {
    import StatsBackendBase.StatsStatus

    def superBackend: ParserBackendBase

    def metaInfoEnv: MetaInfoEnv = superBackend.metaInfoEnv

    var gStat: Long = 0
    val stats = mutable.Map[String, Long]()
    var mainFileUri: Option[String] = None
    var parserInfo: JValue = JNothing
    var parserStatus: Option[ParseResult.Value] = None
    var status: StatsStatus.Value = StatsStatus.ShouldStart

    def backendInfo: JValue = {
      JObject(
        ("backendType" -> JString(getClass().getName())) :: Nil
      )
    }

    def cleanup(): Unit = {}

    /**
     * Informs this backend that the parsing did finish
     * This is useful if the parsing was stopped by an exception and finishedParsing was not called
     * in that case it calls he statsFinishedCallback if available
     */
    def didFinish(): Unit = {
      status match {
        case StatsStatus.ShouldStart | StatsStatus.Collecting =>
          statsFinishedCallback match {
            case Some(callback) => callback(this)
            case None => ()
          }
          status = StatsStatus.Finished
        case StatsStatus.Finished => ()
      }
    }

    /**
     * Started a parsing session
     */
    override def startedParsingSession(
      mainFileUri: Option[String],
      parserInfo: JValue,
      parserStatus: Option[ParseResult.Value] = None,
      parserErrors: JValue = JNothing
    ): Unit = {
      status match {
        case StatsStatus.ShouldStart | StatsStatus.Finished =>
          status = StatsStatus.Collecting
        case StatsStatus.Collecting =>
          statsFinishedCallback match {
            case Some(callback) => callback(this)
            case None => ()
          }
      }
      gStat = 0
      stats.clear()
      this.mainFileUri = mainFileUri
      this.parserInfo = parserInfo
      this.parserStatus = parserStatus
      superBackend.startedParsingSession(mainFileUri, parserInfo, parserStatus, parserErrors)
    }

    /**
     * Finished a parsing session
     */
    override def finishedParsingSession(
      parserStatus: Option[ParseResult.Value],
      parserErrors: JValue = JNothing,
      mainFileUri: Option[String] = None,
      parserInfo: JValue = JNothing,
      parsingStats: Map[String, Long] = Map()
    ): Unit = {
      this.parserStatus match {
        case Some(_) => ()
        case None => this.parserStatus = parserStatus
      }
      this.mainFileUri match {
        case Some(_) => ()
        case None => this.mainFileUri = mainFileUri
      }
      this.parserInfo match {
        case JNothing => this.parserInfo = parserInfo
        case _ => ()
      }
      status match {
        case StatsStatus.ShouldStart | StatsStatus.Collecting =>
          statsFinishedCallback match {
            case Some(callback) => callback(this)
            case None => ()
          }
          status = StatsStatus.Finished
        case StatsStatus.Finished => ()
      }
      superBackend.finishedParsingSession(parserStatus, parserErrors, mainFileUri, parserInfo, parsingStats)
    }

    /**
     * returns the sections that are still open
     *
     * sections are identified by name of the meta info and their gIndex
     */
    override def openSections(): Iterator[(String, Long)] = {
      superBackend.openSections()
    }

    /**
     * returns information on an open section (for debugging purposes)
     */
    override def sectionInfo(metaName: String, gIndex: Long): String = {
      superBackend.sectionInfo(metaName, gIndex)
    }

    /**
     * sets info values of an open section.
     */
    override def setSectionInfo(metaName: String, gIndex: Long, references: Map[String, Long]): Unit = {
      superBackend.setSectionInfo(metaName, gIndex, references)
    }

    /**
     * closes a section
     *
     * after this no other value can be added to the section.
     * metaName is the name of the meta info, gIndex the index of the section
     */
    override def closeSection(metaName: String, gIndex: Long): Unit = {
      superBackend.closeSection(metaName, gIndex)
    }

    /**
     * Adds a repating value to the section the value is in
     *
     * metaName is the name of the meta info, it should have repating=true
     * meaning that there can be multiple values in the same section
     */
    override def addValue(metaName: String, value: JValue, gIndex: Long = -1): Unit = {
      stats += (metaName -> (stats.getOrElse(metaName, 0: Long) + 1))
      superBackend.addValue(metaName, value, gIndex)
    }

    /**
     * Adds a repeating floating point value
     */
    override def addRealValue(metaName: String, value: Double, gIndex: Long = -1): Unit = {
      stats += (metaName -> (stats.getOrElse(metaName, 0: Long) + 1))
      superBackend.addRealValue(metaName, value, gIndex)
    }

    /**
     * Adds a new array of the given size
     */
    override def addArray(metaName: String, shape: Seq[Long], gIndex: Long = -1): Unit = {
      stats += (metaName -> (stats.getOrElse(metaName, 0: Long) + 1))
      superBackend.addArray(metaName, shape, gIndex)
    }

    /**
     * Adds values to the last array added
     */
    override def setArrayValues(metaName: String, values: NArray, offset: Option[Seq[Long]], gIndex: Long = -1): Unit = {
      superBackend.setArrayValues(metaName, values, offset, gIndex)
    }

    /**
     * Adds an array value with the given array values
     */
    override def addArrayValues(metaName: String, values: NArray, gIndex: Long = -1): Unit = {
      stats += (metaName -> (stats.getOrElse(metaName, 0: Long) + 1))
      superBackend.addArrayValues(metaName, values, gIndex)
    }

  }

  class StatsBackendExternal(
      val superBackend: ParserBackendExternal,
      statsUpdateCallback: Option[(StatsBackendBase) => Unit] = None,
      statsFinishedCallback: Option[(StatsBackendBase) => Unit] = None
  ) extends StatsBackendBase(statsUpdateCallback, statsFinishedCallback) with ParserBackendExternal {
    /**
     * opens a new section that had an identifier gIndex
     */
    override def openSectionWithGIndex(metaName: String, gIndex: Long): Unit = {
      stats += (metaName -> (stats.getOrElse(metaName, 0: Long) + 1))
      superBackend.openSectionWithGIndex(metaName, gIndex)
    }
  }

  class StatsBackendInternal(
      val superBackend: ParserBackendInternal,
      statsUpdateCallback: Option[(StatsBackendBase) => Unit] = None,
      statsFinishedCallback: Option[(StatsBackendBase) => Unit] = None
  ) extends StatsBackendBase(statsUpdateCallback, statsFinishedCallback) with ParserBackendInternal {
    /**
     * opens a new section that had an identifier oldGIndex
     */
    override def openSection(metaName: String): Long = {
      stats += (metaName -> (stats.getOrElse(metaName, 0: Long) + 1))
      superBackend.openSection(metaName)
    }
  }

  def statsBackendInternal(
    superBackend: ParserBackendInternal,
    statsUpdateCallback: Option[(StatsBackendBase) => Unit] = None,
    statsFinishedCallback: Option[(StatsBackendBase) => Unit] = None
  ): StatsBackendInternal = {
    new StatsBackendInternal(superBackend, statsUpdateCallback, statsFinishedCallback)
  }

  def statsBackendExternal(
    superBackend: ParserBackendExternal,
    statsUpdateCallback: Option[(StatsBackendBase) => Unit] = None,
    statsFinishedCallback: Option[(StatsBackendBase) => Unit] = None
  ): StatsBackendExternal = {
    new StatsBackendExternal(superBackend, statsUpdateCallback, statsFinishedCallback)
  }

  def statsBackend(
    superBackend: ParserBackendBase,
    statsUpdateCallback: Option[(StatsBackendBase) => Unit] = None,
    statsFinishedCallback: Option[(StatsBackendBase) => Unit] = None
  ): StatsBackendBase = {
    superBackend match {
      case b: ParserBackendInternal =>
        statsBackendInternal(b, statsUpdateCallback, statsFinishedCallback)
      case b: ParserBackendExternal =>
        statsBackendExternal(b, statsUpdateCallback, statsFinishedCallback)
    }
  }
}
