/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.parsers

import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.Files
import java.nio.file.StandardCopyOption
import com.typesafe.scalalogging.StrictLogging
import eu.{ nomad_lab => lab }
import org.{ json4s => jn }
import eu.nomad_lab.h5.FileH5
import eu.nomad_lab.h5
import java.io.File
import org.json4s._
import org.json4s.JsonDSL._
import org.json4s.jackson.JsonMethods._

class CalculationBackendException(
  msg: String
) extends Exception(msg)

object CalculationBackendGenerator {
  type BackendCreate = (OptimizedParser, Path, Map[String, String]) => ParserBackendBase
}

/**
 * Backend generator that creates a single file per calculation
 */
class CalculationBackendGenerator(
    val targetPath: String,
    val targetTempPath: String = " ",
    val replacements: Map[String, String],
    val createBackend: CalculationBackendGenerator.BackendCreate,
    val overwrite: Boolean = false
) extends StrictLogging {

  def backendForCalculation(
    parser: OptimizedParser,
    mainFileUri: String,
    mainFilePath: Path,
    uncompressRoot: Path,
    repl: Map[String, String]
  ): Option[CalculationBackend] = {
    val expandedTargetPath = Paths.get(lab.LocalEnv.makeReplacements(repl, targetPath))
    val expandedTempTargetPath = Paths.get(lab.LocalEnv.makeReplacements(repl, targetTempPath))

    if (expandedTempTargetPath.toFile.exists() && !overwrite)
      return None
    Files.createDirectories(expandedTargetPath.getParent, lab.LocalEnv.directoryPermissionsAttributes)
    val tmpPath = Files.createTempFile(expandedTargetPath.getParent, repl("parsedFileGid").take(5), ".tmp", lab.LocalEnv.filePermissionsAttributes)
    val superBackend: ParserBackendBase = createBackend(parser, tmpPath, repl)
    val backend = superBackend match {
      case b: ParserBackendInternal =>
        new CalculationBackend(mainFileUri, mainFilePath, tmpPath, expandedTargetPath, repl, superBackend) with BaseParserBackend.ForwardBackendInternal {
          override def superBackend: ParserBackendInternal = b
        }
      case b: ParserBackendExternal =>
        new CalculationBackend(mainFileUri, mainFilePath, tmpPath, expandedTargetPath, repl, superBackend) with BaseParserBackend.ForwardBackendExternal {
          override def superBackend: ParserBackendExternal = b
        }
    }
    Some(backend)
  }
}

/**
 * Backend that creates a single file per calculation
 */
class CalculationBackend(
    val mainFileUri: String,
    val mainFilePath: java.nio.file.Path,
    val tmpPath: java.nio.file.Path,
    val targetPath: java.nio.file.Path,
    val repl: Map[String, String],
    superBackend0: ParserBackendBase
) extends BaseParserBackend.ForwardBackendBase {

  def superBackend: ParserBackendBase = superBackend0

  var pResult: Option[ParseResult.Value] = None
  var handledFile: Boolean = false

  override def startedParsingSession(
    mainFileUri: Option[String],
    parserInfo: jn.JValue,
    parserStatus: Option[ParseResult.Value] = None,
    parserErrors: jn.JValue = jn.JNothing
  ): Unit = {
    pResult = parserStatus
    superBackend.startedParsingSession(mainFileUri, parserInfo, parserStatus, parserErrors)
  }

  override def finishedParsingSession(
    parserStatus: Option[ParseResult.Value],
    parserErrors: jn.JValue = jn.JNothing,
    mainFileUri: Option[String] = None,
    parserInfo: jn.JValue = jn.JNothing,
    parsingStats: Map[String, Long] = Map()
  ): Unit = {
    try {
      superBackend.finishedParsingSession(parserStatus, parserErrors, mainFileUri, parserInfo, parsingStats)
    } finally {
      val parsingResult = pResult match {
        case None =>
          parserStatus match {
            case None =>
              throw new CalculationBackendException(s"No parse result parsing $mainFileUri")
            case Some(r) => r
          }
        case Some(r) => r
      }
      if (targetPath.getFileName.toString.endsWith(".h5")) {
        //val lockPath = targetPath.getParent.resolve(targetPath.getFileName.toString + ".lock")
        //val lockF = new java.io.RandomAccessFile(lockPath.toFile(), "rw")
        //lockF.getChannel().lock()
        var targetF: FileH5 = null
        try {
          parsingResult match {
            case ParseResult.ParseSuccess | ParseResult.ParseWithWarnings =>
              targetF = if (!Files.exists(targetPath))
                h5.FileH5.create(targetPath, acquired = false)
              else
                h5.FileH5.open(targetPath, write = true, acquired = false)
              val sourceF = h5.FileH5.open(tmpPath)
              lab.h5.Merge.fileInFile(sourceF, targetF)
              sourceF.release()
            case ParseResult.ParseFailure =>
              // Files.delete(tmpPath) // should be logged in the hdf5
              Files.move(tmpPath, Paths.get(targetPath.toString() + ".failed"), StandardCopyOption.ATOMIC_MOVE)
            case ParseResult.ParseSkipped =>
              Files.delete(tmpPath)
          }
        } finally {
          //if (targetF != null)
          //  targetF.flush()
          //lockF.close()
          if (targetF != null)
            targetF.release()
        }
      } else {
        parsingResult match {
          case ParseResult.ParseSuccess | ParseResult.ParseWithWarnings =>
            Files.move(tmpPath, targetPath, StandardCopyOption.ATOMIC_MOVE)
          case ParseResult.ParseFailure =>
            Files.move(tmpPath, Paths.get(targetPath.toString() + ".failed"), StandardCopyOption.ATOMIC_MOVE)

          case ParseResult.ParseSkipped =>
            Files.delete(tmpPath)
        }
      }
      handledFile = true
    }
  }

  override def cleanup(): Unit = {
    try {
      superBackend.cleanup()
    } finally {
      if (!handledFile) {
        handledFile = true
        Files.delete(tmpPath)
      }
    }
  }
}
