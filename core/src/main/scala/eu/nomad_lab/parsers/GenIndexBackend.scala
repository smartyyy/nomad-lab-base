/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.parsers;
import ucar.ma2.{ Array => NArray }
import scala.collection.mutable
import eu.nomad_lab.meta.MetaInfoEnv
import org.json4s.{ JNothing, JNull, JBool, JDouble, JDecimal, JInt, JString, JArray, JObject, JValue, JField }

/**
 * Backend that generates unique indexes for sections
 *
 * Converts and external backend to an internal one
 */
class GenIndexBackend(val subParser: ParserBackendExternal) extends ParserBackendInternal {
  def metaInfoEnv: MetaInfoEnv = subParser.metaInfoEnv

  class OpenSectionUsageException(msg: String) extends Exception(msg) {}

  val lastIndex: mutable.Map[String, Long] = mutable.Map()

  def backendInfo: org.json4s.JValue = {
    JObject(
      ("backendType" -> JString(getClass().getName())) ::
        ("superBackend" -> subParser.backendInfo) :: Nil
    )
  }

  def cleanup(): Unit = {
    subParser.cleanup()
  }

  /**
   * Started a parsing session
   */
  def startedParsingSession(
    mainFileUri: Option[String],
    parserInfo: JValue,
    parserStatus: Option[ParseResult.Value] = None,
    parserErrors: JValue = JNothing
  ): Unit = {
    if (!lastIndex.isEmpty)
      throw new ParserBackendBase.InvalidCallSequenceException("startParsingSession called when GenIndexBackend.lastIndex is not empty (meaning open session)")
    subParser.startedParsingSession(mainFileUri, parserInfo, parserStatus, parserErrors)
  }

  /**
   * Finished a parsing session
   */
  def finishedParsingSession(
    parserStatus: Option[ParseResult.Value],
    parserErrors: JValue = JNothing,
    mainFileUri: Option[String],
    parserInfo: JValue,
    parsingStats: Map[String, Long] = Map()
  ): Unit = {
    subParser.finishedParsingSession(parserStatus, parserErrors, mainFileUri, parserInfo, parsingStats)
    lastIndex.clear()
  }

  /**
   * returns the sections that are still open
   *
   * sections are identified by name of the meta info and their gIndex
   */
  def openSections(): Iterator[(String, Long)] = subParser.openSections()

  /**
   * returns information on an open section (for debugging purposes)
   */
  def sectionInfo(metaName: String, gIndex: Long): String = {
    subParser.sectionInfo(metaName, gIndex)
  }

  /**
   * opens a new section returning a new identifier for it
   */
  def openSection(metaName: String): Long = {
    val next: Long = lastIndex.getOrElse(metaName, 0: Long) + 1
    lastIndex.update(metaName, next)
    subParser.openSectionWithGIndex(metaName, next)
    next
  }

  /**
   * sets info values of an open section.
   *
   * references should be references to oldGIndex of the root sections this section refers to.
   */
  def setSectionInfo(metaName: String, gIndex: Long, references: Map[String, Long]): Unit = {
    subParser.setSectionInfo(metaName, gIndex, references)
  }

  /**
   * closes a section
   *
   * after this no other value can be added to the section.
   * metaName is the name of the meta info, gIndex the index of the section
   */
  def closeSection(metaName: String, gIndex: Long): Unit = {
    subParser.closeSection(metaName, gIndex)
  }

  /**
   * Adds a repating value to the section the value is in
   *
   * metaName is the name of the meta info, it should have repating=true
   * meaning that there can be multiple values in the same section
   */
  def addValue(metaName: String, value: JValue, gIndex: Long = -1): Unit = {
    subParser.addValue(metaName, value, gIndex)
  }

  /**
   * Adds a repeating floating point value
   */
  def addRealValue(metaName: String, value: Double, gIndex: Long = -1): Unit = {
    subParser.addRealValue(metaName, value, gIndex)
  }

  /**
   * Adds a new array of the given size
   */
  def addArray(metaName: String, shape: Seq[Long], gIndex: Long = -1): Unit = {
    subParser.addArray(metaName, shape, gIndex)
  }

  /**
   * Adds values to the last array added
   */
  def setArrayValues(metaName: String, values: NArray, offset: Option[Seq[Long]], gIndex: Long = -1): Unit = {
    subParser.setArrayValues(metaName, values, offset, gIndex)
  }

  /**
   * Adds an array value with the given array values
   */
  def addArrayValues(metaName: String, values: NArray, gIndex: Long = -1): Unit = {
    subParser.addArrayValues(metaName, values, gIndex)
  }
}
