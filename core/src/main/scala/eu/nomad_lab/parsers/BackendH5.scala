/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.parsers

import eu.nomad_lab.h5.ArchiveSetH5
import eu.nomad_lab.h5.SectionH5
import eu.nomad_lab.h5.SectionTableH5
import eu.nomad_lab.h5.ValueTableH5
import eu.nomad_lab.h5.ValueIndexes
import eu.nomad_lab.h5.CalculationH5
import scala.collection.mutable
import eu.nomad_lab.resolve._
import eu.nomad_lab.ref._
import eu.nomad_lab.meta
import eu.nomad_lab.LocalEnv
import eu.nomad_lab.JsonUtils
import org.{ json4s => jn }
import ucar.ma2
import scala.collection.breakOut
import scala.util.control.NonFatal
import com.typesafe.scalalogging.StrictLogging

object BackendH5 {
  /**
   * Exception in backend
   */
  class BackendException(
    msg: String
  ) extends Exception(msg)
}

object ProcessorLogLevel extends Enumeration {
  type ProcessorLogLevel = Value
  val OFF = Value(0)
  val FATAL = Value(100)
  val ERROR = Value(200)
  val WARN = Value(300)
  val INFO = Value(400)
  val DEBUG = Value(500)
  val TRACE = Value(600)
}

class BackendH5(
    archiveSet: ArchiveSetH5
) extends ParserBackendBase with ParserBackendInternal with StrictLogging {
  import BackendH5.BackendException
  var currentCalc: Option[Calculation] = None
  var calcFromParsingSession: Boolean = false
  var context: Option[ResolvedRef] = None
  val _openSections = mutable.Map[String, Long]()
  var openSectionsAtContextStart = Map[String, Long]()
  var lastParserInfo: jn.JValue = jn.JNothing
  var lastParserStatus: Option[ParseResult.Value] = None
  val lastArray = mutable.Map[String, ValueIndexes]()

  val _parentSectionName = mutable.Map[String, String]()

  /**
   * returns the name of the parent section (cached)
   */
  def parentSectionName(metaName: String): Option[String] = {
    _parentSectionName.get(metaName) match {
      case Some(n) => Some(n)
      case None =>
        metaInfoEnv.parentSectionName(metaName) match {
          case Some(pName) =>
            _parentSectionName += (metaName -> pName)
            Some(pName)
          case None =>
            None
        }
    }
  }

  val _sectionTable = mutable.Map[String, SectionTableH5]()
  /**
   * returns the section table using only name, not the full path
   * (cached)
   */
  def sectionTable(metaName: String): SectionTableH5 = {
    _sectionTable.get(metaName) match {
      case Some(table) => table
      case None =>
        val newTable = parentSectionName(metaName) match {
          case Some(parentName) =>
            val parentTable = sectionTable(parentName)
            parentTable.subSectionTable(metaName)
          case None =>
            calculation.subSectionTable(metaName)
        }
        _sectionTable += (metaName -> newTable)
        newTable
    }
  }

  val _valueTable = mutable.Map[String, ValueTableH5]()
  /**
   * returns the value table using only its name not the full path
   * (cached)
   */
  def valueTable(metaName: String): ValueTableH5 = {
    _valueTable.get(metaName) match {
      case Some(v) => v
      case None =>
        parentSectionName(metaName) match {
          case None =>
            throw new BackendException(s"Error for value $metaName, failed to get parent section")
          case Some(parentName) =>
            val parentTable = sectionTable(parentName)
            val newTable = parentTable.subValueTable(metaName)
            _valueTable += (metaName -> newTable)
            newTable
        }
    }
  }

  /**
   * The metaInfoEnv this parser was optimized for
   */
  override def metaInfoEnv: meta.MetaInfoEnv = {
    archiveSet.metaInfoEnv
  }

  /**
   * Adds the given section to the open sections
   */
  def addSection(sectionName: String, gIndex: Long): Unit = {
    if (_openSections.contains(sectionName))
      throw new BackendException(s"overlapping opening of section ${sectionName} not supported by this backend, open now: $gIndex and ${_openSections(sectionName)}")
    _openSections += (sectionName -> gIndex)
  }

  /**
   * Returns the uri of the current context as string
   */
  def contextUriStr: String = {
    context match {
      case None => ""
      case Some(ctx) => ctx.uriString
    }
  }

  /**
   * Opens the given context, only one context can be open at a time
   */
  override def openContext(nomadUri: NomadUri): Unit = {
    openContext(nomadUri.toRef)
  }

  val supportSections = Set("section_processor_log", "section_processor_info")

  /**
   * Opens a calculation (to correctly log the events)
   *
   * Should be the first call, but can be automatically called if
   * startedParsingSession has an uri that gives the calculation as
   * mainFileUri
   */
  def openCalculation(calc: CalculationRef): Unit = {
    currentCalc match {
      case Some(c) => throw new BackendException(s"openCalculation $calc called with already open calculation $c")
      case None =>
        if (!context.isEmpty || !_openSections.isEmpty)
          throw new BackendException(s"openCalculation called with context $context and openSections ${_openSections.mkString("[", ",", "]")}")
        Resolver.resolveInArchiveSet(archiveSet, calc) match {
          case c: Calculation =>
            currentCalc = Some(c)
            val logTable = calculation.subSectionTable("section_processor_log")
            val logSection = logTable.lengthL match {
              case 0L =>
                logTable(logTable.addSection(None))
              case n =>
                logTable(n - 1)
            }
            addSection(logSection.metaName, logSection.gIndex)
          case r =>
            throw new BackendException(s"openCalculation could not open calculation $calc in openCalculation, got $r")
        }
    }
  }

  /**
   * Closes a calculation
   *
   * Should be the last call, but can be called automatically by
   * finishedParsingSession when openCalculation was called automatically
   * (calcFromParsingSession == true)
   */
  def closeCalculation(calc: CalculationRef): Unit = {
    currentCalc match {
      case Some(c) =>
        currentCalc = None
        if (c.calculation.toRef != calc)
          throw new BackendException(s"mismatched closeCalculation $calc with open calculation $c")
        if (!context.isEmpty)
          throw new BackendException(s"both context ($context) and currentCalc ($calc) are present in finishedParsingsession (invalid nesting)")
        _openSections.get("section_processor_log") match {
          case Some(idx) =>
            closeSection("section_processor_log", idx)
          case None => ()
        }
        _openSections.get("section_processor_info") match {
          case Some(idx) =>
            closeSection("section_processor_info", idx)
          case None => ()
        }
        _valueTable.clear
        _sectionTable.clear
        c.giveBack()
        if (!_openSections.isEmpty)
          throw new BackendException(s"Sections ${openSections.map(_._1).mkString("[", ",", "]")} where still open after closeCalculation")
      case None =>
        throw new BackendException(s"mismatched closeCalculation $calc without an open calculation")
    }
    calcFromParsingSession = false
  }

  /**
   * Opens the given context, only one context can be open at a time
   *
   * Should be called only after startedParsingSession
   */
  def openContext(nomadRef: NomadRef): Unit = {
    context match {
      case Some(ctx) =>
        throw new BackendException(s"Multiple contexts open at once: $ctx and $nomadRef")
      case None =>
        val ctx = Resolver.resolveInArchiveSet(archiveSet, nomadRef)
        openSectionsAtContextStart = _openSections.toMap
        if (!ctx.valid)
          throw new BackendException(s"Error opening context $nomadRef: $ctx")
        context = Some(ctx)
        ctx match {
          case Section(_, sect) =>
            var sectNow: Option[SectionH5] = Some(sect)
            var hasMore: Boolean = true
            while (!sectNow.isEmpty) {
              sectNow match {
                case Some(s) =>
                  addSection(s.metaName, s.gIndex)
                  sectNow = s.parentSection
                case None =>
                  hasMore = false
              }
            }
          case Calculation(_, calc) => ()
          case v =>
            throw new BackendException(s"Expected a calculation or a section as context, not $v")
        }
    }
    if (ParserBackendBase.trace)
      processorLog(s"opened context $contextUriStr")
  }

  override def closeContext(nomadUri: NomadUri): Unit = {
    closeContext(nomadUri.toRef)
  }

  def closeContext(nomadRef: NomadRef): Unit = {
    context match {
      case Some(ctx) =>
        val compatibleClose: Boolean = NomadUri(ctx.uriString).toRef match {
          case sRefOpen: RowRef =>
            nomadRef match {
              case sRefClose: RowRef =>
                (sRefOpen.archiveGid == sRefClose.archiveGid &&
                  sRefOpen.calculationGid == sRefClose.calculationGid &&
                  sRefOpen.metaPath == sRefClose.metaPath)
              case _ => false
            }
          case cRefOpen: CalculationRef =>
            nomadRef match {
              case cRefClose: CalculationRef =>
                (cRefOpen.archiveGid == cRefClose.archiveGid &&
                  cRefOpen.calculationGid == cRefClose.calculationGid)
              case _ => false
            }
          case t =>
            throw new BackendException(s"Unexpected context type $t")
        }
        if (!compatibleClose)
          throw new BackendException(s"Incompatible context close of $nomadRef with context $ctx")
        ctx match {
          case Section(_, sect) =>
            var sectNow: Option[SectionH5] = Some(sect)
            var hasMore: Boolean = true
            while (!sectNow.isEmpty) {
              sectNow match {
                case Some(s) =>
                  val shouldClose = !openSectionsAtContextStart.contains(s.metaName)
                  if (shouldClose && _openSections.remove(s.metaName).isEmpty)
                    throw new BackendException(s"Context section ${s.metaName} was closed manually before the context $nomadRef")
                  sectNow = s.parentSection
                case None =>
                  hasMore = false
              }
            }
          case _ => ()
        }
        val extraSections = _openSections.flatMap {
          case (s, newVal) =>
            if (supportSections(s)) {
              None
            } else {
              openSectionsAtContextStart.get(s) match {
                case None => Some(s -> newVal)
                case Some(oldV) =>
                  if (oldV != newVal)
                    Some(s -> newVal)
                  else
                    None
              }
            }
        }
        if (!extraSections.isEmpty)
          throw new BackendException(s"There are new non supporting open sections in closeContext that were not open when opening the context $nomadRef: ${extraSections.keys.mkString("[", ",", "]")}")
        // removing tables depending on context...
        val vToRm = _valueTable.flatMap {
          case (metaName, vTable) =>
            if (openSectionsAtContextStart.contains(vTable.parentSectionTable.name))
              None
            else
              Some(metaName)
        }
        _valueTable --= vToRm
        val sToRm = _sectionTable.flatMap {
          case (metaName, sTable) =>
            if (openSectionsAtContextStart.contains(metaName))
              None
            else
              Some(metaName)
        }
        _sectionTable --= sToRm
        ctx.giveBack()
        if (ParserBackendBase.trace)
          processorLog(s"closed context $nomadRef")
        context = None
      case None =>
        throw new BackendException(s"close context $nomadRef called without an open context")
    }
  }

  /**
   * Returns the calculation of the current context
   */
  def calculation: CalculationH5 = {
    context match {
      case Some(Section(_, sect)) => sect.table.calculation
      case Some(Calculation(_, calc)) => calc
      case Some(c) => throw new BackendException(s"Asked calculation with context $c")
      case None =>
        currentCalc match {
          case Some(Calculation(_, calc)) => calc
          case None =>
            throw new BackendException(s"Asked calculation without an open calculation")
        }
    }
  }

  /**
   * Increments the counter valueName in parentSection.
   * This is low level an internal, data normally is supposed to be immutable.
   * processing statistics is an exception to this in HDF5 (for space efficiency reasons)
   */
  def increment(parentSection: SectionH5, valueName: String): Unit = {
    parentSection.maybeValue(valueName) match {
      case None =>
        parentSection.addValue(valueName, jn.JInt(1))
      case Some(v) =>
        val arr = new ma2.ArrayLong.D1(1)
        arr.setLong(0, v.longValue + 1)
        v.table.setArrayValues(arr, indexes = v.table.valueIndexes(v.gIndex))
    }
  }

  /**
   * Logs some info of the processor currently running
   */
  def processorLog(msg: String, logLevel: org.apache.logging.log4j.Level): Unit = {
    processorLog(msg, logLevel.intLevel())
  }

  def processorLog(msg: String, logLevel: ProcessorLogLevel.Value = ProcessorLogLevel.INFO): Unit = {
    processorLog(msg, logLevel.id)
  }

  def processorLog(msg: String, logLevel: Int): Unit = {
    logger.debug(s"<processorLog>$logLevel: $msg")
    val eventId = openSection("section_processor_log_event")
    addValue("processor_log_event_level", jn.JInt(logLevel))
    addValue("processor_log_event_message", jn.JString(msg))
    closeSection("section_processor_log_event", eventId)
  }

  /**
   * Started a processing session.
   * This must either follow the openCalculation or have a nomad uri that defines also
   * the calculation to be opened. In this case this opens the calculation, and
   * finishedParsingSession will close that calculation.
   */
  override def startedParsingSession(
    mainFileUri: Option[String],
    parserInfo: jn.JValue,
    parserStatus: Option[ParseResult.Value] = None,
    parserErrors: jn.JValue = jn.JNothing
  ): Unit = {
    if (!context.isEmpty)
      throw new BackendException(s"startedParsingSession $mainFileUri called after opening context $context")
    if (!_openSections.filter({ case (s, _) => !supportSections(s) }).isEmpty)
      throw new BackendException(s"There are non supporting open sections when calling startedParsingSession $mainFileUri: ${_openSections.keys.mkString("[", ",", "]")}")

    val calc = currentCalc match {
      case None =>
        val uri: Option[NomadUri] = try {
          mainFileUri.map({ s: String => NomadUri(s) })
        } catch {
          case NonFatal(e) => None
        }

        uri foreach { uriObj: NomadUri =>
          uriObj.objectKind match {
            case ObjectKind.RawData =>
              None // use CompactSha to calculate calculation ref?
            case ObjectKind.ParsedData | ObjectKind.NormalizedData =>
              val calcRef = uriObj.toRef match {
                case t: TableRef => t.calculationRef
                case r: RowRef => r.calculationRef
                case c: CalculationRef => c
                case ArchiveRef(a) =>
                  CalculationRef(a, "_")
                case r: RawDataRef =>
                  throw new BackendException(s"startedParsingSession raw ref $r called without calling openCalculation before")
              }
              openCalculation(calcRef)
              calcFromParsingSession = true
          }
        }
        currentCalc match {
          case Some(c) => c
          case None => throw new BackendException(s"startedParsingSession called without openCalculation, and could not infer it from $mainFileUri")
        }
      case Some(c) => c
    }

    if (ParserBackendBase.trace) {
      val logMsg = "startedParsingSession " + JsonUtils.normalizedStr(jn.JObject(
        ("mainFileUri" -> (mainFileUri match {
          case Some(str) => jn.JString(str)
          case None => jn.JNothing
        })) ::
          ("parserInfo" -> parserInfo) ::
          ("parserStatus" -> (parserStatus match {
            case None => jn.JNothing
            case Some(stat) => jn.JString(stat.toString)
          })) ::
          ("parserErrors" -> parserErrors) :: Nil
      ))
      processorLog(logMsg)
    }

    emitParsingInfo(mainFileUri, parserInfo, parserStatus, parserErrors, false)
  }

  /**
   * Internal method emitting processor info
   */
  def emitParsingInfo(
    mainFileUri: Option[String],
    parserInfo: jn.JValue,
    parserStatus: Option[ParseResult.Value] = None,
    parserErrors: jn.JValue = jn.JNothing,
    isClose: Boolean = false
  ): Unit = {
    val emitCached: Boolean = (jn.JNothing == lastParserInfo && jn.JNothing != parserInfo)
    if (emitCached) {
      lastParserInfo = parserInfo
      val pid = parserInfo \ "parserId"
      pid match {
        case jn.JString(processorId) =>
          val pidTable = calculation.valueTable(Seq("section_processor_info", "processor_id"))
          var processorInfoSection: Option[SectionH5] = None
          for (value <- pidTable) {
            if (value.stringValue == processorId)
              processorInfoSection = Some(value.parentSection)
          }
          val pInfoSection = processorInfoSection match {
            case None =>
              val pInfoTable = calculation.subSectionTable("section_processor_info")
              val s = pInfoTable(pInfoTable.addSection(None))
              addSection(s.metaName, s.gIndex)
              addValue("processor_id", jn.JString(processorId))
              s
            case Some(s) =>
              addSection(s.metaName, s.gIndex)
              s
          }
          increment(pInfoSection, "processor_number_of_evaluated_contexts")
        case v =>
          throw new BackendException(s"parserInfo should contain a string for the key parserId, not ${JsonUtils.prettyStr(v)} in $calculation")
      }
    }
    val canEmit = (jn.JNothing != lastParserInfo)
    if (isClose && !canEmit)
      throw new BackendException(s"still no parser info on close, cannot emit in $contextUriStr")
    var emitParserStatus: Boolean = emitCached
    lastParserStatus = lastParserStatus match {
      case None =>
        emitParserStatus = canEmit
        parserStatus
      case Some(s) => Some(s)
    }
    if (emitParserStatus) {
      lastParserStatus match {
        case None => ()
        case Some(stat) =>
          val pInfoSection = openSectionH5("section_processor_info")
          stat match {
            case ParseResult.ParseSuccess | ParseResult.ParseWithWarnings =>
              increment(pInfoSection, "processor_number_of_successful_contexts")
            case ParseResult.ParseSkipped =>
              increment(pInfoSection, "processor_number_of_skipped_contexts")
            case ParseResult.ParseFailure =>
              increment(pInfoSection, "processor_number_of_failed_contexts")
          }
      }
    }
    if (isClose && lastParserInfo == jn.JNothing)
      throw new BackendException(s"No parser status know on close in $contextUriStr")
  }

  /**
   * Finished a processing session.
   * If the calculation was opened by startedParsingSession this will close that calculation.
   */
  override def finishedParsingSession(
    parserStatus: Option[ParseResult.Value],
    parserErrors: jn.JValue = jn.JNothing,
    mainFileUri: Option[String] = None,
    parserInfo: jn.JValue = jn.JNothing,
    parsingStats: Map[String, Long] = Map()
  ): Unit = {
    if (!context.isEmpty)
      throw new BackendException(s"finishedParsingSession called with still open context $context")
    if (currentCalc.isEmpty)
      throw new BackendException(s"finishedParsingSession called without an open calculation")
    if (ParserBackendBase.trace) {
      val logMsg = "finishedParsingSession " + JsonUtils.normalizedStr(jn.JObject(
        ("mainFileUri" -> (mainFileUri match {
          case Some(str) => jn.JString(str)
          case None => jn.JNothing
        })) ::
          ("parserInfo" -> parserInfo) ::
          ("parserStatus" -> (parserStatus match {
            case None => jn.JNothing
            case Some(stat) => jn.JString(stat.toString)
          })) ::
          ("parserErrors" -> parserErrors) ::
          ("parsingStats" -> (if (parsingStats.isEmpty) {
            jn.JNothing
          } else {
            val statList: List[(String, jn.JValue)] = parsingStats.map {
              case (s, l) =>
                (s -> jn.JInt(l))
            }(breakOut)
            jn.JObject(statList)
          })) :: Nil
      ))
      processorLog(logMsg)
    }
    emitParsingInfo(mainFileUri, parserInfo, parserStatus, parserErrors, true)
    if (calcFromParsingSession)
      closeCalculation(currentCalc.get.calculation.toRef)
  }

  /**
   * hande match telemetry:
   *   - source data (e.g. code output lines)
   *   - match information (which information was extracted)
   * default implementation does nothing, concrete implementations
   * in backend are optional, in case the backend can handle match
   * telemetry data
   */
  override def matchTelemetry(
    gIndex: Long,
    matcherName: String,
    matcherGroup: List[String],
    fInLine: String,
    fInLineNr: Long,
    matchFlags: Long,
    matchSpansFlat: List[Long]
  ): Unit = {}

  /**
   * returns the sections that are still open
   *
   * sections are identified by metaName and their gIndex
   */
  override def openSections(): Iterator[(String, Long)] = {
    _openSections.iterator
  }

  /**
   * Returns a SectionH5 object for an open section
   */
  def openSectionH5(sectionName: String): SectionH5 = {
    _openSections.get(sectionName) match {
      case Some(idx) =>
        sectionTable(sectionName)(idx)
      case None =>
        throw new BackendException(s"section $sectionName is not open")
    }
  }

  /**
   * returns information on an open section (for debugging purposes)
   */
  override def sectionInfo(metaName: String, gIndex: Long): String = {
    _openSections.get(metaName) match {
      case None =>
        s"Non open section $metaName $gIndex"
      case Some(sec) =>
        if (sec == gIndex)
          s"Section $metaName $gIndex: ${openSectionH5(metaName)}"
        else
          s"Non open section $metaName $gIndex ($sec is open)"
    }
  }

  /**
   * opens a new section, returning a valid gIndex
   */
  override def openSection(metaName: String): Long = {
    _openSections.get(metaName) match {
      case None =>
        val t = sectionTable(metaName)
        val newGIndex = parentSectionName(metaName) match {
          case Some(parentName) =>
            _openSections.get(parentName) match {
              case Some(pSect) =>
                t.addSection(Some(pSect))
              case None =>
                throw new BackendException(s"Cannot open section $metaName with no open parent section $parentName")
            }
          case None =>
            t.addSection(None)
        }
        addSection(metaName, newGIndex)
        newGIndex
      case Some(openIdx) =>
        throw new BackendException(s"Cannot open section $metaName multiple times concurrently, currently opened $openIdx")
    }
  }

  /**
   * sets info values of an open section.
   *
   * references should be references to gIndex of the root sections this section refers to.
   */
  override def setSectionInfo(metaName: String, gIndex: Long, references: Map[String, Long]): Unit = {
    parentSectionName(metaName) match {
      case Some(pSection) =>
        references.get(pSection) match {
          case Some(parentIdx) =>
            if (parentIdx != openSectionH5(metaName).parentSection.get.gIndex)
              throw new BackendException(s"Invalid setSectionInfo, expected ${openSectionH5(metaName).parentSection.get.gIndex} but got $parentIdx when closing $metaName")
          case None =>
            throw new BackendException(s"Invalid setSectionInfo $gIndex on closed section $metaName, references $references misses parent $pSection")
        }
      case None =>
        ()
    }
  }

  /**
   * closes a section
   *
   * after this no other value can be added to the section.
   * metaName is the name of the meta info, gIndex the index of the section
   */
  override def closeSection(metaName: String, gIndex: Long): Unit = {
    _openSections.remove(metaName) match {
      case Some(idx) =>
        if (gIndex != -1 && idx != gIndex)
          throw new BackendException(s"closeSection $metaName was expected to close section $idx not $gIndex")
      case None =>
        throw new BackendException(s"closeSection $metaName called on closed section $gIndex")
    }
  }

  /**
   * Adds a json value corresponding to metaName.
   *
   * The value is added to the section the meta info metaName is in.
   * A gIndex of -1 means the latest section.
   */
  override def addValue(metaName: String, value: jn.JValue, gIndex: Long = -1): Unit = {
    valueTable(metaName).addValue(value, sectionGIndex = gIndex match {
      case -1 =>
        _openSections(parentSectionName(metaName).get)
      case i => i
    })
  }

  /**
   * Adds a floating point value corresponding to metaName.
   *
   * The value is added to the section the meta info metaName is in.
   * A gIndex of -1 means the latest section.
   */
  override def addRealValue(metaName: String, value: Double, gIndex: Long = -1): Unit = {
    valueTable(metaName).addRealValue(value, sectionGIndex = gIndex match {
      case -1 =>
        _openSections(parentSectionName(metaName).get)
      case i => i
    })
  }

  /**
   * Adds a new array of the given size corresponding to metaName.
   *
   * The value is added to the section the meta info metaName is in.
   * A gIndex of -1 means the latest section.
   * The array is unitialized.
   */
  override def addArray(metaName: String, shape: Seq[Long], gIndex: Long = -1): Unit = {
    val sectionIdx = parentSectionName(metaName) match {
      case Some(pName) =>
        _openSections.get(pName) match {
          case Some(idx) => idx
          case None => throw new BackendException(s"addArray $metaName failed as parent section $pName is closed")
        }
      case None => throw new BackendException(s"addArray $metaName could not get parent section name")
    }
    if (gIndex != -1 && gIndex != sectionIdx)
      throw new BackendException(s"addArray $metaName called on section $gIndex, while the open section is $sectionIdx")
    val arrayIdx = valueTable(metaName).addArray(shape, sectionGIndex = sectionIdx)
    lastArray += (metaName -> arrayIdx)
  }

  /**
   * Adds values to the last array added
   */
  override def setArrayValues(
    metaName: String, values: ma2.Array,
    offset: Option[Seq[Long]] = None,
    gIndex: Long = -1
  ): Unit = {
    val sectionIdx = parentSectionName(metaName) match {
      case Some(pName) =>
        _openSections.get(pName) match {
          case Some(idx) => idx
          case None => throw new BackendException(s"setArrayValues $metaName failed as parent section $pName is closed")
        }
      case None => throw new BackendException(s"setArrayValues $metaName could not get parent section name")
    }
    if (gIndex != -1 && gIndex != sectionIdx)
      throw new BackendException(s"setArrayValues $metaName called on section $gIndex, while the open section is $sectionIdx")
    lastArray.get(metaName) match {
      case Some(idx) =>
        valueTable(metaName).setArrayValues(values, offset, idx)
      case None =>
        throw new BackendException(s"setArrayValues $metaName called without a previous array")
    }
  }

  /**
   * Adds an array value with the given array values
   */
  def addArrayValues(metaName: String, values: ma2.Array, gIndex: Long = -1): Unit = {
    val sectionIdx = parentSectionName(metaName) match {
      case Some(pName) =>
        _openSections.get(pName) match {
          case Some(idx) => idx
          case None => throw new BackendException(s"setArrayValues $metaName failed as parent section $pName is closed")
        }
      case None => throw new BackendException(s"setArrayValues $metaName could not get parent section name")
    }
    if (gIndex != -1 && gIndex != sectionIdx)
      throw new BackendException(s"setArrayValues $metaName called on section $gIndex, while the open section is $sectionIdx")
    valueTable(metaName).addArrayValues(values, sectionIdx)
  }

  /**
   * Cleans up resources
   */
  def cleanup(): Unit = {
  }

  /** Returns backend information */
  def backendInfo: jn.JValue = {
    jn.JObject(
      ("backendType" -> jn.JString(getClass().getName())) :: Nil
    )
  }
}
