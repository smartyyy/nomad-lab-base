/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.parsers;
import com.typesafe.scalalogging.StrictLogging
import com.typesafe.config.{ Config, ConfigFactory }
import ucar.ma2.{ Array => NArray }
import ucar.ma2.{ IndexIterator => NIndexIterator }
import ucar.ma2.DataType
import ucar.ma2.ArrayString
import scala.collection.breakOut
import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import eu.nomad_lab.JsonUtils
import eu.nomad_lab.ref.NomadUri
import eu.nomad_lab.meta.MetaInfoEnv
import eu.nomad_lab.meta.MetaInfoRecord
import java.io.Writer
import org.json4s.{ JNothing, JNull, JBool, JDouble, JDecimal, JInt, JString, JArray, JObject, JValue, JField }
import org.json4s.CustomSerializer
import scala.util.control.NonFatal

object ParseEvent extends StrictLogging {
  class ParseEventParseError(
    msg: String, what: Throwable
  ) extends Exception(msg, what)

  /**
   * builds an array of the given size looking up the type from the backend metaInfo
   */
  def buildArray(backend: Option[ParserBackendExternal], metaName: String, valuesShape: Seq[Long], flatValues: List[JValue]): NArray = {
    import org.json4s.DefaultFormats;
    implicit val formats = DefaultFormats

    var dtypeStr: String = ""
    if (!backend.isEmpty && !metaName.isEmpty) {
      backend.get.metaInfoEnv.metaInfoRecordForName(metaName) match {
        case Some(metaI) =>
          metaI.dtypeStr match {
            case Some(dt) =>
              dtypeStr = dt
            case None =>
              ()
          }
        case None =>
          ()
      }
    }
    if (dtypeStr.isEmpty) {
      val size = if (!valuesShape.isEmpty)
        valuesShape.foldLeft(1: Long)(_ * _)
      else
        flatValues.size.longValue
      if (size > 0) {
        dtypeStr = flatValues(0) match {
          case _: JInt =>
            "i64"
          case _: JDouble =>
            "f64"
          case _: JString =>
            "C"
          case _: JBool =>
            "b"
          case _: JObject =>
            "D"
          case _ =>
            throw new GenericBackend.InternalErrorException(s"cannot recover dtypeStr for $metaName from ${flatValues(0)}")
        }
      }
    }
    val ishape: Array[Int] = valuesShape.map(_.intValue).toArray
    val newArr = dtypeStr match {
      case "i" | "i32" =>
        NArray.factory(DataType.INT, ishape)
      case "i64" | "r" =>
        NArray.factory(DataType.LONG, ishape)
      case "f" | "f64" =>
        NArray.factory(DataType.DOUBLE, ishape)
      case "f32" =>
        NArray.factory(DataType.FLOAT, ishape)
      case "b" =>
        NArray.factory(DataType.BOOLEAN, ishape)
      case "B" =>
        NArray.factory(DataType.STRING, ishape)
      case "C" =>
        NArray.factory(DataType.STRING, ishape)
      case "D" =>
        NArray.factory(DataType.STRING, ishape)
      case _ =>
        throw new GenericBackend.InternalErrorException(s"unsupported dtypeStr $dtypeStr for $metaName")
    }
    val writer = dtypeStr match {
      case "i" | "i32" =>
        { (it: NIndexIterator, value: JValue) =>
          it.setIntNext(value.extract[Int])
        }
      case "i64" | "r" =>
        { (it: NIndexIterator, value: JValue) =>
          it.setLongNext(value.extract[Long])
        }
      case "f" | "f64" =>
        { (it: NIndexIterator, value: JValue) =>
          it.setDoubleNext(value.extract[Double])
        }
      case "f32" =>
        { (it: NIndexIterator, value: JValue) =>
          it.setFloatNext(value.extract[Float])
        }
      case "b" =>
        { (it: NIndexIterator, value: JValue) =>
          it.setBooleanNext(value.extract[Boolean])
        }
      case "B" =>
        { (it: NIndexIterator, value: JValue) =>
          it.setObjectNext(value.extract[String])
        }
      case "C" =>
        { (it: NIndexIterator, value: JValue) =>
          it.setObjectNext(value.extract[String])
        }
      case "D" =>
        { (it: NIndexIterator, value: JValue) =>
          value match {
            case obj: JObject =>
              it.setObjectNext(JsonUtils.prettyStr(obj))
            case _ =>
              throw new JsonUtils.InvalidValueError(
                fieldName = metaName,
                context = "",
                value = JsonUtils.prettyStr(value),
                expected = "json dictionary"
              )
          }
        }
      case _ =>
        throw new GenericBackend.InternalErrorException(s"Unexpected dtypeStr $dtypeStr for $metaName")
    }
    var l = flatValues

    val iter = newArr.getIndexIterator()
    while (iter.hasNext() && !l.isEmpty) {
      l match {
        case h :: t =>
          writer(iter, h)
          l = t
        case Nil =>
          ()
      }
    }
    newArr
  }

  /**
   * converts back from json
   */
  def fromJValue(backend: Option[ParserBackendExternal], value: JValue)(implicit format: org.json4s.Formats): ParseEvent = {
    value match {
      case JObject(obj) => {
        var event: String = "";
        var metaName: String = "";
        var gIndex: Long = -1;
        var shape: Option[Seq[Long]] = None;
        var valuesShape: Seq[Long] = Seq();
        var offset: Option[Seq[Long]] = None;
        var flatValues: List[JValue] = Nil;
        var references: Map[String, Long] = Map();
        var mainFileUri: Option[String] = None;
        var parserInfo: JValue = JNothing;
        var parserStatus: Option[ParseResult.Value] = None;
        var parserErrors: JValue = JNothing;
        var jsonValue: JValue = JNothing;
        var nomadUri: Option[String] = None

        // additional stuff for parsing matchTelemetry events
        var fInLine: String = "";
        var fInLineNr: Long = -1;
        var matcherName: String = "";
        var matcherGroup: List[String] = Nil;
        var matchFlags: Long = 0;
        var matchSpansFlat: List[Long] = Nil;
        var parsingStats: Map[String, Long] = Map();
        try {
          obj foreach {
            case JField("event", value) =>
              value match {
                case JString(s) => event = s
                case JNothing | JNull => ()
                case _ => throw new JsonUtils.InvalidValueError(
                  "event", "ParseEvent", JsonUtils.prettyStr(value), "a string"
                )
              }
            case JField("metaName", value) =>
              value match {
                case JString(s) => metaName = s
                case JNothing | JNull => ()
                case _ => throw new JsonUtils.InvalidValueError(
                  "metaName", "ParseEvent", JsonUtils.prettyStr(value), "a string"
                )
              }
            case JField("gIndex", value) =>
              value match {
                case JInt(i) => gIndex = i.longValue
                case JDecimal(i) => gIndex = i.longValue
                case JNothing | JNull => ()
                case _ => throw new JsonUtils.InvalidValueError(
                  "gIndex", "NomadMetaInfo", JsonUtils.prettyStr(value), "a string"
                )
              }
            case JField("value", value) =>
              jsonValue = value
            case JField("shape", value) =>
              if (!value.toOption.isEmpty)
                shape = Some(value.extract[Seq[Long]])
            case JField("valuesShape", value) =>
              if (!value.toOption.isEmpty)
                valuesShape = value.extract[Seq[Long]]
            case JField("offset", value) =>
              if (!value.toOption.isEmpty)
                offset = Some(value.extract[Seq[Long]])
            case JField("flatValues", value) =>
              if (!value.toOption.isEmpty)
                value match {
                  case JArray(arr) =>
                    flatValues = arr
                  case JNothing | JNull =>
                    ()
                  case _ =>
                    throw new JsonUtils.InvalidValueError(
                      "flatValues", "ParseEvent", JsonUtils.prettyStr(value), "an array"
                    )
                }
            case JField("references", value) =>
              if (!value.toOption.isEmpty)
                references = value.extract[Map[String, Long]]
            case JField("mainFileUri", value) =>
              value match {
                case JString(s) => mainFileUri = Some(s)
                case JNothing | JNull => ()
                case _ => throw new JsonUtils.InvalidValueError(
                  "mainFileUri", "ParseEvent", JsonUtils.prettyStr(value), "a string"
                )
              }
            case JField("nomadUri", value) =>
              value match {
                case JString(s) => nomadUri = Some(s)
                case JNothing | JNull =>
                  ()
                case _ =>
                  throw new JsonUtils.InvalidValueError(
                    "nomadUri", "Context", JsonUtils.prettyStr(value), "a string"
                  )
              }
            case JField("parserInfo", value) =>
              parserInfo = value
            case JField("parserStatus", value) =>
              value match {
                case JString(s) =>
                  parserStatus = Some(ParseResult.withName(s))
                case JNothing | JNull => ()
                case _ => throw new JsonUtils.InvalidValueError(
                  "parserStatus", "ParseEvent", JsonUtils.prettyStr(value), s"a string value that is one of ${ParseResult.values.mkString("[", ",", "]")}"
                )
              }
            case JField("parserErrors", value) =>
              parserErrors = value
            // additional stuff for parsing matchTelemetry events
            case JField("fInLine", value) =>
              value match {
                case JString(s) => fInLine = s
                case JNothing | JNull => ()
                case _ => throw new JsonUtils.InvalidValueError(
                  "fInLine", "ParseEvent", JsonUtils.prettyStr(value), "a string"
                )
              }
            case JField("fInLineNr", value) =>
              value match {
                case JInt(i) => fInLineNr = i.longValue
                case _ => throw new JsonUtils.InvalidValueError(
                  "fInLineNr", "NomadMetaInfo", JsonUtils.prettyStr(value), "an integer"
                )
              }
            case JField("matcherName", value) =>
              value match {
                case JString(s) => matcherName = s
                case JNothing | JNull => ()
                case _ => throw new JsonUtils.InvalidValueError(
                  "matcherName", "ParseEvent", JsonUtils.prettyStr(value), "a string"
                )
              }
            case JField("matcherGroup", value) =>
              value match {
                case JArray(arr) =>
                  matcherGroup = value.extract[List[String]]
                case _ =>
                  throw new JsonUtils.InvalidValueError(
                    "matcherGroup", "ParseEvent", JsonUtils.prettyStr(value), "an array"
                  )
              }
            case JField("matchFlags", value) =>
              value match {
                case JInt(i) => matchFlags = i.longValue
                case _ => throw new JsonUtils.InvalidValueError(
                  "matchFlags", "NomadMetaInfo", JsonUtils.prettyStr(value), "an integer"
                )
              }
            case JField("matchSpansFlat", value) =>
              value match {
                case JArray(arr) =>
                  matchSpansFlat = value.extract[List[Long]]
                case _ =>
                  throw new JsonUtils.InvalidValueError(
                    "matcherSpansFlat", "ParseEvent", JsonUtils.prettyStr(value), "an array"
                  )
              }
            case JField("parsingStats", value) =>
              value match {
                case JObject(obj) =>
                  parsingStats = value.extract[Map[String, Long]]
                case _ =>
                  throw new JsonUtils.InvalidValueError(
                    "parsingStats", "ParseEvent", JsonUtils.prettyStr(value), "a dictionary"
                  )
              }
            case JField(field, value) =>
              throw new JsonUtils.UnexpectedFieldError("ParseEvent", field, value)
          }
          event match {
            case "openContext" =>
              nomadUri match {
                case Some(u) => OpenContext(NomadUri(u))
                case None => throw new JsonUtils.InvalidValueError(
                  "nomadUri", "openContext", "*missing*", "a string"
                )
              }
            case "closeContext" =>
              nomadUri match {
                case Some(u) => CloseContext(NomadUri(u))
                case None => throw new JsonUtils.InvalidValueError(
                  "nomadUri", "openContext", "*missing*", "a string"
                )
              }
            case "startedParsingSession" =>
              StartedParsingSession(mainFileUri, parserInfo, parserStatus, parserErrors)
            case "finishedParsingSession" =>
              FinishedParsingSession(parserStatus, parserErrors, mainFileUri, parserInfo, parsingStats)
            case "setSectionInfo" =>
              SetSectionInfo(metaName, gIndex, references)
            case "closeSection" =>
              CloseSection(metaName, gIndex)
            case "addValue" =>
              AddValue(metaName, jsonValue, gIndex)
            case "addRealValue" =>
              jsonValue match {
                case JDouble(d) =>
                  AddRealValue(metaName, d, gIndex)
                case JInt(i) =>
                  AddRealValue(metaName, i.doubleValue, gIndex)
                case JDecimal(d) =>
                  AddRealValue(metaName, d.doubleValue, gIndex)
                case _ =>
                  throw new JsonUtils.InvalidValueError(
                    "value", "ParseEvent", JsonUtils.prettyStr(jsonValue), "addRealValue expects a real value"
                  )
              }
            case "addArray" =>
              shape match {
                case Some(dims) =>
                  AddArray(metaName, dims, gIndex)
                case None =>
                  throw new JsonUtils.InvalidValueError(
                    "shape", "ParseEvent", shape.mkString("[", ",", "]"), "addArray requires a shape"
                  )
              }
            case "setArrayValues" =>
              SetArrayValues(metaName, buildArray(backend, metaName, valuesShape, flatValues),
                offset, gIndex)
            case "addArrayValues" =>
              AddArrayValues(metaName, buildArray(backend, metaName, valuesShape, flatValues), gIndex)
            case "openSection" =>
              OpenSectionWithGIndex(metaName, gIndex)
            case "matchTelemetry" =>
              MatchTelemetry(gIndex, matcherName, matcherGroup, fInLine, fInLineNr, matchFlags, matchSpansFlat)
          }
        } catch {
          case NonFatal(e) =>
            throw new ParseEventParseError(s"Error trying to parse a ParseEvent from ${JsonUtils.prettyStr(value)}.", e)
        }
      }
      case v =>
        throw new JsonUtils.InvalidValueError(
          "value", "ParseEvent", JsonUtils.prettyStr(value), "parse event should be an object"
        )
    }
  }
}

/**
 * Events of an external parser stream
 */
sealed abstract class ParseEvent {
  /**
   * name of this event
   */
  def eventName: String;

  /**
   * converts to a json value
   */
  def toJValue: JValue;

  /**
   * emits the corresponding event on a backend
   */
  def emitOnBackend(backend: ParserBackendExternal): Unit;
}

/**
 * Json serialization to and deserialization support for MetaInfoRecord
 */
class ParseEventSerializer extends CustomSerializer[ParseEvent](format => (
  {
    case obj: JObject => {
      ParseEvent.fromJValue(None, obj)(format)
    }
  },
  {
    case x: MetaInfoRecord => {
      x.toJValue()
    }
  }
))

/**
 * Started a parsing session
 */
final case class StartedParsingSession(
    mainFileUri: Option[String],
    parserInfo: JValue,
    parserStatus: Option[ParseResult.Value],
    parserErrors: JValue
) extends ParseEvent {
  override def eventName: String = "startedParsingSession"

  override def toJValue: JValue = {
    import org.json4s.JsonDSL._;
    ("event" -> eventName) ~
      ("mainFileUri" -> (mainFileUri match {
        case None => JNothing
        case Some(s) => JString(s)
      })) ~
      ("parserInfo" -> parserInfo) ~
      ("parserStatus" -> (parserStatus match {
        case None => JNothing
        case Some(s) => JString(s.toString)
      })) ~
      ("parserErrors" -> parserErrors)
  }

  override def emitOnBackend(backend: ParserBackendExternal): Unit = {
    backend.startedParsingSession(mainFileUri, parserInfo, parserStatus, parserErrors)
  }
}

/**
 * finished a parsing session
 */
final case class FinishedParsingSession(
    parserStatus: Option[ParseResult.Value],
    parserErrors: JValue,
    mainFileUri: Option[String],
    parserInfo: JValue,
    parsingStats: Map[String, Long]
) extends ParseEvent {
  override def eventName: String = "finishedParsingSession"

  override def toJValue: JValue = {
    import org.json4s.JsonDSL._;
    ("event" -> eventName) ~
      ("parserStatus" -> (parserStatus match {
        case None => JNothing
        case Some(status) => JString(status.toString())
      })) ~
      ("parserErrors" -> parserErrors) ~
      ("mainFileUri" -> (mainFileUri match {
        case None => JNothing
        case Some(uri) => JString(uri)
      })) ~
      ("parserInfo" -> parserInfo) ~
      ("parsingStats" -> parsingStats)
  }

  override def emitOnBackend(backend: ParserBackendExternal): Unit = {
    backend.finishedParsingSession(parserStatus, parserErrors, mainFileUri, parserInfo, parsingStats)
  }
}

/**
 * class to enable ignoring an event
 *
 */
final case class IgnoreEvent() extends ParseEvent {
  override def eventName: String = "ignoreEvent"
  override def toJValue: JValue = {
    import org.json4s.JsonDSL._;
    ("event" -> eventName)
  }
  override def emitOnBackend(backend: ParserBackendExternal): Unit = {
  }
}

/**
 * sets info values for a matchTelemetry event
 *
 */
final case class MatchTelemetry(
    gIndex: Long, matcherName: String, matcherGroup: List[String], fInLine: String, fInLineNr: Long, matchFlags: Long, matchSpansFlat: List[Long]
) extends ParseEvent {
  override def eventName: String = "matchTelemetry"
  override def toJValue: JValue = {
    import org.json4s.JsonDSL._;
    ("event" -> eventName) ~
      ("gIndex" -> gIndex) ~
      ("fInLine" -> fInLine) ~
      ("fInLineNr" -> fInLineNr) ~
      ("matcherName" -> matcherName) ~
      ("matchFlags" -> matchFlags) ~
      ("matchSpansFlat" -> matchSpansFlat) ~
      ("matcherGroup" -> matcherGroup)
  }
  override def emitOnBackend(backend: ParserBackendExternal): Unit = {
    backend.matchTelemetry(gIndex, matcherName, matcherGroup, fInLine, fInLineNr, matchFlags, matchSpansFlat)
  }
}

/**
 * sets info values of an open section.
 *
 * references should be references to gIndex of the root sections this section refers to.
 */
final case class SetSectionInfo(
    metaName: String, gIndex: Long, references: Map[String, Long]
) extends ParseEvent {
  override def eventName: String = "setSectionInfo"

  override def toJValue: JValue = {
    import org.json4s.JsonDSL._;
    ("event" -> eventName) ~
      ("gIndex" -> gIndex) ~
      ("references" -> references)
  }

  override def emitOnBackend(backend: ParserBackendExternal): Unit = {
    backend.setSectionInfo(metaName, gIndex, references)
  }
}

/**
 * closes a section
 *
 * after this no other value can be added to the section.
 * metaName is the name of the meta info, gIndex the index of the section
 */
final case class CloseSection(
    metaName: String, gIndex: Long
) extends ParseEvent {
  override def eventName: String = "closeSection"

  override def toJValue: JValue = {
    import org.json4s.JsonDSL._;
    ("event" -> eventName) ~
      ("metaName" -> metaName) ~
      ("gIndex" -> gIndex)
  }

  override def emitOnBackend(backend: ParserBackendExternal): Unit = {
    backend.closeSection(metaName, gIndex)
  }
}

/**
 * Adds a json value corresponding to metaName.
 *
 * The value is added to the section the meta info metaName is in.
 * A gIndex of -1 means the latest section.
 */
final case class AddValue(
    metaName: String, value: JValue, gIndex: Long = -1
) extends ParseEvent {
  override def eventName: String = "addValue"

  override def toJValue: JValue = {
    import org.json4s.JsonDSL._;
    ("event" -> eventName) ~
      ("metaName" -> metaName) ~
      ("gIndex" -> (if (gIndex == -1) JNothing else JInt(gIndex))) ~
      ("value" -> value)
  }

  override def emitOnBackend(backend: ParserBackendExternal): Unit = {
    backend.addValue(metaName, value, gIndex)
  }
}

/**
 * Adds a floating point value corresponding to metaName.
 *
 * The value is added to the section the meta info metaName is in.
 * A gIndex of -1 means the latest section.
 */
final case class AddRealValue(
    metaName: String, value: Double, gIndex: Long = -1
) extends ParseEvent {
  override def eventName: String = "addRealValue"

  override def toJValue: JValue = {
    import org.json4s.JsonDSL._;
    ("event" -> eventName) ~
      ("metaName" -> metaName) ~
      ("gIndex" -> (if (gIndex == -1) JNothing else JInt(gIndex))) ~
      ("value" -> value)
  }

  override def emitOnBackend(backend: ParserBackendExternal): Unit = {
    backend.addRealValue(metaName, value, gIndex)
  }
}

/**
 * Adds a new array of the given size corresponding to metaName.
 *
 * The value is added to the section the meta info metaName is in.
 * A gIndex of -1 means the latest section.
 * The array is unitialized.
 */
final case class AddArray(
    metaName: String, shape: Seq[Long], gIndex: Long = -1
) extends ParseEvent {
  override def eventName = "addArrayValue"

  override def toJValue: JValue = {
    import org.json4s.JsonDSL._;
    ("event" -> eventName) ~
      ("metaName" -> metaName) ~
      ("gIndex" -> (if (gIndex == -1) JNothing else JInt(gIndex))) ~
      ("shape" -> shape)
  }

  override def emitOnBackend(backend: ParserBackendExternal): Unit = {
    backend.addArray(metaName, shape, gIndex)
  }
}

object SetArrayValues {
  class UnexpectedDtypeException(
    metaName: String,
    dtype: DataType
  ) extends Exception(s"Unexpected data type $dtype in $metaName") {}

  def flatValues(metaName: String, values: NArray): JArray = {
    val dtype = values.getDataType()
    val extractor: NIndexIterator => JValue = (
      if (dtype.isFloatingPoint()) { (it: NIndexIterator) =>
        JDouble(it.getDoubleNext())
      } else if (dtype.isIntegral()) { (it: NIndexIterator) =>
        JInt(it.getLongNext())
      } else if (dtype.isString()) { (it: NIndexIterator) =>
        JString(it.next().toString())
      } else { (it: NIndexIterator) =>
        JString(it.next().toString())
      }
    )
    val it = values.getIndexIterator()

    val arr = Array.fill[JValue](values.getSize().intValue)(JNothing)
    var ii = 0
    while (it.hasNext()) {
      arr(ii) = extractor(it)
      ii += 1
    }
    JArray(arr.toList)
  }
}

/**
 * Adds values to the last array added
 */
final case class SetArrayValues(
    metaName: String,
    values: NArray,
    offset: Option[Seq[Long]] = None,
    gIndex: Long = -1
) extends ParseEvent {
  override def eventName = "setArrayValues"

  override def toJValue: JValue = {
    import org.json4s.JsonDSL._;
    ("event" -> eventName) ~
      ("metaName" -> metaName) ~
      ("gIndex" -> (if (gIndex == -1) JNothing else JInt(gIndex))) ~
      ("offset" -> (offset match {
        case Some(off) => JArray(off.map { (o: Long) => JInt(o) }(breakOut): List[JInt])
        case None => JNothing
      })) ~
      ("valuesShape" -> JArray(values.getShape().map { (s: Int) => JInt(s) }(breakOut): List[JInt])) ~
      ("flatValues" -> SetArrayValues.flatValues(metaName, values))
  }

  override def emitOnBackend(backend: ParserBackendExternal): Unit = {
    backend.setArrayValues(metaName, values, offset, gIndex)
  }
}

/**
 * Adds an array value with the given array values
 */
final case class AddArrayValues(
    metaName: String, values: NArray, gIndex: Long = -1
) extends ParseEvent {
  override def eventName = "addArrayValues"

  override def toJValue: JValue = JObject(
    ("event" -> JString(eventName)) ::
      ("metaName" -> JString(metaName)) ::
      ("gIndex" -> (if (gIndex == -1) JNothing else JInt(gIndex))) ::
      ("valuesShape" -> JArray(values.getShape().map { (i: Int) => JInt(i) }(breakOut): List[JInt])) ::
      ("flatValues" -> SetArrayValues.flatValues(metaName, values)) :: Nil
  )

  override def emitOnBackend(backend: ParserBackendExternal): Unit = {
    backend.addArrayValues(metaName, values, gIndex)
  }
}

/**
 * Informs tha backend that a section with the given gIndex should be opened
 *
 * The index is assumed to be unused, it is an error to reopen an existing section.
 */
final case class OpenSectionWithGIndex(
    metaName: String, gIndex: Long
) extends ParseEvent {
  override def eventName = "openSection"

  override def toJValue: JValue = {
    import org.json4s.JsonDSL._;
    ("event" -> eventName) ~
      ("metaName" -> metaName) ~
      ("gIndex" -> gIndex)
  }

  override def emitOnBackend(backend: ParserBackendExternal): Unit = {
    backend.openSectionWithGIndex(metaName, gIndex)
  }
}

/**
 * Opens the given context (used for normalization)
 */
final case class OpenContext(
    nomadUri: NomadUri
) extends ParseEvent {
  override def eventName = "openContext"

  override def toJValue: JValue = {
    import org.json4s.JsonDSL._;
    ("event" -> eventName) ~
      ("nomadUri" -> nomadUri.toUri)
  }

  override def emitOnBackend(backend: ParserBackendExternal): Unit = {
    backend.openContext(nomadUri)
  }
}

/**
 * Closes the given context (used for normalization)
 */
final case class CloseContext(
    nomadUri: NomadUri
) extends ParseEvent {
  override def eventName = "closeContext"

  override def toJValue: JValue = {
    import org.json4s.JsonDSL._;
    ("event" -> eventName) ~
      ("nomadUri" -> nomadUri.toUri)
  }

  override def emitOnBackend(backend: ParserBackendExternal): Unit = {
    backend.closeContext(nomadUri)
  }
}
