/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.parsers

import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.Files
import java.nio.file.StandardCopyOption
import com.typesafe.scalalogging.StrictLogging
import eu.{ nomad_lab => lab }
import org.{ json4s => jn }
import scala.util.control.NonFatal
import scala.collection.mutable
import scala.collection.breakOut
import ucar.ma2.{ Array => NArray }
import ucar.ma2

class CombinedBackendException(
  msg: String,
  what: Throwable = null
) extends Exception(msg, what)

class CombinedBackend(
  val internalBackend: ParserBackendInternal,
  val externalBackends: Seq[ParserBackendExternal],
  val backendInfoOverride: jn.JValue = jn.JNothing
) extends BaseParserBackend(
  internalBackend.metaInfoEnv
) with ParserBackendInternal with StrictLogging {

  override def backendInfo: jn.JValue = {
    backendInfoOverride match {
      case jn.JNothing =>
        jn.JObject(
          ("backendType" -> jn.JString(getClass().getName())) ::
            ("internalBackend" -> internalBackend.backendInfo) ::
            ("externalBackends" -> jn.JArray(
              externalBackends.map(_.backendInfo)(breakOut)
            )) :: Nil
        )
      case _ =>
        backendInfoOverride
    }
  }

  def forAllBackends(opName: String)(op: ParserBackendBase => Unit): Unit = {
    val exs = mutable.ListBuffer[Throwable]()
    try {
      op(internalBackend)
    } catch {
      case NonFatal(e) =>
        logger.warn(s"$opName error with ${lab.JsonUtils.normalizedStr(internalBackend.backendInfo)}", e)
        exs.append(e)
    }
    for (backend <- externalBackends) {
      try {
        op(backend)
      } catch {
        case NonFatal(e) =>
          logger.warn(s"$opName error with ${lab.JsonUtils.normalizedStr(backend.backendInfo)}", e)
          exs.append(e)
      }
    }
    if (!exs.isEmpty)
      throw new CombinedBackendException("Errors in $opName: $exs", exs.head)
  }

  def forAllBackends1(opName: String)(op: ParserBackendBase => Unit): Unit = {
    val exs = mutable.ListBuffer[Throwable]()
    op(internalBackend)
    for (backend <- externalBackends) {
      op(backend)
    }
  }

  override def cleanup(): Unit = {
    forAllBackends("cleanup")(_.cleanup())
  }

  override def startedParsingSession(
    mainFileUri: Option[String],
    parserInfo: jn.JValue,
    parserStatus: Option[ParseResult.Value] = None,
    parserErrors: jn.JValue = jn.JNothing
  ): Unit = {
    forAllBackends("startedParsingSession")(_.startedParsingSession(mainFileUri, parserInfo, parserStatus, parserErrors))
  }

  override def finishedParsingSession(
    parserStatus: Option[ParseResult.Value],
    parserErrors: jn.JValue = jn.JNothing,
    mainFileUri: Option[String] = None,
    parserInfo: jn.JValue = jn.JNothing,
    parsingStats: Map[String, Long] = Map()
  ): Unit = {
    forAllBackends("finishedParsingSession")(_.finishedParsingSession(parserStatus, parserErrors, mainFileUri, parserInfo, parsingStats))
  }

  override def setSectionInfo(metaName: String, gIndex: Long, references: Map[String, Long]): Unit = {
    forAllBackends1("setSectionInfo")(_.setSectionInfo(metaName, gIndex, references))
  }

  override def closeSection(metaName: String, gIndex: Long): Unit = {
    forAllBackends1("closeSection")(_.closeSection(metaName, gIndex))
  }

  override def addValue(metaName: String, value: jn.JValue, gIndex: Long = -1): Unit = {
    forAllBackends1("addValue")(_.addValue(metaName, value, gIndex))
  }

  /**
   * Adds a floating point value corresponding to metaName.
   *
   * The value is added to the section the meta info metaName is in.
   * A gIndex of -1 means the latest section.
   */
  override def addRealValue(metaName: String, value: Double, gIndex: Long = -1): Unit = {
    forAllBackends1("addRealValue")(_.addRealValue(metaName, value, gIndex))
  }

  /**
   * Adds a new array of the given size corresponding to metaName.
   *
   * The value is added to the section the meta info metaName is in.
   * A gIndex of -1 means the latest section.
   * The array is unitialized.
   */
  override def addArray(metaName: String, shape: Seq[Long], gIndex: Long = -1): Unit = {
    forAllBackends1("addArray")(_.addArray(metaName, shape, gIndex))
  }

  /**
   * Adds values to the last array added
   */
  override def setArrayValues(
    metaName: String,
    values: NArray,
    offset: Option[Seq[Long]] = None,
    gIndex: Long = -1
  ): Unit = {
    forAllBackends1("setArrayValues")(_.setArrayValues(metaName, values, offset, gIndex))
  }

  /**
   * Adds an array value with the given array values
   */
  override def addArrayValues(metaName: String, values: NArray, gIndex: Long = -1): Unit = {
    forAllBackends1("addArrayValues")(_.addArrayValues(metaName, values, gIndex))
  }

  /**
   * returns the sections that are still open
   *
   * sections are identified by metaName and their gIndex
   */
  override def openSections(): Iterator[(String, Long)] = internalBackend.openSections()

  /**
   * returns information on an open section (for debugging purposes)
   */
  override def sectionInfo(metaName: String, gIndex: Long): String = {
    internalBackend.sectionInfo(metaName, gIndex)
  }

  /**
   * closes a section
   *
   * after this no other value can be added to the section.
   * metaName is the name of the meta info, gIndex the index of the section
   */

  override def openSection(metaName: String): Long = {
    val gIndex = internalBackend.openSection(metaName)
    val exs = mutable.ListBuffer[Throwable]()
    for (backend <- externalBackends) {
      try {
        backend.openSectionWithGIndex(metaName, gIndex)
      } catch {
        case NonFatal(e) =>
          logger.warn(s"openSection error with ${lab.JsonUtils.normalizedStr(backend.backendInfo)}", e)
          exs.append(e)
      }
    }
    if (!exs.isEmpty)
      throw new CombinedBackendException("Errors in openSection: $exs", exs.head)
    gIndex
  }

}
