/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.parsers;
import com.typesafe.scalalogging.StrictLogging
import com.typesafe.config.{ Config, ConfigFactory }
import org.json4s.{ JNothing, JNull, JBool, JDouble, JDecimal, JInt, JString, JArray, JObject, JValue, JField }

/**
 * Methods to connect to the local relational DB
 */
object PythonParser extends StrictLogging {

  /**
   * The settings required to connect to the local relational DB
   */
  class Settings(config: Config) {
    // validate vs. reference.conf
    config.checkValid(ConfigFactory.defaultReference(), "simple-lib")

    val pythonExe: String = config.getString("nomad_lab.parsers.python_exe")

    def toJson: JValue = {
      import org.json4s.JsonDSL._;
      (("python_exe" -> pythonExe))
    }
  }
}

class PythonParser(
    config: PythonParser.Settings
) {

}
