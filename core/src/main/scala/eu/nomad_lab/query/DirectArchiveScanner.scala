/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.query

import eu.nomad_lab.h5.ArchiveSetH5
import eu.nomad_lab.h5.ArchiveH5
import eu.nomad_lab.resolve._

/**
 * Scans an archive and executes the given query op on it
 */
class DirectArchiveScanner(
    val archiveSet: ArchiveSetH5,
    val archive: ArchiveH5,
    val scanOp: QueryScanOp
) {
  def run(): Unit = {
    val filter = scanOp.filter
    if (!filter.filterArchive(archive))
      return ()
    scanOp.context match {
      case "archive" | "archive_context" =>
        val ctx = Archive(archiveSet, archive)
        if (filter.filterContext(ctx))
          scanOp.workOnContext(ctx)
      case "calculation" | "calculation_context" =>
        for (calc <- archive.calculations) {
          if (filter.filterCalculation(calc)) {
            val ctx = Calculation(archiveSet, calc)
            if (filter.filterContext(ctx))
              scanOp.workOnContext(ctx)
          }
        }
      case metaName =>
        var metaNamePath: List[String] = metaName :: Nil
        var metaNameNow: String = metaName
        while (scanOp.metaInfoEnv.parentSectionName(metaNameNow) match {
          case None => false
          case Some(s) =>
            if (metaNamePath contains s)
              throw new QueryError(s"circular reference in meta info for $s: $metaNamePath")
            metaNameNow = s
            metaNamePath = s :: metaNamePath
            true
        }) ()
        scanOp.metaInfoEnv.metaInfoRecordForName(metaName) match {
          case None => throw new QueryError(s"could not find meta info for context $metaName")
          case Some(metaInfo) =>
            metaInfo.kindStr match {
              case "type_section" =>
                for (calc <- archive.calculations) {
                  if (filter.filterCalculation(calc)) {
                    val sectionTable = calc.sectionTable(metaNamePath.toSeq)
                    for (section <- sectionTable) {
                      val ctx = Section(archiveSet, section)
                      if (filter.filterContext(ctx))
                        scanOp.workOnContext(ctx)
                    }
                  }
                }
              case "type_document_content" =>
                for (calc <- archive.calculations) {
                  if (filter.filterCalculation(calc)) {
                    val valueTable = calc.valueTable(metaNamePath.toSeq)
                    for (value <- valueTable) {
                      val ctx = Value(archiveSet, value)
                      if (filter.filterContext(ctx))
                        scanOp.workOnContext(ctx)
                    }
                  }
                }
              case t =>
                throw new QueryError(s"Invalid context $t")
            }
        }
    }
  }

  def cleanup(): Unit = {
    scanOp.cleanup()
  }
}
