/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.query

/**
 * Quantifiers allowed in an atomic query term
 *
 * Any: at least one data corresponding to the metaInfo satisifies the comparison (exist quantifier)
 * AllData: all data corresponding to the metaInfo satisfy one (or more) of the comparison, some target values might be used multiple times, others not at all (i.e. 1,1 = 1,2 is considered true as both 1 values can be matched) (left biased set like for all quantifier, or set inclusion <)
 * AllTarget: all target value corresponding to the metaInfo satisfy one (or more) of the comparison, some data values might be used multiple times, others not at all (i.e. 3,2,1 = 1,2 is considered true as both the 1 and 2 target values can be matched) (right biased set like for all quantifier, or set inclusion >)
 * All: all data and all targets have one or more value that satifies the comparison, some values might be used multiple times, no value (be it data or target) has no relatioship returning true (i.e. 0,1 < 2 is considered true as both 0,1 and 2 have at least one relationship being true) (symmetric set like for all quantifier)
 * Matching: values have to correspond exactly to the target values (i.e. 1,2 != 2,1)  (vector comparison)
 */
object QueryQuantifier extends Enumeration {
  type QueryQuantifier = Value
  val Any, AllData, AllTarget, All, Matching = Value

  def fromString(quantifier: String): QueryQuantifier = {
    if (quantifier.compareToIgnoreCase("any") == 0)
      Any
    else if (quantifier.compareToIgnoreCase("all") == 0)
      All
    else if (quantifier.compareToIgnoreCase("alldata") == 0)
      AllData
    else if (quantifier.compareToIgnoreCase("alltarget") == 0)
      AllTarget
    else if (quantifier.compareToIgnoreCase("matching") == 0)
      Matching
    else
      throw new InvalidTerm(s"Unknown quantifier $quantifier")
  }

  def toString(quantifier: QueryQuantifier.Value): String = {
    quantifier match {
      case QueryQuantifier.Any =>
        "any"
      case QueryQuantifier.All =>
        "all"
      case QueryQuantifier.AllData =>
        "alldata"
      case QueryQuantifier.AllTarget =>
        "alltarget"
      case QueryQuantifier.Matching =>
        "matching"
    }
  }

  val compareRe = """\A\s*([aA][nN][yY]|[aA][lL][lL](?:|[dD][aA][tT][aA]|[tT][aA][rR][gG][eE][tT])|[mM][aA][tT][cC][hH][iI][nN][gG])\s*""".r
  def parse(inStr: String): (Option[QueryQuantifier], String) = {
    compareRe.findFirstMatchIn(inStr) match {
      case Some(m) =>
        (Some(fromString(m.group(1))), inStr.drop(m.end))
      case None =>
        (None, inStr)
    }
  }
}
