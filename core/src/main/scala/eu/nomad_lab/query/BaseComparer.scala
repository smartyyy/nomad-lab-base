/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.query

import scala.collection.mutable
import scala.collection.SortedSet
import eu.nomad_lab.meta.MetaInfoRecord
import scala.math.Ordered
import com.typesafe.scalalogging.LazyLogging

object BaseComparer {
  import scala.language.implicitConversions

  /**
   * Order seunces lexicografically
   */
  implicit def seq2ordered[A](l1: Seq[A])(implicit elem2ordered: A => Ordered[A]): Ordered[Seq[A]] = {
    new Ordered[Seq[A]] {
      override def compare(l2: Seq[A]): Int = {
        var i1: Int = 0
        var i2: Int = 0
        while (i1 < l1.length && i2 < l2.length) {
          val c = elem2ordered(l1(i1)).compare(l2(i2))
          if (c != 0) return c
          i1 += 1
          i2 += 1
        }
        if (i1 < l1.length) return 1
        if (i2 < l2.length) return -1
        0
      }
    }
  }
}

/**
 * An helper object that generates code to evaluate a term Involving values
 * of type T against an iterator of type T
 */
class BaseComparer[T <% Ordered[T]](
    val term: AtomicTerm,
    val metaInfo: MetaInfoRecord,
    val targetVals: Seq[T]
) extends LazyLogging {
  import QueryExpression.traceFiltering

  type ValueType = T
  val targetValsSet = SortedSet[T](targetVals: _*)

  /**
   * compares a value (first argument) with a target (second argument)
   */
  def eval2: (T, T) => Boolean = {
    term.compareOp.getOrElse(CompareOp.OpEqual) match {
      case CompareOp.OpMatch =>
        throw new InternalError(s"regexp string matching not supported")
      case CompareOp.OpEqual =>
        { (v1: T, v2: T) => v1 == v2 }
      case CompareOp.OpNotEqual =>
        { (v1: T, v2: T) => v1 != v2 }
      case CompareOp.OpSmaller =>
        { (v1: T, v2: T) => v1 < v2 }
      case CompareOp.OpSmallerEqual =>
        { (v1: T, v2: T) => v1 <= v2 }
      case CompareOp.OpBigger =>
        { (v1: T, v2: T) => v1 > v2 }
      case CompareOp.OpBiggerEqual =>
        { (v1: T, v2: T) => v1 >= v2 }
    }
  }

  /**
   * returns the non inverted evaluator comparing a value to all targets.
   * If some target value matches true then it is removed from the second argument
   */
  def rawEval: (T, mutable.Set[T]) => Boolean = {
    term.compareOp.getOrElse(CompareOp.OpEqual) match {
      case CompareOp.OpMatch =>
        throw new InternalError(s"regexp string matching not supported")
      case CompareOp.OpEqual =>
        { (value: T, missingTargets: mutable.Set[T]) =>
          if (targetValsSet.contains(value)) {
            missingTargets.remove(value)
            true
          } else {
            false
          }
        }
      case CompareOp.OpNotEqual =>
        { (value: T, missingTargets: mutable.Set[T]) =>
          if (!targetValsSet.contains(value)) {
            missingTargets.remove(value)
            true
          } else {
            false
          }
        }
      case CompareOp.OpSmaller =>
        { (value: T, missingTargets: mutable.Set[T]) =>
          if (value < targetValsSet.max) {
            if (value < missingTargets.max)
              missingTargets.filter(value < _).foreach(missingTargets.remove(_))
            true
          } else {
            false
          }
        }
      case CompareOp.OpSmallerEqual =>
        { (value: T, missingTargets: mutable.Set[T]) =>
          if (value <= targetValsSet.max) {
            if (value <= missingTargets.max)
              missingTargets.filter(value <= _).foreach(missingTargets.remove(_))
            true
          } else {
            false
          }
        }
      case CompareOp.OpBigger =>
        { (value: T, missingTargets: mutable.Set[T]) =>
          if (value > targetValsSet.min) {
            if (value > missingTargets.min)
              missingTargets.filter(value > _).foreach(missingTargets.remove(_))
            true
          } else {
            false
          }
        }
      case CompareOp.OpBiggerEqual =>
        { (value: T, missingTargets: mutable.Set[T]) =>
          if (value >= targetValsSet.min) {
            if (value >= missingTargets.min)
              missingTargets.filter(value >= _).foreach(missingTargets.remove(_))
            true
          } else {
            false
          }
        }
    }
  }

  /**
   * returns the non inverted evaluator that compares a value (the argument) to
   * all targets.
   *
   * An optimized version should probably be written
   */
  def eval1: (T) => Boolean = {
    val rEval = rawEval

    { (value: T) =>
      rEval(value, mutable.Set())
    }
  }

  def requireAny: (Iterator[T]) => Boolean = {
    val eval = eval1
    val ok: Boolean = !term.invert

    def ev(vals: Iterator[T]): Boolean = {
      for (v <- vals)
        if (eval(v)) {
          if (traceFiltering) logger.debug(s"$term $ok")
          return ok
        }
      if (traceFiltering) logger.debug(s"$term ${!ok}")
      !ok
    }
    ev
  }

  def requireAllTargets: (Iterator[T]) => Boolean = {
    val eval = rawEval
    val ok = !term.invert

    { (vals: Iterator[T]) =>
      val missingTargets = mutable.Set[T]()
      missingTargets ++= targetValsSet
      while (!missingTargets.isEmpty && vals.hasNext) {
        val v = vals.next()
        eval(v, missingTargets)
      }
      val res = if (missingTargets.isEmpty)
        ok
      else
        !ok
      if (traceFiltering) logger.debug(s"$term $res")
      res
    }
  }

  def requireAllValues: (Iterator[T]) => Boolean = {
    val eval = eval1
    val ok = !term.invert

    def ev(vals: Iterator[T]): Boolean = {
      for (v <- vals)
        if (!eval(v)) {
          if (traceFiltering) logger.debug(s"$term ${!ok}")
          return !ok
        }
      if (traceFiltering) logger.debug(s"$term $ok")
      ok
    }
    ev
  }

  def requireAll: (Iterator[T]) => Boolean = {
    val eval = rawEval
    val ok = !term.invert

    { (vals: Iterator[T]) =>
      val missingTargets = mutable.Set[T]()
      missingTargets ++= targetValsSet
      var allVals: Boolean = true
      for (v <- vals)
        allVals = eval(v, missingTargets) && allVals
      val res = if (allVals && missingTargets.isEmpty)
        ok
      else
        !ok
      if (traceFiltering) logger.debug(s"$term $res")
      res
    }
  }

  def matching: (Iterator[T]) => Boolean = {
    val eval = eval2
    val ok = !term.invert

    def f(vals: Iterator[T]): Boolean = {
      val iter = targetVals.toIterator
      var allVals: Boolean = true
      for (v <- vals)
        if (!iter.hasNext || !eval(v, iter.next)) {
          if (traceFiltering) logger.debug(s"$term ${!ok}")
          return !ok
        }
      val res = if (iter.hasNext)
        !ok
      else
        ok
      if (traceFiltering) logger.debug(s"$term $res")
      res
    }
    f
  }

  /**
   * Method that returns a function that evaluates this.term against the values
   * returned by its arguemnt (an iterator of type T).
   */
  def evalWithIterator: (Iterator[T]) => Boolean = {
    term.quantifier match {
      case QueryQuantifier.Any =>
        requireAny
      case QueryQuantifier.AllData =>
        requireAllValues
      case QueryQuantifier.AllTarget =>
        requireAllTargets
      case QueryQuantifier.All =>
        requireAll
      case QueryQuantifier.Matching =>
        matching
    }
  }
}
