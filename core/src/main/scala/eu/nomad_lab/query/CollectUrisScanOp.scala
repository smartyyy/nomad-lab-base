/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.query

import eu.nomad_lab.meta
import eu.nomad_lab.resolve
import org.{ json4s => jn }
import com.typesafe.scalalogging.StrictLogging

class CollectUrisScanOp(
    val context: String,
    val filter: Filter,
    val metaInfoEnv0: Option[meta.MetaInfoEnv] = None
) extends QueryScanOp with StrictLogging {
  var _uris: Seq[String] = Seq()
  def uris: Seq[String] = {
    synchronized {
      _uris
    }
  }

  val metaInfoEnv: meta.MetaInfoEnv = metaInfoEnv0.getOrElse(meta.KnownMetaInfoEnvs.all)

  val emitLength = 4 * 1024

  def workOnContext(context: resolve.ResolvedRef): Unit = {
    var toWrite: Option[String] = None
    synchronized {
      _uris = _uris :+ context.uriString
    }
  }

  def cleanup(): Unit = {
    synchronized {
      _uris = Seq()
    }
  }

  def workerJValue: jn.JValue = jn.JNothing

}
