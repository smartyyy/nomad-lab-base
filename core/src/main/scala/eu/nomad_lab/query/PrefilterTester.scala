/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.query

import scala.collection.mutable
import org.{ json4s => jn }
import eu.nomad_lab.h5._
import eu.nomad_lab.resolve._
import com.typesafe.scalalogging.LazyLogging

object FilterStatus extends Enumeration {
  type FilterStatus = Value
  val Unknown, AllFalse, SomeTrue, AllTrue = Value

  def addValue(filterStatus: FilterStatus, value: Boolean): FilterStatus = {
    if (value) {
      filterStatus match {
        case Unknown => AllTrue
        case AllFalse => SomeTrue
        case SomeTrue => SomeTrue
        case AllTrue => AllTrue
      }
    } else {
      filterStatus match {
        case Unknown => AllFalse
        case AllFalse => AllFalse
        case SomeTrue => SomeTrue
        case AllTrue => SomeTrue
      }
    }
  }
}

object PrefilteringLevel extends Enumeration {
  type PrefilteringLevel = Value
  val Undefined, Weak, Exact, Incorrect = Value
}

class PrefilterTester(
    val filter: Filter,
    val throwOnError: Boolean
) extends Filter with LazyLogging {
  import FilterStatus._
  import PrefilteringLevel._

  val filterStatus = mutable.Map[String, (FilterStatus, FilterStatus)]()

  def jValue: jn.JValue = {
    jn.JObject(
      ("type" -> jn.JString("PrefilterTester")) ::
        ("filter" -> filter.jValue) :: Nil
    )
  }

  def verify(origFilter: FilterStatus, actualFilter: FilterStatus): PrefilteringLevel = {
    origFilter match {
      case Unknown => Undefined
      case AllFalse =>
        actualFilter match {
          case Unknown => Undefined
          case AllFalse => Exact
          case SomeTrue | AllTrue => Incorrect
        }
      case SomeTrue | AllTrue =>
        actualFilter match {
          case Unknown => Undefined
          case SomeTrue | AllTrue => Exact
          case AllFalse => Weak
        }
    }
  }

  def filterArchive(archive: ArchiveH5): Boolean = {
    var actualFilter = Unknown
    filterStatus.get(archive.archiveGid) match {
      case Some(stats) =>
        stats._1 match {
          case Unknown =>
            actualFilter = stats._2
          case _ => return true
        }
      case None => ()
    }
    val origFilter = if (filter.filterArchive(archive))
      FilterStatus.SomeTrue
    else
      FilterStatus.AllFalse
    filterStatus += archive.archiveGid -> (origFilter -> actualFilter)
    verify(origFilter, actualFilter) match {
      case Incorrect =>
        val msg = s"Incorrect prefilter for archive $archive"
        if (throwOnError)
          throw new Exception(msg)
        else
          logger.warn(msg)
      case _ => ()
    }
    true
  }

  def addActualEval(gid: String, filterValue: Boolean): Unit = {
    filterStatus.get(gid) match {
      case Some(stats) =>
        val newRealVal = stats._2 match {
          case Unknown =>
            if (filterValue)
              AllTrue
            else
              AllFalse
          case AllFalse =>
            if (filterValue)
              SomeTrue
            else
              AllFalse
          case AllTrue =>
            if (filterValue)
              AllTrue
            else
              SomeTrue
          case SomeTrue =>
            SomeTrue
        }
        if (newRealVal != stats._2) {
          filterStatus += gid -> (stats._1 -> newRealVal)
          verify(stats._1, newRealVal) match {
            case Incorrect =>
              val msg = s"Incorrect prefilter for $gid"
              if (throwOnError)
                throw new Exception(msg)
              else
                logger.warn(msg)
            case _ => ()
          }
        }
      case None =>
        filterStatus += gid -> (Unknown -> (if (filterValue) AllTrue else AllFalse))
    }
  }

  def filterCalculation(calculation: CalculationH5): Boolean = {
    var actualFilter = Unknown
    filterStatus.get(calculation.calculationGid) match {
      case Some(stats) =>
        stats._1 match {
          case Unknown =>
            actualFilter = stats._2
          case _ => return true
        }
      case None => ()
    }
    val filterValue = filter.filterCalculation(calculation)
    val archiveGid = calculation.archive.archiveGid
    addActualEval(archiveGid, filterValue)
    val origFilter = if (filterValue)
      FilterStatus.SomeTrue
    else
      FilterStatus.AllFalse
    verify(origFilter, actualFilter) match {
      case Incorrect =>
        val msg = s"Incorrect prefilter for calculation $calculation"
        if (throwOnError)
          throw new Exception(msg)
        else
          logger.warn(msg)
      case _ => ()
    }
    filterStatus += calculation.calculationGid -> (origFilter -> actualFilter)
    true
  }

  def filterContext(context: ResolvedRef): Boolean = {
    val res = filter.filterContext(context)
    val calc = context match {
      case FailedResolution(uriStr, msg) =>
        throw new QueryError(s"Failed resolution $uriStr (due to $msg) in filterContext")
      case RawData(_, _, _) =>
        throw new QueryError(s"RawData $context in filterContext")
      case SectionTable(_, s) =>
        s.calculation
      case Section(_, s) =>
        s.table.calculation
      case ValueTable(_, v) =>
        v.parentSectionTable.calculation
      case Value(_, v) =>
        v.parentSection.table.calculation
      case Calculation(_, c) =>
        c
      case Archive(_, archive) =>
        val archiveGid = archive.archiveGid
        addActualEval(archiveGid, res)
        return res
    }
    addActualEval(calc.calculationGid, res)
    res
  }

}
