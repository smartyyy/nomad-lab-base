/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.query

import org.{ json4s => jn }
import scala.collection.breakOut
import scala.collection.mutable
import scala.collection.SortedMap
import eu.nomad_lab.h5._
import eu.nomad_lab.meta.MetaInfoEnv
import eu.nomad_lab.meta
import eu.nomad_lab.resolve._
import eu.nomad_lab.H5Lib
import eu.nomad_lab.JsonUtils
import scala.util.control.NonFatal
import ucar.ma2
import com.typesafe.scalalogging.LazyLogging

class MethodSectionIterator(
    var currentSection: Option[SectionH5] = None,
    var visitedSections: Set[Long] = Set(),
    var toVisit: Set[Long] = Set()
) extends Iterator[SectionH5] {

  override def hasNext: Boolean = {
    !currentSection.isEmpty
  }

  override def next: SectionH5 = {
    currentSection match {
      case Some(s) =>
        for (sects <- s.subSectionCollection("section_method_to_method_refs")) {
          sects.maybeValue("method_to_method_kind") match {
            case Some(v) =>
              if (v.stringValue == "core_settings") {
                sects.maybeValue("method_to_method_ref") match {
                  case Some(vv) =>
                    val newIndex = vv.longValue
                    if (!visitedSections(newIndex))
                      toVisit += newIndex
                  case None => ()
                }
              }
            case None => ()
          }
        }
        toVisit.headOption match {
          case None =>
            currentSection = None
          case Some(idx) =>
            currentSection = Some(s.table(idx))
            toVisit = toVisit.drop(1)
            visitedSections += idx
        }
        s
      case None =>
        throw new Exception(s"called nex with no next value in MethodSectionIterator")
    }
  }
}

class MethodSectionIterable(
    val startSection: Option[SectionH5] = None
) extends Iterable[SectionH5] {
  override def iterator: MethodSectionIterator = {
    new MethodSectionIterator(startSection)
  }
}

object CompiledQuery {
  /**
   * binary search that finds the lower index position, i.e. the smallest index i such arr(i) >= key.
   * Inserting key before this index keeps the array sorted
   */
  def lBoundString(arr: ma2.ArrayString.D1, key: String, low0: Option[Long], high0: Option[Long]): Long = {
    var low: Long = low0.getOrElse(0)
    var high: Long = high0.getOrElse(arr.getSize - 1)
    while (low <= high) {
      val mid = (low + high) / 2
      if (arr.getObject(mid.toInt).toString < key)
        low = mid + 1
      else
        high = mid - 1
    }
    low
  }

  def apply(queryExpression: QueryExpression, metaInfoEnv: MetaInfoEnv): CompiledQuery = {
    new CompiledQuery(metaInfoEnv = metaInfoEnv, query = queryExpression)
  }

  def fromJValue(jValue: jn.JValue, metaInfoEnv: Option[MetaInfoEnv] = None): CompiledQuery = {
    val qStr = jValue \ "query_string" match {
      case jn.JString(q) => q
      case jn.JNothing =>
        throw new QueryError(s"Could not decode query, missing query_string entry in '${JsonUtils.normalizedStr(jValue)}'")
      case v =>
        throw new QueryError(s"Could not decode query, expected a string, not '${JsonUtils.normalizedStr(v)}' for the query_string entry in '$JsonUtils.normalizedStr(jValue)'")
    }
    val expr = QueryExpression.parseExpression(qStr)
    apply(expr, metaInfoEnv.getOrElse(meta.KnownMetaInfoEnvs.all))
  }
}

/**
 * represents a compiled query that can filter archive, calculation and contexts
 */
class CompiledQuery(
    val metaInfoEnv: MetaInfoEnv,
    val query: QueryExpression,
    var metaInfoStatus: SortedMap[String, MetaStatus.Value] = SortedMap(),
    var archiveTerms: Seq[AtomicTerm] = Seq(),
    var calculationTerms: Seq[AtomicTerm] = Seq(),
    val rootTree: TermTree = new TermTree(""),
    var cachedValuesTerms: SortedMap[String, Seq[CompiledSectionTerm]] = SortedMap()
) extends Filter with LazyLogging {
  import QueryExpression.traceFiltering

  override def jValue: jn.JObject = {
    jn.JObject(
      ("type" -> jn.JString("query")) ::
        ("query_string" -> jn.JString(query.toString)) :: Nil
    )
  }

  /**
   * Information on the compiled query
   */
  def info: jn.JObject = {
    jn.JObject(
      ("metaInfoStatus" -> jn.JObject(
        metaInfoStatus.map {
          case (k, v) =>
            (k -> jn.JString(v.toString))
        }(breakOut): List[(String, jn.JString)]
      )) :: Nil
    )
  }

  // compile the query expression
  query match {
    case AndConjunction(els) =>
      for (el <- els)
        addTerm(el)
    case t: AtomicTerm =>
      addTerm(t)
  }

  def addTerm(term: AtomicTerm): Unit = {
    val archiveTermNames = Set("archive_context", "archive_gid")
    val calculationTermNames = Set("calculation_context", "calculation_gid")
    if (archiveTermNames(term.metaName)) {
      archiveTerms = archiveTerms :+ term.normalizeTerm(metaInfoEnv)
    } else if (calculationTermNames(term.metaName)) {
      calculationTerms = calculationTerms :+ term.normalizeTerm(metaInfoEnv)
    } else {
      metaInfoEnv.metaInfoRecordForName(term.metaName) match {
        case None => throw new InvalidTerm(s"meta name ${term.metaName} is unknown")
        case Some(mi) =>
          var path: List[String] = Nil
          var sAtt = term.metaName
          mi.kindStr match {
            case "type_document_content" => ()
            case "type_section" =>
              path = term.metaName :: path
            case t =>
              throw new InvalidTerm(s"query supports only concrete types and sections, not meta info of type $t as in '$term'")
          }
          while (metaInfoEnv.parentSectionName(sAtt) match {
            case None => false
            case Some(parentName) =>
              path = parentName :: path
              sAtt = parentName
              true
          }) ()
          metaInfoStatus += (term.metaName -> MetaStatus.merge(metaInfoStatus.getOrElse(term.metaName, MetaStatus.Unknown), term.metaInfoStatus))
          val compiledTerm = CompiledSectionTerm(metaInfoEnv, term)
          if (mi.kindStr == "type_document_content") {
            val compiledCachedValuesTerm = term.quantifier match {
              case QueryQuantifier.Any | QueryQuantifier.AllData | QueryQuantifier.AllTarget | QueryQuantifier.All =>
                compiledTerm
              case QueryQuantifier.Matching =>
                CompiledSectionTerm(metaInfoEnv, term.copy(quantifier = QueryQuantifier.All))
            }
            cachedValuesTerms += (term.metaName -> (cachedValuesTerms.getOrElse(term.metaName, Seq()) :+ compiledCachedValuesTerm))
          }
          rootTree.insertTerm(path, compiledTerm)
      }
    }
  }

  override def filterArchive(archive: ArchiveH5): Boolean = {
    for (term <- archiveTerms) {
      term.metaName match {
        case "archive_context" =>
          val metaInfo = metaInfoEnv.metaInfoRecordForName("archive_context").get
          val v = term.metaElement match {
            case None => 1L
            case Some(e) =>
              e match {
                case MetaElement.Data =>
                  throw new InvalidTerm(s"Data meta element not valid for section ${term.metaName} in '$term'")
                case MetaElement.Shape =>
                  throw new InvalidTerm(s"Shape meta element not valid for section ${term.metaName} in '$term'")
                case MetaElement.Length => 1L
                case MetaElement.CIndex => 1L
              }
          }
          val comparer = new BaseComparer[Long](term, metaInfo, term.values.map(term.jToLong)(breakOut))
          if (!comparer.evalWithIterator(Iterator(v))) {
            logger.debug(s"discarding $archive due to $term")
            return false
          }
        case "archive_gid" =>
          val metaInfo = metaInfoEnv.metaInfoRecordForName("archive_gid").get
          term.metaElement.get match {
            case MetaElement.Data =>
              val targetStrs: Seq[String] = term.values.map(term.jToString)(breakOut)
              term.compareOp.get match {
                case CompareOp.OpMatch =>
                  if (targetStrs.length != 1)
                    throw new InvalidTerm(s"regex matching expects exactly one value, not ${targetStrs.length} as in '$term'")
                  val m = new RegexMatcher(term, metaInfo, targetStrs.head.r)
                  if (!m.evalWithIterator(Iterator(archive.archiveGid))) {
                    logger.debug(s"Discarding $archive due to '$term'")
                    return false
                  }
                case op =>
                  val comparer = new BaseComparer[String](term, metaInfo, targetStrs)
                  if (!comparer.evalWithIterator(Iterator(archive.archiveGid))) {
                    logger.debug(s"Discarding $archive due to $term")
                    return false
                  }
              }
            case MetaElement.Shape =>
              import scala.math.Ordered
              import BaseComparer.seq2ordered

              val comparer = new BaseComparer[Seq[Long]](term, metaInfo, term.values.map(term.jToSeqLong)(breakOut))
              if (!comparer.evalWithIterator(Iterator(Seq()))) {
                logger.debug(s"Discarding $archive due to $term")
                return false
              }
            case MetaElement.Length | MetaElement.CIndex =>
              val comparer = new BaseComparer[Long](term, metaInfo, term.values.map(term.jToLong)(breakOut))
              if (!comparer.evalWithIterator(Iterator(1L))) {
                logger.debug(s"Discarding $archive due to $term")
                return false
              }
          }
      }
    }

    val calc = try {
      archive.openCalculation("_")
    } catch {
      case NonFatal(e) =>
        logger.warn(s"Could not open archive summary (calculation '_') in $archive, no pre filter performed")
        return true
    }
    try {
      filterCalculationMetaInfo(calc)
    } finally {
      calc.release()
    }
  }

  def filterCalculationMetaInfo(calculation: CalculationH5): Boolean = {
    val statuses = metaInfoStatus.values.toSet
    if (statuses.contains(MetaStatus.Impossible)) {
      logger.debug(s"Impossible meta status, filtering out $calculation")
      return false
    }
    if (statuses.contains(MetaStatus.Required) || statuses.contains(MetaStatus.Excluded)) {
      (try {
        Some(calculation.valueTable(Seq("section_stats", "stats_meta_present")))
      } catch {
        case NonFatal(e) =>
          logger.warn(s"Could not find index of meta infos (i.e. stats_meta_present) in $calculation, skipping prefilter")
          None
      }) map { metaVTable: ValueTableH5 =>
        val (metaDatasetName, metaDataset) = metaVTable.openValueDataset(Seq(1), create = false)
        if (metaDataset >= 0) {
          val metaPresent = try {
            val nMetaPresentL = metaVTable.length
            val nMetaPresent: Int = nMetaPresentL.toInt
            if (nMetaPresentL > 100000)
              throw new QueryError(s"Calculation $calculation has too many meta infos ($nMetaPresent) in $metaVTable")
            val metaP = new ma2.ArrayString.D1(nMetaPresent)
            val r = metaVTable.valueGetLocalRange(
              valuesDatasetId = metaDataset,
              localIndex0 = 0,
              localIndexSize = nMetaPresent,
              value = Some(metaP)
            )
            if (r.getShape()(0) != metaP.getShape()(0))
              throw new QueryError(s"Internal error with present meta info filtering, unexpected shape ${r.getShape().mkString("[", ",", "]")}, when expecting $nMetaPresent values.")
            metaP
          } finally {
            H5Lib.datasetClose(metaDataset)
          }

          var lBound: Long = 0
          val uBound: Long = (metaPresent.getShape()(0) - 1).toLong

          for ((metaName, status) <- metaInfoStatus) {
            lazy val present = {
              val lIndex = CompiledQuery.lBoundString(metaPresent, metaName, Some(lBound), Some(uBound))
              lBound = lIndex
              lIndex >= 0 && lIndex <= uBound && metaPresent.getObject(lIndex.toInt).toString == metaName
            }
            if (traceFiltering)
              logger.debug(s"meta $metaName present=$present")
            status match {
              case MetaStatus.Required =>
                if (!present) {
                  logger.debug(s"$metaName filtering out $calculation")
                  return false
                }
              case MetaStatus.Excluded =>
                if (present) {
                  logger.debug(s"$metaName filtering out $calculation")
                  return false
                }
              case MetaStatus.Unknown =>
              case MetaStatus.Impossible =>
                logger.debug(s"$metaName filtering out $calculation")
                return false
            }
          }
        }
      }
    }
    // value checks to do
    (try {
      Some(calculation.valueTable(Seq("section_stats", "section_stats_values", "stats_values_meta_name")))
    } catch {
      case NonFatal(e) =>
        logger.warn(s"Could not find cached values (i.e. stats_values_meta_name) in $calculation, skipping prefilter")
        None
    }) map { cachedValuesTable: ValueTableH5 =>
      val (cachedMetaName, cachedMetaDataset) = cachedValuesTable.openValueDataset(Seq(1), create = false)
      if (cachedMetaDataset >= 0) {
        val metaCached = try {
          val nMetaCachedL = cachedValuesTable.length
          val nMetaCached: Int = nMetaCachedL.toInt
          if (nMetaCachedL > 10000)
            throw new QueryError(s"Calculation $calculation has too many cached meta infos ($nMetaCached) in $cachedValuesTable")
          val metaC = new ma2.ArrayString.D1(nMetaCached)
          val r = cachedValuesTable.valueGetLocalRange(
            valuesDatasetId = cachedMetaDataset,
            localIndex0 = 0,
            localIndexSize = nMetaCached,
            value = Some(metaC)
          )
          if (r.getShape()(0) != metaC.getShape()(0))
            throw new QueryError(s"Internal error with present meta info filtering, unexpected shape ${r.getShape().mkString("[", ",", "]")}, when expecting $nMetaCached values.")
          metaC
        } finally {
          H5Lib.datasetClose(cachedMetaDataset)
        }
        val cachedMeta = mutable.Map[String, Long]()
        val nMetaCached = metaCached.getShape()(0)
        for (i <- 0 until nMetaCached)
          cachedMeta += (metaCached.getObject(i).toString -> i)
        lazy val cachedValuesSectionTable = calculation.sectionTable(Seq("section_stats", "section_stats_values"))
        for ((metaName, compiledTerms) <- cachedValuesTerms) {
          cachedMeta.get(metaName) match {
            case Some(i) =>
              val cachedValuesSection = cachedValuesSectionTable(i)
              compiledTerms.head.metaInfo.dtypeStr match {
                case Some("C") =>
                  compiledTerms.head.metaInfo.shape match {
                    case None | Some(Seq()) | Some(Seq(Left(1L))) =>
                      val values = cachedValuesSection.valueCollection("stats_values_string_values")
                      for (compiledTerm <- compiledTerms) {
                        if (!compiledTerm.evaluateOnValueCollection(values)) {
                          logger.debug(s"cached values for $metaName discarded $calculation")
                          return false
                        }
                      }
                    case Some(v) =>
                      logger.warn(s"ignoring cached value for $metaName because it has a shape ${v.mkString("[", ",", "]")}")
                  }
                case v =>
                  logger.warn(s"ignoring cached value for $metaName because it has dtypeStr $v")
              }
            case None => ()
          }
        }
      }
    }
    true
  }

  override def filterCalculation(calculation: CalculationH5): Boolean = {
    for (term <- calculationTerms) {
      term.metaName match {
        case "calculation_context" =>
          val metaInfo = metaInfoEnv.metaInfoRecordForName("calculation_context").get
          val v = term.metaElement match {
            case None => 1L
            case Some(e) =>
              e match {
                case MetaElement.Data =>
                  throw new InvalidTerm(s"Data meta element not valid for section ${term.metaName} in '$term'")
                case MetaElement.Shape =>
                  throw new InvalidTerm(s"Shape meta element not valid for section ${term.metaName} in '$term'")
                case MetaElement.Length => 1L
                case MetaElement.CIndex => 1L
              }
          }
          val comparer = new BaseComparer[Long](term, metaInfo, term.values.map(term.jToLong)(breakOut))
          if (!comparer.evalWithIterator(Iterator(v))) {
            logger.debug(s"discarding $calculation due to $term")
            return false
          }
        case "calculation_gid" =>
          val metaInfo = metaInfoEnv.metaInfoRecordForName("calculation_gid").get
          term.metaElement.get match {
            case MetaElement.Data =>
              val targetStrs: Seq[String] = term.values.map(term.jToString)(breakOut)
              term.compareOp.get match {
                case CompareOp.OpMatch =>
                  if (targetStrs.length != 1)
                    throw new InvalidTerm(s"regex matching expects exactly one value, not ${targetStrs.length} as in '$term'")
                  val m = new RegexMatcher(term, metaInfo, targetStrs.head.r)
                  if (!m.evalWithIterator(Iterator(calculation.calculationGid))) {
                    logger.debug(s"Discarding $calculation due to '$term'")
                    return false
                  }
                case op =>
                  val comparer = new BaseComparer[String](term, metaInfo, targetStrs)
                  if (!comparer.evalWithIterator(Iterator(calculation.calculationGid))) {
                    logger.debug(s"Discarding $calculation due to $term")
                    return false
                  }
              }
            case MetaElement.Shape =>
              import scala.math.Ordered
              import BaseComparer.seq2ordered

              val comparer = new BaseComparer[Seq[Long]](term, metaInfo, term.values.map(term.jToSeqLong)(breakOut))
              if (!comparer.evalWithIterator(Iterator(Seq()))) {
                logger.debug(s"Discarding $calculation due to $term")
                return false
              }
            case MetaElement.Length | MetaElement.CIndex =>
              val comparer = new BaseComparer[Long](term, metaInfo, term.values.map(term.jToLong)(breakOut))
              if (!comparer.evalWithIterator(Iterator(1L))) {
                logger.debug(s"Discarding $calculation due to $term")
                return false
              }
          }
      }
    }

    filterCalculationMetaInfo(calculation)
  }

  override def filterContext(context: ResolvedRef): Boolean = {
    context match {
      case FailedResolution(_, _) =>
      case RawData(_, _, _) =>
      case Archive(archiveSet, archive) =>
      case Calculation(archiveSet, calculation) =>
      case Section(archiveSet, section) =>
        if (rootTree.subTrees.isEmpty)
          return true
        var collectionPath: List[SectionCollectionH5] = Nil
        var sectionNow: Option[SectionH5] = Some(section)

        while (sectionNow match {
          case None => false
          case Some(sect) =>
            val sectionCollection = new SectionCollectionH5(
              table = sect.table,
              parentSectionLowerGIndex = 0,
              parentSectionUpperGIndex = -1,
              lowerGIndex0 = Some(sect.gIndex),
              upperGIndex0 = Some(sect.gIndex)
            )
            collectionPath = sectionCollection :: collectionPath
            sectionNow = sect.parentSection
            true
        }) ()

        var tree: Option[TermTree] = Some(rootTree)
        while (!collectionPath.isEmpty && !tree.isEmpty) {
          val collection = collectionPath.head
          if (traceFiltering) logger.debug(s"collection: $collection, tree: $tree")
          collectionPath = collectionPath.tail
          for ((meta, subT) <- tree.get.subTrees) {
            if (meta == collection.table.name) {
              if (collectionPath.isEmpty) {
                if (!subT.evaluateOnSectionCollection(collection)) {
                  if (traceFiltering) logger.debug(s"comparing $meta recursively returned false, discarding ${context.uriString}")
                  return false
                } else {
                  if (traceFiltering) logger.debug(s"comparing $meta recursively returned true, checking further ${context.uriString}")
                }
              } else if (!subT.evaluateLocalOnSectionCollection(collection)) {
                if (traceFiltering) logger.debug(s"comparing $meta returned false, discarding ${context.uriString}")
                return false
              } else {
                if (traceFiltering) logger.debug(s"comparing $meta returned true, checking further ${context.uriString}")
              }
            } else {
              meta match {
                case "section_system" =>
                  section.metaName match {
                    case "section_single_configuration_calculation" =>
                      val v = section.valueCollection("single_configuration_calculation_to_system_ref")
                      val sysTable = section.parentSection.get.table.subSectionTable("section_system")
                      val sysIt = v.map { i => sysTable(i.longValue) }
                      if (!subT.evaluateOnSectionIterable(sysIt)) {
                        if (traceFiltering) logger.debug(s"comparing section_system recursively returned false, discarding ${context.uriString}")
                        return false
                      } else {
                        if (traceFiltering) logger.debug(s"comparing section_system recursively returned true, checking further ${context.uriString}")
                      }
                    case _ =>
                      throw new QueryError(s"Cannot filter on section $meta in context $section")
                  }
                case "section_method" =>
                  section.metaName match {
                    case "section_single_configuration_calculation" =>
                      val v = section.valueCollection("single_configuration_to_calculation_method_ref")
                      val methodTable = section.parentSection.get.table.subSectionTable("section_method")
                      val methodIt = v.flatMap { i =>
                        val methodSect = try {
                          Some(methodTable(i.longValue))
                        } catch {
                          case NonFatal(e) =>
                            if (traceFiltering) logger.warn(s"Failed resolution of method ref $i in $section", e)
                            None
                        }
                        new MethodSectionIterable(methodSect)
                      }
                      if (!subT.evaluateOnSectionIterable(methodIt)) {
                        if (traceFiltering) logger.debug(s"comparing section_method recursively returned false, discarding ${context.uriString}")
                        return false
                      } else {
                        if (traceFiltering) logger.debug(s"comparing section_method recursively returned true, checking further ${context.uriString}")
                      }
                    case _ =>
                      throw new QueryError(s"Cannot filter on section $meta in context $section")
                  }
                case _ =>
                  throw new QueryError(s"Cannot filter on section $meta in context $section")
              }
            }
          }
          tree = tree.get.subTrees.get(collection.table.name)
        }
      case SectionTable(archiveSet, archive) =>
      case Value(archiveSet, value) =>
      case ValueTable(archiveSet, valueTable) =>
    }
    if (traceFiltering) logger.debug(s"accepted ${context.uriString}")
    true
  }

}
