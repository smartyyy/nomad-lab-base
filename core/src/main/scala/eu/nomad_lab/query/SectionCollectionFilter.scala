/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.query

import eu.nomad_lab.h5.SectionCollectionH5
import eu.nomad_lab.h5.ValueCollectionH5
import eu.nomad_lab.h5.SectionH5

/**
 * A filter tahat can be applied to a section collection of the section sectionMetaName
 */
abstract class SectionCollectionFilter {
  def sectionMetaName: String

  def evaluateOnSectionCollection(collection: SectionCollectionH5): Boolean = {
    evaluateOnSectionIterable(collection)
  }

  def evaluateOnSectionIterable(sections: Iterable[SectionH5]): Boolean

  def evaluateOnValueCollection(collection: ValueCollectionH5): Boolean
}
