/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.query

object MetaStatus extends Enumeration {
  type MetaStatus = Value
  val Required, Unknown, Excluded, Impossible = Value

  def merge(s1: Value, s2: Value): Value = {
    s1 match {
      case Unknown => s2
      case Required =>
        s2 match {
          case Required => Required
          case Unknown => Required
          case Excluded => Impossible
          case Impossible => Impossible
        }
      case Excluded =>
        s2 match {
          case Required => Impossible
          case Unknown => Excluded
          case Excluded => Excluded
          case Impossible => Impossible
        }
      case Impossible => Impossible
    }
  }
}
