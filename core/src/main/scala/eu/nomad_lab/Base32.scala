/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab

/**
 * Base 32 conversion, currently only long -> base32, and base32 -> long are implemented
 * We use the alphabet of base32 from https://tools.ietf.org/html/rfc4648, but lowercase,
 * to avoid O/0 1/I/l confusion and uppercase/lowercase issues
 * we encode positive numbers with as few characters as possible
 * In the back transformation we assume lowercase printing, and so 1/l confusion,
 * not 1/I confusion
 */
object Base32 {
  /**
   * A character map for base 32 encoding
   */
  class CharMap(
      val name: String,
      final val b64UrlMapStr: Array[Char],
      extraRemap: Map[Char, Char] = Map()
  ) {
    final val b64UrlMap = b64UrlMapStr.map(_.toByte)
    final val b64UrlBackMap: Array[Byte] = {
      val res = Array.fill[Byte](256)((-1).toByte)
      for ((v, i) <- b64UrlMapStr.zipWithIndex) {
        if (v.isLetter) {
          res(v.toUpper.toByte & 0xff) = i.toByte
          res(v.toLower.toByte & 0xff) = i.toByte
        } else {
          res(v.toByte & 0xff) = i.toByte
        }
      }
      for ((k, v) <- extraRemap)
        res(k.toByte & 0xFF) = b64UrlMapStr.indexOf(v).toByte
      res
    }
  }

  /**
   * The encoding defined in RFC 4648 (uppercase values, assumes 0/O 1/I confusion (not 1/l)
   */
  object RFC4648Uppercase extends CharMap(
    name = "RFC4648Uppercase",
    b64UrlMapStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567".toArray,
    extraRemap = Map('0' -> 'O', '1' -> 'I')
  )

  /**
   * Variant of the encoding defined in RFC 4648 that uses lowercase values
   * assumes 0/o 1/l confusion (not 1/I)
   */
  object RFC4648Lowercase extends CharMap(
    name = "RFC4648Lowercase",
    b64UrlMapStr = "abcdefghijklmnopqrstuvwxyz234567".toArray,
    extraRemap = Map('0' -> 'O', '1' -> 'l')
  )

  /**
   * default extension of lowercase esadecimal digits to 32 digits
   */
  object Base32Lower extends CharMap(
    name = "Base32Lower",
    b64UrlMapStr = "0123456789abcdefghijklmnopqrstuv".toArray
  )

  val defaultEncoding = RFC4648Uppercase

  /**
   * encodes a long to base32
   */
  def b32Nr(value: Long, encoding: CharMap = defaultEncoding): String = {
    var v: Long = value
    val res = Array.fill[Char](13)(' ')
    var nChar: Int = 0
    if (v == 0)
      return new String(Array(encoding.b64UrlMapStr(0)))
    while (v != 0) {
      res(nChar) = encoding.b64UrlMapStr((v & 0x1F).intValue())
      v >>>= 5
      nChar += 1
    }
    new String(res, 0, nChar)
  }

  /**
   * decodes a long from a base32 string
   */
  def b32DecodeLong(v: String, encoding: CharMap = defaultEncoding): Long = {
    var res: Long = 0
    for (c <- v.reverse) {
      val cv = encoding.b64UrlBackMap(c.toInt & 0xff)
      if (cv < 0)
        throw new Exception(s"Invalid character $c in base32 string")
      res = (res << 5) | (cv & 0x1f)
    }
    res
  }

  /**
   * Repository-specific Base32-encoding compliant with Tom's custom pID-assignment.
   */
  def b32NrRepository(value: Long): String = {
    b32Nr(value, Base32Lower).reverse
  }

  /**
   * Repository-specific Base32-decoding compliant with Tom's custom pID-assignment.
   */
  def b32DecodeLongRepository(v: String): Long = {
    b32DecodeLong(v.reverse, Base32Lower)
  }
}
