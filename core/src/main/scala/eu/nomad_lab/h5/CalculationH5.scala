/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.h5

import eu.nomad_lab.{ IteratorH5, H5Lib, JsonUtils, RefCount }
import eu.nomad_lab.meta.MetaInfoEnv
import eu.nomad_lab.meta.MetaInfoRecord
import eu.nomad_lab.ref._

import scala.collection.mutable

object CalculationH5 {
  val calculationRe = "^(?:[PC][-_a-zA-Z0-9]{28}|[Uu]nknown|_)$".r

  def unapply(calc: CalculationH5): Option[Tuple3[ArchiveH5, String, Long]] = {
    Some((calc.archive, calc.calculationGid, calc.calculationGroup))
  }

  /**
   * Error in a HDF5 calculation
   */
  class CalculationH5Error(
    msg: String,
    what: Throwable = null
  ) extends Exception(msg, what)

}

/**
 * class that represents a calculation stored in an HDF5 file
 *
 * takes ownership of calculationGroup
 * A calculation is reference counted and should be explicitly released to ensure that the underlying HDF5 handles get closed.
 */
class CalculationH5(
    val archive: ArchiveH5,
    val calculationGid: String,
    val calculationGroup: Long,
    val instantiateSectionTable: (MetaInfoRecord, SectionTableContainerH5, CalculationH5, Option[Long]) => SectionTableH5 = SectionTableH5.apply,
    val instantiateValueTable: (String, SectionTableH5) => ValueTableH5 = ValueTableH5.apply,
    val sectionTables0: Map[String, SectionTableH5] = Map(),
    val valueTables0: Map[String, ValueTableH5] = Map()
) extends SectionTableContainerH5 with RefCount {
  /**
   * Reference to this calculation
   */
  def toRef: CalculationRef = {
    CalculationRef(archive.archiveGid, calculationGid)
  }

  /**
   * True if this calculation is a pseudo calculation (i.e. an index)
   */
  def isPseudo: Boolean = {
    calculationGid == "_"
  }

  archive.retain()

  override def name: String = calculationGid

  override def calculation: CalculationH5 = this

  override def sectionGroup(create: Boolean = false): Long = calculationGroup

  protected var _subSectionTables: Map[String, SectionTableH5] = sectionTables0

  final override def equals(other: Any): Boolean = {
    val that = other.asInstanceOf[CalculationH5]
    if (that == null)
      false
    else
      calculationGid == that.calculationGid && archive == that.archive
  }

  def metaInfoEnv: MetaInfoEnv = {
    archive.fileH5.metaInfoEnv
  }

  override def release0(): Unit = {
    clearCache()
    H5Lib.groupClose(calculationGroup)
    archive.release()
  }

  /**
   * returns the section table for the given metaName path
   *
   * section can be at any depth, and will be cached
   */
  def sectionTable(path: Seq[String]): SectionTableH5 = {
    if (path.isEmpty)
      throw new CalculationH5.CalculationH5Error(s"Empty path in calculation.sectionTable")
    val first: SectionTableH5 = subSectionTable(path(0))
    path.drop(1).foldLeft(first) { (r, el) => r.subSectionTable(el) }
  }

  def sectionTableFromPath(path: String): SectionTableH5 = {
    sectionTable(path.split('/').toSeq)
  }

  /**
   * returns the section table for the given metaName
   *
   * section can be at any depth, and will be cached
   */
  def valueTable(path: Seq[String]): ValueTableH5 = {
    if (path.length < 2)
      throw new CalculationH5.CalculationH5Error(s"Path in calculation.valueTable must have at least 2 elements (section and value), got ${path.mkString("[", ",", "]")}")
    val sTable = sectionTable(path.dropRight(1))
    sTable.subValueTable(path.last)
  }

  def valueTableFromPath(path: String): ValueTableH5 = {
    valueTable(path.split('/').toSeq)
  }

  /**
   * clears the section table cache (and closes the HDF5 handles)
   */
  override def clearLocalCache(): Unit = {
  }

  override def toString(): String = {
    s"CalculationH5($calculationGid)"
  }
}
