/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.h5
import eu.nomad_lab.parsers.{ ParserBackendExternal }
import eu.nomad_lab.resolve.{ Archive, Calculation, ResolvedRef, Section }
import org.json4s.JsonAST.{ JNothing, JString }
import ucar.ma2

class EmitEventsVisitor(
    backend: ParserBackendExternal
) extends H5Visitor {
  var archiveIndex = 0L
  var calcIndex = 0L

  override def willVisitContext(context: ResolvedRef): Unit = {
    backend.startedParsingSession(None, JNothing, None)
    context match {
      case Archive(_, arch) => ()
      case Calculation(_, calc) => shouldVisitArchive(calc.archive)
      case Section(_, sec) =>
        var sectionPath: List[SectionH5] = Nil
        var sAtt: SectionH5 = sec
        while (sAtt.parentSection match {
          case Some(p) =>
            sAtt = p
            sectionPath = p :: sectionPath
            true
          case None =>
            false
        }) ()
        val calc = sec.table.calculation
        shouldVisitArchive(calc.archive)
        shouldVisitCalculation(calc)
        sectionPath.foreach(shouldVisitSection)
      case _ => throw new Exception(s"Can not accept context of type ${context}")
    }
  }

  override def didVisitContext(context: ResolvedRef): Unit = {
    context match {
      case Archive(_, arch) => ()
      case Calculation(_, calc) => didVisitArchive(calc.archive)
      case Section(_, sec) =>
        var sectionPath: List[SectionH5] = Nil
        var sAtt: SectionH5 = sec
        while (sAtt.parentSection match {
          case Some(p) =>
            sAtt = p
            sectionPath = p :: sectionPath
            true
          case None =>
            false
        }) ()
        val calc = sec.table.calculation
        sectionPath.reverse.foreach(didVisitSection)
        didVisitCalculation(calc)
        didVisitArchive(calc.archive)
      case _ => throw new Exception(s"Can not accept context of type ${context}")
    }
    backend.finishedParsingSession(None, JNothing, None)
  }
  override def shouldVisitArchive(archive: ArchiveH5): Boolean = {
    backend.openSectionWithGIndex("archive_context", archiveIndex)
    backend.addValue("archive_gid", JString(archive.archiveGid))
    true
  }

  override def didVisitArchive(archiveH5: ArchiveH5): Unit = {
    backend.closeSection("archive_context", archiveIndex)
    archiveIndex += 1
  }

  override def shouldVisitCalculation(calculationH5: CalculationH5): Boolean = {
    backend.openSectionWithGIndex("calculation_context", calcIndex)
    backend.addValue("calculation_gid", JString(calculationH5.calculationGid))
    true
  }

  override def didVisitCalculation(calculationH5: CalculationH5): Unit = {
    backend.closeSection("calculation_context", calcIndex)
    calcIndex += 1
  }

  override def shouldVisitSection(section: SectionH5): Boolean = {
    backend.openSectionWithGIndex(section.metaName, section.gIndex)
    true
  }

  override def didVisitSection(section: SectionH5): Unit = {
    backend.closeSection(section.metaName, section.gIndex)
  }

  override def visitScalarValue(value: ValueH5): Unit = {
    backend.addValue(value.metaName, value.jValue, value.parentSection.gIndex)
  }

  override def visitArrayValue(value: ValueH5): Unit = {
    val maxEls = 10000000L
    val shape = value.arrayShape
    val nEl = shape.foldLeft(1L)(_ * _)
    val metaName = value.table.name
    if (nEl < maxEls) {
      backend.addArrayValues(metaName, value.arrayValue())
    } else {
      backend.addArray(metaName, shape)
      val offset = Array.fill(shape.length)(0L)
      val subShape = Array.fill(shape.length)(1L)
      var nSubEl: Long = 1L
      var iDim: Int = shape.length - 1
      while (iDim >= 0 && nSubEl < maxEls / 2) {
        var newDim: Long = shape(iDim)
        if (newDim * nSubEl > maxEls)
          newDim = (maxEls + nSubEl - 1) / nSubEl
        subShape(iDim) = newDim
        nSubEl = newDim * nSubEl
        iDim -= 1
      }

      var hasMoreData: Boolean = true
      val shapeNow = Array.fill(shape.length)(1L)
      while (hasMoreData) {
        for ((s, i) <- shape.zipWithIndex)
          shapeNow(i) = ((s - offset(i)).min(subShape(i))).max(1)
        val arr: ma2.Array = value.arrayValue(valueOffset = offset.toSeq, valueDims = shapeNow.toSeq)
        backend.setArrayValues(metaName, offset = Some(offset.toSeq), values = arr)
        var iDim2: Int = shape.length - 1
        var riporto: Int = 0
        while (iDim2 >= 0) {
          if (offset(iDim2) + shapeNow(iDim2) + riporto < shape(iDim2)) {
            offset(iDim2) += shapeNow(iDim2) + riporto
            riporto = 0
          } else {
            riporto = 1
            for (i <- iDim2 until shape.length)
              offset(i) = 0
          }
          iDim2 -= 1
        }
        if (riporto > 0)
          hasMoreData = false
      }
      backend.closeArray(metaName, shape)
    }
  }
}
