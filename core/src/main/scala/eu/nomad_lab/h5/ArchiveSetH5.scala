/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.h5
import collection.mutable
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.Files
import eu.nomad_lab.meta
import com.typesafe.config.Config
import com.typesafe.config.ConfigFactory
import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.ref._
import eu.nomad_lab.LocalEnv
import scala.util.control.NonFatal
import eu.nomad_lab.H5Lib

object ArchiveSetH5 {
  /**
   * The settings of an archive set
   */
  class Settings(config: Config) {
    // validate vs. reference.conf
    config.checkValid(ConfigFactory.defaultReference(), "simple-lib")
    val baseDir: Path = Paths.get(LocalEnv.makeReplacements(LocalEnv.defaultSettings.replacements, config.getString("archiveSet.baseDirectory")))
    val hasPrefix: Boolean = config.getBoolean("archiveSet.hasPrefix")
    val writeable: Boolean = config.getBoolean("archiveSet.writeable")
    val filePrefix: String = config.getString("archiveSet.filePrefix")
    val defaultMetaEnv: String = config.getString("archiveSet.defaultMetaEnv")
  }

  def apply(settings: Settings): ArchiveSetH5 = {
    apply(settings, None)
  }

  def apply(settings: Settings, superArchiveSet: Option[ArchiveSetH5]): ArchiveSetH5 = {
    val metaName = settings.defaultMetaEnv
    val metaEnv: meta.MetaInfoEnv = if (metaName.contains('/')) {
      meta.SimpleMetaInfoEnv.fromFilePath(metaName, new meta.RelativeDependencyResolver())
    } else if (!metaName.isEmpty) {
      val it = meta.KnownMetaInfoEnvs.versionsWithName(metaName)
      if (it.hasNext)
        it.next
      else
        throw new ArchiveSetException(s"could not find meta info $metaName for ArchiveSet")
    } else {
      meta.KnownMetaInfoEnvs.all
    }
    new ArchiveSetH5(settings.baseDir, settings.hasPrefix, settings.writeable, Some(metaEnv), filePrefix = settings.filePrefix, superArchiveSet = superArchiveSet)
  }

  def settingsWithPrefix(config: Option[Config] = None, prefix: String = "normalized"): Settings = {
    val conf = config match {
      case None => eu.nomad_lab.LocalEnv.defaultConfig
      case Some(c) => c
    }
    new Settings(conf.getConfig(prefix))
  }

  var _parsedSet: Option[ArchiveSetH5] = None
  /**
   * Archive Set on the parsed data
   */
  def parsedSet: ArchiveSetH5 = {
    _parsedSet match {
      case Some(qs) => qs
      case None =>
        val newQS = apply(settingsWithPrefix(None, "nomad_lab.parsed"))
        _parsedSet = Some(newQS)
        newQS
    }
  }

  var _normalizedSet: Option[ArchiveSetH5] = None
  /**
   * Archive Set on the normalized data
   */
  def normalizedSet: ArchiveSetH5 = {
    _normalizedSet match {
      case Some(qs) => qs
      case None =>
        val newQS = apply(settingsWithPrefix(None, "nomad_lab.normalized"))
        _normalizedSet = Some(newQS)
        newQS
    }
  }

  var _toNormalizeSet: Option[ArchiveSetH5] = None
  /**
   * Archive Set to be normalized
   */
  def toNormalizeSet: ArchiveSetH5 = {
    _toNormalizeSet match {
      case Some(qs) => qs
      case None =>
        val newQS = apply(settingsWithPrefix(None, "nomad_lab.toNormalize"), Some(parsedSet))
        _toNormalizeSet = Some(newQS)
        newQS
    }
  }

  /**
   * called when there is an error un the archive set (tipically an invalid or missing reference)
   */
  class ArchiveSetException(
    msg: String
  ) extends Exception(msg)

  /**
   * creates an object that manages a set of archives
   */
  def apply(
    basePath: Path,
    hasPrefixDir: Boolean = true,
    writable: Boolean = false,
    metaInfoEnv0: Option[meta.MetaInfoEnv] = None,
    filePrefix: String = "N",
    superArchiveSet: Option[ArchiveSetH5] = None
  ): ArchiveSetH5 = {
    new ArchiveSetH5(basePath, hasPrefixDir, writable, metaInfoEnv0, filePrefix, superArchiveSet)
  }

}

/**
 * Object that represents a set of parsed or normalized hdf files
 * Can be used to resolve NomadRef, and/or to synchronize the
 * creation and completion (adding of normalized data) to Hdf5 files.
 *
 * lookup (resolving) of NomadRef is done with the acquire*FromRef methods
 * The returned values must be returned after use with the giveBack* methods
 */
class ArchiveSetH5(
    val basePath: Path,
    val hasPrefixDir: Boolean = true,
    val writable: Boolean = false,
    metaInfoEnv0: Option[meta.MetaInfoEnv] = None,
    val filePrefix: String = "N",
    val superArchiveSet: Option[ArchiveSetH5] = None
) extends StrictLogging {
  import ArchiveSetH5.ArchiveSetException

  def objectKind: ObjectKind.Value = {
    filePrefix match {
      case "N" =>
        ObjectKind.NormalizedData
      case "S" =>
        ObjectKind.ParsedData
      case _ =>
        ObjectKind.NormalizedData
    }
  }

  val metaInfoEnv: meta.MetaInfoEnv = metaInfoEnv0 match {
    case Some(m) => m
    case None => meta.KnownMetaInfoEnvs.all
  }
  private val archives: mutable.Map[String, ArchiveH5] = mutable.Map()
  private val archivesCount: mutable.Map[String, Int] = mutable.Map()

  /**
   * called if an archive is missing from the archive set
   */
  def onMissingArchive(archiveGid: String, targetPath: Path): ArchiveH5 = {
    if (writable) {
      if (!Files.exists(targetPath.getParent()))
        Files.createDirectories(targetPath.getParent(), LocalEnv.directoryPermissionsAttributes)
      val file = FileH5.create(targetPath, Some(metaInfoEnv))
      val arch = superArchiveSet match {
        case Some(superA1) =>
          val superA2 = try {
            Some(superA1.acquireArchive(archiveGid))
          } catch {
            case NonFatal(e) =>
              logger.warn(s"missing archive $archiveGid is not present in superArchiveSet $superA1, creating empty archive at $targetPath", e)
              None
          }
          superA2 match {
            case Some(superA3) =>
              H5Lib.objectCopy(superA3.archiveGroup, ".", file.h5FileGroup, "./" + archiveGid)
              file.openArchive(archiveGid, create = false)
            case None =>
              file.openArchive(archiveGid, create = true)
          }
        case None =>
          file.openArchive(archiveGid, create = true)
      }
      file.release()
      arch
    } else {
      throw new ArchiveSetException(s"read-only ArchiveSet could not find file for archive $archiveGid at $targetPath")
    }
  }

  /**
   * called if an calculation is missing from the archive set
   */
  def onMissingCalculation(calculationGid: String, archiveH5: ArchiveH5): CalculationH5 = {
    if (writable)
      archiveH5.openCalculation(calculationGid, create = true)
    else
      throw new ArchiveSetException(s"ArchiveSet could not create calculation $calculationGid in archive $archiveH5")
  }

  /**
   * lookups the given archive
   * you have to call giveBackArchive when you are done using it
   */
  def acquireArchiveFromRef(archiveRef: ArchiveRef): ArchiveH5 = {
    acquireArchive(archiveRef.archiveGid)
  }

  /**
   * lookups the given archive
   * you have to call giveBackArchive when you are done using it
   */
  def acquireArchiveFromUriStr(uriStr: String): ArchiveH5 = {
    NomadUri(uriStr).toRef match {
      case ArchiveRef(archiveGid) =>
        acquireArchive(archiveGid)
      case r =>
        throw new ArchiveSetException(s"expected a plain reference to an archive, not $r parsing $uriStr in acquireArchiveFromUriStr")
    }
  }

  /**
   * lookups the archive with the given archiveGid
   * you have to call giveBackArchive when you are done using it
   */
  def acquireArchive(archiveGid: String): ArchiveH5 = {
    synchronized {
      archivesCount += (archiveGid -> (archivesCount.getOrElse(archiveGid, 0) + 1))
      archives.get(archiveGid) match {
        case Some(arch) => arch
        case None =>
          val targetPath = if (hasPrefixDir)
            basePath.resolve(filePrefix + archiveGid.substring(1, 3)).resolve(filePrefix + archiveGid.substring(1) + ".h5")
          else
            basePath.resolve(archiveGid + ".h5")
          val archiveH5 = if (!Files.exists(targetPath)) {
            onMissingArchive(archiveGid, targetPath)
          } else {
            val file = FileH5.open(targetPath, writable, Some(metaInfoEnv))
            val arch = file.openArchive(archiveGid)
            file.release()
            arch
          }
          archives += (archiveGid -> archiveH5)
          archiveH5
      }
    }
  }

  /**
   * call this when you are done with an archive you got though acquireArchive*
   */
  def giveBackArchive(archive: ArchiveH5): Unit = {
    synchronized {
      val archiveGid = archive.archiveGid
      val oldC = archivesCount.getOrElse(archiveGid, 0)
      if (oldC > 1) {
        archivesCount += (archiveGid -> (oldC - 1))
      } else if (oldC == 1) {
        archivesCount.remove(archiveGid)
        archives(archiveGid).release()
        archives.remove(archiveGid)
        if (archive.refCount != 0)
          throw new ArchiveSetException(s"archive is still retained when giving it back")
      } else {
        throw new ArchiveSetException(s"giving back non open archive $archiveGid")
      }
    }
  }

  private val calculations: mutable.Map[String, CalculationH5] = mutable.Map()

  /**
   * lookups the given calculation
   * you have to call giveBackCalculation when you are done using it
   */
  def acquireCalculationFromRef(calc: CalculationRef): CalculationH5 = {
    acquireCalculation(calc.archiveGid, calc.calculationGid)
  }

  /**
   * lookups the given calculation
   * you have to call giveBackCalculation when you are done using it
   */
  def acquireCalculationFromUriStr(uriStr: String): CalculationH5 = {
    NomadUri(uriStr).toRef match {
      case CalculationRef(archiveGid, calculationGid) =>
        acquireCalculation(archiveGid, calculationGid)
      case r =>
        throw new ArchiveSetException(s"expected a plain reference to a calculation, not $r parsing $uriStr in acquireCalculationFromUriStr")
    }
  }

  /**
   * lookups the given calculation
   * you have to call giveBackCalculation when you are done using it
   */
  def acquireCalculation(archiveGid: String, calculationGid: String): CalculationH5 = {
    acquireArchive(archiveGid).openCalculation(calculationGid)
  }

  /**
   * Releases the resources (HDF5 handles) used by a calculation that was acquired
   */
  def giveBackCalculation(calc: CalculationH5): Unit = {
    val arch = calc.archive
    calc.release()
    if (calc.refCount != 0)
      throw new ArchiveSetException(s"calculation is still retained when giving it back")
    giveBackArchive(arch)
  }

  /**
   * Looks up a section table
   *
   * you have to call giveBackSectionTable when you are done using it
   */
  def acquireSectionTableFromRef(tableRef: TableRef): SectionTableH5 = {
    acquireCalculation(tableRef.archiveGid, tableRef.calculationGid).sectionTable(tableRef.path)
  }

  /**
   * Looks up a section table
   *
   * you have to call giveBackSectionTable when you are done using it
   */
  def acquireSectionTableFromUriStr(uriStr: String): SectionTableH5 = {
    NomadUri(uriStr).toRef match {
      case tr: TableRef =>
        acquireSectionTableFromRef(tr)
      case r =>
        throw new ArchiveSetException(s"expected a plain reference to an table, not $r parsing $uriStr in acquireSectionTableFromUriStr")
    }
  }

  /**
   * Releases the resources (HDF5 handles) used by a section table that was acquired
   */
  def giveBackSectionTable(sectionTable: SectionTableH5): Unit = {
    // sectionTable.clearLocalCache()
    giveBackCalculation(sectionTable.calculation)
  }

  /**
   * Resolves a table ref to either a section or a value table
   *
   * The returned object will have to be released with giveBackSection or giveBackValue
   */
  def acquireTableRef(tableRef: TableRef): Either[SectionTableH5, ValueTableH5] = {
    if (tableRef.path.length == 1) {
      Left(acquireSectionTableFromRef(tableRef))
    } else {
      val parentSectionTable = acquireSectionTableFromRef(TableRef(tableRef.archiveGid, tableRef.calculationGid, tableRef.path.dropRight(1)))
      val res = parentSectionTable.entryKindStr(tableRef.path.last) match {
        case Some("type_section") =>
          Left(acquireSectionTableFromRef(tableRef))
        case Some("type_document_content") =>
          Right(acquireValueTableFromRef(tableRef))
        case v =>
          throw new ArchiveSetException(s"cannot resolve type of $tableRef, got $v")
      }
      giveBackSectionTable(parentSectionTable)
      res
    }
  }

  /**
   * Resolves a row reference to either a section of a value
   *
   * The returned object will have to be released with giveBackSection or giveBackValue
   */
  def acquireRowRef(rowRef: RowRef): Either[SectionH5, ValueH5] = {
    if (rowRef.indexedPath.length == 1) {
      Left(acquireSectionFromRef(rowRef))
    } else {
      val parentSectionTable = acquireSectionTableFromRef(TableRef(rowRef.archiveGid, rowRef.calculationGid, rowRef.metaPath.dropRight(1)))
      val res = parentSectionTable.entryKindStr(rowRef.metaPath.last) match {
        case Some("type_section") =>
          Left(acquireSectionFromRef(rowRef))
        case Some("type_document_content") =>
          Right(acquireValueFromRef(rowRef))
        case v =>
          throw new ArchiveSetException(s"cannot resolve type of $rowRef, got $v")
      }
      giveBackSectionTable(parentSectionTable)
      res
    }
  }

  /**
   * looks up a value table
   *
   * The returned object will have to be released with giveBackValueTable
   */
  def acquireValueTableFromUriStr(uriStr: String): ValueTableH5 = {
    NomadUri(uriStr).toRef match {
      case tr: TableRef =>
        acquireValueTableFromRef(tr)
      case r =>
        throw new ArchiveSetException(s"expected a plain reference to an table, not $r parsing $uriStr in acquireValueTableFromUriStr")
    }
  }

  /**
   * looks up a value table
   *
   * The returned object will have to be released with giveBackValueTable
   */
  def acquireValueTableFromRef(tableRef: TableRef): ValueTableH5 = {
    acquireCalculationFromRef(tableRef.calculationRef).sectionTable(tableRef.path.dropRight(1)).
      subValueTable(tableRef.path.last)
  }

  /**
   * Releases the resources (HDF5 handles) used by a value table that was acquired
   */
  def giveBackValueTable(valueTable: ValueTableH5): Unit = {
    giveBackCalculation(valueTable.parentSectionTable.calculation)
  }

  /**
   * looks up section
   *
   * The returned object will have to be released with giveBackSection
   */
  def acquireSectionFromRef(row: RowRef): SectionH5 = {
    val calc = acquireCalculation(row.archiveGid, row.calculationGid)
    val lastSegment = row.indexedPath.last
    lastSegment._2.get("c") match {
      case Some(i) =>
        val sectionTable = calc.sectionTable(row.metaPath)
        sectionTable(i)
      case None =>
        var anchoredSection = row.indexedPath
        while (anchoredSection.length > 1 && !anchoredSection.last._2.contains("c"))
          anchoredSection = anchoredSection.dropRight(1)
        val anchoredMetaNames = anchoredSection.map(_._1)
        val anchoredIdxs = anchoredSection.last._2
        val anchoredIdx = anchoredIdxs.get("c") match {
          case Some(idx) => idx
          case None =>
            anchoredIdxs.get("l") match {
              case Some(idx) => idx
              case None =>
                throw new ArchiveSetException(s"cannot acquire section $row with invalid row reference that does specify neither calculation indexes (<nr>c) not local indexes (<nr>l).")
            }
        }
        var sectionAtt = calc.sectionTable(anchoredMetaNames)(anchoredIdx)
        for ((sect, localIndex) <- row.indexedPath.drop(anchoredSection.length)) {
          localIndex.get("l") match {
            case Some(i) =>
              sectionAtt = sectionAtt.subSectionCollection(sect)(i)
            case None =>
              throw new ArchiveSetException(s"cannot acquire section $row with invalid row reference that does specify neither calculation indexes (<nr>c) nor local indexes (<nr>l), $sect misses its index.")
          }
        }
        sectionAtt
    }
  }

  /**
   * looks up section
   *
   * The returned object will have to be released with giveBackSection
   */
  def acquireSectionFromUriStr(uriStr: String): SectionH5 = {
    NomadUri(uriStr).toRef match {
      case rowRef: RowRef =>
        acquireSectionFromRef(rowRef)
      case r =>
        throw new ArchiveSetException(s"expected a plain reference to an archive, not $r parsing $uriStr in acquireSectionFromUriStr")
    }
  }

  /**
   * Releases the resources (HDF5 handles) used by a section that was acquired
   */
  def giveBackSection(section: SectionH5): Unit = {
    giveBackCalculation(section.table.calculation)
  }

  /**
   * Looks up a value
   *
   * The returned object will have to be released with giveBackSection
   */
  def acquireValueFromUriStr(uriStr: String): ValueH5 = {
    NomadUri(uriStr).toRef match {
      case rRef: RowRef =>
        acquireValueFromRef(rRef)
      case r =>
        throw new ArchiveSetException(s"expected a plain row reference to a value, not $r parsing $uriStr in acquireValueFromUriStr")
    }
  }

  /**
   * Looks up a value
   *
   * The returned object will have to be released with giveBackSection
   */
  def acquireValueFromRef(row: RowRef): ValueH5 = {
    val calc = acquireCalculation(row.archiveGid, row.calculationGid)
    val lastSegment = row.indexedPath.last
    lastSegment._2.get("c") match {
      case Some(idx) =>
        val sectionTable = calc.sectionTable(row.indexedPath.dropRight(1).map(_._1))
        sectionTable.subValueTable(lastSegment._1)(idx)
      case None =>
        lastSegment._2.get("l") match {
          case Some(idx) =>
            val parentSection = acquireSectionFromRef(row.copy(indexedPath = row.indexedPath.dropRight(1)))
            parentSection.valueCollection(lastSegment._1)(idx)
          case None =>
            throw new ArchiveSetException(s"cannot acquire section $row with invalid row reference that does specify neither calculation indexes (<nr>c) nor local indexes, last segment has no local or calculation index.")
        }
    }
  }

  /**
   * Releases the resources (HDF5 handles) used by a value that was acquired
   */
  def giveBackValue(value: ValueH5): Unit = {
    giveBackCalculation(value.table.parentSectionTable.calculation)
  }
}
