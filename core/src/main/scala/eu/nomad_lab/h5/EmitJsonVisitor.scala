/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.h5
import eu.nomad_lab.ref._
import eu.nomad_lab.resolve._
import eu.nomad_lab.resolve.Resolver
import eu.nomad_lab.resolve.ResolvedRef
import scala.util.control.NonFatal
import scala.collection.mutable
import eu.nomad_lab.JsonUtils
import org.{ json4s => jn }
import scala.collection.breakOut

object EmitJsonVisitor {
  import java.nio.charset.StandardCharsets
  import java.nio.file.Path
  import java.nio.file.Paths
  import java.nio.file.Files
  import eu.nomad_lab.LocalEnv

  def dumpToJson(archive: Archive, targetDir: Path): Unit = {
    if (!Files.exists(targetDir))
      Files.createDirectories(targetDir, LocalEnv.directoryPermissionsAttributes)
    for (c <- archive.archive.calculations) {
      val cPath = targetDir.resolve(c.calculationGid + ".json")
      Files.createFile(cPath, LocalEnv.filePermissionsAttributes)
      val outF = new java.io.FileOutputStream(cPath.toFile)
      val w = new java.io.OutputStreamWriter(outF, StandardCharsets.UTF_8)
      val archivePrefix = archive.objectKind match {
        case ObjectKind.NormalizedData => "N"
        case ObjectKind.ParsedData => "S"
        case ObjectKind.RawData => "R"
      }
      w.write(s"""{
                 |  "type": "nomad_archive_2_0",
                 |  "name": "archive_context",
                 |  "uri": "${archive.uriString}",
                 |  "archive_gid": "A${archive.archive.archiveGid.drop(1)}",
                 |  "calculation_context": [""".stripMargin)
      val visitor = new EmitJsonVisitor(
        writeOut = { x: String => w.write(x) }
      )
      val scanner = new H5EagerScanner
      scanner.scanResolvedRef(archive, visitor)
      w.write(s"]\n}\n")
      w.flush()
      w.close()
    }
  }

  object MessageLevel extends Enumeration {
    type MessageLevel = Value
    val Debug, Info, Warn, Error = Value
  }

  case class Message(
    msg: String,
    level: MessageLevel.Value = MessageLevel.Warn
  )

}

final class EmitJsonVisitor(
    val writeOut: String => Unit,
    val startIndentLevel: Int = 0,
    val indentAmount: Int = 2,
    val sizePerMeta: Long = Long.MaxValue,
    val embedWarningAtTopLevel: Boolean = false
) extends H5Visitor {
  import EmitJsonVisitor.Message
  import EmitJsonVisitor.MessageLevel

  var indentLevel: Int = startIndentLevel
  val emptyObj = mutable.ListBuffer[Boolean](true)
  var context: Option[ResolvedRef] = None
  var metaOutSizes: Map[String, Long] = Map()
  var limitedMeta: Set[String] = Set()
  var _warnings: Seq[Message] = Seq()
  var _warningsCount: Map[Message, Int] = Map()

  /**
   * writes out and updates the amount outputted to metaName
   */
  def writeAndCount(metaName: String, toWrite: String): Unit = {
    metaOutSizes += metaName -> (metaOutSizes.getOrElse(metaName, 0L) + toWrite.length)
    writeOut(toWrite)
  }

  /**
   * checks if the given metaName should be skipped because it reaced the maximum size
   * if it should be skipped registers that it was actually skipped
   */
  def shouldSkip(metaName: String): Boolean = {
    if (metaOutSizes.getOrElse(metaName, 0L) > sizePerMeta) {
      limitedMeta += metaName
      true
    } else {
      false
    }
  }

  /**
   * Writes the metaName if it has not yet reached the maximum size, otherwise skips it.
   *
   * Returns true if it did write.
   */
  def maybeWrite(metaName: String, toWrite: String): Boolean = {
    if (!shouldSkip(metaName)) {
      writeAndCount(metaName, toWrite)
      true
    } else {
      false
    }
  }

  override def shouldVisitSection(section: SectionH5): Boolean = {
    val metaName = section.metaName
    if (!shouldSkip(metaName)) {
      if (!emptyObj.last)
        writeAndCount(metaName, ", ")
      else
        emptyObj.update(emptyObj.length - 1, false)
      indentLevel += indentAmount
      writeAndCount(metaName, s"""{
${" " * indentLevel}"type": "nomad_section_2_0",
${" " * indentLevel}"name": "${section.metaName}",
${" " * indentLevel}"gIndex": ${section.gIndex},
${" " * indentLevel}"uri": "${section.toRef.toUriStr(objectKind)}"""")
      emptyObj.append(false)
      true
    } else {
      false
    }
  }

  override def didVisitSection(section: SectionH5): Unit = {
    if (emptyObj.length == 1)
      willCloseLastObject()
    val metaName = section.metaName
    emptyObj.trimEnd(1)
    indentLevel -= indentAmount
    if (!emptyObj.last)
      writeAndCount(metaName, "\n" + " " * indentLevel)
    else
      emptyObj.update(emptyObj.length - 1, false)
    writeAndCount(metaName, "}")
  }

  override def visitScalarValue(value: ValueH5): Unit = {
    val metaName = value.metaName
    if (!emptyObj.last) {
      if (shouldSkip(metaName))
        return ()
      writeAndCount(metaName, ",")
    } else {
      emptyObj.update(emptyObj.length - 1, false)
    }
    writeAndCount(metaName, JsonUtils.normalizedStr(value.jValue))
  }

  override def visitArrayValue(value: ValueH5): Unit = {
    val metaName = value.metaName
    if (!emptyObj.last) {
      if (shouldSkip(metaName))
        return ()
      writeAndCount(metaName, ",")
    } else {
      emptyObj.update(emptyObj.length - 1, false)
    }
    // to improve
    if (ValueH5.maxElsJValue < value.arrayShape.foldLeft(1L)(_ * _))
      limitedMeta += metaName
    writeAndCount(metaName, JsonUtils.normalizedStr(value.jValue))
  }

  override def shouldVisitArchive(archive: ArchiveH5): Boolean = {
    val metaName = "archive_context"
    emptyObj.append(true)
    indentLevel += indentAmount
    writeAndCount(metaName, s"""{
${" " * indentLevel}"type": "nomad_archive_2_0",
${" " * indentLevel}"name": "archive_context",
${" " * indentLevel}"archive_gid": "${archive.archiveGid}",
${" " * indentLevel}"uri": "${archive.toRef.toUriStr(objectKind)}",
${" " * indentLevel}"calculation_context": [""")
    indentLevel += indentAmount
    true
  }

  override def didVisitArchive(archive: ArchiveH5): Unit = {
    val metaName = "archive_context"
    indentLevel -= indentAmount
    if (!emptyObj.last)
      writeAndCount(metaName, "\n" + (" " * indentLevel))
    indentLevel -= indentAmount
    writeAndCount(metaName, "]")
    if (emptyObj.length == 2)
      willCloseLastObject()
    emptyObj.trimEnd(1)
    writeAndCount(metaName, s"""
${" " * indentLevel}}""")
  }

  override def shouldVisitCalculation(calculation: CalculationH5): Boolean = {
    val metaName = "calculation_context"
    if (!emptyObj.last)
      writeAndCount(metaName, ", ")
    else
      emptyObj.update(emptyObj.length - 1, false)
    if (shouldSkip(metaName)) {
      val newIndentLevel = indentLevel + indentAmount
      writeAndCount(metaName, s"""{
${" " * newIndentLevel}"type": "nomad_calculation_2_0",
${" " * newIndentLevel}"name": "calculation_context",
${" " * newIndentLevel}"uri": "${calculation.toRef.toUriStr(objectKind)}",
${" " * newIndentLevel}"calculation_gid": "${calculation.calculationGid}"
${" " * indentLevel}}""")
      false
    } else {
      emptyObj.append(false)
      indentLevel += indentAmount
      writeAndCount(metaName, s"""{
${" " * indentLevel}"type": "nomad_calculation_2_0",
${" " * indentLevel}"name": "calculation_context",
${" " * indentLevel}"uri": "${calculation.toRef.toUriStr(objectKind)}",
${" " * indentLevel}"calculation_gid": "${calculation.calculationGid}"""")
      true
    }
  }

  override def didVisitCalculation(calculation: CalculationH5): Unit = {
    if (emptyObj.length == 2)
      willCloseLastObject()
    val metaName = "calculation_context"
    metaOutSizes = Map(
      "archive_context" -> metaOutSizes.getOrElse("archive_context", 0L),
      metaName -> metaOutSizes.filter { _._1 != "archive_context" }.foldLeft(0L)(_ + _._2)
    )
    indentLevel -= indentAmount
    if (!emptyObj.last)
      writeAndCount(metaName, "\n" + (" " * indentLevel))
    emptyObj.trimEnd(1)
    writeAndCount(metaName, s"""}""")
  }

  override def shouldVisitSectionTable(sectionTable: SectionTableH5): Boolean = {
    val metaName = sectionTable.name
    if (shouldSkip(metaName))
      return false
    if (!emptyObj.last)
      writeAndCount(metaName, ", ")
    else
      emptyObj.update(emptyObj.length - 1, false)
    writeAndCount(metaName, s"""
${" " * indentLevel}"${sectionTable.name}": """)
    emptyObj.append(true)
    if (sectionTable.metaInfo.repeats.getOrElse(true)) {
      writeAndCount(metaName, "[")
      indentLevel += indentAmount
    }
    true
  }

  override def didVisitSectionTable(sectionTable: SectionTableH5): Unit = {
    val metaName = sectionTable.name
    if (sectionTable.metaInfo.repeats.getOrElse(true)) {
      indentLevel -= indentAmount
      if (!emptyObj.last)
        writeAndCount(metaName, "\n" + " " * indentLevel)
      writeAndCount(metaName, "]")
    }
    emptyObj.trimEnd(1)
  }

  override def shouldVisitValueTable(valueTable: ValueTableH5): Boolean = {
    val metaName = valueTable.name
    if (shouldSkip(metaName))
      return false
    if (!emptyObj.last)
      writeAndCount(metaName, ", ")
    else
      emptyObj.update(emptyObj.length - 1, false)
    writeAndCount(metaName, s"""
${" " * indentLevel}"${valueTable.name}": """)
    emptyObj.append(true)
    if (valueTable.metaInfo.repeats.getOrElse(false)) {
      writeAndCount(metaName, "[")
      indentLevel += indentAmount
    }
    true
  }

  override def didVisitValueTable(valueTable: ValueTableH5): Unit = {
    val metaName = valueTable.name
    if (valueTable.metaInfo.repeats.getOrElse(false)) {
      indentLevel -= indentAmount
      writeAndCount(metaName, "]")
    }
    emptyObj.trimEnd(1)
  }

  var objectKind: ObjectKind.Value = ObjectKind.NormalizedData

  /**
   * reset warnings and size count
   */
  def reset(): Unit = {
    metaOutSizes = Map()
    limitedMeta = Set()
    _warnings = Seq()
    _warningsCount = Map()
  }

  override def willVisitContext(context: ResolvedRef): Unit = {
    this.context = Some(context)
    objectKind = context.objectKind
    context match {
      case r: RawData => ()
      case s: Section => ()
      case c: Calculation => ()
      case a: Archive => ()
      case v: Value =>
        indentLevel += indentAmount
        emptyObj.append(false)
        writeOut(s"""{
${" " * indentLevel}"type": "nomad_value_2_0",
${" " * indentLevel}"uri": "${v.uriString}",
${" " * indentLevel}"value": """)
      case f: FailedResolution =>
        writeOut(s"""{
${" " * (indentLevel + indentAmount)}"type": "nomad_error_2_0",
${" " * (indentLevel + indentAmount)}"error_message": "Failed resolution of ${f.uriString}: $f"
${" " * indentLevel}}""")
      case st: SectionTable =>
        indentLevel += indentAmount
        emptyObj.append(false)
        writeOut(s"""{
${" " * indentLevel}"type": "nomad_section_table_2_0",
${" " * indentLevel}"uri": "${context.uriString}"""")
      case vt: ValueTable =>
        indentLevel += indentAmount
        emptyObj.append(false)
        writeOut(s"""{
${" " * indentLevel}"type": "nomad_value_table_2_0",
${" " * indentLevel}"uri": "${context.uriString}"""")
    }
  }

  def limitedMetaWarningMsg: String = "The meta infos listed in shortened_meta_infos were shortened to avoid the creation of a too large document, to avoid it, access a part of the document directly, or access elements non recursively"

  def addWarning(msg: String, level: MessageLevel.Value = MessageLevel.Warn): Unit = {
    val newM = Message(msg, level)
    if (_warningsCount.contains(newM)) {
      _warningsCount += (newM -> (_warningsCount(newM) + 1))
    } else {
      _warnings = _warnings :+ newM
      _warningsCount += (newM -> 1)
    }
  }

  def warnings: Seq[Message] = {
    if (limitedMeta.isEmpty)
      _warnings
    else {
      val m = Message(limitedMetaWarningMsg)
      _warningsCount.get(m) match {
        case None =>
          _warnings :+ m
        case Some(_) =>
          _warnings
      }
    }
  }

  def warningsJValue: jn.JValue = {
    jn.JArray(warnings.map { m =>
      jn.JObject(
        ("response_message" -> jn.JString(m.msg)) ::
          ("response_message_level" -> jn.JInt(m.level.id)) ::
          ("response_message_count" -> (_warningsCount.getOrElse(m, 1) match {
            case 1 =>
              jn.JNothing
            case n =>
              jn.JInt(n)
          })) :: Nil
      )
    }(breakOut))
  }

  def willCloseLastObject(): Unit = {
    if (embedWarningAtTopLevel && !warnings.isEmpty) {
      if (!emptyObj.last)
        writeOut(", ")
      else
        emptyObj.update(emptyObj.length - 1, false)
      writeOut(s"""
${" " * indentLevel}"section_response_warnings": """)
      writeOut(JsonUtils.normalizedStr(warningsJValue))
      if (!limitedMeta.isEmpty) {
        writeOut(s""",
${" " * indentLevel}"shortened_meta_info": """)
        writeOut(JsonUtils.normalizedStr(jn.JArray(limitedMeta.map(x => jn.JString(x))(breakOut))))
      }
    }
  }

  override def didVisitContext(context: ResolvedRef): Unit = {
    this.context = None
    context match {
      case r: RawData => ()
      case s: Section => ()
      case c: Calculation => ()
      case a: Archive => ()
      case v: Value =>
        willCloseLastObject()
        indentLevel -= indentAmount
        emptyObj.trimEnd(1)
        writeOut(s"""
${" " * indentLevel}}""")
      case f: FailedResolution => ()
      case st: SectionTable =>
        willCloseLastObject()
        indentLevel -= indentAmount
        emptyObj.trimEnd(1)
        writeOut(s"""
${" " * indentLevel}}""")
      case vt: ValueTable =>
        willCloseLastObject()
        indentLevel -= indentAmount
        emptyObj.trimEnd(1)
        writeOut(s"""
${" " * indentLevel}}"""")
    }
  }
}
