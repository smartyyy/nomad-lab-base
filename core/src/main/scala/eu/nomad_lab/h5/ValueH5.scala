/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.h5

import eu.nomad_lab.{ Base64, IteratorH5, H5Lib, JsonUtils }
import eu.nomad_lab.meta.MetaInfoRecord
import eu.nomad_lab.ref

import scala.util.control.NonFatal
import scala.collection.breakOut
import scala.collection
import ucar.ma2.{ Array => NArray }
import ucar.ma2
import org.{ json4s => jn }

object ValueH5 {
  class ValueError(msg: String) extends Exception(msg)
  val maxElsJValue: Long = 10000000L

  def apply(
    table: ValueTableH5,
    gIndex: Long,
    indexDatasetId: Option[Long] = None,
    valueDatasetId: Option[Long] = None,
    localIndex: Option[Long] = None
  ): ValueH5 = {
    val metaInfo = table.metaInfo
    metaInfo.shape match {
      case None | Some(Seq()) =>
        var lIndex: Long = -1
        var vDataset: Long = -1
        if (table.isEmpty || gIndex < 0) {
        } else if (table.openDimensions.isEmpty) {
          lIndex = localIndex match {
            case Some(i) => i
            case None => gIndex
          }
          vDataset = valueDatasetId match {
            case Some(i) => i
            case None =>
              H5Lib.datasetOpen(table.parentGroup, metaInfo.name + "-v")
          }
        } else {
          val iDatasetId = indexDatasetId match {
            case Some(i) => i
            case None =>
              H5Lib.datasetOpen(table.parentGroup, metaInfo.name + "-index")
          }
          try {
            if (localIndex.isEmpty || valueDatasetId.isEmpty) {
              val r = table.resolveGlobalIndex(iDatasetId, gIndex)
              lIndex = r.localIndex
              vDataset = valueDatasetId match {
                case Some(i) => i
                case None =>
                  H5Lib.datasetOpen(table.parentGroup, r.valueDatasetName)
              }
            } else {
              lIndex = localIndex.get
              vDataset = valueDatasetId.get
            }
          } finally {
            indexDatasetId match {
              case Some(_) =>
              case None =>
                if (iDatasetId >= 0)
                  H5Lib.datasetClose(iDatasetId)
            }
          }
        }
        try {
          metaInfo.dtypeStr match {
            case Some(x) =>
              x match {
                case "f" | "f64" =>
                  new ValueH5_f64(table, gIndex, vDataset, lIndex)
                case "f32" =>
                  new ValueH5_f32(table, gIndex, vDataset, lIndex)
                case "i" | "i32" =>
                  new ValueH5_i32(table, gIndex, vDataset, lIndex)
                case "i64" =>
                  new ValueH5_i64(table, gIndex, vDataset, lIndex)
                case "r" =>
                  new ValueH5_r(table, gIndex, vDataset, lIndex)
                case "b" =>
                  new ValueH5_b(table, gIndex, vDataset, lIndex)
                case "C" =>
                  new ValueH5_C(table, gIndex, vDataset, lIndex)
                case "D" =>
                  new ValueH5_D(table, gIndex, vDataset, lIndex)
              }
            case None =>
              throw new ValueH5.ValueError("value does not define dtypeStr")
          }
        } finally {
          valueDatasetId match {
            case Some(_) =>
            case None =>
              if (vDataset >= 0)
                H5Lib.datasetClose(vDataset)
          }
        }
      case _ =>
        val iDatasetId = indexDatasetId match {
          case Some(i) => i
          case None =>
            H5Lib.datasetOpen(table.parentGroup, metaInfo.name + "-index")
        }
        try {
          metaInfo.dtypeStr match {
            case Some(x) =>
              x match {
                case "f" | "f64" =>
                  new ValueH5_Af64(table, gIndex, iDatasetId)
                case "f32" =>
                  new ValueH5_Af32(table, gIndex, iDatasetId)
                case "i" | "i32" =>
                  new ValueH5_Ai32(table, gIndex, iDatasetId)
                case "i64" =>
                  new ValueH5_Ai64(table, gIndex, iDatasetId)
                case "r" =>
                  new ValueH5_Ar(table, gIndex, iDatasetId)
                case "b" =>
                  new ValueH5_Ab(table, gIndex, iDatasetId)
                case "C" =>
                  new ValueH5_AC(table, gIndex, iDatasetId)
                case "D" =>
                  new ValueH5_AD(table, gIndex, iDatasetId)
              }
            case None =>
              throw new ValueH5.ValueError("value does not define dtypeStr")
          }
        } finally {
          indexDatasetId match {
            case Some(_) =>
            case None =>
              if (iDatasetId >= 0)
                H5Lib.datasetClose(iDatasetId)
          }
        }
    }
  }
}

/**
 * represents a single value that can be read.
 * It might be read and cached at initialization.
 */
abstract class ValueH5 {
  /** table that contains this value */
  def table: ValueTableH5

  /** gIndex of the value */
  def gIndex: Long

  def metaName: String = {
    table.name
  }

  final override def equals(other: Any): Boolean = {
    val that = other.asInstanceOf[ValueH5]
    if (that == null)
      false
    else
      gIndex == that.gIndex && table == that.table
  }

  /**
   * returns the section that contains this value
   */
  def parentSection: SectionH5 = {
    val idxDatasetId = table.openIndexDataset(create = false)
    val idxValArray = new ma2.ArrayLong.D1(1)
    val idxVal = H5Lib.datasetRead(
      datasetId = idxDatasetId,
      dtypeStr = "i64",
      dimsToRead = Seq(1: Long, 1),
      offset = Seq(gIndex, 0),
      value = Some(idxValArray)
    )
    //val idxValIt = idxVal.getIndexIterator
    //val superGIndex = idxValIt.getLongNext()
    val superGIndex = idxValArray.get(0)
    table.parentSectionTable(superGIndex)
  }

  /**
   * returns the indexed path that leads to this value
   */
  def indexedPath: Seq[(String, collection.SortedMap[String, Long])] = {
    parentSection.indexedPath :+ (table.name -> collection.SortedMap("c" -> gIndex))
  }

  /**
   * returns a reference to this value table
   */
  def toRef: ref.RowRef = {
    val calc = table.parentSectionTable.calculation
    ref.RowRef(calc.archive.archiveGid, calc.calculationGid, indexedPath)
  }

  /**
   * the value is empty (i.e. does not exist)
   */
  def isEmpty(): Boolean = {
    if (table.isEmpty() || gIndex < 0)
      true
    else
      false
  }

  override def toString(): String = {
    s"${this.getClass.getName}(${table.metaInfo.name}, $gIndex)"
  }

  def intValue: Int = {
    throw new ValueH5.ValueError(s"Trying to access intValue of $this with dtypeStr ${table.metaInfo.dtypeStr} and shape ${table.metaInfo.shape} which is not implemented")
  }

  def longValue: Long = {
    throw new ValueH5.ValueError(s"Trying to access longValue of $this with dtypeStr ${table.metaInfo.dtypeStr} and shape ${table.metaInfo.shape} which is not implemented")
  }

  def floatValue: Float = {
    throw new ValueH5.ValueError(s"Trying to access floatValue of $this with dtypeStr ${table.metaInfo.dtypeStr} and shape ${table.metaInfo.shape} which is not implemented")
  }

  def doubleValue: Double = {
    throw new ValueH5.ValueError(s"Trying to access longValue of $this with dtypeStr ${table.metaInfo.dtypeStr} and shape ${table.metaInfo.shape} which is not implemented")
  }

  def stringValue: String = {
    throw new ValueH5.ValueError(s"Trying to access stringValue of $this with dtypeStr ${table.metaInfo.dtypeStr} and shape ${table.metaInfo.shape} which is not implemented")
  }

  def jValue: jn.JValue;

  def boolValue: Boolean = {
    throw new ValueH5.ValueError(s"Trying to access boolValue of $this with dtypeStr ${table.metaInfo.dtypeStr} and shape ${table.metaInfo.shape} which is not implemented")
  }

  def arrayValue(valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq()): NArray;

  def arrayShape: Seq[Long];

  def seqDoubleValue(valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq()): Seq[Double] = {
    val arr = arrayValue(valueOffset, valueDims)
    Seq.tabulate[Double](arr.getSize().toInt)(arr.getDouble)
  }

  def seqLongValue(valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq()): Seq[Long] = {
    val arr = arrayValue(valueOffset, valueDims)
    Seq.tabulate[Long](arr.getSize().toInt)(arr.getLong)
  }

  def seqStringValue(valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq()): Seq[String] = {
    val arr = arrayValue(valueOffset, valueDims)
    Seq.tabulate[String](arr.getSize().toInt) { i: Int => arr.getObject(i).toString }
  }

  def seqBooleanValue(valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq()): Seq[Boolean] = {
    val arr = arrayValue(valueOffset, valueDims)
    Seq.tabulate[Boolean](arr.getSize().toInt)(arr.getBoolean)
  }
}

/** represents a concrete value with scala type T */
abstract class ValueTH5[T]() extends ValueH5() {
  def value: Option[T]
}

/**
 * scalar int value
 */
final class ValueH5_i32(
    val table: ValueTableH5,
    val gIndex: Long,
    valueDatasetId: Long,
    localIndex: Long
) extends ValueTH5[Int]() {

  override val value: Option[Int] = {
    if (gIndex < 0 || table.isEmpty) {
      None
    } else {
      val arrayValue = new ma2.ArrayInt.D1(1)
      table.valueGetLocal(valueDatasetId, localIndex, value = Some(arrayValue))
      Some(arrayValue.getInt(0))
    }
  }

  override def intValue: Int = {
    value match {
      case Some(i) => i
      case None => throw new ValueH5.ValueError(s"Trying to access in value of an empty value $this")
    }
  }

  override def longValue: Long = {
    value match {
      case Some(i) => i.toLong
      case None => throw new ValueH5.ValueError(s"Trying to access in value of an empty value $this")
    }
  }

  override def jValue: jn.JValue = {
    value match {
      case Some(i) => jn.JInt(i)
      case None => jn.JNothing
    }
  }

  override def arrayShape(): Seq[Long] = Seq()

  override def arrayValue(valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq()): NArray = {
    val arr = new ma2.ArrayInt.D0()
    arr.set(intValue)
    arr
  }
}

/**
 * scalar long value
 */
final class ValueH5_i64(
    val table: ValueTableH5,
    val gIndex: Long,
    valueDatasetId: Long,
    localIndex: Long
) extends ValueTH5[Long]() {

  override val value: Option[Long] = {
    if (gIndex < 0 || table.isEmpty) {
      None
    } else {
      val arrayValue = new ma2.ArrayLong.D1(1)
      table.valueGetLocal(valueDatasetId, localIndex, value = Some(arrayValue))
      Some(arrayValue.getLong(0))
    }
  }

  override def longValue: Long = {
    value match {
      case Some(i) => i
      case None => throw new ValueH5.ValueError(s"Trying to access in value of an empty value $this")
    }
  }

  override def jValue: jn.JValue = {
    value match {
      case Some(i) => jn.JInt(i)
      case None => jn.JNothing
    }
  }

  override def arrayShape(): Seq[Long] = Seq()

  override def arrayValue(valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq()): NArray = {
    val arr = new ma2.ArrayLong.D0()
    arr.set(longValue)
    arr
  }
}

/**
 * scalar float value
 */
final class ValueH5_f32(
    val table: ValueTableH5,
    val gIndex: Long,
    valueDatasetId: Long,
    localIndex: Long
) extends ValueTH5[Float]() {

  override val value: Option[Float] = {
    if (gIndex < 0 || table.isEmpty) {
      None
    } else {
      val arrayValue = new ma2.ArrayFloat.D1(1)
      table.valueGetLocal(valueDatasetId, localIndex, value = Some(arrayValue))
      Some(arrayValue.getFloat(0))
    }
  }

  override def floatValue: Float = {
    value match {
      case Some(f) => f
      case None => throw new ValueH5.ValueError(s"Trying to access in value of an empty value $this")
    }
  }

  override def doubleValue: Double = {
    value match {
      case Some(f) => f.toDouble
      case None => throw new ValueH5.ValueError(s"Trying to access in value of an empty value $this")
    }
  }

  override def jValue: jn.JValue = {
    value match {
      case Some(f) => jn.JDouble(f.toDouble)
      case None => jn.JNothing
    }
  }

  override def arrayShape(): Seq[Long] = Seq()

  override def arrayValue(valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq()): NArray = {
    val arr = new ma2.ArrayFloat.D0()
    arr.set(floatValue)
    arr
  }
}

/**
 * scalar double value
 */
final class ValueH5_f64(
    val table: ValueTableH5,
    val gIndex: Long,
    valueDatasetId: Long,
    localIndex: Long
) extends ValueTH5[Double]() {

  override val value: Option[Double] = {
    if (gIndex < 0 || table.isEmpty) {
      None
    } else {
      val arrayValue = new ma2.ArrayDouble.D1(1)
      table.valueGetLocal(valueDatasetId, localIndex, value = Some(arrayValue))
      Some(arrayValue.getDouble(0))
    }
  }

  override def doubleValue: Double = {
    value match {
      case Some(f) => f
      case None => throw new ValueH5.ValueError(s"Trying to access in value of an empty value $this")
    }
  }

  override def jValue: jn.JValue = {
    value match {
      case Some(f) => jn.JDouble(f)
      case None => jn.JNothing
    }
  }

  override def arrayShape(): Seq[Long] = Seq()

  override def arrayValue(valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq()): NArray = {
    val arr = new ma2.ArrayDouble.D0()
    arr.set(doubleValue)
    arr
  }
}

/**
 * scalar string value
 */
final class ValueH5_C(
    val table: ValueTableH5,
    val gIndex: Long,
    valueDatasetId: Long,
    localIndex: Long
) extends ValueTH5[String]() {

  override val value: Option[String] = {
    if (gIndex < 0 || table.isEmpty) {
      None
    } else {
      val arrayValue = new ma2.ArrayString.D1(1)
      table.valueGetLocal(valueDatasetId, localIndex, value = Some(arrayValue))
      Some(arrayValue.getObject(0) match {
        case s: String => s
      })
    }
  }

  override def stringValue: String = {
    value match {
      case Some(f) => f
      case None => throw new ValueH5.ValueError(s"Trying to access in value of an empty value $this")
    }
  }

  override def jValue: jn.JValue = {
    value match {
      case Some(s) => jn.JString(s)
      case None => jn.JNothing
    }
  }

  override def arrayShape(): Seq[Long] = Seq()

  override def arrayValue(valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq()): NArray = {
    val arr = new ma2.ArrayString.D0()
    arr.set(stringValue)
    arr
  }
}

/**
 * scalar boolean value
 */
final class ValueH5_b(
    val table: ValueTableH5,
    val gIndex: Long,
    valueDatasetId: Long,
    localIndex: Long
) extends ValueTH5[Boolean]() {

  override val value: Option[Boolean] = {
    if (gIndex < 0 || table.isEmpty) {
      None
    } else {
      val arrayValue = new ma2.ArrayBoolean.D1(1)
      table.valueGetLocal(valueDatasetId, localIndex, value = Some(arrayValue))
      Some(arrayValue.getBoolean(0))
    }
  }

  override def boolValue: Boolean = {
    value match {
      case Some(b) => b
      case None => throw new ValueH5.ValueError(s"Trying to access in value of an empty value $this")
    }
  }

  override def jValue: jn.JValue = {
    value match {
      case Some(b) => jn.JBool(b)
      case None => jn.JNothing
    }
  }

  override def arrayShape(): Seq[Long] = Seq()

  override def arrayValue(valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq()): NArray = {
    val arr = new ma2.ArrayBoolean.D0()
    arr.set(boolValue)
    arr
  }
}

/**
 * reference long value
 */
final class ValueH5_r(
    val table: ValueTableH5,
    val gIndex: Long,
    valueDatasetId: Long,
    localIndex: Long
) extends ValueTH5[Long]() {

  override val value: Option[Long] = {
    if (gIndex < 0 || table.isEmpty) {
      None
    } else {
      val arrayValue = new ma2.ArrayLong.D1(1)
      table.valueGetLocal(valueDatasetId, localIndex, value = Some(arrayValue))
      Some(arrayValue.getLong(0))
    }
  }

  override def longValue: Long = {
    value match {
      case Some(i) => i
      case None => throw new ValueH5.ValueError(s"Trying to access in value of an empty value $this")
    }
  }

  override def jValue: jn.JValue = {
    value match {
      case Some(i) => jn.JInt(i)
      case None => jn.JNothing
    }
  }

  override def arrayShape(): Seq[Long] = Seq()

  override def arrayValue(valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq()): NArray = {
    val arr = new ma2.ArrayLong.D0()
    arr.set(longValue)
    arr
  }
}

/**
 * scalar json value value
 */
final class ValueH5_D(
    val table: ValueTableH5,
    val gIndex: Long,
    valueDatasetId: Long,
    localIndex: Long
) extends ValueTH5[jn.JValue]() {

  val _value: jn.JValue = {
    if (gIndex < 0 || table.isEmpty) {
      jn.JNothing
    } else {
      val arrayValue = new ma2.ArrayObject.D1(classOf[org.json4s.JsonAST.JValue], 1)
      table.valueGetLocal(valueDatasetId, localIndex, value = Some(arrayValue))
      arrayValue.getObject(0) match {
        case v: jn.JValue => v
      }
    }
  }

  override def value: Option[jn.JValue] = {
    _value match {
      case jn.JNothing => None
      case v =>
        Some(v)
    }
  }

  override def jValue: jn.JValue = {
    _value
  }

  override def intValue: Int = {
    _value match {
      case jn.JInt(i) => i.toInt
      case _ =>
        throw new ValueH5.ValueError(s"Trying to access intValue of $this that contains a non int json")
    }
  }

  override def longValue: Long = {
    _value match {
      case jn.JInt(i) => i.toLong
      case _ =>
        throw new ValueH5.ValueError(s"Trying to access longValue of $this that contains a non int json")
    }
  }

  override def floatValue: Float = {
    _value match {
      case jn.JDouble(d) => d.toFloat
      case jn.JInt(i) => i.toFloat
      case _ =>
        throw new ValueH5.ValueError(s"Trying to access floatValue of $this that contains a non number json")
    }
  }

  override def doubleValue: Double = {
    _value match {
      case jn.JDouble(d) => d.toDouble
      case jn.JInt(i) => i.toDouble
      case _ =>
        throw new ValueH5.ValueError(s"Trying to access doubleValue of $this that contains a non number json")
    }
  }

  override def stringValue: String = {
    _value match {
      case jn.JString(s) => s
      case _ =>
        throw new ValueH5.ValueError(s"Trying to access stringValue of $this that contains a non string json")
    }
  }

  override def boolValue: Boolean = {
    _value match {
      case jn.JBool(b) => b
      case _ =>
        throw new ValueH5.ValueError(s"Trying to access boolValue of $this that contains a non boolean json")
    }
  }

  override def arrayShape(): Seq[Long] = Seq()

  override def arrayValue(valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq()): NArray = {
    val arr = new ma2.ArrayObject.D0(classOf[org.json4s.JsonAST.JValue])
    arr.set(_value)
    arr
  }
}

abstract class ValueArrayH5[T <: NArray](
    val table: ValueTableH5,
    val gIndex: Long,
    indexDatasetId: Long
) extends ValueH5() {
  import ValueH5.maxElsJValue

  /** row of the index dataset corresponding to this value */
  val indexRow: Option[ma2.ArrayLong.D1] = {
    if (gIndex < 0) {
      None
    } else {
      val iDim: Long = if (table.openDimensions.isEmpty) 1 else (2 + table.openDimensions.length.toLong)
      val a = new ma2.ArrayLong.D1(iDim.toInt)
      H5Lib.datasetRead(
        datasetId = indexDatasetId,
        dtypeStr = "i64",
        dimsToRead = Seq(1: Long, iDim),
        offset = Seq(gIndex, 0),
        value = Some(a)
      )
      Some(a)
    }
  }

  /** name of the dataset of this value */
  lazy val valueDatasetName: Option[String] = {
    indexRow match {
      case None => None
      case Some(idxValues) =>
        if (table.openDimensions.isEmpty) {
          Some(table.metaInfo.name + "-v")
        } else {
          var ii: Int = 1
          val openDimensionsValues = table.openDimensions.map {
            case (dimName, idx) =>
              ii += 1
              Base64.b64Nr(idxValues.getLong(ii))
          }
          Some(openDimensionsValues.foldLeft(table.metaInfo.name + "-v") {
            _ + "." + _
          })
        }
    }
  }

  /** index within the valueDataset */
  def localIndex: Long = {
    indexRow match {
      case None => -1
      case Some(i) =>
        if (table.openDimensions.isEmpty)
          gIndex
        else
          i.getLong(1)
    }
  }

  /** shape of the value (if present) */
  override val arrayShape: Seq[Long] = {
    var ii: Int = 1
    table.metaInfo.shape match {
      case None =>
        Seq()
      case Some(s) =>
        s.map {
          case Left(l) => l
          case Right(_) =>
            ii += 1
            indexRow match {
              case Some(i) => i.getLong(ii)
              case None => throw new ValueH5.ValueError(s"requested shape of non present value")
            }
        }
    }
  }

  /**
   * returns the value as ma2.Array, throws if empty
   */
  def arrValue(valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq(), value: Option[NArray] = None): NArray = {
    valueDatasetName match {
      case None => throw new ValueH5.ValueError(s"array vaue for non present value $this")
      case Some(dName) =>
        val valueDatasetId = H5Lib.datasetOpen(table.parentSectionTable.sectionGroup(), dName)
        try {
          table.valueGetLocal(valueDatasetId, localIndex, valueOffset, valueDims, value = value)
        } finally {
          H5Lib.datasetClose(valueDatasetId)
        }
    }
  }

  override def arrayValue(valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq()): T;

  def jValue: jn.JValue = {
    val shape = arrayShape
    val toEl: (NArray, Int) => jn.JValue = {
      table.metaInfo.dtypeStr match {
        case None => throw new ValueH5.ValueError(s"missing dtypeStr in $this")
        case Some(x) => x match {
          case "i" | "i32" | "r" =>
            { (a: NArray, i: Int) => jn.JInt(a.getInt(i)) }
          case "i64" =>
            { (a: NArray, i: Int) => jn.JInt(a.getLong(i)) }
          case "f" | "f64" =>
            { (a: NArray, i: Int) => jn.JDouble(a.getDouble(i)) }
          case "f32" =>
            { (a: NArray, i: Int) => jn.JDouble(a.getFloat(i).toDouble) }
          case "C" =>
            { (a: NArray, i: Int) => jn.JString(a.getObject(i).toString) }
          case "D" =>
            { (a: NArray, i: Int) =>
              a.getObject(i) match {
                case j: jn.JsonAST.JValue => j
              }
            }
          case "b" =>
            { (a: NArray, i: Int) => jn.JBool(a.getBoolean(i)) }
        }
      }
    }
    if (shape.foldLeft(1: Long)(_ * _) > maxElsJValue) {
      val pShape = Array.fill[Long](shape.size)(1)
      var sizeNow: Long = 1
      for (i <- (shape.size - 1).to(0, -1)) {
        val el = shape(i)
        if (sizeNow * el > maxElsJValue)
          pShape(i) = (maxElsJValue + sizeNow - 1) / sizeNow
        else
          pShape(i) = el
        sizeNow *= pShape(i)
      }
      val arr = arrayValue(valueDims = pShape)
      jn.JObject(
        ("shape" -> jn.JArray(shape.map { l => jn.JInt(l) }(breakOut))) ::
          ("partialShape" -> jn.JArray(pShape.map { l => jn.JInt(l) }(breakOut))) ::
          ("partialData" -> jn.JArray(
            0.until(arr.getSize().toInt).map { (i: Int) =>
              toEl(arr, i)
            }(breakOut)
          )) :: Nil
      )
    } else {
      val arr = arrayValue()
      jn.JObject(
        ("shape" -> jn.JArray(shape.map { x: Long => jn.JInt(x) }(breakOut))) ::
          ("flatData" -> jn.JArray(
            0.until(arr.getSize().toInt).map { (i: Int) =>
              toEl(arr, i)
            }(breakOut)
          )) :: Nil
      )
    }
  }
}

/**
 * int array value
 */
final class ValueH5_Ai32(
    table: ValueTableH5,
    gIndex: Long,
    indexDatasetId: Long
) extends ValueArrayH5[ma2.ArrayInt](table, gIndex, indexDatasetId) {

  override def arrayValue(valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq()): ma2.ArrayInt = {
    arrValue(valueOffset = valueOffset, valueDims = valueDims) match {
      case a: ma2.ArrayInt =>
        a
    }
  }
}

/**
 * long array value
 */
final class ValueH5_Ai64(
    table: ValueTableH5,
    gIndex: Long,
    indexDatasetId: Long
) extends ValueArrayH5[ma2.ArrayLong](table, gIndex, indexDatasetId) {

  override def arrayValue(valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq()): ma2.ArrayLong = {
    arrValue(valueOffset = valueOffset, valueDims = valueDims) match {
      case a: ma2.ArrayLong =>
        a
    }
  }
}

/**
 * float array value
 */
final class ValueH5_Af32(
    table: ValueTableH5,
    gIndex: Long,
    indexDatasetId: Long
) extends ValueArrayH5[ma2.ArrayFloat](table, gIndex, indexDatasetId) {

  override def arrayValue(valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq()): ma2.ArrayFloat = {
    arrValue(valueOffset = valueOffset, valueDims = valueDims) match {
      case a: ma2.ArrayFloat =>
        a
    }
  }
}

/**
 * double array value
 */
final class ValueH5_Af64(
    table: ValueTableH5,
    gIndex: Long,
    indexDatasetId: Long
) extends ValueArrayH5[ma2.ArrayDouble](table, gIndex, indexDatasetId) {

  override def arrayValue(valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq()): ma2.ArrayDouble = {
    arrValue(valueOffset = valueOffset, valueDims = valueDims) match {
      case a: ma2.ArrayDouble =>
        a
    }
  }
}

/**
 * string array value
 */
final class ValueH5_AC(
    table: ValueTableH5,
    gIndex: Long,
    indexDatasetId: Long
) extends ValueArrayH5[ma2.ArrayString](table, gIndex, indexDatasetId) {

  override def arrayValue(valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq()): ma2.ArrayString = {
    arrValue(valueOffset = valueOffset, valueDims = valueDims) match {
      case a: ma2.ArrayString =>
        a
    }
  }
}

/**
 * boolean array value
 */
final class ValueH5_Ab(
    table: ValueTableH5,
    gIndex: Long,
    indexDatasetId: Long
) extends ValueArrayH5[ma2.ArrayBoolean](table, gIndex, indexDatasetId) {

  override def arrayValue(valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq()): ma2.ArrayBoolean = {
    arrValue(valueOffset = valueOffset, valueDims = valueDims) match {
      case a: ma2.ArrayBoolean =>
        a
    }
  }
}

/**
 * reference array value
 */
final class ValueH5_Ar(
    table: ValueTableH5,
    gIndex: Long,
    indexDatasetId: Long
) extends ValueArrayH5[ma2.ArrayLong](table, gIndex, indexDatasetId) {

  override def arrayValue(valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq()): ma2.ArrayLong = {
    arrValue(valueOffset = valueOffset, valueDims = valueDims) match {
      case a: ma2.ArrayLong =>
        a
    }
  }
}

/**
 * json value array
 */
final class ValueH5_AD(
    table: ValueTableH5,
    gIndex: Long,
    indexDatasetId: Long
) extends ValueArrayH5[ma2.ArrayObject](table, gIndex, indexDatasetId) {

  override def arrayValue(valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq()): ma2.ArrayObject = {
    arrValue(valueOffset = valueOffset, valueDims = valueDims) match {
      case a: ma2.ArrayObject =>
        a
    }
  }
}
