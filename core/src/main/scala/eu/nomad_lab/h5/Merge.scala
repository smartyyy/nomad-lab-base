/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.h5;

import eu.nomad_lab.H5Lib
import scala.util.control.NonFatal

object Merge {
  /**
   * exception during merge of h5 files
   */
  class MergeException(
    msg: String,
    what: Throwable = null
  ) extends Exception(msg, what)

  /**
   * merges the given calculation into the given file
   */
  def calculationInFile(calculation: CalculationH5, file: FileH5): Unit = {
    val archive = calculation.archive
    val sourceFile = archive.fileH5
    if (file == sourceFile)
      throw new MergeException(s"merge from $sourceFile into same file ($file)")
    // ensure archive group
    val targetArchiveGroup: Long = H5Lib.groupGet(file.h5FileGroup, archive.archiveGid, create = true)
    try {
      H5Lib.objectCopy(calculation.calculationGroup, ".", targetArchiveGroup, "./" + calculation.calculationGid)
    } catch {
      case NonFatal(e) =>
        throw new MergeException(s"merging $calculation in $file", e)
    } finally {
      H5Lib.groupClose(targetArchiveGroup)
    }
  }

  /**
   * merges the calculations of fileIn in fileOut
   */
  def fileInFile(fileIn: FileH5, fileOut: FileH5): Unit = {
    for (archive <- fileIn.archives()) {
      for (calculation <- archive.calculations()) {
        calculationInFile(calculation, fileOut)
      }
    }
  }
}
