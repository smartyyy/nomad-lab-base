/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.h5
import eu.nomad_lab.ref._
import eu.nomad_lab.resolve._
import eu.nomad_lab.resolve.Resolver
import eu.nomad_lab.resolve.ResolvedRef
import eu.nomad_lab.parsers.ParserBackendExternal
import scala.util.control.NonFatal
import scala.collection.mutable
import eu.nomad_lab.JsonUtils
import com.typesafe.scalalogging.StrictLogging

object ScanType extends Enumeration {
  type ScanType = Value
  val RowScan, TableScan = Value
}

class H5EagerScanner(val scanType: ScanType.Value = ScanType.RowScan, sortMeta: Boolean = false) extends StrictLogging {

  def scanResolvedRef(context: ResolvedRef, visitor: H5Visitor): Unit = {
    try {
      visitor.willVisitContext(context)
      context match {
        case FailedResolution(uriString, msg) =>
          throw new H5ScanException(s"Cannot scan $uriString, as it cannot be resolved: $msg")
        case r: RawData =>
          throw new H5ScanException(s"raw data reference $r cannot be iterated")
        case Section(_, section) =>
          scanType match {
            case ScanType.RowScan =>
              scanSection(section, visitor)
            case ScanType.TableScan =>
              throw new Exception(s"section $section cannot be use for a table scan")
          }
        case SectionTable(_, sTable) =>
          scanSectionTable(sTable, visitor)
        case ValueTable(_, sTable) =>
          scanValueTable(sTable, visitor)
        case Value(_, value) =>
          scanType match {
            case ScanType.RowScan =>
              scanValue(value, visitor)
            case ScanType.TableScan =>
              throw new Exception(s"value $value cannot be use for a table scan")
          }
        case Calculation(_, calc) =>
          scanCalculation(calc, visitor)
        case Archive(_, arch) =>
          scanArchive(arch, visitor)
      }
    } catch {
      case NonFatal(e) =>
        throw new H5ScanException(s"Error scanning ${context.uriString}", e)
    } finally {
      //visitor.didVisitContext(context)
    }
    visitor.didVisitContext(context)
  }

  def scanSection(section: SectionH5, visitor: H5Visitor): Unit = {
    if (visitor.shouldVisitSection(section)) {
      try {
        if (visitor.shouldVisitSectionSubValues(section)) {
          for (subValueCollection <- section.valueCollections(sortMeta)) {
            val coll = if (subValueCollection.lengthL > 1 && !subValueCollection.table.metaInfo.repeats.getOrElse(false)) {
              logger.warn(s"Found multiple values $subValueCollection for non repeating value in ${section.toRef.toUriStr(ObjectKind.NormalizedData)}, scanning only first one.")
              new ValueCollectionH5(
                table = subValueCollection.table,
                parentSectionLowerGIndex = subValueCollection.parentSectionLowerGIndex,
                parentSectionUpperGIndex = subValueCollection.parentSectionUpperGIndex,
                lowerGIndex0 = Some(subValueCollection.lowerGIndex),
                upperGIndex0 = Some(subValueCollection.lowerGIndex)
              )
            } else {
              subValueCollection
            }
            if (visitor.shouldVisitValueCollection(coll)) {
              for (subValue <- coll)
                scanValue(subValue, visitor)
              visitor.didVisitValueCollection(coll)
            }
          }
          visitor.didVisitSectionSubValues(section)
        }
        if (visitor.shouldVisitSectionSubSections(section)) {
          for (subSectionCollection <- section.subSectionCollections(sortMeta)) {
            val coll = if (subSectionCollection.lengthL > 1 && !subSectionCollection.table.metaInfo.repeats.getOrElse(true)) {
              logger.warn(s"Found multiple sections $subSectionCollection for non repeating section in ${section.toRef.toUriStr(ObjectKind.NormalizedData)}, scanning only first one.")
              new SectionCollectionH5(
                table = subSectionCollection.table,
                parentSectionLowerGIndex = subSectionCollection.parentSectionLowerGIndex,
                parentSectionUpperGIndex = subSectionCollection.parentSectionUpperGIndex,
                lowerGIndex0 = Some(subSectionCollection.lowerGIndex),
                upperGIndex0 = Some(subSectionCollection.lowerGIndex)
              )
            } else {
              subSectionCollection
            }
            if (visitor.shouldVisitSectionCollection(coll)) {
              for (subSection <- coll)
                scanSection(subSection, visitor)
              visitor.didVisitSectionCollection(coll)
            }
          }
          visitor.didVisitSectionSubSections(section)
        }
      } catch {
        case NonFatal(e) =>
          throw new H5ScanException(s"failure scanning $section", e)
      }
      visitor.didVisitSection(section)
    }
  }

  def scanValue(value: ValueH5, visitor: H5Visitor): Unit = {
    if (value.arrayShape.isEmpty)
      visitor.visitScalarValue(value)
    else
      visitor.visitArrayValue(value)
  }

  def scanArchive(archive: ArchiveH5, visitor: H5Visitor): Unit = {
    if (visitor.shouldVisitArchive(archive)) {
      for (calculation <- archive.calculations)
        scanCalculation(calculation, visitor)
      visitor.didVisitArchive(archive)
    }
  }

  def scanCalculation(calculation: CalculationH5, visitor: H5Visitor): Unit = {
    if (visitor.shouldVisitCalculation(calculation)) {
      for (sectionTable <- calculation.subSectionTables(sortMeta))
        scanSectionTable(sectionTable, visitor)
      visitor.didVisitCalculation(calculation)
    }
  }

  def scanSectionTable(sectionTable: SectionTableH5, visitor: H5Visitor): Unit = {
    if (visitor.shouldVisitSectionTable(sectionTable)) {
      scanType match {
        case ScanType.RowScan =>
          for (section <- sectionTable)
            scanSection(section, visitor)
        case ScanType.TableScan =>
          if (visitor.shouldVisitSubValueTables(sectionTable)) {
            for (table <- sectionTable.subValueTables(sortMeta))
              scanValueTable(table, visitor)
            visitor.didVisitSubValueTables(sectionTable)
          }
          if (visitor.shouldVisitSubSectionTables(sectionTable)) {
            for (table <- sectionTable.subSectionTables(sortMeta))
              scanSectionTable(table, visitor)
            visitor.didVisitSubSectionTables(sectionTable)
          }
      }
      visitor.didVisitSectionTable(sectionTable)
    }
  }

  def scanValueTable(valueTable: ValueTableH5, visitor: H5Visitor): Unit = {
    if (visitor.shouldVisitValueTable(valueTable)) {
      scanType match {
        case ScanType.RowScan =>
          for (value <- valueTable)
            scanValue(value, visitor)
        case ScanType.TableScan =>
      }
      visitor.didVisitValueTable(valueTable)
    }
  }
}
