/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.h5

import eu.nomad_lab.{ Base64, IteratorH5, H5Lib, JsonUtils }
import eu.nomad_lab.meta.MetaInfoRecord
import eu.nomad_lab.ref
import eu.{ nomad_lab => lab }
import scala.util.control.NonFatal
import scala.collection.breakOut
import scala.collection
import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer
import scala.collection.mutable
import ucar.ma2.{ Array => NArray }
import ucar.ma2
import org.{ json4s => jn }
import org.json4s.{ JNothing, JNull, JBool, JDouble, JDecimal, JInt, JString, JArray, JObject, JValue, JField }
import com.typesafe.scalalogging.StrictLogging

object ValueTableH5 extends StrictLogging {
  /**
   * Trying to add/set a value incompatible with the metadata known
   */
  class InvalidAssignementException(
    metaInfo: MetaInfoRecord,
    msg: String,
    what: Throwable = null
  ) extends Exception(s"Invalid assignement for meta data ${metaInfo.name}: $msg", what) {}

  /**
   * Some meta info is invalid
   */
  class InvalidMetaInfoException(
    metaInfo: MetaInfoRecord, msg: String
  ) extends Exception(s"${metaInfo.name} is invalid: $msg, metaInfo: ${JsonUtils.prettyStr(metaInfo.toJValue())}") {}

  /**
   * error creting value table
   */
  class ValueTableException(
    msg: String
  ) extends Exception(msg)

  def apply(
    metaInfo: MetaInfoRecord,
    parentSectionTable: SectionTableH5
  ): ValueTableH5 = {
    if (metaInfo.kindStr == "type_dimension")
      throw new ValueTableException(s"dimension meta infos not fully supported yet in hdf5")
    if (metaInfo.kindStr != "type_document_content")
      throw new ValueTableException(s"ValueTable can be instantiated only for conrete data (kindStr = type_document_content), not for ${lab.JsonUtils.normalizedStr(metaInfo.toJValue())}")
    val scalar = metaInfo.shape match {
      case Some(shape) =>
        if (shape.isEmpty)
          true
        else
          false
      case None =>
        throw new InvalidMetaInfoException(metaInfo, "concrete meta info should have a shape ([] for scalar values)")
    }
    val dtypeStr = metaInfo.dtypeStr match {
      case Some(s) =>
        s
      case None =>
        throw new InvalidMetaInfoException(metaInfo, "concrete meta info should have a specific dtypeStr")
    }
    if (scalar) {
      dtypeStr match {
        case "f" | "f64" => new ValueTableH5_f64(metaInfo, parentSectionTable)
        case "f32" => new ValueTableH5_f32(metaInfo, parentSectionTable)
        case "i" | "i32" => new ValueTableH5_i32(metaInfo, parentSectionTable)
        case "i64" | "r" => new ValueTableH5_i64(metaInfo, parentSectionTable)
        case "b" => new ValueTableH5_b(metaInfo, parentSectionTable)
        case "B" => new ValueTableH5_B64(metaInfo, parentSectionTable)
        case "C" => new ValueTableH5_C(metaInfo, parentSectionTable)
        case "D" => new ValueTableH5_D(metaInfo, parentSectionTable)
        case _ =>
          throw new InvalidMetaInfoException(metaInfo, "Unknown dtypeStr, known types: f,f32,f64,i,i32,i64,r,b,B,C,D")
      }
    } else {
      dtypeStr match {
        case "f" | "f64" => new ValueTableH5_Af64(metaInfo, parentSectionTable)
        case "f32" => new ValueTableH5_Af32(metaInfo, parentSectionTable)
        case "i" | "i32" => new ValueTableH5_Ai32(metaInfo, parentSectionTable)
        case "i64" | "r" => new ValueTableH5_Ai64(metaInfo, parentSectionTable)
        case "b" => new ValueTableH5_Ab(metaInfo, parentSectionTable)
        case "B" => new ValueTableH5_AB64(metaInfo, parentSectionTable)
        case "C" => new ValueTableH5_AC(metaInfo, parentSectionTable)
        case "D" => new ValueTableH5_AD(metaInfo, parentSectionTable)
        case _ =>
          throw new InvalidMetaInfoException(metaInfo, "Unknown dtypeStr, known types: f,f32,f64,i,i32,i64,r,b,B,C,D")
      }
    }
  }

  def apply(
    name: String,
    parentSectionTable: SectionTableH5
  ): ValueTableH5 = {
    //val group = parentSectionTable.sectionGroup() // try to look up the index and its attributes?
    parentSectionTable.calculation.metaInfoEnv.metaInfoRecordForName(name) match {
      case Some(meta) =>
        apply(meta, parentSectionTable)
      case None =>
        logger.warn(s"Cannot find meta info for value $name, replacing with dummy empty ValueTable")
        new ValueTableH5_dummy(
          name,
          s"Placeholder because cannot find meta info for value $name, to instantiate a ValueTable", // try to get it from the hdf5 attributes?
          parentSectionTable
        )
    }
  }

  /**
   * abstact class to handle integer scalar values
   */
  abstract class ValueTableH5_i(
      metaInfo: MetaInfoRecord,
      parentSectionTable: SectionTableH5
  ) extends ValueTableH5(metaInfo, parentSectionTable) {

    def longToArray(value: Long): NArray;

    override def addValue(value: JValue, sectionGIndex: Long = -1): ValueIndexes = {
      convertJValue_i(value) match {
        case Some(v) =>
          val a = longToArray(v)
          addArrayValuesInternal(a, sectionGIndex = sectionGIndex)
        case None => throw new ValueTableException(s"failed conversion to integer of ${JsonUtils.normalizedStr(value)}")
      }
    }

    override def addRealValue(value: Double, sectionGIndex: Long = -1): ValueIndexes = {
      val a = longToArray(value.longValue)
      addArrayValuesInternal(a, sectionGIndex = sectionGIndex)
    }

    override def addArray(shape: Seq[Long], sectionGIndex: Long = -1): ValueIndexes = {
      if (!(shape.isEmpty || shape.length == 1 && shape(0) == 1))
        throw new InvalidAssignementException(metaInfo, "tried to add an non scalar array value to a scalar integer")
      addArrayInternal(shape, sectionGIndex = sectionGIndex)
    }

    override def setArrayValues(values: NArray, offset: Option[Seq[Long]] = None, indexes: ValueIndexes): Unit = {
      if (values.getSize() != 1)
        throw new InvalidAssignementException(metaInfo, s"invalid size (${values.getSize()}) for scalar value")
      offset match {
        case None =>
          ()
        case Some(o) =>
          if (!o.isEmpty && (o.length != 1 || o(0) != 0))
            throw new InvalidAssignementException(metaInfo, s"invalid offset ${o.mkString("[", ",", "]")} for scalar value")
      }
      setArrayValuesInternal(values, offset = offset, indexes = indexes)
    }

    override def addArrayValues(values: NArray, sectionGIndex: Long = -1): ValueIndexes = {
      if (values.getSize() != 1)
        throw new InvalidAssignementException(metaInfo, s"invalid size (${values.getSize()}) for scalar value")
      addArrayValuesInternal(values, sectionGIndex)
    }
  }

  /**
   * class to handle integer scalar values
   */
  final class ValueTableH5_i32(
      metaInfo: MetaInfoRecord,
      parentSectionTable: SectionTableH5
  ) extends ValueTableH5_i(metaInfo, parentSectionTable) {

    override def apply(i: Long): ValueH5 = {
      val n = if (length == 0 || i >= length) -1 else i
      val indexDatasetId = openIndexDataset(create = false)
      val indexes = resolveGlobalIndex(indexDatasetId = indexDatasetId, globalIndex = n)
      val vDatasetId = openValueDatasetWithName(indexes.valueDatasetName)
      val res = new ValueH5_i32(table = this, gIndex = n, valueDatasetId = vDatasetId, localIndex = indexes.localIndex)
      H5Lib.datasetClose(vDatasetId)
      H5Lib.datasetClose(indexDatasetId)
      res
    }

    override def longToArray(value: Long): ma2.ArrayInt = {
      val dArray = new ma2.ArrayInt.D1(1)
      dArray.set(0, value.toInt)
      dArray
    }
  }

  /**
   * class to handle integer scalar values
   */
  final class ValueTableH5_i64(
      metaInfo: MetaInfoRecord,
      parentSectionTable: SectionTableH5
  ) extends ValueTableH5_i(metaInfo, parentSectionTable) {

    override def apply(i: Long): ValueH5 = {
      if (length == 0 || i >= length) throw new IndexOutOfBoundsException(s"index $i is out of bounds for values of $this")
      val indexDatasetId = openIndexDataset(create = false)
      val indexes = resolveGlobalIndex(indexDatasetId = indexDatasetId, globalIndex = i)
      val vDatasetId = openValueDatasetWithName(indexes.valueDatasetName)
      val res = new ValueH5_i64(table = this, gIndex = i, valueDatasetId = vDatasetId, localIndex = indexes.localIndex)
      H5Lib.datasetClose(vDatasetId)
      H5Lib.datasetClose(indexDatasetId)
      res
    }

    override def longToArray(value: Long): ma2.ArrayLong = {
      val dArray = new ma2.ArrayLong.D1(1)
      dArray.set(0, value)
      dArray
    }
  }

  /**
   * abstact class to handle integer scalar values
   */
  final class ValueTableH5_b(
      metaInfo: MetaInfoRecord,
      parentSectionTable: SectionTableH5
  ) extends ValueTableH5(metaInfo, parentSectionTable) {
    def booleanToArray(value: Boolean): ma2.ArrayBoolean = {
      val a = new ma2.ArrayBoolean.D1(1)
      a.set(0, value)
      a
    }

    override def addValue(value: JValue, sectionGIndex: Long = -1): ValueIndexes = {
      convertJValue_b(value) match {
        case Some(v) =>
          val a = booleanToArray(v)
          addArrayValuesInternal(a, sectionGIndex = sectionGIndex)
        case None => throw new ValueTableException(s"failed conversion to integer of ${JsonUtils.normalizedStr(value)}")
      }
    }

    override def apply(i: Long): ValueH5 = {
      if (length == 0 || i >= length) throw new IndexOutOfBoundsException(s"index $i is out of bounds for values of $this")
      val indexDatasetId = openIndexDataset(create = false)
      val indexes = resolveGlobalIndex(indexDatasetId = indexDatasetId, globalIndex = i)
      val vDatasetId = openValueDatasetWithName(indexes.valueDatasetName)
      val res = new ValueH5_b(table = this, gIndex = i, valueDatasetId = vDatasetId, localIndex = indexes.localIndex)
      H5Lib.datasetClose(vDatasetId)
      H5Lib.datasetClose(indexDatasetId)
      res
    }

    override def addRealValue(value: Double, sectionGIndex: Long = -1): ValueIndexes = {
      throw new InvalidAssignementException(metaInfo, s"cannot add a real value to a boolean metadata")
    }

    override def addArray(shape: Seq[Long], sectionGIndex: Long = -1): ValueIndexes = {
      if (!(shape.isEmpty || shape.length == 1 && shape(0) == 1))
        throw new InvalidAssignementException(metaInfo, "tried to add an non scalar array value to a scalar integer")
      addArrayInternal(shape, sectionGIndex = sectionGIndex)
    }

    override def setArrayValues(values: NArray, offset: Option[Seq[Long]] = None, indexes: ValueIndexes): Unit = {
      if (values.getSize() != 1)
        throw new InvalidAssignementException(metaInfo, s"invalid size (${values.getSize()}) for scalar value")
      offset match {
        case None =>
          ()
        case Some(o) =>
          if (!o.isEmpty && (o.length != 1 || o(0) != 0))
            throw new InvalidAssignementException(metaInfo, s"invalid offset ${o.mkString("[", ",", "]")} for scalar value")
      }

    }

    override def addArrayValues(values: NArray, sectionGIndex: Long = -1): ValueIndexes = {
      if (values.getSize() != 1)
        throw new InvalidAssignementException(metaInfo, s"invalid size (${values.getSize()}) for scalar value")
      addArrayValuesInternal(values, sectionGIndex = sectionGIndex)
    }
  }

  /**
   * abstact class to handle floating point scalar values
   */
  abstract class ValueTableH5_f(
      metaInfo: MetaInfoRecord,
      parentSectionTable: SectionTableH5
  ) extends ValueTableH5(metaInfo, parentSectionTable) {

    def doubleToArray(f: Double): NArray

    override def addValue(value: JValue, sectionGIndex: Long = -1): ValueIndexes = {
      convertJValue_f(value) match {
        case Some(v) =>
          val a = doubleToArray(v)
          addArrayValuesInternal(a, sectionGIndex = sectionGIndex)
        case None => throw new ValueTableException(s"failed conversion to integer of ${JsonUtils.normalizedStr(value)}")
      }
    }

    override def addRealValue(value: Double, sectionGIndex: Long = -1): ValueIndexes = {
      addArrayValuesInternal(doubleToArray(value), sectionGIndex = sectionGIndex)
    }

    override def addArray(shape: Seq[Long], sectionGIndex: Long = -1): ValueIndexes = {
      if (!(shape.isEmpty || shape.length == 1 && shape(0) == 1))
        throw new InvalidAssignementException(metaInfo, "tried to add an non scalar array value to a scalar integer")
      addArrayInternal(shape, sectionGIndex = sectionGIndex)
    }

    override def setArrayValues(values: NArray, offset: Option[Seq[Long]] = None, indexes: ValueIndexes): Unit = {
      if (values.getSize() != 1)
        throw new InvalidAssignementException(metaInfo, s"invalid size (${values.getSize()}) for scalar value")
      offset match {
        case None =>
          ()
        case Some(o) =>
          if (!o.isEmpty && (o.length != 1 || o(0) != 0))
            throw new InvalidAssignementException(metaInfo, s"invalid offset ${o.mkString("[", ",", "]")} for scalar value")
      }
      setArrayValuesInternal(values, offset = offset, indexes = indexes)
    }

    override def addArrayValues(values: NArray, sectionGIndex: Long = -1): ValueIndexes = {
      if (values.getSize() != 1)
        throw new InvalidAssignementException(metaInfo, s"invalid size (${values.getSize()}) for scalar value")
      addArrayValuesInternal(doubleToArray(values.getDouble(0)), sectionGIndex = sectionGIndex)
    }
  }

  /**
   * single precision float value
   */
  final class ValueTableH5_f32(
      metaInfo: MetaInfoRecord,
      parentSectionTable: SectionTableH5
  ) extends ValueTableH5_f(metaInfo, parentSectionTable) {
    override def apply(i: Long): ValueH5 = {
      if (length == 0 || i >= length) throw new IndexOutOfBoundsException(s"index $i is out of bounds for values of $this")
      val indexDatasetId = openIndexDataset(create = false)
      val indexes = resolveGlobalIndex(indexDatasetId = indexDatasetId, globalIndex = i)
      val vDatasetId = openValueDatasetWithName(indexes.valueDatasetName)
      val res = new ValueH5_f32(table = this, gIndex = i, valueDatasetId = vDatasetId, localIndex = indexes.localIndex)
      H5Lib.datasetClose(vDatasetId)
      H5Lib.datasetClose(indexDatasetId)
      res
    }

    override def doubleToArray(value: Double): ma2.ArrayFloat = {
      val dArray = new ma2.ArrayFloat.D1(1)
      dArray.set(0, value.toFloat)
      dArray
    }
  }

  /**
   * single precision float value
   */
  final class ValueTableH5_f64(
      metaInfo: MetaInfoRecord,
      parentSectionTable: SectionTableH5
  ) extends ValueTableH5_f(metaInfo, parentSectionTable) {
    override def apply(i: Long): ValueH5 = {
      if (length == 0 || i >= length) throw new IndexOutOfBoundsException(s"index $i is out of bounds for values of $this")
      val indexDatasetId = openIndexDataset(create = false)
      val indexes = resolveGlobalIndex(indexDatasetId = indexDatasetId, globalIndex = i)
      val vDatasetId = openValueDatasetWithName(indexes.valueDatasetName)
      val res = new ValueH5_f64(table = this, gIndex = i, valueDatasetId = vDatasetId, localIndex = indexes.localIndex)
      H5Lib.datasetClose(vDatasetId)
      H5Lib.datasetClose(indexDatasetId)
      res
    }

    override def doubleToArray(value: Double): ma2.ArrayDouble = {
      val dArray = new ma2.ArrayDouble.D1(1)
      dArray.set(0, value)
      dArray
    }
  }

  /**
   * class to handle json dictionary scalar values
   */
  final class ValueTableH5_D(
      metaInfo: MetaInfoRecord,
      parentSectionTable: SectionTableH5
  ) extends ValueTableH5(metaInfo, parentSectionTable) {
    override def apply(i: Long): ValueH5 = {
      if (length == 0 || i >= length) throw new IndexOutOfBoundsException(s"index $i is out of bounds for values of $this")
      val indexDatasetId = openIndexDataset(create = false)
      val indexes = resolveGlobalIndex(indexDatasetId = indexDatasetId, globalIndex = i)
      val vDatasetId = openValueDatasetWithName(indexes.valueDatasetName)
      val res = new ValueH5_D(table = this, gIndex = i, valueDatasetId = vDatasetId, localIndex = indexes.localIndex)
      H5Lib.datasetClose(vDatasetId)
      H5Lib.datasetClose(indexDatasetId)
      res
    }

    def jToArray(value: JObject): ma2.ArrayObject = {
      val arr = new ma2.ArrayObject.D1(classOf[org.json4s.JsonAST.JObject], 1)
      arr.set(0, value)
      arr
    }

    override def addValue(value: JValue, sectionGIndex: Long = -1): ValueIndexes = {
      convertJValue_D(value) match {
        case Some(obj) => addArrayValuesInternal(jToArray(obj), sectionGIndex = sectionGIndex)
        case None => throw new ValueTableException(s"cannot convert value ${JsonUtils.normalizedStr(value)} to dictionary")
      }
    }

    override def addRealValue(value: Double, sectionGIndex: Long = -1): ValueIndexes = {
      throw new InvalidAssignementException(metaInfo, "addRealValue not supported for dictionary type")
    }

    override def addArray(shape: Seq[Long], sectionGIndex: Long = -1): ValueIndexes = {
      if (shape.length != 1)
        throw new InvalidAssignementException(metaInfo, "tried to add an non scalar array value to a scalar dictionary")
      addArrayInternal(shape, sectionGIndex = sectionGIndex)
    }

    override def setArrayValues(values: NArray, offset: Option[Seq[Long]] = None, indexes: ValueIndexes): Unit = {
      if (values.getSize() != 1)
        throw new InvalidAssignementException(metaInfo, s"invalid size (${values.getSize()}) for scalar value")
      offset match {
        case None =>
          ()
        case Some(o) =>
          if (!o.isEmpty && (o.length != 1 || o(0) != 0))
            throw new InvalidAssignementException(metaInfo, s"invalid offset ${o.mkString("[", ",", "]")} for scalar value")
      }
      setArrayValuesInternal(values, offset, indexes)
    }

    override def addArrayValues(values: NArray, sectionGIndex: Long = -1): ValueIndexes = {
      if (values.getSize() != 1)
        throw new InvalidAssignementException(metaInfo, s"invalid size (${values.getSize()}) for scalar value")
      addArrayValuesInternal(values, sectionGIndex)
    }
  }

  /**
   * class to handle string scalar values
   */
  final class ValueTableH5_C(
      metaInfo: MetaInfoRecord,
      parentSectionTable: SectionTableH5
  ) extends ValueTableH5(metaInfo, parentSectionTable) {

    override def apply(i: Long): ValueH5 = {
      if (length == 0 || i >= length) throw new IndexOutOfBoundsException(s"index $i is out of bounds for values of $this")
      val indexDatasetId = openIndexDataset(create = false)
      val indexes = resolveGlobalIndex(indexDatasetId = indexDatasetId, globalIndex = i)
      val vDatasetId = openValueDatasetWithName(indexes.valueDatasetName)
      val res = new ValueH5_C(table = this, gIndex = i, valueDatasetId = vDatasetId, localIndex = indexes.localIndex)
      H5Lib.datasetClose(vDatasetId)
      H5Lib.datasetClose(indexDatasetId)
      res
    }

    def strToArray(value: String): ma2.ArrayString = {
      val arr = new ma2.ArrayString.D1(1)
      arr.set(0, value)
      arr
    }

    override def addValue(value: JValue, sectionGIndex: Long = -1): ValueIndexes = {
      convertJValue_C(value) match {
        case Some(s) =>
          val arr = strToArray(s)
          addArrayValuesInternal(arr, sectionGIndex = sectionGIndex)
        case None =>
          throw new ValueTableException(s"could not convert value ${JsonUtils.normalizedStr(value)} to String")
      }
    }

    override def addRealValue(value: Double, sectionGIndex: Long = -1): ValueIndexes = {
      throw new InvalidAssignementException(metaInfo, "addRealValue not supported for string values")
    }

    override def addArray(shape: Seq[Long], sectionGIndex: Long = -1): ValueIndexes = {
      if (shape.length != 1)
        throw new InvalidAssignementException(metaInfo, "tried to add an non scalar array value to a scalar string")
      addArrayInternal(shape, sectionGIndex = sectionGIndex)
    }

    override def setArrayValues(values: NArray, offset: Option[Seq[Long]] = None, indexes: ValueIndexes): Unit = {
      if (values.getSize() != 1)
        throw new InvalidAssignementException(metaInfo, s"invalid size (${values.getSize()}) for scalar value")
      offset match {
        case None =>
          ()
        case Some(o) =>
          if (!o.isEmpty && (o.length != 1 || o(0) != 0))
            throw new InvalidAssignementException(metaInfo, s"invalid offset ${o.mkString("[", ",", "]")} for scalar value")
      }
      setArrayValuesInternal(values, offset, indexes)
    }

    override def addArrayValues(values: NArray, sectionGIndex: Long = -1): ValueIndexes = {
      if (values.getSize() != 1)
        throw new InvalidAssignementException(metaInfo, s"invalid size (${values.getSize()}) for scalar value")
      addArrayValuesInternal(values, sectionGIndex = sectionGIndex)
    }
  }

  /**
   * class to handle binary blob scalar values
   */
  final class ValueTableH5_B64(
      metaInfo: MetaInfoRecord,
      parentSectionTable: SectionTableH5
  ) extends ValueTableH5(metaInfo, parentSectionTable) {
    override def apply(i: Long): ValueH5 = {
      val n = if (length == 0 || i >= length) -1 else i
      throw new InvalidAssignementException(metaInfo, s"binary blobs not yet supported in hdf5")
    }

    def b64ToArray(value: String): NArray = {
      val arr = new ma2.ArrayString.D1(1)
      arr.set(0, value)
      arr
    }

    override def addValue(value: JValue, sectionGIndex: Long = -1): ValueIndexes = {
      convertJValue_B64(value) match {
        case Some(s) =>
          addArrayValuesInternal(b64ToArray(s), sectionGIndex = sectionGIndex)
        case None =>
          throw new ValueTableException(s"could not convert value ${JsonUtils.normalizedStr(value)} to base64 encoded string")
      }
    }

    override def addRealValue(value: Double, sectionGIndex: Long = -1): ValueIndexes = {
      throw new InvalidAssignementException(metaInfo, "addRealValue not supported by base64 encoding")
    }

    override def addArray(shape: Seq[Long], sectionGIndex: Long = -1): ValueIndexes = {
      if (shape.length != 1)
        throw new InvalidAssignementException(metaInfo, "tried to add an non scalar array value to a scalar b64 encoding")
      addArrayInternal(shape, sectionGIndex = sectionGIndex)
    }

    override def setArrayValues(values: NArray, offset: Option[Seq[Long]] = None, indexes: ValueIndexes): Unit = {
      if (values.getSize() != 1)
        throw new InvalidAssignementException(metaInfo, s"invalid size (${values.getSize()}) for scalar value")
      offset match {
        case None =>
          ()
        case Some(o) =>
          if (!o.isEmpty && (o.length != 1 || o(0) != 0))
            throw new InvalidAssignementException(metaInfo, s"invalid offset ${o.mkString("[", ",", "]")} for scalar value")
      }
      setArrayValuesInternal(values, offset, indexes)
    }

    override def addArrayValues(values: NArray, sectionGIndex: Long = -1): ValueIndexes = {
      if (values.getSize() != 1)
        throw new InvalidAssignementException(metaInfo, s"invalid size (${values.getSize()}) for scalar value")
      if (values.getSize() != 1)
        throw new InvalidAssignementException(metaInfo, s"invalid size (${values.getSize()}) for scalar value")
      addArrayValuesInternal(values, sectionGIndex = sectionGIndex)
    }
  }

  /**
   * returns shape an a sample elements of the nested json array
   *
   * if given expectedRank is the expected rank of the array, then the returned
   * shape will not be bigger than than expectedRank, it might still be smaller
   * and the caller is expected to handle that error
   */
  def shapeAndSampleEl(value: JValue, expectedRank: Int = -1): (Seq[Long], JValue) = {
    val shape: ListBuffer[Long] = ListBuffer[Long]()
    var el: JValue = value
    var missingRank: Int = expectedRank
    while (missingRank != 0) {
      el match {
        case JArray(arr) =>
          shape.append(arr.count {
            case JNothing => false
            case _ => true
          }.toLong)
          missingRank -= 1
          if (arr.isEmpty) {
            // if missingRank > 0 we could throw/warn, but we leave it to the caller
            missingRank = 0
            el = JNothing
          } else {
            el = arr(0)
          }
        case _ =>
          // if missingRank > 0 we could throw/warn, but we leave it to the caller
          missingRank = 0
      }
    }
    shape.toSeq -> el
  }

  /**
   * If shape is not compatible with refShape throws an exception
   */
  def shapeCheck(metaInfo: MetaInfoRecord, shape: Seq[Long], refShape: Seq[Either[Long, String]]): Unit = {
    if (shape.length != refShape.length)
      throw new InvalidAssignementException(metaInfo, s"invalid shape.length (${shape.length} vs ${refShape.length})")
    for ((expected, actual) <- refShape.zip(shape)) {
      expected match {
        case Left(i) =>
          if (i != actual)
            throw new InvalidAssignementException(metaInfo, s"shape $shape is not compatible with the expected one ${refShape.mkString("[", ",", "]")}")
        case Right(s) =>
          ()
      }
    }
  }

  /**
   * An iterator on a rectangular JArray of known shape for a given meta info
   */
  class RectangularJArrayIterator(
      val metaInfo: MetaInfoRecord,
      val shape: Seq[Long],
      var el: JValue,
      var iterators: List[List[JValue]] = Nil,
      var depth: Int = 0,
      idx: Option[Seq[Long]] = None,
      offset0: Option[Seq[Long]] = None
  ) extends Iterator[(Array[Long], JValue)] with StrictLogging {
    val offset = Array.fill[Long](shape.size)(0)
    if (!offset0.isEmpty)
      offset0.get.copyToArray(offset)
    val index = Array.fill[Long](shape.size)(0)
    if (!idx.isEmpty)
      idx.get.copyToArray(index)
    else
      offset.copyToArray(index)
    val prevIndex = Array.fill[Long](shape.size)(-1)

    /**
     * goes on on the same level or up until it can progress
     */
    def advance(): Unit = {

      iterators match {
        case currentList :: otherLists =>
          currentList.dropWhile(_ == JNothing) match {
            case newEl :: newList =>
              el = newEl
              iterators = newList :: otherLists
              index(depth - 1) += 1
            case Nil =>
              depth -= 1
              index(depth) = offset(depth)
              iterators = otherLists
              advance()
          }
        case Nil =>
          el = JNothing
      }
    }

    /**
     * checks if the iterator ended properly
     */
    def checkEnd(): Unit = {
      val leftOver = el
      el = JNothing
      var invalid: Boolean = false

      for ((i, j) <- offset.zip(index)) {
        if (i != j)
          invalid = true
      }
      val invalidMsg = if (invalid)
        s"iterator finished with index ${index.mkString("[", ",", "]")} when shape is ${shape.mkString("[", ",", "]")}"
      else
        ""
      if (leftOver != JNothing)
        throw new InvalidAssignementException(
          metaInfo,
          s"array finished with leftover objects, $invalidMsg, left over: ${JsonUtils.prettyStr(leftOver)}"
        )
      if (invalid)
        throw new InvalidAssignementException(metaInfo, invalidMsg)
    }

    /**
     * Goes deeper until it reaches the wanted depth (rank)
     */
    def goToRank(): Unit = {
      while (depth != shape.size) {
        el match {
          case JArray(arr) =>
            arr.dropWhile(_ == JNothing) match {
              case newEl :: rest =>
                el = newEl
                iterators = rest :: iterators
                depth += 1
              case Nil =>
                checkEnd()
                return
            }
          case JNothing =>
            checkEnd()
            return
          case _ =>
            checkEnd()
            return
        }
      }
    }

    // guarantee that the initial state is valid state
    // this simplifies hasNext, next
    goToRank()

    /**
     * returns true if the iterator has more elements
     */
    def hasNext: Boolean = el != JNothing

    /**
     * goes to the next element and returns it
     *
     * The index array will be invalidated with the next call to next()
     */
    def next(): (Array[Long], JValue) = {
      index.copyToArray(prevIndex)
      val prevEl = el
      advance()
      goToRank()
      (prevIndex, prevEl)
    }
  }

  /**
   * converts a JValue to an array
   *
   * assumes that createArray returns a canonically ordered array with the given shape
   */
  def convertJValueToArray[T](
    metaInfo: MetaInfoRecord,
    value: JValue,
    createArray: (Seq[Long]) => T,
    setValueAtFlatIndex: (T, Long, JValue) => Unit
  ): T = {
    try {
      val expectedShape = metaInfo.shape.getOrElse(Seq())
      val (shape, sampleVal) = shapeAndSampleEl(value, expectedRank = expectedShape.length)
      shapeCheck(metaInfo, shape, expectedShape)
      val iter = new RectangularJArrayIterator(
        metaInfo = metaInfo,
        shape = shape,
        el = value
      )
      val array = createArray(shape)
      var ii: Long = 0
      while (iter.hasNext) {
        val (index, el) = iter.next()
        setValueAtFlatIndex(array, ii, el)
        ii += 1
      }
      array
    } catch {
      case NonFatal(e) =>
        throw new InvalidAssignementException(metaInfo, s"Error converting value ${JsonUtils.normalizedStr(value)} to an array", e)
    }
  }

  abstract class ArrayValueTableH5(
      metaInfo: MetaInfoRecord,
      parentSectionTable: SectionTableH5
  ) extends ValueTableH5(metaInfo, parentSectionTable) {
    override def setArrayValues(values: NArray, offset: Option[Seq[Long]], indexes: ValueIndexes): Unit = {
      setArrayValuesInternal(values, offset, indexes)
    }

    /** adds an array value */
    override def addArrayValues(values: NArray, sectionGIndex: Long): ValueIndexes = {
      val expectedShape = metaInfo.shape.getOrElse(Seq())
      val shape: Seq[Long] = values.getShape().map(_.toLong)(breakOut)
      shapeCheck(metaInfo, shape, expectedShape)
      addArrayValuesInternal(values, sectionGIndex)
    }

    override def addArray(shape: Seq[Long], sectionGIndex: Long): ValueIndexes = {
      val (valDatasetName, valDatasetId) = openValueDataset(shape, create = true)
      val idxDatasetId = openIndexDataset(create = true)
      val valueIndex = valueAdd(idxDatasetId, valuesDatasetName = valDatasetName, valuesDatasetId = valDatasetId, dimensions = shape, superSectionGindex = sectionGIndex)
      H5Lib.datasetClose(valDatasetId)
      H5Lib.datasetClose(idxDatasetId)
      valueIndex
    }

    override def addRealValue(value: Double, sectionGIndex: Long = -1): ValueIndexes = {
      throw new InvalidAssignementException(metaInfo, "addRealValue supported only in scalar values")
    }
  }

  /**
   * meta info for array of ints
   */
  final class ValueTableH5_Ai32(
      metaInfo: MetaInfoRecord,
      parentSectionTable: SectionTableH5
  ) extends ArrayValueTableH5(metaInfo, parentSectionTable) {
    override def apply(i: Long): ValueH5 = {
      if (length == 0 || i >= length) throw new IndexOutOfBoundsException(s"index $i is out of bounds for values of $this")
      val indexDatasetId = openIndexDataset(create = false)
      val res = new ValueH5_Ai32(table = this, gIndex = i, indexDatasetId = indexDatasetId)
      H5Lib.datasetClose(indexDatasetId)
      res
    }

    def createArray(shape: Seq[Long]): NArray = {
      NArray.factory(ma2.DataType.INT, shape.map(_.intValue).toArray)
    }

    def convertAllJValue_i32(value: JValue): Int = {
      convertJValue_i(value) match {
        case Some(longVal) =>
          if (longVal < Int.MinValue || longVal > Int.MaxValue)
            throw new InvalidAssignementException(metaInfo, s"Out of bounds value $value for type i32")
          longVal.intValue
        case None =>
          throw new InvalidAssignementException(metaInfo, s"cannot convert value from $value")
      }
    }

    def setFlatIndex_i32(array: NArray, i: Long, value: Int): Unit = {
      array.setInt(i.intValue, value)
    }

    override def addValue(value: JValue, sectionGIndex: Long): ValueIndexes = {
      addArrayValues(convertJValueToArray[NArray](metaInfo, value, createArray _, {
        (array: NArray, i: Long, value: JValue) =>
          setFlatIndex_i32(array, i, convertAllJValue_i32(value))
      }), sectionGIndex)
    }

  }

  /**
   * meta info for array of longs
   */
  final class ValueTableH5_Ai64(
      metaInfo: MetaInfoRecord,
      parentSectionTable: SectionTableH5
  ) extends ArrayValueTableH5(metaInfo, parentSectionTable) {
    override def apply(i: Long): ValueH5 = {
      if (length == 0 || i >= length) throw new IndexOutOfBoundsException(s"index $i is out of bounds for values of $this")
      val indexDatasetId = openIndexDataset(create = false)
      val res = new ValueH5_Ai64(table = this, gIndex = i, indexDatasetId = indexDatasetId)
      H5Lib.datasetClose(indexDatasetId)
      res
    }

    def createArray(shape: Seq[Long]): NArray = {
      NArray.factory(Long.getClass(), shape.map(_.intValue).toArray)
    }

    def convertAllJValue_i64(value: JValue): Long = {
      convertJValue_i(value) match {
        case Some(i) =>
          i
        case None =>
          throw new InvalidAssignementException(metaInfo, s"cannot convert value from $value")
      }
    }

    def setFlatIndex_i64(array: NArray, i: Long, value: Long): Unit = {
      array.setLong(i.intValue, value)
    }

    override def addValue(value: JValue, sectionGIndex: Long): ValueIndexes = {
      addArrayValues(convertJValueToArray[NArray](metaInfo, value, createArray _, {
        (array: NArray, i: Long, value: JValue) =>
          setFlatIndex_i64(array, i, convertAllJValue_i64(value))
      }), sectionGIndex)
    }
  }

  /**
   * meta info for array of floats
   */
  final class ValueTableH5_Af32(
      metaInfo: MetaInfoRecord,
      parentSectionTable: SectionTableH5
  ) extends ArrayValueTableH5(metaInfo, parentSectionTable) {
    override def apply(i: Long): ValueH5 = {
      if (length == 0 || i >= length) throw new IndexOutOfBoundsException(s"index $i is out of bounds for values of $this")
      val indexDatasetId = openIndexDataset(create = false)
      val res = new ValueH5_Af32(table = this, gIndex = i, indexDatasetId = indexDatasetId)
      H5Lib.datasetClose(indexDatasetId)
      res
    }

    def createArray(shape: Seq[Long]): NArray = {
      NArray.factory(Float.getClass(), shape.map(_.intValue).toArray)
    }

    def convertAllJValue_f32(value: JValue): Float = {
      convertJValue_f(value) match {
        case Some(d) =>
          d.floatValue
        case None =>
          throw new InvalidAssignementException(metaInfo, s"cannot convert value from $value")
      }
    }

    def setFlatIndex_f32(array: NArray, i: Long, value: Float): Unit = {
      array.setFloat(i.intValue, value)
    }

    override def addValue(value: JValue, sectionGIndex: Long): ValueIndexes = {
      addArrayValues(convertJValueToArray[NArray](metaInfo, value, createArray _, {
        (array: NArray, i: Long, value: JValue) =>
          setFlatIndex_f32(array, i, convertAllJValue_f32(value))
      }), sectionGIndex)
    }
  }

  /**
   * meta info for array of doubles
   */
  final class ValueTableH5_Af64(
      metaInfo: MetaInfoRecord,
      parentSectionTable: SectionTableH5
  ) extends ArrayValueTableH5(metaInfo, parentSectionTable) {
    override def apply(i: Long): ValueH5 = {
      if (length == 0 || i >= length) throw new IndexOutOfBoundsException(s"index $i is out of bounds for values of $this")
      val indexDatasetId = openIndexDataset(create = false)
      val res = new ValueH5_Af64(table = this, gIndex = i, indexDatasetId = indexDatasetId)
      H5Lib.datasetClose(indexDatasetId)
      res
    }

    def createArray(shape: Seq[Long]): NArray = {
      NArray.factory(classOf[java.lang.Double], shape.map(_.intValue).toArray)
    }

    def convertAllJValue_f64(value: JValue): Double = {
      convertJValue_f(value) match {
        case Some(d) =>
          d
        case None =>
          throw new InvalidAssignementException(metaInfo, s"cannot convert value from $value")
      }
    }

    def setFlatIndex_f64(array: NArray, i: Long, value: Double): Unit = {
      array.setDouble(i.intValue, value)
    }

    override def addValue(value: JValue, sectionGIndex: Long): ValueIndexes = {
      addArrayValues(convertJValueToArray[NArray](metaInfo, value, createArray _, {
        (array: NArray, i: Long, value: JValue) =>
          setFlatIndex_f64(array, i, convertAllJValue_f64(value))
      }), sectionGIndex)
    }
  }

  /**
   * meta info for array of booleans
   */
  final class ValueTableH5_Ab(
      metaInfo: MetaInfoRecord,
      parentSectionTable: SectionTableH5
  ) extends ArrayValueTableH5(metaInfo, parentSectionTable) {
    override def apply(i: Long): ValueH5 = {
      if (length == 0 || i >= length) throw new IndexOutOfBoundsException(s"index $i is out of bounds for values of $this")
      val indexDatasetId = openIndexDataset(create = false)
      val res = new ValueH5_Ab(table = this, gIndex = i, indexDatasetId = indexDatasetId)
      H5Lib.datasetClose(indexDatasetId)
      res
    }

    def createArray(shape: Seq[Long]): NArray = {
      NArray.factory(Boolean.getClass(), shape.map(_.intValue).toArray)
    }

    def convertAllJValue_b(value: JValue): Boolean = {
      convertJValue_b(value) match {
        case Some(b) => b
        case None =>
          throw new InvalidAssignementException(metaInfo, s"cannot convert value from $value")
      }
    }

    def setFlatIndex_b(array: NArray, i: Long, value: Boolean): Unit = {
      array.setBoolean(i.intValue, value)
    }

    override def addValue(value: JValue, sectionGIndex: Long): ValueIndexes = {
      addArrayValues(convertJValueToArray[NArray](metaInfo, value, createArray _, {
        (array: NArray, i: Long, value: JValue) =>
          setFlatIndex_b(array, i, convertAllJValue_b(value))
      }), sectionGIndex)
    }
  }

  /**
   * meta info for array of strings
   */
  final class ValueTableH5_AC(
      metaInfo: MetaInfoRecord,
      parentSectionTable: SectionTableH5
  ) extends ArrayValueTableH5(metaInfo, parentSectionTable) {
    override def apply(i: Long): ValueH5 = {
      if (length == 0 || i >= length) throw new IndexOutOfBoundsException(s"index $i is out of bounds for values of $this")
      val indexDatasetId = openIndexDataset(create = false)
      val res = new ValueH5_AC(table = this, gIndex = i, indexDatasetId = indexDatasetId)
      H5Lib.datasetClose(indexDatasetId)
      res
    }

    def createArray(shape: Seq[Long]): ma2.ArrayString = {
      new ma2.ArrayString(shape.map(_.intValue).toArray)
    }

    def convertAllJValue_C(value: JValue): String = {
      convertJValue_C(value) match {
        case Some(s) =>
          s
        case None =>
          throw new InvalidAssignementException(metaInfo, s"cannot convert value from $value")
      }
    }

    def setFlatIndex_C(array: ma2.ArrayString, i: Long, value: String): Unit = {
      val index = array.getIndex() // inefficient
      index.setCurrentCounter(i.intValue)
      array.set(index, value)
    }

    override def addValue(value: JValue, sectionGIndex: Long): ValueIndexes = {
      addArrayValues(convertJValueToArray[ma2.ArrayString](metaInfo, value, createArray _, {
        (array: ma2.ArrayString, i: Long, value: JValue) =>
          setFlatIndex_C(array, i, convertAllJValue_C(value))
      }), sectionGIndex)
    }
  }

  /**
   * meta info for array of byte arrays (blobs)
   *
   * This should be improved, currently data is stored as Base64 url encoded
   * strings, but ArraySequence of bytes is probably better and should be
   * evaluated
   */
  final class ValueTableH5_AB64(
      metaInfo: MetaInfoRecord,
      parentSectionTable: SectionTableH5
  ) extends ArrayValueTableH5(metaInfo, parentSectionTable) {
    override def apply(i: Long): ValueH5 = {
      val n = if (length == 0 || i >= length) -1 else i
      throw new ValueTableException(s"binary blobs not yet implemented in hdf5")
    }

    def createArray(shape: Seq[Long]): ma2.ArrayString = {
      new ma2.ArrayString(shape.map(_.intValue).toArray)
    }

    def convertAllJValue_B64(value: JValue): String = {
      convertJValue_C(value) match {
        case Some(s) =>
          s
        case None =>
          throw new InvalidAssignementException(metaInfo, s"cannot convert value from $value")
      }
    }

    def setFlatIndex_B64(array: ma2.ArrayString, i: Long, value: String): Unit = {
      val index = array.getIndex() // inefficient
      index.setCurrentCounter(i.intValue)
      array.set(index, value)
    }

    override def addValue(value: JValue, sectionGIndex: Long): ValueIndexes = {
      addArrayValues(convertJValueToArray[ma2.ArrayString](metaInfo, value, createArray _, {
        (array: ma2.ArrayString, i: Long, value: JValue) =>
          setFlatIndex_B64(array, i, convertAllJValue_B64(value))
      }), sectionGIndex)
    }
  }

  /**
   * array of json dictionaries
   */
  final class ValueTableH5_AD(
      metaInfo: MetaInfoRecord,
      parentSectionTable: SectionTableH5
  ) extends ArrayValueTableH5(metaInfo, parentSectionTable) {
    override def apply(i: Long): ValueH5 = {
      if (length == 0 || i >= length) throw new IndexOutOfBoundsException(s"index $i is out of bounds for values of $this")
      val indexDatasetId = openIndexDataset(create = false)
      val res = new ValueH5_AD(table = this, gIndex = i, indexDatasetId = indexDatasetId)
      H5Lib.datasetClose(indexDatasetId)
      res
    }

    def createArray(shape: Seq[Long]): ma2.ArrayString = {
      new ma2.ArrayString(shape.map(_.intValue).toArray)
    }

    def convertAllJValue_D(value: JValue): String = {
      convertJValue_D(value) match {
        case Some(d) =>
          JsonUtils.normalizedStr(value)
        case None =>
          throw new InvalidAssignementException(metaInfo, s"cannot convert value from $value")
      }
    }

    def setFlatIndex_D(array: ma2.ArrayString, i: Long, value: String): Unit = {
      val index = array.getIndex() // inefficient
      index.setCurrentCounter(i.intValue)
      array.set(index, value)
    }

    override def addValue(value: JValue, sectionGIndex: Long): ValueIndexes = {
      addArrayValues(convertJValueToArray[ma2.ArrayString](metaInfo, value, createArray _, {
        (array: ma2.ArrayString, i: Long, value: JValue) =>
          setFlatIndex_D(array, i, convertAllJValue_D(value))
      }), sectionGIndex)
    }
  }
}

/**
 * Manager for sections with a given metadata
 */
abstract class ValueTableH5(
    val metaInfo: MetaInfoRecord,
    val parentSectionTable: SectionTableH5,
    val checkSequence: Boolean = true
) extends Traversable[ValueH5] {
  import ValueTableH5.ValueTableException
  import ValueTableH5.InvalidAssignementException

  if (metaInfo.kindStr != "type_document_content")
    throw new CalculationH5.CalculationH5Error(s"Cannot instantiate a ValueTableH5 with a non concrete meta info ${JsonUtils.normalizedStr(metaInfo.toJValue())}")
  val rank = metaInfo.shape.getOrElse(Seq()).length

  def name: String = metaInfo.name

  final override def equals(other: Any): Boolean = {
    val that = other.asInstanceOf[ValueTableH5]
    if (that == null)
      false
    else
      metaInfo.name == that.metaInfo.name && parentSectionTable == that.parentSectionTable
  }

  /**
   * returns the path from the root section until this value table (incuded)
   */
  def metaPath: Seq[String] = {
    parentSectionTable.metaPath :+ name
  }

  /**
   * returns a reference to this value table
   */
  def toRef: ref.TableRef = {
    val calc = parentSectionTable.calculation
    ref.TableRef(calc.archive.archiveGid, calc.calculationGid, metaPath)
  }

  /**
   * dimensions that do not have a fixed value
   */
  val openDimensions = metaInfo.shape match {
    case Some(dims) =>
      dims.zipWithIndex.filter {
        case (Right(str), idx) => true
        case _ => false
      }
    case None =>
      Seq()
  }

  /**
   * returns a dataset to store values with the given size
   * locId should be the group of the section containing the concrete meta info metaName
   * If create is false returns -1 if not existing, otherwise creates it if needed.
   */
  def openValueDataset(valueDimensions: Seq[Long], create: Boolean = false): (String, Long) = {
    val openDimensionsValues = openDimensions.map {
      case (dimName, idx) =>
        Base64.b64Nr(valueDimensions(idx))
    }
    val name = openDimensionsValues.foldLeft(metaInfo.name + "-v") {
      _ + "." + _
    }
    val group = parentSectionTable.sectionGroup(create = create)
    val datasetId = if (group < 0)
      -1
    else
      H5Lib.datasetGet(group, name, dtypeStr = metaInfo.dtypeStr.get,
        dimensions = (0: Long) +: valueDimensions,
        maxDimensions = (H5Lib.unlimitedDim) +: valueDimensions,
        create = create)
    (name -> datasetId)
  }

  def openValueDatasetWithName(name: String): Long = {
    H5Lib.datasetOpen(parentSectionTable.sectionGroup(), name)
  }

  /**
   * returns the dataset for the index for the given metaName
   * locId should be the group of the section containing the concrete meta info metaName
   * If create is false returns -1 if not existing, otherwise creates it if needed.
   */
  def openIndexDataset(create: Boolean = false): Long = {
    val name = metaInfo.name + "-index"
    val indexDim: Long = openDimensions.length.toLong + (if (openDimensions.isEmpty) 1 else 2)
    parentSectionTable.sectionGroup(create = create) match {
      case x if x >= 0 =>
        H5Lib.datasetGet(locId = x, name, "i64",
          dimensions = Seq(0: Long, indexDim),
          maxDimensions = Seq(H5Lib.unlimitedDim, indexDim),
          create = create)
      case x if x < 0 =>
        -1: Long
    }
  }

  /**
   * gets the value at the given local index
   */
  def valueGetLocal(valuesDatasetId: Long, localIndex: Long, valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq(), value: Option[NArray] = None): NArray = {
    val dimsId = H5Lib.datasetGetDimensionsId(valuesDatasetId)
    val valueDims = H5Lib.dimsExtents(dimsId)
    val offset = Array.fill[Long](valueDims.length)(0: Long)
    offset(0) = localIndex
    for ((off, i) <- valueOffset.zipWithIndex)
      offset(i + 1) = off
    valueDims(0) = 1

    val array = value match {
      case Some(a) => a
      case None => H5Lib.createNArray(metaInfo.dtypeStr.get, valueDims.drop(1).map(_.toInt))
    }
    H5Lib.datasetRead(
      datasetId = valuesDatasetId,
      dtypeStr = metaInfo.dtypeStr match {
        case Some(t) => t
        case None => throw new CalculationH5.CalculationH5Error(s"meta info ${metaInfo.name} expected to be a meta info with dtypeStr, not ${JsonUtils.normalizedStr(metaInfo.toJValue())}")
      },
      dimsToRead = valueDims,
      offset = offset,
      value = Some(array)
    )
  }

  /**
   * Gets the values starting with localIndex0, and up to localIndex0 + localIndexSize
   * non included, which might be non continuous in the global indexes,
   * returns the values read (if they are less than localIndexSize then
   * the first dimension of the returned array is adjusted accordingly)
   */
  def valueGetLocalRange(valuesDatasetId: Long, localIndex0: Long, localIndexSize: Long, valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq(), value: Option[NArray] = None): NArray = {
    val dimsId = H5Lib.datasetGetDimensionsId(valuesDatasetId)
    val valueDims = H5Lib.dimsExtents(dimsId)
    valueDims(0) = (valueDims(0) - localIndex0) min localIndexSize
    val offset = Array.fill[Long](valueDims.length)(0: Long)
    offset(0) = localIndex0
    for ((off, i) <- valueOffset.zipWithIndex)
      offset(i + 1) = off

    val array = value match {
      case Some(a) =>
        val aShape = a.getShape()
        if (aShape(0).toLong != valueDims(0)) {
          if (aShape(0).toLong < valueDims(0))
            throw new CalculationH5.CalculationH5Error(s"Input array ${aShape.mkString("[", ",", "]")} is too small in first index for the requested value range ${valueDims.mkString("[", ",", "]")}")
          val ranges = mutable.Buffer.fill[ma2.Range](aShape.length)(null)
          ranges(0) = new ma2.Range(valueDims(0).toInt)
          a.sectionNoReduce(ranges.asJava)
        } else {
          a
        }
      case None =>
        H5Lib.createNArray(metaInfo.dtypeStr.get, valueDims.drop(1).map(_.toInt))
    }
    H5Lib.datasetRead(
      datasetId = valuesDatasetId,
      dtypeStr = metaInfo.dtypeStr match {
        case Some(t) => t
        case None => throw new CalculationH5.CalculationH5Error(s"meta info ${metaInfo.name} expected to be a meta info with dtypeStr, not ${JsonUtils.normalizedStr(metaInfo.toJValue())}")
      },
      dimsToRead = valueDims,
      offset = offset,
      value = Some(array)
    )
  }

  /**
   * resolves a global index
   */
  def resolveGlobalIndex(indexDatasetId: Long, globalIndex: Long): ValueIndexes = {
    if (globalIndex < 0)
      return ValueIndexes(metaInfo.name + "-v", -1, -1)
    var localIndex: Long = -1
    var name: String = ""
    var sectionGIndex: Option[Long] = None
    if (openDimensions.isEmpty) {
      localIndex = globalIndex
      name = metaInfo.name + "-v"
    } else {
      val idxVal = H5Lib.datasetRead(
        datasetId = indexDatasetId,
        dtypeStr = "i64",
        dimsToRead = Seq(1: Long, if (openDimensions.isEmpty) 1 else (2 + openDimensions.length.toLong)),
        offset = Seq(globalIndex, 0)
      )
      val idxValIt = idxVal.getIndexIterator
      sectionGIndex = Some(idxValIt.getLongNext())
      localIndex = idxValIt.getLongNext()
      val openDimensionsValues = openDimensions.map {
        case (dimName, idx) =>
          Base64.b64Nr(idxValIt.getLongNext())
      }
      name = openDimensionsValues.foldLeft(metaInfo.name + "-v") {
        _ + "." + _
      }
    }
    ValueIndexes(name, globalIndex = globalIndex, localIndex = localIndex, sectionGIndex = sectionGIndex)
  }

  /**
   * Number of elements in the table
   */
  def length: Long = {
    val index = openIndexDataset()
    if (index < 0) return 0
    val dimsId = H5Lib.datasetGetDimensionsId(index)
    val dims = H5Lib.dimsExtents(dimsId)
    val res = dims(0)
    H5Lib.dimsClose(dimsId)
    H5Lib.datasetClose(index)
    res
  }

  def lengthL: Long = length

  /**
   * Is the table empty
   */
  override def isEmpty(): Boolean = {
    length == 0
  }

  /**
   * First value of the table
   */
  def first: ValueH5 = {
    apply(0)
  }

  /**
   * Last value of the table
   */
  override def last: ValueH5 = {
    apply(length - 1)
  }

  /**
   * i th value of the table
   */
  def apply(i: Long): ValueH5 = {
    val n = if (length == 0 || i >= length) -1 else i
    ValueH5(table = this, gIndex = n)
  }

  /**
   * group of the parent section
   */
  def parentGroup: Long = {
    parentSectionTable.sectionGroup()
  }

  override def toString(): String = {
    s"ValueTableH5(${metaInfo.name})"
  }

  /**
   * loops on all values
   */
  def foreach[U](f: ValueH5 => U): Unit = {
    for (i <- (0: Long).until(length)) {
      f(apply(i))
    }
  }

  /** adds a value of the given size to the metaName, returns its global and local index */
  def valueAdd(indexDatasetId: Long, valuesDatasetName: String, valuesDatasetId: Long, dimensions: Seq[Long], superSectionGindex: Long): ValueIndexes = {
    val valueDimId = H5Lib.datasetGetDimensionsId(valuesDatasetId)
    val valueDims = H5Lib.dimsExtents(valueDimId)
    H5Lib.dimsClose(valueDimId)
    if (valueDims.length != dimensions.length + 1)
      throw new ValueTableException(s"Incompatible sizes for value and dataset: ${valueDims.mkString("[", ",", "]")} vs dimensions in $metaInfo.name")
    for (i <- 0 until dimensions.length) {
      if (valueDims(i + 1) != dimensions(i))
        throw new ValueTableException(s"Incompatible sizes for value and dataset: ${valueDims.mkString("[", ",", "]")} vs dimensions in $metaInfo.name")
    }
    val localIndex = valueDims(0)
    valueDims(0) += 1
    H5Lib.datasetResize(valuesDatasetId, valueDims)

    val indexDimId: Long = H5Lib.datasetGetDimensionsId(indexDatasetId)
    val indexDims: Array[Long] = H5Lib.dimsExtents(indexDimId)

    H5Lib.dimsClose(indexDimId)
    val globalIndex = indexDims(0)
    indexDims(0) += 1
    H5Lib.datasetResize(indexDatasetId, indexDims)
    val newIndexValues = new ma2.ArrayLong.D2(1, indexDims(1).toInt)
    newIndexValues.set(0, 0, superSectionGindex match {
      case -1 => parentSectionTable.lastSectionGIndex
      case i => i
    })
    if (!openDimensions.isEmpty) {
      newIndexValues.set(0, 1, localIndex)
      var ii: Int = 2
      openDimensions.foreach {
        case (name, index) =>
          newIndexValues.set(0, ii, dimensions(index))
          ii += 1
      }
    }
    H5Lib.datasetWrite(
      indexDatasetId,
      value = newIndexValues,
      offset = Seq(globalIndex, 0),
      dtypeStr = "i64"
    )
    ValueIndexes(valuesDatasetName, globalIndex, localIndex)
  }

  /**
   * low level method that sets the value at localIndex to the given value
   */
  def valueSetLocal(valuesDatasetId: Long, localIndex: Long, value: NArray, valueOffset: Seq[Long] = Seq()): Unit = {
    val dimsId = H5Lib.datasetGetDimensionsId(valuesDatasetId)
    val valueDims = H5Lib.dimsExtents(dimsId)
    val offset = Array.fill[Long](valueDims.length)(0: Long)
    offset(0) = localIndex
    for ((off, i) <- valueOffset.zipWithIndex)
      offset(i + 1) = off
    if (offset(0) < 0) offset(0) += valueDims(0)
    val valueSizes = Array.fill[Long](offset.length)(1: Long)
    val valueShape = value.getShape
    if (valueShape.length + 1 != valueDims.length)
      throw new ValueTableException(s"Invalid shape for value ${valueShape.mkString("[", ",", "]")} vs ${valueDims.drop(1).mkString("[", ",", "]")} in ${metaInfo.name}")
    for ((s, i) <- valueShape.zipWithIndex) {
      if (s.toLong != valueDims(i + 1))
        throw new ValueTableException(s"Invalid shape for value ${valueShape.mkString("[", ",", "]")} vs ${valueDims.drop(1).mkString("[", ",", "]")} in ${metaInfo.name}")
      valueSizes(i + 1) = s.toLong
    }
    H5Lib.datasetWrite(valuesDatasetId, value, offset = offset, dtypeStr = metaInfo.dtypeStr.get, valueSizes = valueSizes)
  }

  /** worker method that adds an array value */
  protected def addArrayValuesInternal(values: NArray, sectionGIndex: Long): ValueIndexes = {
    val (valDatasetName, valDatasetId) = openValueDataset(values.getShape.map(_.toLong).toSeq, create = true)
    val idxDatasetId = openIndexDataset(create = true)
    val valueIndex = valueAdd(idxDatasetId, valuesDatasetName = valDatasetName, valuesDatasetId = valDatasetId, dimensions = values.getShape.map(_.toLong).toSeq, superSectionGindex = sectionGIndex)
    valueSetLocal(valDatasetId, localIndex = valueIndex.localIndex, value = values)
    if (checkSequence && valueIndex.globalIndex > 0) {
      val v = H5Lib.datasetRead(datasetId = idxDatasetId, dtypeStr = "i64", dimsToRead = Seq(1, 1), offset = Seq(valueIndex.globalIndex - 1, 0))
      val oldSectionGIndex = v.getLong(0)
      if (oldSectionGIndex > sectionGIndex)
        throw new ValueTableException(s"incorrect sequence, adding value for section $sectionGIndex after values of section $oldSectionGIndex to $this")
    }
    H5Lib.datasetClose(valDatasetId)
    H5Lib.datasetClose(idxDatasetId)
    valueIndex
  }

  /** method that adds an array value of that shape */
  protected def addArrayInternal(shape: Seq[Long], sectionGIndex: Long): ValueIndexes = {
    val (valDatasetName, valDatasetId) = openValueDataset(shape, create = true)
    val idxDatasetId = openIndexDataset(create = true)
    val valueIndex = valueAdd(idxDatasetId, valuesDatasetName = valDatasetName, valuesDatasetId = valDatasetId, dimensions = shape, superSectionGindex = sectionGIndex)
    if (checkSequence && valueIndex.globalIndex > 0) {
      val v = H5Lib.datasetRead(datasetId = idxDatasetId, dtypeStr = "i64", dimsToRead = Seq(1, 1), offset = Seq(valueIndex.globalIndex - 1, 0))
      val oldSectionGIndex = v.getLong(0)
      if (oldSectionGIndex > sectionGIndex)
        throw new ValueTableException(s"incorrect sequence, adding value for section $sectionGIndex after values of section $oldSectionGIndex to $this")
    }
    H5Lib.datasetClose(valDatasetId)
    H5Lib.datasetClose(idxDatasetId)
    valueIndex
  }

  /** worker method that sets values in an array */
  protected def setArrayValuesInternal(values: NArray, offset: Option[Seq[Long]], indexes: ValueIndexes): Unit = {
    val valDatasetId = H5Lib.datasetOpen(parentSectionTable.sectionGroup(create = false), indexes.valueDatasetName)
    valueSetLocal(valDatasetId, localIndex = indexes.localIndex, value = values, valueOffset = offset.getOrElse(Seq()))
    H5Lib.datasetClose(valDatasetId)
  }

  /**
   * Converts a json value to a long
   */
  protected final def convertJValue_i(value: JValue, shouldThrow: Boolean = true): Option[Long] = {
    value match {
      case JBool(b) =>
        if (b) Some(1: Long) else Some(0: Long)
      case JInt(i) =>
        Some(i.longValue)
      case JDecimal(d) =>
        Some(d.longValue)
      case JDouble(d) =>
        Some(d.longValue)
      case JNothing =>
        None
      case _ =>
        if (shouldThrow)
          throw new InvalidAssignementException(metaInfo, s"invalid value $value when expecting integer")
        None
    }
  }

  /**
   * Converts a json value to a boolean
   */
  protected final def convertJValue_b(value: JValue, shouldThrow: Boolean = true): Option[Boolean] = {
    value match {
      case JBool(b) => Some(b)
      case JInt(i) => Some(i != 0)
      case JNothing =>
        None
      case _ =>
        if (shouldThrow)
          throw new InvalidAssignementException(metaInfo, s"invalid value $value when expecting boolean")
        None
    }
  }

  /**
   * Converts a json value to a floating point double
   */
  protected final def convertJValue_f(value: JValue, shouldThrow: Boolean = true): Option[Double] = {
    value match {
      case JInt(i) =>
        Some(i.doubleValue)
      case JDecimal(d) =>
        Some(d.doubleValue)
      case JDouble(d) =>
        Some(d)
      case JNothing =>
        None
      case _ =>
        if (shouldThrow)
          throw new InvalidAssignementException(metaInfo, s"invalid value $value when expecting a floating point")
        None
    }
  }

  /**
   * Converts a json value to a json dictionary
   */
  protected final def convertJValue_D(value: JValue, shouldThrow: Boolean = true): Option[JObject] = {
    value match {
      case JObject(obj) =>
        Some(JObject(obj))
      case JNothing =>
        None
      case _ =>
        if (shouldThrow)
          throw new InvalidAssignementException(metaInfo, s"invalid value $value when expecting a json dictionary")
        None
    }
  }

  /**
   * Converts a json value to a string
   */
  protected final def convertJValue_C(value: JValue, shouldThrow: Boolean = true): Option[String] = {
    value match {
      case JString(s) =>
        Some(s)
      case JNothing =>
        None
      case _ =>
        if (shouldThrow)
          throw new InvalidAssignementException(metaInfo, s"invalid value $value when expecting a string")
        None
    }
  }

  /**
   * Converts a json value to a Base64 encoded binary value
   */
  protected final def convertJValue_B64(value: JValue, shouldThrow: Boolean = true): Option[String] = {
    value match {
      case JString(s) =>
        Some(s)
      case JArray(arr) =>
        val byteArray = arr.flatMap {
          case JNothing =>
            None
          case JInt(i) =>
            if (i < Byte.MinValue || i > 255)
              throw new InvalidAssignementException(metaInfo, s"value $value out of bounds for Byte")
            Some(i.byteValue)
          case _ =>
            throw new InvalidAssignementException(metaInfo, s"unexpected value ($value) for Byte")
        }.toArray
        Some(Base64.b64EncodeStr(byteArray))
      case JNothing =>
        None
      case _ =>
        if (shouldThrow)
          throw new InvalidAssignementException(metaInfo, s"invalid value $value when expecting a base64 encoded value")
        None
    }
  }

  /**
   * Adds value to the section sectionGIndex and returns its gIndex
   */
  def addValue(value: JValue, sectionGIndex: Long = -1): ValueIndexes = {
    throw new InvalidAssignementException(metaInfo, "addValue not supported")
  }

  /**
   * Adds a floating point value and returns its gIndex
   */
  def addRealValue(value: Double, sectionGIndex: Long = -1): ValueIndexes = {
    throw new InvalidAssignementException(metaInfo, "addRealValue not supported")
  }

  /**
   * Adds a new array of the given size and returns its gIndex
   */
  def addArray(shape: Seq[Long], sectionGIndex: Long = -1): ValueIndexes = {
    throw new InvalidAssignementException(metaInfo, "addArray not supported")
  }

  /**
   * Adds values to the array with the given gIndex
   */
  def setArrayValues(values: NArray, offset: Option[Seq[Long]] = None, indexes: ValueIndexes): Unit = {
    throw new InvalidAssignementException(metaInfo, "setArrayValues not supported")
  }

  /**
   * Adds a new array with the given values and returns its gIndex
   */
  def addArrayValues(values: NArray, sectionGIndex: Long = -1): ValueIndexes = {
    val indexes = addArray(values.getShape().map(_.toLong).toSeq, sectionGIndex)
    setArrayValues(values, offset = None, indexes = indexes)
    indexes
  }

  /**
   * Returns the value indexes for the give gIndex
   */
  def valueIndexes(i: Long): ValueIndexes = {
    val n = if (length == 0 || i >= length) -1 else i
    val indexDatasetId = openIndexDataset(create = false)
    val indexes = resolveGlobalIndex(indexDatasetId = indexDatasetId, globalIndex = n)
    H5Lib.datasetClose(indexDatasetId)
    indexes
  }

}

/**
 * Dummy value table
 */
final class ValueTableH5_dummy(
  val meta_name: String,
  val meta_description: String,
  parentSectionTable: SectionTableH5
) extends ValueTableH5(
  metaInfo = MetaInfoRecord(
    name = meta_name,
    kindStr = "type_document_content",
    description = meta_description,
    superNames = Seq(parentSectionTable.name),
    dtypeStr = Some("i")
  ),
  parentSectionTable = parentSectionTable,
  checkSequence = false
) {
  import ValueTableH5.ValueTableException
  import ValueTableH5.InvalidAssignementException

  /**
   * returns a dataset to store values with the given size
   * locId should be the group of the section containing the concrete meta info metaName
   * If create is false returns -1 if not existing, otherwise creates it if needed.
   */
  override def openValueDataset(valueDimensions: Seq[Long], create: Boolean = false): (String, Long) = {
    if (create)
      throw new Exception(s"cannot create ${metaInfo.name} because it is a dummy value replacement")
    val name = metaInfo.name + "-v"
    val datasetId = -1
    (name -> datasetId)
  }

  override def openValueDatasetWithName(name: String): Long = {
    -1
  }

  /**
   * returns the dataset for the index for the given metaName
   * locId should be the group of the section containing the concrete meta info metaName
   * If create is false returns -1 if not existing, otherwise creates it if needed.
   */
  override def openIndexDataset(create: Boolean = false): Long = {
    val name = metaInfo.name + "-index"
    -1: Long
  }

  /**
   * gets the value at the given local index
   */
  override def valueGetLocal(valuesDatasetId: Long, localIndex: Long, valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq(), value: Option[NArray] = None): NArray = {
    throw new ValueTableException(s"no values can be read from this dummy placeholder for ${metaInfo.name}")
  }

  /**
   * Gets the values starting with localIndex0, and up to localIndex0 + localIndexSize
   * non included, which might be non continuous in the global indexes,
   * returns the values read (if they are less than localIndexSize then
   * the first dimension of the returned array is adjusted accordingly)
   */
  override def valueGetLocalRange(valuesDatasetId: Long, localIndex0: Long, localIndexSize: Long, valueOffset: Seq[Long] = Seq(), valueDims: Seq[Long] = Seq(), value: Option[NArray] = None): NArray = {
    throw new ValueTableException(s"no values can be read from this dummy placeholder for ${metaInfo.name}")
  }

  /**
   * resolves a global index
   */
  override def resolveGlobalIndex(indexDatasetId: Long, globalIndex: Long): ValueIndexes = {
    return ValueIndexes(metaInfo.name + "-v", -1, -1)
  }

  /**
   * Number of elements in the table
   */
  override def length: Long = {
    return 0
  }

  /**
   * i th value of the table
   */
  override def apply(i: Long): ValueH5 = {
    ValueH5(table = this, gIndex = -1)
  }

  override def toString(): String = {
    s"ValueTableH5_dummy(${metaInfo.name})"
  }

  /** adds a value of the given size to the metaName, returns its global and local index */
  override def valueAdd(indexDatasetId: Long, valuesDatasetName: String, valuesDatasetId: Long, dimensions: Seq[Long], superSectionGindex: Long): ValueIndexes = {
    throw new ValueTableException(s"Cannot add value to dummy placeholder for ${metaInfo.name}")
  }

  /**
   * low level method that sets the value at localIndex to the given value
   */
  override def valueSetLocal(valuesDatasetId: Long, localIndex: Long, value: NArray, valueOffset: Seq[Long] = Seq()): Unit = {
    throw new ValueTableException(s"Cannot set values in dummy placeholder for ${metaInfo.name}")
  }

  /** worker method that adds an array value */
  override protected def addArrayValuesInternal(values: NArray, sectionGIndex: Long): ValueIndexes = {
    throw new ValueTableException(s"Cannot add values in dummy placeholder for ${metaInfo.name}")
  }

  /** method that adds an array value of that shape */
  override protected def addArrayInternal(shape: Seq[Long], sectionGIndex: Long): ValueIndexes = {
    throw new ValueTableException(s"Cannot add values in dummy placeholder for ${metaInfo.name}")
  }

  /** worker method that sets values in an array */
  override protected def setArrayValuesInternal(values: NArray, offset: Option[Seq[Long]], indexes: ValueIndexes): Unit = {
    throw new ValueTableException(s"Cannot set values in dummy placeholder for ${metaInfo.name}")
  }

  /**
   * Adds value to the section sectionGIndex and returns its gIndex
   */
  override def addValue(value: JValue, sectionGIndex: Long = -1): ValueIndexes = {
    throw new InvalidAssignementException(metaInfo, "addValue not supported")
  }

  /**
   * Adds a floating point value and returns its gIndex
   */
  override def addRealValue(value: Double, sectionGIndex: Long = -1): ValueIndexes = {
    throw new InvalidAssignementException(metaInfo, s"addArray not supported in dummy value for ${metaInfo.name}")
  }

  /**
   * Adds a new array of the given size and returns its gIndex
   */
  override def addArray(shape: Seq[Long], sectionGIndex: Long = -1): ValueIndexes = {
    throw new InvalidAssignementException(metaInfo, s"addArray not supported in dummy value for ${metaInfo.name}")
  }

  /**
   * Adds a new array with the given values and returns its gIndex
   */
  override def addArrayValues(values: NArray, sectionGIndex: Long = -1): ValueIndexes = {
    throw new InvalidAssignementException(metaInfo, s"addArray not supported in dummy value for ${metaInfo.name}")
  }

  /**
   * Adds values to the array with the given gIndex
   */
  override def setArrayValues(values: NArray, offset: Option[Seq[Long]] = None, indexes: ValueIndexes): Unit = {
    throw new InvalidAssignementException(metaInfo, "setArrayValues not supported")
  }

}
