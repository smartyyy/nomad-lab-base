/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab

object RefCount {
  class RefCountError(msg: String) extends Exception(msg)
}

trait RefCount {
  var refCount: Int = 1

  def retain(): Unit = {
    if (refCount < 1)
      throw new RefCount.RefCountError(s"retain error, refCount = $refCount")
    refCount += 1
  }

  def release(): Unit = {
    if (refCount < 1)
      throw new RefCount.RefCountError(s"release error, refCount = $refCount")
    refCount -= 1
    if (refCount == 0)
      release0()
  }

  /**
   * called when the file/group should be closed
   */
  protected def release0(): Unit
}
