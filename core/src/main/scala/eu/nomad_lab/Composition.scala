/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab
import scala.collection.breakOut

/**
 * operations to handle a chemical composition
 */
object Composition {
  /**
   * transform a formula in dictionary form to a normalized string form
   */
  def dictToFormulaStr(fDict: Map[Int, Int], reduce: Boolean = false, stringBuilder: Option[StringBuilder] = None): String = {
    val formula = stringBuilder match {
      case Some(b) => b
      case None => new StringBuilder
    }
    var nUndefDigits: Int = 0
    var gcd: BigInt = BigInt(1)
    if (reduce) {
      val nrs = fDict.values
      if (!nrs.isEmpty) {
        gcd = BigInt(nrs.head)
        for (n <- nrs.tail)
          gcd = gcd.gcd(BigInt(n))
      }
    }
    for (k <- fDict.keys.toSeq.sorted(math.Ordering[Int].reverse)) {
      val vv = fDict(k) / gcd.toInt
      if (vv > 0)
        formula ++= AtomData.labelForAtomNr(k)
      if (vv > 1)
        formula ++= vv.toString
    }
    return formula.result()
  }

  /**
   * converts a formula to a dictionary symbol -> amount
   */
  def formulaStrToSymDict(formula: String): Map[String, Int] = {
    val elRe = "([A-Z][a-z]*)([0-9]*)".r
    var fDict: Map[String, Int] = Map()
    for (m <- elRe.findAllMatchIn(formula)) {
      val el = m.group(1)
      val amount = m.group(2)
      fDict += (el -> (fDict.getOrElse(el, 0) + (if (amount.isEmpty) 1 else amount.toInt)))
    }
    val noMatch = elRe.split(formula).filter(!_.isEmpty)
    if (!noMatch.isEmpty)
      throw new Exception(s"could not parse formula from ${noMatch.mkString(",")} in $formula")
    fDict
  }

  def formulaStrToDict(formula: String): Map[Int, Int] = {
    val sDict = formulaStrToSymDict(formula)
    val keys = sDict.keys.toSeq
    val atNrs = AtomData.atomNrsFromLabels(keys)
    var fDict: Map[Int, Int] = Map()
    ((for ((symb, nr) <- keys.zip(atNrs)) yield {
      nr -> sDict(symb)
    })(breakOut)): Map[Int, Int]
  }

  /**
   * Returns a formula reweighted toward having percentage compositions
   */
  def formulaDictReweighted(fDict: Map[Int, Int]): Map[Int, Int] = {
    val target = 100
    def reweight(d: Map[Int, Int]): Map[Int, Int] = {
      val totI = d.values.foldLeft(0)(_ + _)
      d.map {
        case (k, v) =>
          k -> (target * v + totI - 1) / totI
      }
    }
    var oldVal: Map[Int, Int] = fDict
    // var m = Map(1-> 200); for (i <- 2 to 100) m += (i -> 1) converges it 11 steps (and is detected in the 12th)
    val maxIter = 15
    for (i <- 0 until maxIter) {
      val newVal = reweight(oldVal)
      if (newVal == oldVal)
        return newVal
      oldVal = newVal
    }
    throw new Exception(s"failed to find reweighting fixed value for $fDict, got $oldVal after $maxIter iterations")
  }

  val repetitionsRe = """\A\s*([0-9]+)\s*(?:\*|&middot;)\s*(.+)\Z""".r
  /**
   * Converts a string to a formula
   */
  def fromString(f: String, nRep: Option[Int] = None): Composition = {
    var nRepI: Int = nRep.getOrElse(1)
    val fStr = f match {
      case repetitionsRe(nRepStr, rest) =>
        nRepI = nRepI * nRepStr.toInt
        rest
      case _ =>
        f
    }
    val fDict = Composition.formulaStrToDict(fStr)
    Composition(fDict, nRepI)
  }

  /**
   * converts an html string to a formula
   */
  def fromHtml(f: String, nRep: Option[Int] = None): Composition = {
    fromString(f.replaceAllLiterally("<sub>", "").replaceAllLiterally("</sub>", ""), nRep)
  }
}

case class Composition(
    formulaDict: Map[Int, Int],
    nRep: Int = 1
) {
  override def toString: String = {
    val s = new StringBuilder
    if (nRep != 1) {
      s ++= nRep.toString
      s ++= "*"
    }
    Composition.dictToFormulaStr(formulaDict, stringBuilder = Some(s))
  }

  def toHtml: String = {
    val s = new StringBuilder
    if (nRep != 1) {
      s ++= nRep.toString
      s ++= "&middot;"
    }
    Composition.dictToFormulaStr(formulaDict, stringBuilder = Some(s))
  }

  /**
   * Reweighted formula so that components sum up to ~100
   *
   * accuracy in the composition is thus ~1%, small traces are rounded up and overrepreseted.
   *
   */
  def to100: Composition = {
    val expandedDict = formulaDict.map { case (k, v) => k -> (nRep * v) }
    Composition(Composition.formulaDictReweighted(expandedDict))
  }

  /**
   * moves the gcd of the formula to the nRep part of the composition
   */
  def toLiftedGcd: Composition = {
    val nrs = formulaDict.values
    var gcd: BigInt = BigInt(1)
    if (!nrs.isEmpty) {
      BigInt(nrs.head)
      for (n <- nrs.tail)
        gcd = gcd.gcd(BigInt(n))
    }
    val gcdI = gcd.toInt
    Composition(formulaDict.map { case (k, v) => k -> v / gcdI }, nRep * gcdI)
  }
}
