/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.parsing_queue

import java.io.{ BufferedInputStream, File, FileInputStream, InputStream }
import java.nio.file.{ Files, Path, Paths, StandardCopyOption }

import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.LocalEnv._
import eu.{ nomad_lab => lab }
import eu.nomad_lab.parsers.ParserCollection
import eu.nomad_lab.parsers.CandidateParser
import eu.nomad_lab.{ JsonSupport, TreeType }
import eu.nomad_lab.QueueMessage.{ CalculationParserRequest, TreeParserRequest }
import eu.nomad_lab.TreeType.TreeType
import eu.nomad_lab.parsing_queue.TreeParser.{ ParserAssociation, TreeParserException }
import org.apache.commons.compress.archivers.{ ArchiveInputStream, ArchiveStreamFactory }
import org.apache.commons.compress.archivers.zip.{ ZipArchiveEntry, ZipFile }
import org.apache.tika.Tika

import scala.annotation.tailrec
import scala.collection.mutable
import scala.util.control.NonFatal

object TreeParser {

  /**
   * An exception that indicates failure during finding a suitable parser.
   *
   * @param message
   * @param msg
   * @param what
   */
  class TreeParserException(
    message: TreeParserRequest,
    msg: String,
    what: Throwable = null
  ) extends Exception(
    s"$msg when parsing ${JsonSupport.writeStr(message)} ",
    what
  )

  /**
   * exception while fetching archive
   */
  class FetchException(
    msg: String
  ) extends Exception(
    msg
  )

  val tika = new Tika()
  val archieveStreamFactory = new ArchiveStreamFactory()

  case class ParserAssociation(
    rootNamespace: String,
    incomingMessage: TreeParserRequest,
    mainFileUri: String,
    filePath: String,
    candidateParsers: Seq[CandidateParser],
    treeType: TreeType.TreeType
  )

  def calculateCalculationGid(mainFileUri: String): String = {
    val sha = lab.CompactSha()
    sha.updateStr(mainFileUri)
    sha.gidStr("C")
  }

  def createCalculationParserRequest(parserAssociation: TreeParser.ParserAssociation): CalculationParserRequest = {
    CalculationParserRequest(
      parserName = parserAssociation.candidateParsers.head.parserName,
      mainFileUri = parserAssociation.mainFileUri,
      relativeFilePath = parserAssociation.filePath,
      treeUri = parserAssociation.incomingMessage.treeUri,
      treeFilePath = parserAssociation.incomingMessage.treeFilePath,
      treeType = parserAssociation.treeType
    )
  }
}

class TreeParser(
    val parserCollection: ParserCollection
) extends StrictLogging {

  val rawDataUriRe = "^nmd://(R[-a-zA-Z0-9_]{28})(/.*)?$".r
  def maybeFetchRawData(treeUri: String): Unit = {
    val settings = lab.LocalEnv.defaultSettings
    settings.remoteHost match {
      case None => ()
      case Some(host) =>
        treeUri match {
          case rawDataUriRe(archiveGid, relPath) =>
            val repl = settings.replacements +
              ("archiveGid" -> archiveGid) +
              ("archiveGidPrefix" -> archiveGid.slice(0, 3))
            val path = Paths.get(lab.LocalEnv.makeReplacements(repl, settings.rawDataArchives))
            if (!Files.exists(path)) {
              val remotePath = lab.LocalEnv.makeReplacements(repl, settings.remoteRawDataArchives)
              Files.createDirectories(path.getParent(), lab.LocalEnv.directoryPermissionsAttributes)
              val tmpPath = Files.createTempFile(
                path.getParent(),
                path.getFileName().toString().slice(0, 6),
                ".tmp",
                lab.LocalEnv.filePermissionsAttributes
              )
              if (scala.sys.process.Process("scp", Seq(host + ":" + remotePath, tmpPath.toString())).! == 0)
                Files.move(tmpPath, path, StandardCopyOption.ATOMIC_MOVE)
              else
                throw new TreeParser.FetchException(s"Could not sucessfully fetch data for $treeUri")
            }
          case _ => ()
        }
    }
  }

  /**
   * Find the parsable files and parsers.
   *
   * @param incomingMessage
   * @return list of messages containing the parsable files and the corresponding parsers.
   */
  def findParserAndPublish(publish: TreeParser.ParserAssociation => Unit, incomingMessage0: TreeParserRequest): Unit = {
    //Read file and get the corresponding parsers
    val incomingMessage = TreeParserRequest(
      treeUri = incomingMessage0.treeUri,
      treeFilePath = if (!incomingMessage0.treeFilePath.startsWith(lab.LocalEnv.defaultSettings.rawDataRoot.toString))
      incomingMessage0.treeFilePath.replace(
      "/raw-data/data/",
      lab.LocalEnv.defaultSettings.rawDataRoot.resolve("data").toString() + "/"
    )
    else
      incomingMessage0.treeFilePath,
      treeType = incomingMessage0.treeType,
      relativeTreeFilePath = incomingMessage0.relativeTreeFilePath,
      maxDepth = incomingMessage0.maxDepth,
      followSymlinks = incomingMessage0.followSymlinks
    )
    val f = new File(incomingMessage.treeFilePath)
    maybeFetchRawData(incomingMessage.treeUri)
    if (f.isDirectory) {
      logger.info(s"mimeType: Directory")
      scanDirectory(publish, incomingMessage)
    } else if (f.isFile) {
      //    To infer the type of the file before hand, in case not passed to the tree parser; At the moment the type has been fixed; Only Zip are handled correctly
      val treeType: TreeType.Value = incomingMessage.treeType match {
        case TreeType.Unknown =>
          val tempfis: InputStream = new BufferedInputStream(new FileInputStream(f))
          val buf = Array.fill[Byte](1024)(0)
          val nRead = parserCollection.tryRead(tempfis, buf, 0)
          val minBuf = buf.dropRight(buf.size - nRead)
          val mimeType: String = TreeParser.tika.detect(minBuf, f.getName)
          logger.info(s"mimeType: $mimeType")
          tempfis.close()
          mimeType match {
            case "application/zip" => TreeType.Zip
            case "application/x-tar" => TreeType.Tar
            case _ => TreeType.Unknown
          }
        case v => v
      }
      logger.info(s"incomingMessage: ${JsonSupport.writePrettyStr(incomingMessage)}, treeType: $treeType")
      treeType match {
        case TreeType.Zip => scanZipFile(publish, incomingMessage)
        case TreeType.Tar =>
          val bis: InputStream = new BufferedInputStream(new FileInputStream(incomingMessage.treeFilePath))
          val ais: ArchiveInputStream = TreeParser.archieveStreamFactory.createArchiveInputStream(bis)
          scanArchivedInputStream(publish, ais, incomingMessage)
          ais.close()
          bis.close()
        case _ => throw new TreeParserException(incomingMessage, s"Type ${incomingMessage.treeType} is not supported by method findParserAndPublish")
      }
    } else if (!f.exists())
      logger.error(f + " doesn't exist")
    else
      logger.error(f + " file type not recognized!")
  }

  /**
   * Returns the main file uri
   */
  def calculateMainFileUri(incomingMessage: TreeParserRequest, filePath: String): String = {
    val partialFilePath = incomingMessage.relativeTreeFilePath match {
      case Some(rPath) =>
        if (filePath.startsWith(rPath))
          filePath.drop(rPath.size)
        else
          filePath
      case None =>
        filePath
    }
    val nUri = if (incomingMessage.treeUri.endsWith("/"))
      incomingMessage.treeUri.dropRight(1)
    else
      incomingMessage.treeUri
    nUri + "/" + partialFilePath.dropWhile(_ == '/')
  }

  /**
   * Scan a zip file for files and find parsable files using scanInputStream
   *
   */
  def scanZipFile(publish: TreeParser.ParserAssociation => Unit, incomingMessage: TreeParserRequest): Unit = {
    incomingMessage.treeType match {
      case TreeType.Zip =>
        val zipFile = new ZipFile(incomingMessage.treeFilePath)
        val entries = zipFile.getEntries()
        val relPath = incomingMessage.relativeTreeFilePath.getOrElse("").dropWhile(_ == '/')
        while (entries.hasMoreElements()) {
          val zipEntry: ZipArchiveEntry = entries.nextElement()
          if (!zipEntry.isDirectory && !zipEntry.isUnixSymlink) {
            //Only check non directory and symlink for now; TODO: Add support to read symlink
            val internalFilePath = Paths.get(zipEntry.getName)
            //            if (internalFilePath.startsWith(Paths.get(incomingMessage.relativeTreeFilePath.getOrElse("")))) { //Path.startsWith emptyPath like "" resolves to false. Hence we use string
            if (internalFilePath.toString.startsWith(relPath)) { //Every string starts with an empty string
              val zIn: InputStream = zipFile.getInputStream(zipEntry)
              scanInputStream(publish, incomingMessage, zIn, internalFilePath.toString, incomingMessage.treeType)
              zIn.close()
            }
          }
        }
      case _ => throw new TreeParserException(incomingMessage, s"Type ${incomingMessage.treeType} is not supported by method scanZipFile")
    }
  }

  def scanInputStream(publish: (ParserAssociation) => Unit, incomingMessage: TreeParserRequest, zIn: InputStream, internalFilePath: String, treeType: TreeType): Unit = {
    val buf = Array.fill[Byte](8 * 1024)(0)
    val nRead = parserCollection.tryRead(zIn, buf, 0)
    val minBuf = buf.dropRight(buf.size - nRead)
    val candidateParsers = parserCollection.scanFile(internalFilePath, minBuf).sorted
    if (candidateParsers.nonEmpty) {
      logger.info(s"${internalFilePath} -> ${candidateParsers.head.parserName}")
      publish(TreeParser.ParserAssociation(lab.LocalEnv.rootNamespace, incomingMessage, calculateMainFileUri(incomingMessage, internalFilePath), internalFilePath, candidateParsers, treeType))
    }
  }

  /**
   * Recursively scan a directory for files and find parsable files using scanInputStream
   *
   * @param publish
   * @param incomingMessage
   */
  def scanDirectory(publish: (ParserAssociation) => Unit, incomingMessage: TreeParserRequest): Unit = {
    val treeType = TreeType.Directory
    val parDir = new File(incomingMessage.treeFilePath)
    val path: Path = parDir.toPath
    var hasErrors: Boolean = false
    if (parDir.exists && parDir.isDirectory) {
      Files.walkFileTree(path, new java.nio.file.SimpleFileVisitor[Path]() {
        import java.nio.file.FileVisitResult
        import java.nio.file.attribute.BasicFileAttributes

        override def visitFile(file: Path, attrs: BasicFileAttributes): FileVisitResult = {

          val internalFilePath = file.toAbsolutePath.subpath(path.toAbsolutePath.getNameCount, file.toAbsolutePath.getNameCount)
          //          logger.info(s"Scanning file: $file; Internal File Path: $internalFilePath, relativeFilePath: ${incomingMessage.relativeTreeFilePath}")
          try {
            //            if (internalFilePath.startsWith(Paths.get(incomingMessage.relativeTreeFilePath.getOrElse("")))) { //Path.startsWith emptyPath like "" resolves to false. Hence we use string
            if (internalFilePath.toString.startsWith(incomingMessage.relativeTreeFilePath.getOrElse(""))) { //Every string starts with an empty string
              val inStream: InputStream = new BufferedInputStream(new FileInputStream(file.toFile))
              scanInputStream(publish, incomingMessage, inStream, internalFilePath.toString, treeType)
              inStream.close()
            }
          } catch {
            case NonFatal(exc) =>
              logger.warn(s"error while scanning $file in $path: $exc")
              hasErrors = true
          }
          return FileVisitResult.CONTINUE
        }
      })
    } else if (parDir.isFile()) {
      try {
        val inStream: InputStream = new FileInputStream(parDir)
        scanInputStream(publish, incomingMessage, inStream, parDir.toString, treeType)
        inStream.close()
      } catch {
        case NonFatal(exc) =>
          logger.warn(s"error while deleting $path: $exc")
          hasErrors = true
      }
    }
  }

  /**
   * Scan a ArchiveInputStream and if possible, find the most appropriate parser for each file in the archive
   *
   * @param ais
   * @return
   */
  @tailrec final def scanArchivedInputStream(publish: TreeParser.ParserAssociation => Unit, ais: ArchiveInputStream, incomingMessage: TreeParserRequest): Unit = {
    Option(ais.getNextEntry) match {
      case Some(ae) =>
        if (ae.isDirectory) {
          logger.info(ae.getName + ". It is a directory. Skipping it. Its children will be handled automatically in the recursion")
        } else {
          val internalFilePath = Paths.get(ae.getName)
          //            if (internalFilePath.startsWith(Paths.get(incomingMessage.relativeTreeFilePath.getOrElse("")))) { //Path.startsWith emptyPath like "" resolves to false. Hence we use string
          if (internalFilePath.toString.startsWith(incomingMessage.relativeTreeFilePath.getOrElse(""))) { //Every string starts with an empty string
            val buf = Array.fill[Byte](8 * 1024)(0)
            val nRead = parserCollection.tryRead(ais, buf, 0)
            val minBuf = buf.dropRight(buf.size - nRead)
            val candidateParsers = parserCollection.scanFile(ae.getName, minBuf).sorted
            //          logger.info("Candidate Parsers:" + candidateParsers)
            if (candidateParsers.nonEmpty)
              publish(TreeParser.ParserAssociation(lab.LocalEnv.rootNamespace, incomingMessage, calculateMainFileUri(incomingMessage, internalFilePath.toString), internalFilePath.toString, candidateParsers, TreeType.Tar))
          }
        }
        scanArchivedInputStream(publish, ais, incomingMessage)
      case _ =>
        ()
    }
  }
}

