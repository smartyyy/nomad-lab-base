/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab

/**
 * Fundamental Physical Constants --- Complete Listing
 *
 * All constants defined by CODATA 2010 with small editing to satify scala:
 * - remove "." from abbreviations like "mag. mom."
 * - replace "/" with "_per_"
 * - use "_" as spacer
 * - replace "-" with "_"
 * - replace "{220} lattice spacing of silicon" with Lattice_spacing_of_220_silicon
 * - uppercase first letter
 * - switch from "_" to camel case:
 *   query-replace-regexp "_\([a-zA-Z]\)" "\,(upcase \1)"
 *   query-replace-regexp "_\([0-9]\)" "\1"             (warning! need to skip some things like 273_15)
 * - align-regexp " +="
 *
 * From:  http://physics.nist.gov/constants
 */
sealed trait Codata2010T {
  //  Quantity                                                            Value                 Uncertainty           Unit
  //-----------------------------------------------------------------------------------------------------------------------------
  final val LatticeSpacingOf220Silicon = 192.0155714e-12 // 0.0000032e-12         m
  final val AlphaParticleElectronMassRatio = 7294.2995361 // 0.0000029
  final val AlphaParticleMass = 6.64465675e-27 // 0.00000029e-27        kg
  final val AlphaParticleMassEnergyEquivalent = 5.97191967e-10 // 0.00000026e-10        J
  final val AlphaParticleMassEnergyEquivalentInMeV = 3727.379240 // 0.000082              MeV
  final val AlphaParticleMassInU = 4.001506179125 // 0.000000000062        u
  final val AlphaParticleMolarMass = 4.001506179125e-3 // 0.000000000062e-3     kg mol^-1
  final val AlphaParticleProtonMassRatio = 3.97259968933 // 0.00000000036
  final val AngstromStar = 1.00001495e-10 // 0.00000090e-10        m
  final val AtomicMassConstant = 1.660538921e-27 // 0.000000073e-27       kg
  final val AtomicMassConstantEnergyEquivalent = 1.492417954e-10 // 0.000000066e-10       J
  final val AtomicMassConstantEnergyEquivalentInMeV = 931.494061 // 0.000021              MeV
  final val AtomicMassUnitElectronVoltRelationship = 931.494061e6 // 0.000021e6            eV
  final val AtomicMassUnitHartreeRelationship = 3.4231776845e7 // 0.0000000024e7        EH
  final val AtomicMassUnitHertzRelationship = 2.2523427168e23 // 0.0000000016e23       Hz
  final val AtomicMassUnitInverseMeterRelationship = 7.5130066042e14 // 0.0000000053e14       m^-1
  final val AtomicMassUnitJouleRelationship = 1.492417954e-10 // 0.000000066e-10       J
  final val AtomicMassUnitKelvinRelationship = 1.08095408e13 // 0.00000098e13         K
  final val AtomicMassUnitKilogramRelationship = 1.660538921e-27 // 0.000000073e-27       kg
  final val AtomicUnitOf1stHyperpolarizability = 3.206361449e-53 // 0.000000071e-53       C^3 m^3 J^-2
  final val AtomicUnitOf2ndHyperpolarizability = 6.23538054e-65 // 0.00000028e-65        C^4 m^4 J^-3
  final val AtomicUnitOfAction = 1.054571726e-34 // 0.000000047e-34       J s
  final val AtomicUnitOfCharge = 1.602176565e-19 // 0.000000035e-19       C
  final val AtomicUnitOfChargeDensity = 1.081202338e12 // 0.000000024e12        C m^-3
  final val AtomicUnitOfCurrent = 6.62361795e-3 // 0.00000015e-3         A
  final val AtomicUnitOfElectricDipoleMom = 8.47835326e-30 // 0.00000019e-30        C m
  final val AtomicUnitOfElectricField = 5.14220652e11 // 0.00000011e11         V m^-1
  final val AtomicUnitOfElectricFieldGradient = 9.71736200e21 // 0.00000021e21         V m^-2
  final val AtomicUnitOfElectricPolarizability = 1.6487772754e-41 // 0.0000000016e-41      C^2 m^2 J^-1
  final val AtomicUnitOfElectricPotential = 27.21138505 // 0.00000060            V
  final val AtomicUnitOfElectricQuadrupoleMom = 4.486551331e-40 // 0.000000099e-40       C m^2
  final val AtomicUnitOfEnergy = 4.35974434e-18 // 0.00000019e-18        J
  final val AtomicUnitOfForce = 8.23872278e-8 // 0.00000036e-8         N
  final val AtomicUnitOfLength = 0.52917721092e-10 // 0.00000000017e-10     m
  final val AtomicUnitOfMagDipoleMom = 1.854801936e-23 // 0.000000041e-23       J T^-1
  final val AtomicUnitOfMagFluxDensity = 2.350517464e5 // 0.000000052e5         T
  final val AtomicUnitOfMagnetizability = 7.891036607e-29 // 0.000000013e-29       J T^-2
  final val AtomicUnitOfMass = 9.10938291e-31 // 0.00000040e-31        kg
  final val AtomicUnitOfMomUm = 1.992851740e-24 // 0.000000088e-24       kg m s^-1
  final val AtomicUnitOfPermittivity = 1.112650056e-10 // (exact)               F m^-1
  final val AtomicUnitOfTime = 2.418884326502e-17 // 0.000000000012e-17    s
  final val AtomicUnitOfVelocity = 2.18769126379e6 // 0.00000000071e6       m s^-1
  final val AvogadroConstant = 6.02214129e23 // 0.00000027e23         mol^-1
  final val BohrMagneton = 927.400968e-26 // 0.000020e-26          J T^-1
  final val BohrMagnetonInEVPerT = 5.7883818066e-5 // 0.0000000038e-5       eV T^-1
  final val BohrMagnetonInHzPerT = 13.99624555e9 // 0.00000031e9          Hz T^-1
  final val BohrMagnetonInInverseMetersPerTesla = 46.6864498 // 0.0000010             m^-1 T^-1
  final val BohrMagnetonInKPerT = 0.67171388 // 0.00000061            K T^-1
  final val BohrRadius = 0.52917721092e-10 // 0.00000000017e-10     m
  final val BoltzmannConstant = 1.3806488e-23 // 0.0000013e-23         J K^-1
  final val BoltzmannConstantInEVPerK = 8.6173324e-5 // 0.0000078e-5          eV K^-1
  final val BoltzmannConstantInHzPerK = 2.0836618e10 // 0.0000019e10          Hz K^-1
  final val BoltzmannConstantInInverseMetersPerKelvin = 69.503476 // 0.000063              m^-1 K^-1
  final val CharacteristicImpedanceOfVacuum = 376.730313461 // (exact)               ohm
  final val ClassicalElectronRadius = 2.8179403267e-15 // 0.0000000027e-15      m
  final val ComptonWavelength = 2.4263102389e-12 // 0.0000000016e-12      m
  final val ComptonWavelengthOver2Pi = 386.15926800e-15 // 0.00000025e-15        m
  final val ConductanceQuantum = 7.7480917346e-5 // 0.0000000025e-5       S
  final val ConventionalValueOfJosephsonConstant = 483597.9e9 // (exact)               Hz V^-1
  final val ConventionalValueOfVonKlitzingConstant = 25812.807 // (exact)               ohm
  final val CuXUnit = 1.00207697e-13 // 0.00000028e-13        m
  final val DeuteronElectronMagMomRatio = -4.664345537e-4 // 0.000000039e-4
  final val DeuteronElectronMassRatio = 3670.4829652 // 0.0000015
  final val DeuteronGFactor = 0.8574382308 // 0.0000000072
  final val DeuteronMagMom = 0.433073489e-26 // 0.000000010e-26       J T^-1
  final val DeuteronMagMomToBohrMagnetonRatio = 0.4669754556e-3 // 0.0000000039e-3
  final val DeuteronMagMomToNuclearMagnetonRatio = 0.8574382308 // 0.0000000072
  final val DeuteronMass = 3.34358348e-27 // 0.00000015e-27        kg
  final val DeuteronMassEnergyEquivalent = 3.00506297e-10 // 0.00000013e-10        J
  final val DeuteronMassEnergyEquivalentInMeV = 1875.612859 // 0.000041              MeV
  final val DeuteronMassInU = 2.013553212712 // 0.000000000077        u
  final val DeuteronMolarMass = 2.013553212712e-3 // 0.000000000077e-3     kg mol^-1
  final val DeuteronNeutronMagMomRatio = -0.44820652 // 0.00000011
  final val DeuteronProtonMagMomRatio = 0.3070122070 // 0.0000000024
  final val DeuteronProtonMassRatio = 1.99900750097 // 0.00000000018
  final val DeuteronRmsChargeRadius = 2.1424e-15 // 0.0021e-15            m
  final val ElectricConstant = 8.854187817e-12 // (exact)               F m^-1
  final val ElectronChargeToMassQuotient = -1.758820088e11 // 0.000000039e11        C kg^-1
  final val ElectronDeuteronMagMomRatio = -2143.923498 // 0.000018
  final val ElectronDeuteronMassRatio = 2.7244371095e-4 // 0.0000000011e-4
  final val ElectronGFactor = -2.00231930436153 // 0.00000000000053
  final val ElectronGyromagRatio = 1.760859708e11 // 0.000000039e11        s^-1 T^-1
  final val ElectronGyromagRatioOver2Pi = 28024.95266 // 0.00062               MHz T^-1
  final val ElectronHelionMassRatio = 1.8195430761e-4 // 0.0000000017e-4
  final val ElectronMagMom = -928.476430e-26 // 0.000021e-26          J T^-1
  final val ElectronMagMomAnomaly = 1.15965218076e-3 // 0.00000000027e-3
  final val ElectronMagMomToBohrMagnetonRatio = -1.00115965218076 // 0.00000000000027
  final val ElectronMagMomToNuclearMagnetonRatio = -1838.28197090 // 0.00000075
  final val ElectronMass = 9.10938291e-31 // 0.00000040e-31        kg
  final val ElectronMassEnergyEquivalent = 8.18710506e-14 // 0.00000036e-14        J
  final val ElectronMassEnergyEquivalentInMeV = 0.510998928 // 0.000000011           MeV
  final val ElectronMassInU = 5.4857990946e-4 // 0.0000000022e-4       u
  final val ElectronMolarMass = 5.4857990946e-7 // 0.0000000022e-7       kg mol^-1
  final val ElectronMuonMagMomRatio = 206.7669896 // 0.0000052
  final val ElectronMuonMassRatio = 4.83633166e-3 // 0.00000012e-3
  final val ElectronNeutronMagMomRatio = 960.92050 // 0.00023
  final val ElectronNeutronMassRatio = 5.4386734461e-4 // 0.0000000032e-4
  final val ElectronProtonMagMomRatio = -658.2106848 // 0.0000054
  final val ElectronProtonMassRatio = 5.4461702178e-4 // 0.0000000022e-4
  final val ElectronTauMassRatio = 2.87592e-4 // 0.00026e-4
  final val ElectronToAlphaParticleMassRatio = 1.37093355578e-4 // 0.00000000055e-4
  final val ElectronToShieldedHelionMagMomRatio = 864.058257 // 0.000010
  final val ElectronToShieldedProtonMagMomRatio = -658.2275971 // 0.0000072
  final val ElectronTritonMassRatio = 1.8192000653e-4 // 0.0000000017e-4
  final val ElectronVolt = 1.602176565e-19 // 0.000000035e-19       J
  final val ElectronVoltAtomicMassUnitRelationship = 1.073544150e-9 // 0.000000024e-9        u
  final val ElectronVoltHartreeRelationship = 3.674932379e-2 // 0.000000081e-2        EH
  final val ElectronVoltHertzRelationship = 2.417989348e14 // 0.000000053e14        Hz
  final val ElectronVoltInverseMeterRelationship = 8.06554429e5 // 0.00000018e5          m^-1
  final val ElectronVoltJouleRelationship = 1.602176565e-19 // 0.000000035e-19       J
  final val ElectronVoltKelvinRelationship = 1.1604519e4 // 0.0000011e4           K
  final val ElectronVoltKilogramRelationship = 1.782661845e-36 // 0.000000039e-36       kg
  final val ElementaryCharge = 1.602176565e-19 // 0.000000035e-19       C
  final val ElementaryChargeOverH = 2.417989348e14 // 0.000000053e14        A J^-1
  final val FaradayConstant = 96485.3365 // 0.0021                C mol^-1
  final val FaradayConstantForConventionalElectricCurrent = 96485.3321 // 0.0043                C_90 mol^-1
  final val FermiCouplingConstant = 1.166364e-5 // 0.000005e-5           GeV^-2
  final val FineStructureConstant = 7.2973525698e-3 // 0.0000000024e-3
  final val FirstRadiationConstant = 3.74177153e-16 // 0.00000017e-16        W m^2
  final val FirstRadiationConstantForSpectralRadiance = 1.191042869e-16 // 0.000000053e-16       W m^2 sr^-1
  final val HartreeAtomicMassUnitRelationship = 2.9212623246e-8 // 0.0000000021e-8       u
  final val HartreeElectronVoltRelationship = 27.21138505 // 0.00000060            eV
  final val HartreeEnergy = 4.35974434e-18 // 0.00000019e-18        J
  final val HartreeEnergyInEV = 27.21138505 // 0.00000060            eV
  final val HartreeHertzRelationship = 6.579683920729e15 // 0.000000000033e15     Hz
  final val HartreeInverseMeterRelationship = 2.194746313708e7 // 0.000000000011e7      m^-1
  final val HartreeJouleRelationship = 4.35974434e-18 // 0.00000019e-18        J
  final val HartreeKelvinRelationship = 3.1577504e5 // 0.0000029e5           K
  final val HartreeKilogramRelationship = 4.85086979e-35 // 0.00000021e-35        kg
  final val HelionElectronMassRatio = 5495.8852754 // 0.0000050
  final val HelionGFactor = -4.255250613 // 0.000000050
  final val HelionMagMom = -1.074617486e-26 // 0.000000027e-26       J T^-1
  final val HelionMagMomToBohrMagnetonRatio = -1.158740958e-3 // 0.000000014e-3
  final val HelionMagMomToNuclearMagnetonRatio = -2.127625306 // 0.000000025
  final val HelionMass = 5.00641234e-27 // 0.00000022e-27        kg
  final val HelionMassEnergyEquivalent = 4.49953902e-10 // 0.00000020e-10        J
  final val HelionMassEnergyEquivalentInMeV = 2808.391482 // 0.000062              MeV
  final val HelionMassInU = 3.0149322468 // 0.0000000025          u
  final val HelionMolarMass = 3.0149322468e-3 // 0.0000000025e-3       kg mol^-1
  final val HelionProtonMassRatio = 2.9931526707 // 0.0000000025
  final val HertzAtomicMassUnitRelationship = 4.4398216689e-24 // 0.0000000031e-24      u
  final val HertzElectronVoltRelationship = 4.135667516e-15 // 0.000000091e-15       eV
  final val HertzHartreeRelationship = 1.5198298460045e-16 // 0.0000000000076e-16   EH
  final val HertzInverseMeterRelationship = 3.335640951e-9 // (exact)               m^-1
  final val HertzJouleRelationship = 6.62606957e-34 // 0.00000029e-34        J
  final val HertzKelvinRelationship = 4.7992434e-11 // 0.0000044e-11         K
  final val HertzKilogramRelationship = 7.37249668e-51 // 0.00000033e-51        kg
  final val InverseFineStructureConstant = 137.035999074 // 0.000000044
  final val InverseMeterAtomicMassUnitRelationship = 1.33102505120e-15 // 0.00000000094e-15     u
  final val InverseMeterElectronVoltRelationship = 1.239841930e-6 // 0.000000027e-6        eV
  final val InverseMeterHartreeRelationship = 4.556335252755e-8 // 0.000000000023e-8     EH
  final val InverseMeterHertzRelationship = 299792458 // (exact)               Hz
  final val InverseMeterJouleRelationship = 1.986445684e-25 // 0.000000088e-25       J
  final val InverseMeterKelvinRelationship = 1.4387770e-2 // 0.0000013e-2          K
  final val InverseMeterKilogramRelationship = 2.210218902e-42 // 0.000000098e-42       kg
  final val InverseOfConductanceQuantum = 12906.4037217 // 0.0000042             ohm
  final val JosephsonConstant = 483597.870e9 // 0.011e9               HzV^-1
  final val JouleAtomicMassUnitRelationship = 6.70053585e9 // 0.00000030e9          u
  final val JouleElectronVoltRelationship = 6.24150934e18 // 0.00000014e18         eV
  final val JouleHartreeRelationship = 2.29371248e17 // 0.00000010e17         EH
  final val JouleHertzRelationship = 1.509190311e33 // 0.000000067e33        Hz
  final val JouleInverseMeterRelationship = 5.03411701e24 // 0.00000022e24         m^-1
  final val JouleKelvinRelationship = 7.2429716e22 // 0.0000066e22          K
  final val JouleKilogramRelationship = 1.112650056e-17 // (exact)               kg
  final val KelvinAtomicMassUnitRelationship = 9.2510868e-14 // 0.0000084e-14         u
  final val KelvinElectronVoltRelationship = 8.6173324e-5 // 0.0000078e-5          eV
  final val KelvinHartreeRelationship = 3.1668114e-6 // 0.0000029e-6          EH
  final val KelvinHertzRelationship = 2.0836618e10 // 0.0000019e10          Hz
  final val KelvinInverseMeterRelationship = 69.503476 // 0.000063              m^-1
  final val KelvinJouleRelationship = 1.3806488e-23 // 0.0000013e-23         J
  final val KelvinKilogramRelationship = 1.5361790e-40 // 0.0000014e-40         kg
  final val KilogramAtomicMassUnitRelationship = 6.02214129e26 // 0.00000027e26         u
  final val KilogramElectronVoltRelationship = 5.60958885e35 // 0.00000012e35         eV
  final val KilogramHartreeRelationship = 2.061485968e34 // 0.000000091e34        EH
  final val KilogramHertzRelationship = 1.356392608e50 // 0.000000060e50        Hz
  final val KilogramInverseMeterRelationship = 4.52443873e41 // 0.00000020e41         m^-1
  final val KilogramJouleRelationship = 8.987551787e16 // (exact)               J
  final val KilogramKelvinRelationship = 6.5096582e39 // 0.0000059e39          K
  final val LatticeParameterOfSilicon = 543.1020504e-12 // 0.0000089e-12         m
  final val LoschmidtConstant273_15K100KPa = 2.6516462e25 // 0.0000024e25          m^-3
  final val LoschmidtConstant273_15K101_325KPa = 2.6867805e25 // 0.0000024e25          m^-3
  final val MagConstant = 12.566370614e-7 // (exact)               N A^-2
  final val MagFluxQuantum = 2.067833758e-15 // 0.000000046e-15       Wb
  final val MolarGasConstant = 8.3144621 // 0.0000075             J mol^-1 K^-1
  final val MolarMassConstant = 1e-3 // (exact)               kg mol^-1
  final val MolarMassOfCarbon12 = 12e-3 // (exact)               kg mol^-1
  final val MolarPlanckConstant = 3.9903127176e-10 // 0.0000000028e-10      J s mol^-1
  final val MolarPlanckConstantTimesC = 0.119626565779 // 0.000000000084        J m mol^-1
  final val MolarVolumeOfIdealGas273_15K100KPa = 22.710953e-3 // 0.000021e-3           m^3 mol^-1
  final val MolarVolumeOfIdealGas273_15K101_325KPa = 22.413968e-3 // 0.000020e-3           m^3 mol^-1
  final val MolarVolumeOfSilicon = 12.05883301e-6 // 0.00000080e-6         m^3 mol^-1
  final val MoXUnit = 1.00209952e-13 // 0.00000053e-13        m
  final val MuonComptonWavelength = 11.73444103e-15 // 0.00000030e-15        m
  final val MuonComptonWavelengthOver2Pi = 1.867594294e-15 // 0.000000047e-15       m
  final val MuonElectronMassRatio = 206.7682843 // 0.0000052
  final val MuonGFactor = -2.0023318418 // 0.0000000013
  final val MuonMagMom = -4.49044807e-26 // 0.00000015e-26        J T^-1
  final val MuonMagMomAnomaly = 1.16592091e-3 // 0.00000063e-3
  final val MuonMagMomToBohrMagnetonRatio = -4.84197044e-3 // 0.00000012e-3
  final val MuonMagMomToNuclearMagnetonRatio = -8.89059697 // 0.00000022
  final val MuonMass = 1.883531475e-28 // 0.000000096e-28       kg
  final val MuonMassEnergyEquivalent = 1.692833667e-11 // 0.000000086e-11       J
  final val MuonMassEnergyEquivalentInMeV = 105.6583715 // 0.0000035             MeV
  final val MuonMassInU = 0.1134289267 // 0.0000000029          u
  final val MuonMolarMass = 0.1134289267e-3 // 0.0000000029e-3       kg mol^-1
  final val MuonNeutronMassRatio = 0.1124545177 // 0.0000000028
  final val MuonProtonMagMomRatio = -3.183345107 // 0.000000084
  final val MuonProtonMassRatio = 0.1126095272 // 0.0000000028
  final val MuonTauMassRatio = 5.94649e-2 // 0.00054e-2
  final val NaturalUnitOfAction = 1.054571726e-34 // 0.000000047e-34       J s
  final val NaturalUnitOfActionInEVS = 6.58211928e-16 // 0.00000015e-16        eV s
  final val NaturalUnitOfEnergy = 8.18710506e-14 // 0.00000036e-14        J
  final val NaturalUnitOfEnergyInMeV = 0.510998928 // 0.000000011           MeV
  final val NaturalUnitOfLength = 386.15926800e-15 // 0.00000025e-15        m
  final val NaturalUnitOfMass = 9.10938291e-31 // 0.00000040e-31        kg
  final val NaturalUnitOfMomUm = 2.73092429e-22 // 0.00000012e-22        kg m s^-1
  final val NaturalUnitOfMomUmInMeVPerC = 0.510998928 // 0.000000011           MeV/c
  final val NaturalUnitOfTime = 1.28808866833e-21 // 0.00000000083e-21     s
  final val NaturalUnitOfVelocity = 299792458 // (exact)               m s^-1
  final val NeutronComptonWavelength = 1.3195909068e-15 // 0.0000000011e-15      m
  final val NeutronComptonWavelengthOver2Pi = 0.21001941568e-15 // 0.00000000017e-15     m
  final val NeutronElectronMagMomRatio = 1.04066882e-3 // 0.00000025e-3
  final val NeutronElectronMassRatio = 1838.6836605 // 0.0000011
  final val NeutronGFactor = -3.82608545 // 0.00000090
  final val NeutronGyromagRatio = 1.83247179e8 // 0.00000043e8          s^-1 T^-1
  final val NeutronGyromagRatioOver2Pi = 29.1646943 // 0.0000069             MHz T^-1
  final val NeutronMagMom = -0.96623647e-26 // 0.00000023e-26        J T^-1
  final val NeutronMagMomToBohrMagnetonRatio = -1.04187563e-3 // 0.00000025e-3
  final val NeutronMagMomToNuclearMagnetonRatio = -1.91304272 // 0.00000045
  final val NeutronMass = 1.674927351e-27 // 0.000000074e-27       kg
  final val NeutronMassEnergyEquivalent = 1.505349631e-10 // 0.000000066e-10       J
  final val NeutronMassEnergyEquivalentInMeV = 939.565379 // 0.000021              MeV
  final val NeutronMassInU = 1.00866491600 // 0.00000000043         u
  final val NeutronMolarMass = 1.00866491600e-3 // 0.00000000043e-3      kg mol^-1
  final val NeutronMuonMassRatio = 8.89248400 // 0.00000022
  final val NeutronProtonMagMomRatio = -0.68497934 // 0.00000016
  final val NeutronProtonMassDifference = 2.30557392e-30 // 0.00000076e-30
  final val NeutronProtonMassDifferenceEnergyEquivalent = 2.07214650e-13 // 0.00000068e-13
  final val NeutronProtonMassDifferenceEnergyEquivalentInMeV = 1.29333217 // 0.00000042
  final val NeutronProtonMassDifferenceInU = 0.00138844919 // 0.00000000045
  final val NeutronProtonMassRatio = 1.00137841917 // 0.00000000045
  final val NeutronTauMassRatio = 0.528790 // 0.000048
  final val NeutronToShieldedProtonMagMomRatio = -0.68499694 // 0.00000016
  final val NewtonianConstantOfGravitation = 6.67384e-11 // 0.00080e-11           m^3 kg^-1 s^-2
  final val NewtonianConstantOfGravitationOverHBarC = 6.70837e-39 // 0.00080e-39           (GeV/c^2)^-2
  final val NuclearMagneton = 5.05078353e-27 // 0.00000011e-27        J T^-1
  final val NuclearMagnetonInEVPerT = 3.1524512605e-8 // 0.0000000022e-8       eV T^-1
  final val NuclearMagnetonInInverseMetersPerTesla = 2.542623527e-2 // 0.000000056e-2        m^-1 T^-1
  final val NuclearMagnetonInKPerT = 3.6582682e-4 // 0.0000033e-4          K T^-1
  final val NuclearMagnetonInMHzPerT = 7.62259357 // 0.00000017            MHz T^-1
  final val PlanckConstant = 6.62606957e-34 // 0.00000029e-34        J s
  final val PlanckConstantInEVS = 4.135667516e-15 // 0.000000091e-15       eV s
  final val PlanckConstantOver2Pi = 1.054571726e-34 // 0.000000047e-34       J s
  final val PlanckConstantOver2PiInEVS = 6.58211928e-16 // 0.00000015e-16        eV s
  final val PlanckConstantOver2PiTimesCInMeVFm = 197.3269718 // 0.0000044             MeV fm
  final val PlanckLength = 1.616199e-35 // 0.000097e-35          m
  final val PlanckMass = 2.17651e-8 // 0.00013e-8            kg
  final val PlanckMassEnergyEquivalentInGeV = 1.220932e19 // 0.000073e19           GeV
  final val PlanckTemperature = 1.416833e32 // 0.000085e32           K
  final val PlanckTime = 5.39106e-44 // 0.00032e-44           s
  final val ProtonChargeToMassQuotient = 9.57883358e7 // 0.00000021e7          C kg^-1
  final val ProtonComptonWavelength = 1.32140985623e-15 // 0.00000000094e-15     m
  final val ProtonComptonWavelengthOver2Pi = 0.21030891047e-15 // 0.00000000015e-15     m
  final val ProtonElectronMassRatio = 1836.15267245 // 0.00000075
  final val ProtonGFactor = 5.585694713 // 0.000000046
  final val ProtonGyromagRatio = 2.675222005e8 // 0.000000063e8         s^-1 T^-1
  final val ProtonGyromagRatioOver2Pi = 42.5774806 // 0.0000010             MHz T^-1
  final val ProtonMagMom = 1.410606743e-26 // 0.000000033e-26       J T^-1
  final val ProtonMagMomToBohrMagnetonRatio = 1.521032210e-3 // 0.000000012e-3
  final val ProtonMagMomToNuclearMagnetonRatio = 2.792847356 // 0.000000023
  final val ProtonMagShieldingCorrection = 25.694e-6 // 0.014e-6
  final val ProtonMass = 1.672621777e-27 // 0.000000074e-27       kg
  final val ProtonMassEnergyEquivalent = 1.503277484e-10 // 0.000000066e-10       J
  final val ProtonMassEnergyEquivalentInMeV = 938.272046 // 0.000021              MeV
  final val ProtonMassInU = 1.007276466812 // 0.000000000090        u
  final val ProtonMolarMass = 1.007276466812e-3 // 0.000000000090e-3     kg mol^-1
  final val ProtonMuonMassRatio = 8.88024331 // 0.00000022
  final val ProtonNeutronMagMomRatio = -1.45989806 // 0.00000034
  final val ProtonNeutronMassRatio = 0.99862347826 // 0.00000000045
  final val ProtonRmsChargeRadius = 0.8775e-15 // 0.0051e-15            m
  final val ProtonTauMassRatio = 0.528063 // 0.000048
  final val QuantumOfCirculation = 3.6369475520e-4 // 0.0000000024e-4       m^2 s^-1
  final val QuantumOfCirculationTimes2 = 7.2738951040e-4 // 0.0000000047e-4       m^2 s^-1
  final val RydbergConstant = 10973731.568539 // 0.000055              m^-1
  final val RydbergConstantTimesCInHz = 3.289841960364e15 // 0.000000000017e15     Hz
  final val RydbergConstantTimesHcInEV = 13.60569253 // 0.00000030            eV
  final val RydbergConstantTimesHcInJ = 2.179872171e-18 // 0.000000096e-18       J
  final val SackurTetrodeConstant1K100KPa = -1.1517078 // 0.0000023
  final val SackurTetrodeConstant1K101_325KPa = -1.1648708 // 0.0000023
  final val SecondRadiationConstant = 1.4387770e-2 // 0.0000013e-2          m K
  final val ShieldedHelionGyromagRatio = 2.037894659e8 // 0.000000051e8         s^-1 T^-1
  final val ShieldedHelionGyromagRatioOver2Pi = 32.43410084 // 0.00000081            MHz T^-1
  final val ShieldedHelionMagMom = -1.074553044e-26 // 0.000000027e-26       J T^-1
  final val ShieldedHelionMagMomToBohrMagnetonRatio = -1.158671471e-3 // 0.000000014e-3
  final val ShieldedHelionMagMomToNuclearMagnetonRatio = -2.127497718 // 0.000000025
  final val ShieldedHelionToProtonMagMomRatio = -0.761766558 // 0.000000011
  final val ShieldedHelionToShieldedProtonMagMomRatio = -0.7617861313 // 0.0000000033
  final val ShieldedProtonGyromagRatio = 2.675153268e8 // 0.000000066e8         s^-1 T^-1
  final val ShieldedProtonGyromagRatioOver2Pi = 42.5763866 // 0.0000010             MHz T^-1
  final val ShieldedProtonMagMom = 1.410570499e-26 // 0.000000035e-26       J T^-1
  final val ShieldedProtonMagMomToBohrMagnetonRatio = 1.520993128e-3 // 0.000000017e-3
  final val ShieldedProtonMagMomToNuclearMagnetonRatio = 2.792775598 // 0.000000030
  final val SpeedOfLightInVacuum = 299792458 // (exact)               m s^-1
  final val StandardAccelerationOfGravity = 9.80665 // (exact)               m s^-2
  final val StandardAtmosphere = 101325 // (exact)               Pa
  final val StandardStatePressure = 100000 // (exact)               Pa
  final val StefanBoltzmannConstant = 5.670373e-8 // 0.000021e-8           W m^-2 K^-4
  final val TauComptonWavelength = 0.697787e-15 // 0.000063e-15          m
  final val TauComptonWavelengthOver2Pi = 0.111056e-15 // 0.000010e-15          m
  final val TauElectronMassRatio = 3477.15 // 0.31
  final val TauMass = 3.16747e-27 // 0.00029e-27           kg
  final val TauMassEnergyEquivalent = 2.84678e-10 // 0.00026e-10           J
  final val TauMassEnergyEquivalentInMeV = 1776.82 // 0.16                  MeV
  final val TauMassInU = 1.90749 // 0.00017               u
  final val TauMolarMass = 1.90749e-3 // 0.00017e-3            kg mol^-1
  final val TauMuonMassRatio = 16.8167 // 0.0015
  final val TauNeutronMassRatio = 1.89111 // 0.00017
  final val TauProtonMassRatio = 1.89372 // 0.00017
  final val ThomsonCrossSection = 0.6652458734e-28 // 0.0000000013e-28      m^2
  final val TritonElectronMassRatio = 5496.9215267 // 0.0000050
  final val TritonGFactor = 5.957924896 // 0.000000076
  final val TritonMagMom = 1.504609447e-26 // 0.000000038e-26       J T^-1
  final val TritonMagMomToBohrMagnetonRatio = 1.622393657e-3 // 0.000000021e-3
  final val TritonMagMomToNuclearMagnetonRatio = 2.978962448 // 0.000000038
  final val TritonMass = 5.00735630e-27 // 0.00000022e-27        kg
  final val TritonMassEnergyEquivalent = 4.50038741e-10 // 0.00000020e-10        J
  final val TritonMassEnergyEquivalentInMeV = 2808.921005 // 0.000062              MeV
  final val TritonMassInU = 3.0155007134 // 0.0000000025          u
  final val TritonMolarMass = 3.0155007134e-3 // 0.0000000025e-3       kg mol^-1
  final val TritonProtonMassRatio = 2.9937170308 // 0.0000000025
  final val UnifiedAtomicMassUnit = 1.660538921e-27 // 0.000000073e-27       kg
  final val VonKlitzingConstant = 25812.8074434 // 0.0000084             ohm
  final val WeakMixingAngle = 0.2223 // 0.0021
  final val WienFrequencyDisplacementLawConstant = 5.8789254e10 // 0.0000053e10           Hz K^-1
  final val WienWavelengthDisplacementLawConstant = 2.8977721e-3 // 0.0000026e-3           m K
}

object Codata2010 extends Codata2010T {}

/**
 * Fundamental Physical Constants --- Complete Listing
 *
 * convenience alias for the latest version of the latest Codata values, currently Codata 2010
 */
object Codata extends Codata2010T {}
