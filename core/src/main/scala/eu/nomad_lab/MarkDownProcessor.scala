/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab

import com.typesafe.scalalogging.StrictLogging

import scala.collection.mutable
import com.vladsch.flexmark.html.HtmlRenderer
import com.vladsch.flexmark.parser.Parser
import com.vladsch.flexmark.util.options.MutableDataSet

//object MarkDownProcessor extends StrictLogging {
object MarkDownProcessor {

  object MarkDownProcessorState extends Enumeration {
    type MarkDownProcessorState = Value
    val BetweenWords, InWord, InNonWord, InDollarMath, InSquareMath, InSquareBracket, InParentesisMath, BackSlash, InTag, InTagString = Value
  }
  val options = new MutableDataSet();
  // options.set(Parser.EXTENSIONS, Arrays.asList(TablesExtension.create(), StrikethroughExtension.create()))
  val parser = Parser.builder(options).build();
  val renderer = HtmlRenderer.builder(options).build();

  def flexMark(x: String): String = {
    val doc = parser.parse(x)
    renderer.render(doc)
  }

  def processMarkDown(text: String, keys: Set[String], keyLinkBuilder: String => String, markDownProcessor: String => String = null): String = {
    import MarkDownProcessorState._
    val escapedText = new mutable.StringBuilder()
    var state = MarkDownProcessorState.BetweenWords
    var i: Int = 0
    var lastEmit: Int = 0
    var wordStart: Int = -1
    var mathStart: Int = -1
    var placeholderNr: Int = 0
    var inBacktick: Boolean = false
    val placeholderValues = mutable.Map[String, String]()

    def addMathPlaceholder(): Unit = {
      escapedText ++= text.substring(lastEmit, mathStart)
      var placeholder = s"(XXX-$placeholderNr-XXX)"
      placeholderNr += 1
      while (text.contains(placeholder)) {
        var placeholder = s"(XXX-$placeholderNr-XXX)"
        placeholderNr += 1
      }
      placeholderValues += (placeholder ->
        text.substring(mathStart, i).replaceAll("&", "&amp;").replaceAll(">", "&gt;").replaceAll("<", "&lt;"))
      escapedText ++= placeholder
      lastEmit = i
      mathStart = -1
    }
    while (i < text.length()) {
      var cAtt = text.charAt(i)
      i += 1
      state match {
        case BetweenWords =>
          cAtt match {
            case '\\' =>
              state = BackSlash
            case '[' =>
              state = InSquareBracket
            case '`' =>
              inBacktick = !inBacktick
            case '$' =>
              state = InDollarMath
              mathStart = i - 1
            case '<' =>
              state = InTag
            case '>' =>
              escapedText ++= text.substring(lastEmit, i - 1)
              lastEmit = i
              escapedText ++= "&gt;"
            case _ if Character.isLetter(cAtt) =>
              wordStart = i - 1
              state = InWord
            case _ if (Character.isDigit(cAtt) || cAtt == '_') =>
              state = InNonWord
            case _ => ()
          }
        case InWord =>
          while ((Character.isLetterOrDigit(cAtt) || cAtt == '_' || cAtt == '-') && i < text.length()) {
            cAtt = text.charAt(i)
            i += 1
          }
          if (Character.isLetterOrDigit(cAtt) || cAtt == '_' || cAtt == '-')
            i += 1
          val word = text.substring(wordStart, i - 1)
          if (keys(word)) {
            escapedText ++= text.substring(lastEmit, wordStart)
            escapedText ++= keyLinkBuilder(word)
            lastEmit = i - 1
            wordStart = -1
          }
          state = BetweenWords
        case InNonWord =>
          while ((Character.isLetterOrDigit(cAtt) || cAtt == '_') && i < text.length()) {
            cAtt = text.charAt(i)
            i += 1
          }
          state = BetweenWords
        case InDollarMath =>
          var escape: Boolean = false
          var atEnd: Boolean = false
          i -= 1
          while (!atEnd && i < text.length()) {
            cAtt = text.charAt(i)
            i += 1
            if (cAtt == '\\') {
              escape = !escape
            } else if (cAtt == '$') {
              if (!escape)
                atEnd = true
              escape = false
            } else {
              escape = false
            }
          }
          addMathPlaceholder()
          state = BetweenWords
        case InSquareMath =>
          var escape: Boolean = false
          var atEnd: Boolean = false
          i -= 1
          while (!atEnd && i < text.length()) {
            cAtt = text.charAt(i)
            i += 1
            if (cAtt == '\\') {
              escape = !escape
            } else if (cAtt == ']') {
              if (escape)
                atEnd = true
              escape = false
            } else {
              escape = false
            }
          }
          addMathPlaceholder()
          state = BetweenWords
        case InParentesisMath =>
          var escape: Boolean = false
          var atEnd: Boolean = false
          i -= 1
          while (!atEnd && i < text.length()) {
            cAtt = text.charAt(i)
            i += 1
            if (cAtt == '\\') {
              escape = !escape
            } else if (cAtt == ')') {
              if (escape)
                atEnd = true
              escape = false
            } else {
              escape = false
            }
          }
          addMathPlaceholder()
          state = BetweenWords
        case BackSlash =>
          cAtt match {
            case '$' =>
              escapedText ++= text.substring(lastEmit, i - 1)
              escapedText ++= "$\\$$"
              lastEmit = i
            case '(' =>
              if (inBacktick) {
                mathStart = i - 2
                state = InParentesisMath
              }
            case '[' =>
              if (inBacktick) {
                mathStart = i - 2
                state = InSquareMath
              }
            case _ =>
              state = BetweenWords
          }
        case InTag =>
          if (cAtt == '>')
            state = BetweenWords
          else if (cAtt == '"')
            state = InTagString
        case InSquareBracket =>
          if (cAtt == ']')
            state = BetweenWords
        case InTagString =>
          if (cAtt == '\\')
            i += 1
          else if (cAtt == '"')
            state = InTag
      }
    }
    escapedText ++= text.substring(lastEmit)
    val processor: String => String = if (markDownProcessor == null)
      flexMark
    else
      markDownProcessor
    val textWithPlaceholders = escapedText.toString()
    //    logger.debug(textWithPlaceholders)
    var processed: String = processor(textWithPlaceholders)
    for ((placeholder, value) <- placeholderValues) {
      processed = processed.replace(placeholder, value)
    }
    processed
  }

}
