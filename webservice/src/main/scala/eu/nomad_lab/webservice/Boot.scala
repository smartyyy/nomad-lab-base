/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.webservice

import akka.actor.{ ActorSystem, Props }
import akka.io.IO
import spray.can.Http
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import com.typesafe.config.{ Config, ConfigFactory }
import eu.nomad_lab.LocalEnv

object Boot extends App {
  val conf: Config = LocalEnv.defaultConfig

  LocalEnv.setup(conf)

  // we need an ActorSystem to host our application in
  implicit val system = ActorSystem("on-spray-can", conf)

  // create and start our service actor
  val service = system.actorOf(Props[NomadMetaInfoActor], "nomad-frontend")

  implicit val timeout = Timeout(8.seconds)
  // start a new HTTP server on the requested port with our service actor as the handler
  IO(Http) ? Http.Bind(
    service,
    interface = conf.getString("nomad_lab.webservice.interface"),
    port = conf.getInt("nomad_lab.webservice.port")
  )
}
