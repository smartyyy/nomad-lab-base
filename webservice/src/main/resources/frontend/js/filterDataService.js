(function(){
'use strict';
    
var filter = angular.module('metaDataApp.metaFilter', ['metaDataApp.dataModule']);
filter.factory('filterService',['dataService', filterFunction]);
function filterFunction(dataService){
    var filter = {};
    filter.Types= [{displayName:'Any Abstract Type', name:'', parent: '' }];
    dataService.asyncMetaDataList("public").then(function(metaData) {
    var metaItems = {};
    metaItems = angular.fromJson(metaData);
    for (var i = 0; i < metaItems.metaInfos.length; i++) {
        var meta = metaItems.metaInfos[i];
        if(meta.kindStr.indexOf('type_abstract_document_content') > -1){
            if(meta.name.startsWith('energy')){
                filter.Types.push(
                {'displayName': meta.name,
                'name':meta.name,
                 parent: 'Energy' })
            }
            else if(meta.name.startsWith('settings')){
            filter.Types.push(
            {'displayName': meta.name,
            'name':meta.name,
             parent: 'Settings' })
            }
            else if(meta.allparents.indexOf('accessory_info') > -1){
                filter.Types.push(
                 {'displayName': meta.name,
                 'name':meta.name,
                  parent: 'Accessory Info' })
            }
        }
    }
//    filter.Types.sort(function(a, b) {
//        return a.parent.localeCompare(b.parent);
//    });
    filter.Types.sort(function(a, b) {
        return a.name.localeCompare(b.name);
    });
    });
//    filter.Types= [ {displayName:'Any Abstract Type', name:'', parent: '' },
//        {displayName: 'energy',name:'energy', parent: 'Energy' },
//        {displayName: 'energy_component_scf_iteration',name:'energy_component_scf_iteration', parent: 'Energy' },
//        {displayName: 'energy_component_per_atom_scf_iteration',name:'energy_component_per_atom_scf_iteration',parent: 'Energy' },
//        {displayName: 'energy_component',name:'energy_component',parent: 'Energy' },
//        {displayName: 'energy_component_per_atom',name:'energy_component_per_atom',parent: 'Energy' },
//        {displayName: 'energy_total_potential',name:'energy_total_potential',parent: 'Energy' },
//        //                                  # parent: 'Settings' },
//        {displayName: 'settings_GW',name:'settings_GW', parent: 'Settings' },
//        {displayName: 'settings_MCSCF',name:'settings_MCSCF', parent: 'Settings' },
//        {displayName: 'settings_XC',name:'settings_XC', parent: 'Settings' },
//        {displayName: 'settings_XC_functional',name:'settings_XC_functional', parent: 'Settings' },
//        {displayName: 'settings_XC_functional_scf',name:'settings_XC_functional_scf', parent: 'Settings' },
//        {displayName: 'settings_XC_scf',name:'settings_XC_scf', parent: 'Settings' },
//        {displayName: 'settings_atom_kind',name:'settings_atom_kind', parent: 'Settings' },
//        {displayName: 'settings_coupled_cluster',name:'settings_coupled_cluster', parent: 'Settings' },
//        {displayName: 'settings_method',name:'settings_method', parent: 'Settings' },
//        {displayName: 'settings_moller_plesset_perturbation_theory',name:'settings_moller_plesset_perturbation_theory', parent: 'Settings' },
//        {displayName: 'settings_multi_reference',name:'settings_multi_reference', parent: 'Settings' },
//        {displayName: 'settings_post_hartree_fock',name:'settings_post_hartree_fock', parent: 'Settings' },
//        {displayName: 'settings_relativity_treatment',name:'settings_relativity_treatment', parent: 'Settings' },
//        {displayName: 'settings_self_interaction_correction',name:'settings_self_interaction_correction', parent: 'Settings' },
//        {displayName: 'settings_simulation',name:'settings_simulation', parent: 'Settings' },
//        {displayName: 'settings_single_configuration_method',name:'settings_single_configuration_method', parent: 'Settings' },
//        {displayName: 'settings_van_der_Waals_treatment',name:'settings_van_der_Waals_treatment', parent: 'Settings' },
//        //                                  # parent: 'Accessory Info' },
//        {displayName: 'accessory_info',name:'accessory_info', parent: 'Accessory Info' },
//        {displayName: 'parallelization_info',name:'parallelization_info', parent: 'Accessory Info' },
//        {displayName: 'program_info',name:'program_info', parent: 'Accessory Info' },
//        {displayName: 'time_info_run',name:'time_info_run', parent: 'Accessory Info' },
//        {displayName: 'time_info_evaluation',name:'time_info_evaluation', parent: 'Accessory Info' },
//        {displayName: 'time_info_scf_iteration',name:'time_info_scf_iteration', parent: 'Accessory Info' }  ];

    filter.Sections= [  { displayName: 'Any Section' , section:'', parent: 'ROOT' },
        { displayName: 'section_run' , section:'section_run', parent: 'ROOT' },
        { displayName: 'section_method' , section:'section_method', parent: 'section_run' },
        { displayName: 'section_system' , section:'section_system', parent: 'section_run' },
        { displayName: 'section_single_configuration_calculation' , section:'section_single_configuration_calculation', parent: 'section_system_description' },
        { displayName: 'section_scf_iteration' , section:'section_scf_iteration', parent: 'section_run' },
        { displayName: 'section_frame_sequence' , section:'section_frame_sequence', parent: 'section_run' }  ];

    //Another copy of MetaInfoTypes exists in the data Services; Whenever this function is updated the data Service should also be updated
    filter.MetaInfoTypes = [ {displayName: 'Any Meta Info Type', name: ''},
//            {displayName: 'type_document', name: 'type_document'},
        {displayName: 'Dimension', name: 'type_dimension'},
        {displayName: 'Section', name: 'type_section'},
        {displayName: 'Concrete Value', name: 'type_document_content'},
        {displayName: 'Abstract Type', name: 'type_abstract_document_content'}];
    filter.selectedSection= filter.Sections[0] ;
    filter.selectedType=filter.Types[0];
    filter.selectedMetaInfoType=filter.MetaInfoTypes[0];
    filter.search='';
    filter.hideDerived = false;
    var myService = {
        filter:filter,
        setSelected: function(selectedValues){
            filter.selectedSection= selectedValues.selectedSection;
            filter.selectedType=selectedValues.selectedType;
            filter.selectedMetaInfoType=selectedValues.selectedMetaInfoType;
            return filter;
        }
    };
  return myService;
};

})();
