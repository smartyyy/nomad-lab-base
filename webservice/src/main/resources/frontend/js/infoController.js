(function() {
    'use strict';
    // to set a module
    angular.module('metaDataApp.infoController', ['ngSanitize', 'ui.select','queryFilter','metaDataApp.metaFilter','metaDataApp.dataModule','mathJaxDirective','cytoscapeFactory'])
    .controller('infoController', ['$scope', '$http','$location', 'ancestorGraph','filterService','dataService',infoController]);

    function infoController($scope, $http, $location, ancestorGraph,filterService,dataService){
      dataService.asyncVersionList();
      dataService.asyncMetaDataList("public");
      dataService.asyncMetaInfoAncestorChildrenGraph("public","section_single_configuration_calculation");
    };
})();
