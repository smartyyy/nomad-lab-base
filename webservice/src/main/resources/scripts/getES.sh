host=nomad-flink-01
fields=$1
query=$(echo $2| sed 's/%/ /g')


curl -s -XGET "http://$host:9210/nomadarchiverepo/_search?pretty" -H 'Content-Type: application/json' -d"
{
  \"query\": {
   \"query_string\" : {
     \"fields\" : [\"$fields\"],
       \"query\" : \"$query\"
       }
     },
  \"_source\":[\"atom_species\",
               \"program_name\",
               \"system_reweighted_composition\",
               \"system_composition\",
               \"atom_symbols\",
               \"section_repository_info.repository_xc_treatment\",
               \"section_repository_info.repository_system_type\",
               \"section_repository_info.repository_basis_set_type\",
               \"section_repository_info.repository_crystal_system\",
               \"section_repository_info.repository_spacegroup_nr\"
              ],
  \"size\":100
} 
"
