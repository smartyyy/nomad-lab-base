/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.archive

import javax.ws.rs.{ GET, Path, QueryParam }

import akka.http.scaladsl.server.{ Directives, Route }
import eu.nomad_lab.archive.queries.StandardSearch.Formats
import eu.nomad_lab.archive.queries._
import eu.nomad_lab.elasticsearch.ESManager
import eu.nomad_lab.query.QueryExpression
import eu.nomad_lab.webservice_base.PagingFilter
import io.swagger.annotations._

import scala.concurrent.{ ExecutionContext, Future }

object NqlApi {
  def apply(es: ESManager)(implicit exec: ExecutionContext): NqlApi = {
    val autoComplete = new AutoCompletion(es)
    val crossFilter = new CrossFilterStatistics(es)
    val histograms = new Histograms(es)
    val standardSearch = new StandardSearch(es)
    val stats = new SummaryStatistics(es)
    apply(es, autoComplete, crossFilter, histograms, standardSearch, stats)
  }

  protected def apply(es: ESManager, autoComplete: AutoCompletion, crossFilter: CrossFilterStatistics,
    histograms: Histograms, standardSearch: StandardSearch,
    stats: SummaryStatistics)(implicit exec: ExecutionContext): NqlApi = {
    new NqlApi(es, autoComplete, crossFilter, histograms, standardSearch, stats)
  }
}

@Api(
  value = "NOMAD Archive ElasticSearch requests with NOMAD Query Language (NQL)",
  produces = "vnd.api+json"
)
@Path("/nql-api")
@ApiResponses(Array(
  new ApiResponse(code = 200, message = "operation successful"),
  new ApiResponse(code = 400, message = "invalid parameters for request"),
  new ApiResponse(code = 405, message = "invalid HTTP method"),
  new ApiResponse(code = 500, message = "Internal server error")
))
class NqlApi private (
    private val es: ESManager,
    private val autoComplete: AutoCompletion,
    private val crossFilter: CrossFilterStatistics,
    private val histograms: Histograms,
    private val standardSearch: StandardSearch,
    private val stats: SummaryStatistics
)(implicit exec: ExecutionContext) extends Directives {
  import es.connector
  import eu.nomad_lab.webservice_base.ResponseFormatter.resolveQueryResult

  final val queryFieldDesc = "NQL expression for filtering calculations"

  implicit val queryTransform: Option[QueryExpression => QueryExpression] = Some(RequestTransformer.analyseNQLRequest)

  val routes: Route = pathPrefix("nql-api") {
    concat(
      path("search") {
        get {
          parameters('query.?, 'num_results.as[Int] ? 100, 'sort_field ? "calculation_gid",
            'before.?, 'after.?, 'source_fields ? "", 'format ? "nested") {
            (rawQuery, nResults, sortField, before, after, sourceFields, formatString) =>
              calculationQuery(rawQuery, nResults, sortField, before, after, sourceFields,
                formatString)
          }
        }
      },
      path("search_grouped") {
        get {
          parameters('group_by, 'query.?, 'num_results.as[Int] ? 10, 'num_groups.as[Int] ? 10,
            'source_fields ? "", 'format ? "nested") {
              (groupByMetaName, rawQuery, nResults, nGroups, sourceFields, formatString) =>
                calculationQueryGrouped(groupByMetaName, rawQuery, nResults, nGroups,
                  sourceFields, formatString)
            }
        }
      },
      path("autocompletion") {
        get {
          parameters('fields, 'query.?, 'num_groups.as[Int] ? 10) {
            (fields, rawQuery, nGroups) =>
              uniqueValues(fields, rawQuery, nGroups)
          }
        }
      },
      path("statistics") {
        get {
          parameters('query.?, 'num_buckets.as[Int].?) {
            (rawQuery, nBuckets) =>
              queryStatistics(rawQuery, nBuckets)
          }
        }
      },
      path("interactive_stats") {
        get {
          parameters('query.?, 'num_buckets.as[Int].?) {
            (rawQuery, nBuckets) =>
              crossFilteringStatistics(rawQuery, nBuckets)
          }
        }
      },
      path("simple_stats") {
        get {
          parameters('query.?) {
            (rawQuery) =>
              simpleStatistics(rawQuery)
          }
        }
      }
    )
  }

  @GET @Path("/search")
  @ApiOperation(value = "Search individual calculations in the Nomad Archive")
  def calculationQuery(
    @ApiParam(value = queryFieldDesc)@QueryParam("query") rawQuery: Option[String],
    @ApiParam(value = "number of calculations to return", defaultValue = "100")@QueryParam("num_results") nResults: Int,
    @ApiParam(value = "field used for sorting results", defaultValue = "calculation_gid")@QueryParam("sort_field") sortField: String,
    @ApiParam(value = "descending sort, only return calculations before the given value")@QueryParam("before") before: Option[String],
    @ApiParam(value = "ascending sort, only return calculations after the given value")@QueryParam("after") after: Option[String],
    @ApiParam(value = "comma-separated list of requested meta-data fields")@QueryParam("source_fields") sourceFields: String,
    @ApiParam(value = "output format of the meta-data", allowableValues = "flat, nested", defaultValue = "nested")@QueryParam("format") formatString: String
  ): Route = onComplete {
    Future {
      val pagingFilter = PagingFilter(sortField, after, before)
      val paging = pagingFilter.getPagingFilter
      val tmp = Seq(rawQuery, paging).flatten.mkString(" AND ")
      val query = connector.transformToQuery(if (tmp.nonEmpty) Some(tmp) else None)
      val fields = sourceFields.split(",").toSeq.filter(_.length > 0)
      val format = Formats.getFormat(formatString)
      standardSearch.findMatchingCalculations(query, pagingFilter, nResults, fields, format)
    }.flatMap(identity)
  }(resolveQueryResult(_))

  @GET @Path("/search_grouped")
  @ApiOperation(value = "Search individual calculations in the Nomad Archive and return a diversified sample")
  def calculationQueryGrouped(
    @ApiParam(value = "meta-data field used for grouping", required = true)@QueryParam("group_by") groupByMetaName: String,
    @ApiParam(value = queryFieldDesc)@QueryParam("query") rawQuery: Option[String],
    @ApiParam(value = "number of calculations to return for each group", defaultValue = "10")@QueryParam("num_results") nResults: Int,
    @ApiParam(value = "number of distinct calculation groups to return", defaultValue = "10")@QueryParam("num_groups") nGroups: Int,
    @ApiParam(value = "comma-separated list of requested meta-data fields")@QueryParam("source_fields") sourceFields: String,
    @ApiParam(value = "output format of the meta-data", allowableValues = "flat, nested", defaultValue = "nested")@QueryParam("format") formatString: String
  ): Route = onComplete {
    val query = Future { connector.transformToQuery(rawQuery) }
    val fields = sourceFields.split(",").toSeq.filter(_.length > 0)
    val format = Formats.getFormat(formatString)
    val result = query.map(standardSearch.findMatchingGroupedCalculations(
      _, groupByMetaName, nGroups, nResults, fields, format
    ))
    result.flatMap(identity)
  }(resolveQueryResult(_))

  @GET @Path("/autocompletion")
  @ApiOperation(
    value = "Return a collection of unique values for selected meta-data fields",
    notes = "Can be used for auto-completion on fields with limited cardinality."
  )
  def uniqueValues(
    @ApiParam(value = "meta-data fields to report unique values for", required = true)@QueryParam("fields") fields: String,
    @ApiParam(value = queryFieldDesc)@QueryParam("query") rawQuery: Option[String],
    @ApiParam(value = "number of unique values for each field", defaultValue = "10")@QueryParam("num_groups") nGroups: Int
  ): Route = onComplete {
    val query = Future { connector.transformToQuery(rawQuery) }
    val aggFields = fields.split(",").toSeq.filter(_.length > 0)
    val result = query.map(autoComplete.findTopValues(_, aggFields, nGroups))
    result.flatMap(identity)
  }(resolveQueryResult(_))

  @GET @Path("/statistics")
  @ApiOperation(value = "Provide statistics about the calculations matching the given query.")
  def queryStatistics(
    @ApiParam(value = queryFieldDesc)@QueryParam("query") rawQuery: Option[String],
    @ApiParam(value = "number of values each field, uses field-specific defaults if not set", `type` = "integer")@QueryParam("num_buckets") nBuckets: Option[Int]
  ): Route = onComplete {
    val query = Future { connector.transformToQuery(rawQuery) }
    val result = query.map(histograms.getStatistics(_, nBuckets = nBuckets))
    result.flatMap(identity)
  }(resolveQueryResult(_))

  @GET @Path("/interactive_stats")
  @ApiOperation(
    value = "Provide pre-aggregated stats for crossfiltering visualization of the calculations matching the given query.",
    notes = "A calculation may be mapped to the multiple values for one field, this pre-aggregation thus leads to potential double-counting."
  )
  def crossFilteringStatistics(
    @ApiParam(value = queryFieldDesc)@QueryParam("query") rawQuery: Option[String],
    @ApiParam(value = "number of values each field, uses field-specific defaults if not set", `type` = "integer")@QueryParam("num_buckets") nBuckets: Option[Int]
  ): Route = onComplete {
    val query = Future { connector.transformToQuery(rawQuery) }
    val result = query.map(crossFilter.getStatistics(_, nBuckets = nBuckets))
    result.flatMap(identity)
  }(resolveQueryResult(_))

  @GET @Path("/simple_stats")
  @ApiOperation(
    value = "Provide basic statistics for the calculations matched by the given filter.",
    notes = "The provided information includes the number of unique geometries, single-" +
    "configuration calculations and program runs as well as statistics about the system types."
  )
  def simpleStatistics(
    @ApiParam(value = queryFieldDesc)@QueryParam("query") rawQuery: Option[String]
  ): Route = onComplete {
    val query = Future { connector.transformToQuery(rawQuery) }
    val result = query.map(stats.getSimpleStatistics(_, rawQuery.getOrElse("")))
    result.flatMap(identity)
  }(resolveQueryResult(_, loneValue = true))

}
