/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.archive.queries

import com.sksamuel.elastic4s.http.ElasticDsl._
import com.sksamuel.elastic4s.searches.SearchDefinition
import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.JsonSupport.formats
import eu.nomad_lab.elasticsearch.{ ApiCallException, ConnectorElasticSearch, ESManager }
import eu.nomad_lab.jsonapi.BaseValue
import eu.nomad_lab.webservice_base.ResponseFormatter.ResponseData
import org.json4s.Extraction.decompose
import org.{ json4s => jn }

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object CrossFilterStatistics extends StrictLogging {

  case class DataSet(
      keys: Map[String, String],
      numUniqueGeometries: Long,
      numSinglePointConfigs: Long,
      numCalculations: Long
  ) extends BaseValue {
    override def typeStr = "CrossFilterDataSet"

    override def idStr: String = {
      keys.keys.toList.sorted.map(keys(_)).mkString("|")
    }

    override def attributes: Map[String, jn.JValue] = {
      keys.map(x => x._1 -> jn.JString(x._2)) ++ Map(
        "number_unique_geometries" -> jn.JLong(numUniqueGeometries),
        "number_single_point_configs" -> jn.JLong(numSinglePointConfigs),
        "number_calculations" -> jn.JLong(numCalculations)
      )
    }
  }

  val defaultKeys = Seq(
    "system_type",
    "crystal_system",
    "electronic_structure_method",
    "program_name"
  )
}

class CrossFilterStatistics(private val es: ESManager) {
  import CrossFilterStatistics._

  /**
   * Classify the query results along multiple dimensions to obtain statistics for interactive
   * usage like crossfiltering libraries. For each tuple of keys, an entry with the count of
   * documents (calculations), single_point_calculations and unique_geometries will be returned.
   *
   * @note The Nomad metaData does not enforce a one-to-one mapping between calculations and
   * values for most fields. As a consequence this pre-aggregation of entries can lead to
   * multi-counting of results. The entire Nomad Archive data however exceeds the capabilities of
   * available crossfiltering libraries, thus this pre-aggregation is necessary to make it
   * computationally feasible.
   * Also the uniqueness of geometries can only verified within each bucket. The sum of unique
   * geometries from different buckets is therefore an upper bound to the true number of unique
   * geometries.
   *
   * @param representation the base query to operate on (for ElasticSearch)
   * @param keys the meta-data fields to sort among (1-4 field are allowed)
   * @param nBuckets the number of buckets to generate for each field
   * @return list of results and query meta data
   */
  def getStatistics(representation: SearchDefinition, keys: Seq[String] = defaultKeys,
    nBuckets: Option[Int] = None): Future[ResponseData[DataSet]] = {
    import es.connector.metaNameToField

    if (keys.isEmpty || keys.size > 4) {
      throw ApiCallException(s"must use 1-4 selection keys, got ${keys.size}")
    }
    es.connector.validateFields(keys, ConnectorElasticSearch.FieldUsage.Aggregate)

    val query = representation.size(0).aggregations {
      val numBuckets = nBuckets.getOrElse(50).min(100)
      val subAggs = keys.map { metaName =>
        val fieldName = metaNameToField(metaName)
        if (fieldName.isEmpty)
          throw new UnsupportedOperationException(s"unknown meta-data '$metaName'")
        termsAgg(metaName, fieldName).size(numBuckets).missing("<no value>")
      }
      val innermost = subAggs.last.subAggregations(
        cardinalityAgg("unique_geometries", metaNameToField("configuration_raw_gid")),
        sumAgg("single_configurations", metaNameToField("number_of_single_configurations"))
      )
      subAggs.init.foldRight(innermost)((x, y) => x.subAggregations(y))
    }
    es.client.execute(query).map { results =>
      def traverseAggTree(nodeName: String, dataTree: Map[String, Any],
        collectedKeys: Map[String, String]): Seq[DataSet] = {
        val subBuckets = dataTree("buckets").asInstanceOf[Seq[Map[String, Any]]]
        subBuckets.flatMap { bucket =>
          val hasSubAggs = bucket.keys.exists(x => keys.contains(x))
          val bucketKeys = collectedKeys + (nodeName -> bucket("key").toString)
          if (hasSubAggs) {
            bucket.filter(x => keys.contains(x._1)).flatMap { x =>
              traverseAggTree(x._1, x._2.asInstanceOf[Map[String, Any]], bucketKeys)
            }
          } else {
            val nGeos = bucket("unique_geometries").asInstanceOf[Map[String, Any]]("value").
              asInstanceOf[Int].toLong
            val nConfigs = bucket("single_configurations").asInstanceOf[Map[String, Any]]("value").
              asInstanceOf[Double].toLong
            val nCalculations = bucket("doc_count").asInstanceOf[Int].toLong
            Seq(DataSet(bucketKeys, nGeos, nConfigs, nCalculations))
          }
        }
      }
      val aggData = results.aggregationsAsMap(keys.head).asInstanceOf[Map[String, Any]]
      val data = traverseAggTree(keys.head, aggData, Map())
      val aggs = {
        if (keys.contains("system_type")) {
          val groups = data.groupBy(_.keys("system_type"))
          Map("systemtype_statistics" ->
            decompose(
              Map(
                "total_unique_geometries" ->
                  groups.map(x => x._1 -> x._2.foldLeft(0L)(_ + _.numUniqueGeometries)),
                "total_single_point_configs" ->
                  groups.map(x => x._1 -> x._2.foldLeft(0L)(_ + _.numSinglePointConfigs)),
                "total_calculations" ->
                  groups.map(x => x._1 -> x._2.foldLeft(0L)(_ + _.numCalculations))
              )
            ))
        } else {
          Map[String, jn.JValue]()
        }
      }
      val meta0 = Map[String, jn.JValue](
        "total_hits" -> decompose(results.totalHits),
        "total_unique_geometries" -> decompose(data.map(_.numUniqueGeometries).sum),
        "total_single_configurations" -> decompose(data.map(_.numSinglePointConfigs).sum),
        "is_timed_out" -> decompose(results.isTimedOut),
        "is_terminated_early" -> decompose(results.isTerminatedEarly)
      ) ++ aggs
      ResponseData(data, meta0)
    }
  }
}
