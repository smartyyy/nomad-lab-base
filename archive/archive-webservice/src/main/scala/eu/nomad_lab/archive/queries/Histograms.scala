/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.archive.queries

import com.sksamuel.elastic4s.http.ElasticDsl._
import com.sksamuel.elastic4s.http.search.Bucket
import com.sksamuel.elastic4s.searches.SearchDefinition
import eu.nomad_lab.JsonSupport.formats
import eu.nomad_lab.elasticsearch.{ ConnectorElasticSearch, ESManager }
import eu.nomad_lab.jsonapi.BaseValue
import eu.nomad_lab.webservice_base.ResponseFormatter.ResponseData
import org.json4s.Extraction.decompose
import org.{ json4s => jn }

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

object Histograms {

  case class HistogramData(
    identifier: String,
    index_field: String,
    nBuckets: Int,
    displayName: String,
    displayPrefix: Boolean = true,
    keep: (Bucket) => Boolean = { _ => true },
    sortLessThan: (Bucket, Bucket) => Boolean = { (x, y) => x.docCount > y.docCount }
  )

  case class StatisticsEntry(label: String, calculations: Int)

  case class FieldStatistics(
      id: String,
      displayName: String,
      data: Seq[StatisticsEntry]
  ) extends BaseValue {
    override def typeStr = "FieldStatistics"

    override def idStr: String = id

    override def attributes: Map[String, jn.JValue] = {
      Map(
        "display_name" -> jn.JString(displayName),
        "values" -> jn.JArray(data.map(decompose(_)).toList)
      )
    }
  }

  val standardAggregations: Seq[HistogramData] = Seq(
    HistogramData("program_name", "program_name", 10, "Program Names"),
    HistogramData("calc_method", "electronic_structure_method", 10, "Computational Methods"),
    HistogramData("crystal_system", "crystal_system", 10, "Crystal System Types",
      sortLessThan = {
      _.key.toLowerCase < _.key.toLowerCase
    }, displayPrefix = false),
    HistogramData("system_type", "system_type", 10, "System Types",
      sortLessThan = {
      _.key.toLowerCase < _.key.toLowerCase
    }, displayPrefix = false),
    HistogramData("uploader", "calculation_uploader_name", 10, "Uploaders"),
    HistogramData("composition", "system_composition", 20, "System Compositions"),
    HistogramData("reweighted_composition", "system_reweighted_composition", 20,
      "Reweighted System Compositions"),
    HistogramData("spacegroup_nr", "space_group_number", 10,
      "Space Groups"),
    HistogramData("atom_species", "atom_species", 150, "Atomic Elements", keep = {
      _.key.toInt > 0
    },
      sortLessThan = {
      _.key.toInt < _.key.toInt
    })
  ).sortBy(_.identifier)
}

class Histograms(private val es: ESManager) {
  import Histograms._

  /**
   * Get statistics about the query result to guide further refinement searches.
   *
   * @param representation the base query to operate on (for ElasticSearch)
   * @param aggregations the list of aggregations to perform
   * @param nBuckets the number of buckets to generate for each field (except atom_species which
   *                 always returns all elements)
   * @return list of results and query meta data
   */
  def getStatistics(
    representation: SearchDefinition,
    aggregations: Seq[HistogramData] = standardAggregations,
    nBuckets: Option[Int] = None
  ): Future[ResponseData[FieldStatistics]] = {
    import es.connector.metaNameToField

    es.connector.validateFields(
      aggregations.map(_.index_field),
      ConnectorElasticSearch.FieldUsage.Aggregate
    )

    val query = representation.size(0).aggregations(
      aggregations.map { entry =>
        val fieldName = metaNameToField(entry.index_field)
        if (fieldName.isEmpty)
          throw new UnsupportedOperationException(s"unknown meta-data '${entry.index_field}'")
        termsAgg(entry.identifier, fieldName).size(nBuckets.getOrElse(entry.nBuckets))
      }
    )
    val searchRes = es.client.execute(query)
    searchRes.map { results =>
      val statistics = aggregations.map { entry =>
        val ident = entry.identifier
        val buckets = results.termsAgg(ident).buckets.filter(entry.keep).sortWith(entry.sortLessThan)
        val data = buckets.map(x => StatisticsEntry(x.key, x.docCount))
        val label = {
          if (entry.displayPrefix)
            s"Top ${nBuckets.getOrElse(entry.nBuckets)} " + entry.displayName
          else
            entry.displayName
        }
        FieldStatistics(ident, label, data)
      }
      val meta0 = Map[String, jn.JValue](
        "total_hits" -> decompose(results.totalHits),
        "is_timed_out" -> decompose(results.isTimedOut),
        "is_terminated_early" -> decompose(results.isTerminatedEarly)
      )
      ResponseData(statistics, meta0)
    }
  }

}
