/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab

import akka.actor.Actor
import eu.nomad_lab.CalculationMonitorActor.TaskFinished
import eu.nomad_lab.ParserActor.ParseMessage
import eu.nomad_lab.QueueMessage.CalculationParserRequest
import eu.nomad_lab.parsing_queue.CalculationParser

class ParserActor extends Actor {
  def receive = {
    case ParseMessage(func) =>
      func()
    case "done" =>
      context stop self
  }
}

object ParserActor {
  case class ParseMessage(val func: () => Unit)
}
