/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.parsers.parquet

import eu.nomad_lab.{ JsonSupport, JsonUtils }

import collection.JavaConverters._
import org.apache.parquet.hadoop.api.WriteSupport
import org.apache.parquet.hadoop.api.WriteSupport.WriteContext
import org.apache.parquet.io.api.RecordConsumer
import org.json4s.JsonAST.JValue
import org.apache.hadoop.conf.Configuration
import org.apache.parquet.schema.MessageType
import org.apache.parquet.io.api.Binary

import scala.collection.mutable

class ParseEventsWriteSupport(val rootSchema: MessageType) extends WriteSupport[JValue] {
  var recordConsumer: RecordConsumer = null

  override def init(configuration: Configuration): WriteContext = {
    var extraMetaData = mutable.Map("nomad.schema" -> rootSchema.toString)
    return new WriteSupport.WriteContext(rootSchema, extraMetaData.asJava)
  }

  override def write(ev: JValue): Unit = {
    recordConsumer.startMessage()
    recordConsumer.startField("event", 0)
    recordConsumer.addBinary(Binary.fromConstantByteArray(JsonUtils.normalizedUtf8(ev)))
    recordConsumer.endMessage()
  }

  override def prepareForWrite(rc: RecordConsumer): Unit = {
    recordConsumer = rc
  }

}
