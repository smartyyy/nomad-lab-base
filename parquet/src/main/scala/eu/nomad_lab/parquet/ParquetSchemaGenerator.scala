/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.parquet

import scala.collection.JavaConversions._
import org.apache.parquet.schema.Type.Repetition.OPTIONAL
import org.apache.parquet.schema.Type.Repetition.REPEATED
import org.apache.parquet.schema.Type.Repetition.REQUIRED
import org.apache.parquet.schema.Type
import org.apache.parquet.schema.GroupType
import org.apache.parquet.schema.MessageType
import org.apache.parquet.schema.PrimitiveType
import org.apache.parquet.schema.PrimitiveType.PrimitiveTypeName
import org.apache.parquet.schema.Type.Repetition
import eu.nomad_lab.meta.{ KnownMetaInfoEnvs, MetaInfoRecord, MetaInfoEnv }
import eu.nomad_lab.parsers.JsonWriterBackend

import scala.collection.{ SortedSet, mutable }

object ParquetSchemaGenerator {
  lazy val defaultSchema = new ParquetSchemaGenerator(KnownMetaInfoEnvs.all)
}

class ParquetSchemaGenerator(val metaInfoEnv: MetaInfoEnv) {

  var indexMap: Map[String, Int] = Map(("archive_context" -> 0), ("archive_gid" -> 1), ("calculation_context" -> 2),
    ("calculation_gid" -> 1), ("shape" -> 0), ("values" -> 1), ("c_index" -> 0)) // Shape, values and c_index are the only non-unique items in the schema. Separately handled here with the top.
  //    si.foreach((s) => println(s._2.indexMap))

  val schema: MessageType = {
    val rootSections = JsonWriterBackend.defaultRootSections.sorted
    val si = for (r <- rootSections) yield (r, createSchema(metaInfo = r, startIndex = 1))
    val s = for (x <- si) yield new GroupType(REPEATED, x._1, new PrimitiveType(REQUIRED, PrimitiveTypeName.INT64, "c_index") :: x._2)
    val mSchema = new MessageType(
      "File",
      new GroupType(REPEATED, "archive_context",
        new PrimitiveType(REQUIRED, PrimitiveTypeName.INT64, "c_index"),
        new PrimitiveType(REQUIRED, PrimitiveTypeName.BINARY, "archive_gid"),
        new GroupType(REPEATED, "calculation_context",
          new PrimitiveType(REQUIRED, PrimitiveTypeName.INT64, "c_index") ::
            new PrimitiveType(REQUIRED, PrimitiveTypeName.BINARY, "calculation_gid") :: s.toList))
    )
    indexMap ++= rootSections.zipWithIndex.map { case (a, b) => (a -> (b + 2)) }
    //si.foldLeft(index)((m, y) => m ++ y._2.indexMap ++ rootSections.zipWithIndex.map{case (a,b)=> (a -> (b+2))} ))
    mSchema
  }

  def infType(dtypeStr: Option[String]): PrimitiveTypeName = {
    dtypeStr match {
      case Some("f") => PrimitiveTypeName.DOUBLE
      case Some("f32") => PrimitiveTypeName.FLOAT
      case Some("f64") => PrimitiveTypeName.DOUBLE
      case Some("i") => PrimitiveTypeName.INT32
      case Some("i32") => PrimitiveTypeName.INT32
      case Some("i64") => PrimitiveTypeName.INT64
      case Some("C") => PrimitiveTypeName.BINARY
      case Some("D") => PrimitiveTypeName.BINARY
      case Some("b") => PrimitiveTypeName.BOOLEAN
      case Some("r") => PrimitiveTypeName.INT64
      case _ => throw new Exception
    }
  }

  def rep(mi: MetaInfoRecord): Repetition = {
    val r = mi.repeats.getOrElse(mi.kindStr == "type_section")
    if (r) REPEATED else OPTIONAL
  }

  def sectionType(mi: MetaInfoRecord): Type = { //Every sectionType contains the c_index
    new GroupType(rep(mi), mi.name, new PrimitiveType(REQUIRED, PrimitiveTypeName.INT64, "c_index") ::
      createSchema(mi.name, 1))
  }
  def abstractType(mi: MetaInfoRecord): GroupType = {
    new GroupType(rep(mi), mi.name, createSchema(mi.name))
  }
  def scalarValue(mi: MetaInfoRecord): Type = {
    new PrimitiveType(rep(mi), infType(mi.dtypeStr), mi.name)
  }
  def arrayValue(mi: MetaInfoRecord): Type = {
    new GroupType(rep(mi), mi.name,
      new PrimitiveType(REPEATED, PrimitiveTypeName.INT64, "shape"),
      new PrimitiveType(REPEATED, infType(mi.dtypeStr), "values"))
  }

  /**
   *
   * @param metaInfo metaInfo for which schema is created
   * @param startIndex used in case of sectionType when c_index has the 0th index
   * @return schema and index
   */
  def createSchema(metaInfo: String, startIndex: Int = 0): List[Type] = {
    val cDone = mutable.SortedSet[String]()
    var cToDo = metaInfoEnv.allDirectChildrenOf(metaInfo).toSet

    val cAbstract = mutable.Set[String]()
    while (!cToDo.isEmpty) {
      val now = cToDo.head
      cToDo = cToDo.tail
      val mi = metaInfoEnv.metaInfoRecordForName(now).get
      cDone += now
      if (mi.kindStr == "type_abstract_document_content") {
        cAbstract += now
        cToDo ++= metaInfoEnv.allDirectChildrenOf(now).filter { x => !cDone(x) }
      }
    }
    val valueSet: SortedSet[String] = cDone.filter(metaInfo => metaInfoEnv.metaInfoRecordForName(metaInfo).get.kindStr == "type_document_content")
    val sectionSet: SortedSet[String] = cDone.filter(metaInfo => metaInfoEnv.metaInfoRecordForName(metaInfo).get.kindStr == "type_section")
    val sectionStartIndex = startIndex + valueSet.size
    indexMap = indexMap ++ valueSet.zipWithIndex.map { case (x, i) => x -> (i + startIndex) } ++ sectionSet.zipWithIndex.map { case (x, i) => x -> (i + sectionStartIndex) }
    //println(s"cToDo: ${cToDo}, cDone: ${cDone}, valueSet: ${valueSet}, sectionSet: ${sectionSet}")
    //    println("Newly added indices: "+ cSeparated.zipWithIndex.map{ case (x,i) => x -> (i + startIndex)})
    val valueSchema = valueSet.foldLeft(List[Type]())((x, y) => {
      val mi = metaInfoEnv.metaInfoRecordForName(y).get
      if (mi.shape.getOrElse(Seq()).isEmpty) x :+ scalarValue(mi) else x :+ arrayValue(mi)
    })
    val sectionSchema = sectionSet.foldLeft(List[Type]())((x, y) => {
      val mi = metaInfoEnv.metaInfoRecordForName(y).get
      x :+ sectionType(mi)
    })
    valueSchema ++ sectionSchema
  }

}
