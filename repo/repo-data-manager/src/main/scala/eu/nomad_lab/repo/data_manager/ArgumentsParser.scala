/*
  Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed,
                      Markus Schneider
                      Fritz-Haber-Institut der Max-Planck-Gesellschaft

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package eu.nomad_lab.repo.data_manager

import com.typesafe.scalalogging.StrictLogging

/*
 * Arguments Parser Class
 * => everything concerning syntax and sanity checks of provided arguments,
 *    as well as printing help/usage if required or needed
 */
class ArgumentsParser(args: Array[String]) extends StrictLogging {

  val usage00 = """
Usage:
  repoDataManager [--help] <command>
  """

  val usage00AvailCommands = """
    Available commands:
      listOldRestrictedCalcs              list "old restricted calculations"
      setOldRestrictedCalcsOpenAccess     set "old restricted calculations" open access
  """

  val usage00AvailOptions = """
    Run repoDataManager <command> [--help] to view information
    about the options for each command.
  """

  val usage01 = """
  Usage:
    repoDataManager listOldRestrictedCalcs [--help]

    List "old restricted calculations".
  """

  val usage02 = """
  Usage:
    repoDataManager setOldRestrictedCalcsOpenAccess [--help]

    Set "old restricted calculations" open access.
  """

  val usageOldRestrictedCalcs = """
    "Old restricted calculation" in this particular case means
    that all the following points apply:
        - the calculation is restricted
        - the calculation is not a dataset
        - the calculation has either passed its open access date
          OR the upload date is older than 36 months (3 years)
  """

  val usageTop: String = usage00 +
    usage00AvailCommands +
    usageOldRestrictedCalcs +
    usage00AvailOptions

  /**
   * Check syntax and sanity of provided arguments,
   * print help if required/desired
   */
  def checkArgsSyntax(): Unit = {

    // check if arguments have been provided
    checkArgsEmpty()

    // check first argument
    val firstArg = args.head
    firstArg match {
      case "--help" | "-h" => printHelpAndExit(usageTop)
      case "listOldRestrictedCalcs" => checkFurtherArgsSyntax()
      case "setOldRestrictedCalcsOpenAccess" => checkFurtherArgsSyntax()
      case _ => printHelpAndExitOnError(firstArg, usageTop)
    }

  }

  /**
   * Checks if any arguments have been provided
   */
  def checkArgsEmpty(): Unit = {

    if (args.length == 0) {
      println("No arguments have been provided.")
      printHelpAndExit(usageTop)
    }

  }

  /**
   * Print help/usage and exit
   */
  def printHelpAndExit(usage: String): Unit = {

    println(usage)
    sys.exit(0)

  }

  /**
   * Print help/usage and exit on error
   */
  def printHelpAndExitOnError(arg: String, usage: String): Unit = {

    logger.error(s"Invalid argument: $arg \n $usage")
    sys.exit(-1)

  }

  /**
   * Check syntax and sanity of further provided arguments,
   * print help if required/desired
   *
   */
  def checkFurtherArgsSyntax(): Unit = {

    val firstArg = args.head
    val furtherArgs = args.tail
    if (furtherArgs.nonEmpty) {
      val usage = firstArg match {
        case "listOldRestrictedCalcs" => usage01 + usageOldRestrictedCalcs
        case "setOldRestrictedCalcsOpenAccess" => usage02 + usageOldRestrictedCalcs
      }
      val arg = furtherArgs.head
      arg match {
        case "--help" | "-h" => printHelpAndExit(usage)
        case _ => printHelpAndExitOnError(arg, usage)
      }
    }

  }

}
