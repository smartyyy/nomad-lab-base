/*
  Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed,
                      Markus Schneider
                      Fritz-Haber-Institut der Max-Planck-Gesellschaft

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package eu.nomad_lab.repo.data_manager

import eu.nomad_lab.repo.RepoDb

/**
 * Data Manager for the Repository database
 */
object RepoDataManager extends App {

  val ArgumentsParser = new ArgumentsParser(args)

  lazy val repoDb = RepoDb()
  lazy val OldRestictedCalcsHandler = new OldRestictedCalcsHandler(repoDb)

  // check syntax and sanity of provided arguments,
  // print help if required/desired
  ArgumentsParser.checkArgsSyntax()

  // decide what to do
  val firstArg = args.head
  firstArg match {
    case "--help" | "-h" => ArgumentsParser.printHelpAndExit(ArgumentsParser.usageTop)
    case "listOldRestrictedCalcs" => OldRestictedCalcsHandler.listOldRestrictedCalcs()
    case "setOldRestrictedCalcsOpenAccess" => OldRestictedCalcsHandler.setOldRestrictedCalcsOpenAccess()
    case _ => ArgumentsParser.printHelpAndExitOnError(firstArg, ArgumentsParser.usageTop)
  }

}
