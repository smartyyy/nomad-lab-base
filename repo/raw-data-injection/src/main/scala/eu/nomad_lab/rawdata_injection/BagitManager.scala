/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.rawdata_injection

import gov.loc.repository.bagit
import gov.loc.repository.bagit.writer.impl.ZipWriter
import gov.loc.repository.bagit.transformer.impl.DefaultCompleter
import eu.nomad_lab.rawdata.Author
import eu.nomad_lab.Base64
import eu.nomad_lab.CompactSha
import eu.nomad_lab.JsonUtils
import eu.nomad_lab.JsonSupport
import java.io.{ BufferedInputStream, File, FileInputStream, InputStream }

import scala.collection.mutable
import scala.collection.JavaConverters._
import scala.collection.breakOut
import org.{ json4s => jsn }
import java.nio.file.Files
import java.nio.file.attribute
import java.nio.file.Paths
import java.nio.file.Path
import java.nio.file.StandardCopyOption

import com.typesafe.scalalogging.StrictLogging
import gov.loc.repository.bagit.{ Bag, BagFile }
import org.apache.commons.compress.compressors.bzip2

import scala.annotation.tailrec

object BagitManager extends StrictLogging {
  /**
   * exception while calculating the gids
   */
  class GidBuildingException(
    msg: String
  ) extends Exception(msg)

  /**
   * Error extracting the Sha value
   */
  class ShaError(
    msg: String, what: Throwable = null
  ) extends Exception(msg, what)

  /**
   * Tree to store and compute compute file and directories gid values
   */
  abstract sealed class ShaTree {
    def gid: String

    def clearComputedGids(): Unit = ()
  }

  case class ShaFile(
      val sha512: String
  ) extends ShaTree {

    def hexToInt(c: Char): Int = {
      if (c >= '0' && c <= '9')
        c - '0'
      else if (c >= 'A' && c <= 'F')
        c - 'A' + 10
      else if (c >= 'a' && c <= 'f')
        c - 'a' + 10
      else
        throw new ShaError(s"non hex char '$c' in sha")
    }

    override def gid: String = {
      // full sha is 64 bytes, but we use only the first 21
      val bytesNeeded = 21
      val binarySha = Array.fill[Byte](bytesNeeded)(0)
      if (sha512.length != 128)
        throw new ShaError(s"unexpected sha length (${sha512.length}!=128) for sha $sha512")

      for (i <- 0.until(bytesNeeded)) {
        val c1 = sha512.charAt(2 * i)
        val c2 = sha512.charAt(2 * i + 1)
        binarySha(i) = ((hexToInt(c1) | (hexToInt(c2) << 4)) & 0xFF).toByte
      }
      // the sha is the first 28 characters of the full encoded sha
      "f" + Base64.b64EncodeStr(binarySha)
    }
  }

  class ShaDirectory(
      contents0: Map[String, ShaTree] = Map(),
      var _gid: String = ""
  ) extends ShaTree {
    val contents = mutable.Map[String, ShaTree]()
    contents ++= contents0

    def toJValue: jsn.JValue = {
      jsn.JObject(
        contents.map {
          case (name, node) =>
            name -> jsn.JString(node.gid)
        }(breakOut): List[(String, jsn.JValue)]
      )
    }

    override def gid: String = {
      if (_gid.isEmpty()) {
        val sha = CompactSha()
        JsonUtils.normalizedOutputStream(toJValue, sha.outputStream)
        _gid = sha.gidStr("d")
      }
      _gid
    }

    override def clearComputedGids(): Unit = {
      _gid = ""
      for ((k, v) <- contents) {
        v.clearComputedGids()
      }
    }

    def insertShaTree(shaTree: ShaTree, path: String): Unit = {
      val sPath = path.dropWhile(_ == '/').split("/")
      if (sPath.isEmpty)
        throw new GidBuildingException(s"Empty path '$path' in insertShaTree")
      var contentsAtt: mutable.Map[String, ShaTree] = contents
      for (i <- 0.until(sPath.length - 1)) {
        val pathSegment = sPath(i)
        contentsAtt.get(pathSegment) match {
          case Some(ShaDirectory(newContents)) =>
            contentsAtt = newContents
          case None =>
            val newDir = ShaDirectory()
            contentsAtt += (pathSegment -> newDir)
            contentsAtt = newDir.contents
          case Some(ShaFile(_)) =>
            throw new GidBuildingException(s"Found file '$pathSegment' when expecting directory while inserting $path")
        }
      }
      val fileName = sPath(sPath.length - 1)
      contentsAtt.get(fileName) match {
        case Some(t) =>
          throw new GidBuildingException(s"Found value $t when inserting $shaTree at $path")
        case None =>
          contentsAtt += (fileName -> shaTree)
      }
    }
  }

  object ShaDirectory {
    def apply(
      contents: Map[String, ShaTree] = Map(),
      gid: String = ""
    ): ShaDirectory = {
      new ShaDirectory(contents, gid)
    }

    def unapply(d: ShaDirectory): Option[mutable.Map[String, ShaTree]] = {
      Some(d.contents)
    }
  }

  /**
   * Tree to store and compute compute file and directories gid values with modification dates
   */
  abstract sealed class DatedShaTree {
    def dataGid: String

    def gid: String

    def clearComputedGids(): Unit = ()
  }

  case class DatedShaFile(
      val dataGid: String,
      val creationTime: attribute.FileTime,
      val lastModifiedTime: attribute.FileTime
  ) extends DatedShaTree {

    def toJValue: jsn.JValue = {
      jsn.JObject(
        ("dataGid" -> jsn.JString(dataGid)) ::
          ("creationTime" -> jsn.JString(creationTime.toString)) ::
          ("lastModifiedTime" -> jsn.JString(lastModifiedTime.toString)) :: Nil
      )
    }

    override def gid: String = {
      val sha = CompactSha()
      JsonUtils.normalizedOutputStream(toJValue, sha.outputStream)
      sha.gidStr("F")
    }
  }

  class DatedShaDirectory(
      val dataGid: String,
      contents0: Map[String, DatedShaTree] = Map(),
      var _gid: String = ""
  ) extends DatedShaTree {
    val contents = mutable.Map[String, DatedShaTree]()
    contents ++= contents0

    def toJValue: jsn.JValue = {
      jsn.JObject(
        contents.map {
          case (name, node) =>
            name -> jsn.JString(node.gid)
        }(breakOut): List[(String, jsn.JValue)]
      )
    }

    override def gid: String = {
      if (_gid.isEmpty()) {
        val sha = CompactSha()
        JsonUtils.normalizedOutputStream(toJValue, sha.outputStream)
        _gid = sha.gidStr("D")
      }
      _gid
    }

    override def clearComputedGids(): Unit = {
      _gid = ""
      for ((k, v) <- contents) {
        v.clearComputedGids()
      }
    }
  }

  object DatedShaDirectory {
    def apply(
      dataGid: String,
      contents: Map[String, DatedShaTree] = Map(),
      gid: String = ""
    ): DatedShaDirectory = {
      new DatedShaDirectory(dataGid, contents, gid)
    }

    def unapply(d: DatedShaDirectory): Option[(String, mutable.Map[String, DatedShaTree])] = {
      Some((d.dataGid, d.contents))
    }
  }

  def datedFile(data_gid: String, path: Path): DatedShaFile = {
    val file: File = path.toFile
    val tPath = if (file.exists) path else java.nio.file.Paths.get(path.toString + ".bz2")
    val attrs = Files.readAttributes(tPath, classOf[attribute.BasicFileAttributes]) //java.nio.file.LinkOption.NOFOLLOW_LINKS ?
    DatedShaFile(data_gid, attrs.creationTime(), attrs.lastModifiedTime())
  }

  val bagFactory = new bagit.BagFactory

  /**
   * writes the gids to the outF writer
   */
  def writeGids(s: DatedShaDirectory, outF: java.io.Writer, path: String = ""): Unit = {
    s.contents.foreach {
      case (name, tree) =>
        outF.write(tree.gid)
        outF.write(" ")
        outF.write(tree.dataGid)
        outF.write("  ")
        outF.write(path + name)
        outF.write("\n")
        tree match {
          case _: DatedShaFile => ()
          case dir: DatedShaDirectory =>
            writeGids(dir, outF, path + name + "/")
        }
    }
  }

  /**
   * writes modification dates to outF writer
   */
  def writeDates(s: DatedShaDirectory, outF: java.io.Writer, path: String = ""): Unit = {
    s.contents.foreach {
      case (name, tree) =>
        tree match {
          case f: DatedShaFile =>
            outF.write(f.creationTime.toString)
            outF.write(" ")
            outF.write(f.lastModifiedTime.toString)
            outF.write("  ")
            outF.write(path + name)
            outF.write("\n")
          case dir: DatedShaDirectory =>
            writeDates(dir, outF, path + name + "/")
        }
    }
  }

  def toDatedTree(inDir: ShaDirectory, realPath: Path): DatedShaDirectory = {
    val outDir = DatedShaDirectory(inDir.gid)
    inDir.contents.foreach {
      case (name, tree) =>
        tree match {
          case f: ShaFile =>
            outDir.contents += (name -> datedFile(f.gid, realPath.resolve(name)))
          case dir: ShaDirectory =>
            outDir.contents += (name -> toDatedTree(dir, realPath.resolve(name)))
        }
    }
    outDir
  }

  /**
   * Adds checksums, gids and modifications dates to a bag
   * and returns an unique identifier (gid) for the bag
   * (based on the data and modification dates of the files)
   */
  def createGid(bag: bagit.Bag, realBaseDir: Path): String = {
    val manifest = bag.getPayloadManifest(bagit.Manifest.Algorithm.SHA512)
    val rootDir = ShaDirectory()
    if (manifest != null) {
      val entries = manifest.entrySet()
      if (entries != null) {
        val it = entries.iterator()
        while (it.hasNext()) {
          val el = it.next()
          rootDir.insertShaTree(ShaFile(el.getValue), el.getKey)
        }
      }
    }
    val innerDir = rootDir.contents.get("data") match {
      case Some(tree) =>
        tree match {
          case dir: ShaDirectory =>
            toDatedTree(dir, realBaseDir)
          case _ =>
            throw new GidBuildingException("unexpected directory structure data should be a directory")
        }
      case None =>
        throw new GidBuildingException("unexpected directory structure, expected a data entry")
    }
    val datedRootDir = DatedShaDirectory(rootDir.gid, Map("data" -> innerDir))
    val f = File.createTempFile("file_gids", "txt")
    val outF = new java.io.FileWriter(f)
    writeGids(datedRootDir, outF)
    outF.close()
    bag.putBagFile(new bagit.impl.FileBagFile("nomad_gids.txt", f))
    val f2 = File.createTempFile("file_dates", "txt")
    val outF2 = new java.io.FileWriter(f2)
    writeDates(datedRootDir, outF2)
    outF2.close()
    bag.putBagFile(new bagit.impl.FileBagFile("file_dates.txt", f2))
    "R" + innerDir.gid.slice(1, 29)
  }

  def addFilesToBag(bag: Bag, files: Seq[File], rootDir: Option[File]): Unit = {
    for (file <- files) {
      val rDir = rootDir match {
        case None => Some(file.getParentFile()) //Careful: this flattens the files in the bag (as a side effect) if dirs with different roots passed
        case f => f
      }
      if (file.isDirectory) {
        val cFiles = file.listFiles
        if (cFiles != null && !cFiles.isEmpty)
          addFilesToBag(bag, cFiles, rDir)
        // ignore empty directories, change?
      } else if (file.isFile) {
        val fpath = java.nio.file.Paths.get(file.getAbsolutePath)
        val rPath = java.nio.file.Paths.get(rDir.get.getAbsolutePath)
        val internalFilePath = bag.getBagConstants.getDataDirectory + "/" + fpath.subpath(rPath.getNameCount, fpath.getNameCount)
        if (bzip2.BZip2Utils.isCompressedFilename(file.getName)) {
          val bz2BagFile = new Bz2BagFile(filepath = internalFilePath, file = file)
          try {
            if (bz2BagFile.getSize > 0) //Check if valid non corrupted bz2 file
              bag.putBagFile(bz2BagFile) //Put the decompressed bz2 file in the bag
          } catch {
            case e: Exception =>
              logger.error(s"Exception while reading bz2 file: $fpath")
              logger.error(e.getMessage)
              bag.putBagFile(new bagit.impl.FileBagFile(internalFilePath, file)) //Put the corrupted bz2 file in the bag without decompression
          }
        } else {
          bag.putBagFile(new bagit.impl.FileBagFile(internalFilePath, file))
        }
      }
    }
  }

  /**
   * creates a bag for the files in the given directory
   *
   * Returns the bag and its gid
   */
  def createForDir(baseDir: Path, inDirEls: Seq[String] = Seq()): (bagit.Bag, String) = {
    val bag = bagFactory.createBag()
    if (inDirEls.isEmpty)
      addFilesToBag(bag, baseDir.toFile.listFiles(), None)
    else
      addFilesToBag(bag, inDirEls.map { (fName: String) => baseDir.resolve(fName).toFile }, None)

    //val preBag = bagFactory.createPreBag(baseDir)
    //val bag = preBag.makeBagInPlace(bagit.BagFactory.LATEST, true)
    val completer = new DefaultCompleter(bagFactory)
    completer.setPayloadManifestAlgorithm(bagit.Manifest.Algorithm.SHA512)
    completer.setTagManifestAlgorithm(bagit.Manifest.Algorithm.SHA512)
    completer.setClearExistingPayloadManifests(true)
    completer.setCompletePayloadManifests(true)
    completer.setClearExistingTagManifests(true)
    completer.setCompleteTagManifests(false)
    completer.setGenerateBagInfoTxt(false)
    completer.setGenerateTagManifest(false)
    completer.setUpdateBaggingDate(false)
    completer.setUpdateBagSize(true)
    completer.setUpdatePayloadOxum(true)
    val bagWithChecksums = bag.makeComplete(completer)
    val bagGid = createGid(bagWithChecksums, baseDir)
    (bagWithChecksums -> bagGid)
  }

  def setBagInfo(
    bag: bagit.Bag, bagGid: String, uploadName: String, uploadDate: java.sql.Timestamp, uploader: Author, pathInUpload: String, iBag: Int, nBags: Int
  ): Unit = {
    val partFactory = bag.getBagPartFactory()
    val bagInfo = partFactory.createBagInfoTxt()
    bagInfo.addSourceOrganization("NOMAD Repository, http://nomad-repository.eu/")
    bagInfo.addContactEmail("webmaster@nomad-repository.eu")
    bagInfo.addExternalDescription(s"""Part of the data uploaded by ${uploader.firstName} ${uploader.lastName} of ${uploader.affiliation} (${uploader.userName}) on $uploadDate and identified with the upload name $uploadName""")
    bagInfo.addBagGroupIdentifier("nmd-arc://" + uploadName)
    bagInfo.addExternalIdentifier("nmd://" + bagGid)
    bagInfo.addInternalSenderIdentifier("nmd-arc://" + Paths.get(uploadName, pathInUpload))
    bagInfo.setBagCount(iBag, nBags)
    bag.putBagFile(bagInfo)
  }

  def packToZip(bag: bagit.Bag, bagName: String, targetArchive: Path, compressionLevel: Option[Int]): Unit = {
    val baseDir = targetArchive.getParent()
    baseDir.toFile.mkdirs()
    val tmpFile = Files.createTempFile(baseDir, bagName.take(5), ".tmp")
    val completer = new DefaultCompleter(bagFactory)
    completer.setPayloadManifestAlgorithm(bagit.Manifest.Algorithm.SHA512)
    completer.setTagManifestAlgorithm(bagit.Manifest.Algorithm.SHA512)
    completer.setClearExistingPayloadManifests(false)
    completer.setCompletePayloadManifests(false)
    completer.setClearExistingTagManifests(true)
    completer.setCompleteTagManifests(true)
    completer.setGenerateTagManifest(true)
    completer.setUpdateBaggingDate(true)
    val zipWriter = new ZipWriter(bagFactory)
    zipWriter.setCompressionLevel(compressionLevel.getOrElse(6).toInt)
    zipWriter.setBagDir(bagName)
    val zipF = zipWriter.write(bag.makeComplete(completer), tmpFile.toFile())
    zipF.close()
    Files.move(tmpFile, targetArchive, StandardCopyOption.ATOMIC_MOVE)
  }

  case class PackUploadMessage(
    uploadPath: String,
    uploadName: String,
    uploadDate: java.sql.Timestamp,
    uploader: Author,
    pathInUpload: String,
    inPathEls: Seq[String],
    targetDir: String,
    iBag: Int,
    nBags: Int
  )

  case class PackedUploadMessage(
    archiveGid: String,
    archivePath: String,
    didExist: Boolean,
    created: Boolean,
    packRequest: PackUploadMessage
  )

  /**
   * Packs an upload
   */
  def packUpload(inMsg: PackUploadMessage, overwrite: Boolean = false): PackedUploadMessage = {
    logger.info(s"packUpload(${JsonSupport.writeStr(inMsg)}, overwrite = $overwrite)")
    val sourcePath = Paths.get(inMsg.uploadPath, inMsg.pathInUpload)
    if (!sourcePath.toFile().exists) {
      throw new Exception(s"path $sourcePath does not exist")
    } else {
      logger.info(s"will pack $sourcePath of upload ${inMsg.uploadName}")
    }
    val (bag, bagGid) = createForDir(sourcePath, inMsg.inPathEls)
    setBagInfo(
      bag = bag,
      bagGid = bagGid,
      uploadName = inMsg.uploadName,
      uploadDate = inMsg.uploadDate,
      uploader = inMsg.uploader,
      pathInUpload = inMsg.pathInUpload,
      iBag = inMsg.iBag,
      nBags = inMsg.nBags
    )
    val targetPath = Paths.get(inMsg.targetDir, bagGid.slice(0, 3), bagGid + ".zip")
    val exists = targetPath.toFile().exists()
    val create: Boolean = !exists || overwrite
    if (exists)
      logger.warn(s"Found existing archive at $targetPath when trying to pack ${JsonSupport.writeStr(inMsg)} ${if (overwrite) "overwriting" else "skipping"}.")
    if (create)
      packToZip(bag, bagGid, targetPath, None)
    val res = PackedUploadMessage(
      archiveGid = bagGid,
      archivePath = targetPath.toString,
      didExist = exists,
      created = create,
      packRequest = inMsg
    )
    logger.info(JsonSupport.writeStr(res))
    res
  }

  def sizeOfStream(inputStream: InputStream): Long = {
    var readMore = true
    var totalRead: Long = 0
    val buf = Array.fill[Byte](65536)(0)
    while (readMore) {
      val nRead = inputStream.read(buf)
      if (nRead <= 0)
        readMore = false
      else totalRead += nRead
    }
    totalRead
  }
}
