/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.rawdata_injection

import org.specs2.mutable.Specification
import java.nio.charset.StandardCharsets

import com.typesafe.scalalogging.StrictLogging
import java.io.{ File, FileOutputStream, InputStream, OutputStream }
import java.math.RoundingMode
import java.nio.file.{ Files, Paths }
import java.text.DecimalFormat

import org.apache.commons.compress.archivers.zip.{ ZipArchiveEntry, ZipFile }
import gov.loc.repository.bagit
import gov.loc.repository.bagit.transformer.impl.DefaultCompleter
import gov.loc.repository.bagit.writer.impl.ZipWriter
import org.apache.commons.compress.utils.IOUtils
import org.apache.commons.io.FileUtils
/**
 * Specification (fixed tests) for MetaInfo serialization
 */
class BagitManagerSpec extends Specification with StrictLogging {
  val rawSize = Map(("file1" -> 1024), ("file2" -> 1024), ("file3" -> 1024), ("file4" -> 11534336), ("file5" -> 11534336)) //Raw Size in Bytes
  val baseDir = java.nio.file.Paths.get("raw-data-injection/src/test/resources/")
  val testFile = new File("raw-data-injection/src/test/resources/toBag")
  //Complete Bag
  val bagFactory = new bagit.BagFactory
  //FIXME
  /*val (bag, bagName) = BagitManager.createForDir(java.nio.file.Paths.get(testFile.getPath), Seq())
  val tPath = Paths.get(baseDir.toString, bagName.slice(0, 3), bagName + ".zip")
  BagitManager.packToZip(bag, bagName, tPath, None)
  //Partial Bag
  val (partialBag, partialBagName) = BagitManager.createForDir(java.nio.file.Paths.get(testFile.getPath), Seq("dir3"))
  val partialBagPath = Paths.get(baseDir.toString, partialBagName.slice(0, 3), partialBagName + ".zip")
  BagitManager.packToZip(partialBag, partialBagName, partialBagPath, None)
  val df: DecimalFormat = new DecimalFormat("#.###")
  df.setRoundingMode(RoundingMode.CEILING)
  val cLevelToTry = List(1, 3, 6, 9)
  def getAppropriateSizeAndSymbol(size: Long): (String, String) = {
    val kb = 1024.0
    val (sym, sizeToPrint) = if (size > kb * kb * kb) ("G", size / (kb * kb * kb)) else if (size > kb * kb) ("M", size / (kb * kb)) else ("K", size / kb)
    (sym, df.format(sizeToPrint))
  }

  def time[A](f: => A): Double = {
    val s = System.nanoTime
    val ret = f
    val time = (System.nanoTime - s) / 1e6
    time
  }

  def resolvePath(getName: String, i: Int) = {
    baseDir.resolve("decompressed").resolve(i.toString).resolve(getName)
  }

  def ucompress(filePath: java.nio.file.Path, i: Int) = {
    //    println(s"Uncompressing ${filePath.toString}")
    val zipFile = new ZipFile(filePath.toString)
    try {
      val entries = zipFile.getEntries()
      while (entries.hasMoreElements()) {
        val zipEntry: ZipArchiveEntry = entries.nextElement()
        if (!zipEntry.isDirectory && !zipEntry.isUnixSymlink) {
          val destinationPath = resolvePath(zipEntry.getName, i)
          //          printf(s"destinationPath: $destinationPath")
          Files.createDirectories(destinationPath.getParent())
          val zIn: InputStream = zipFile.getInputStream(zipEntry)
          Files.createFile(destinationPath)
          val out: OutputStream = new FileOutputStream(destinationPath.toFile())
          IOUtils.copy(zIn, out)
          IOUtils.closeQuietly(zIn)
          out.close()
        }
      }
    } catch {
      case e: Exception => println(s"Exception: ${e.getMessage}")
    } finally {
      zipFile.close()
    }
  }

  "verify bag" >> {
    "is Complete" >> {
      bag.verifyComplete().isSuccess mustEqual (true)
    }
    "writes to File" >> {
      val zipFile = tPath.toFile
      val exists = zipFile.exists()
      //      FileUtils.deleteDirectory( Paths.get(baseDir.toString, bagName.slice(0,3)).toFile)
      exists must_== true
    }
    "contains only requested files" >> {
      var cFile2 = false
      var cFile3 = false
      var cFile4 = false
      val zipFile = new ZipFile(partialBagPath.toString)
      val fi = partialBagPath.toFile
      val exists = fi.exists()
      val entries = zipFile.getEntries
      while (entries.hasMoreElements) {
        val zipEntry: ZipArchiveEntry = entries.nextElement()
        if (zipEntry.getName.contains("file2"))
          cFile2 = true
        else if (zipEntry.getName.contains("file3"))
          cFile3 = true
        else if (zipEntry.getName.contains("file4"))
          cFile4 = true
      }
      zipFile.close()
      //      FileUtils.deleteDirectory( Paths.get(baseDir.toString, bagName.slice(0,3)).toFile)
      exists must_== true
      cFile2 must_== true
      cFile3 must_== true
      cFile4 must_== false
    }
  }*/

  //"bag Time" >> {
  //  "for compression levels (6,3,1)}" >> {
  //    var tList: List[Double] = List()
  //
  //    for( i <- cLevelToTry){
  //    def f1() = {
  //      val (bg1, bgName1) = BagitManager.createForDir(java.nio.file.Paths.get(testFile.getPath), Seq())
  //      val tP1 = Paths.get(baseDir.toString, bgName1.slice(0, 3), bgName1 + s"${i}.zip")
  //      BagitManager.packToZip(bg1, bgName1, tP1, Some(i))
  //      val (sm1, dv1) = getAppropriateSizeAndSymbol(new File(tP1.toString).length())
  //      println(s"Size compression for level $i: ${dv1}${sm1}")
  //    }
  //    val t1 = time(f1)
  //    tList = t1 :: tList
  //
  //    println(s"Bag creation and compression time for compression level $i: ${t1} ms")
  //    }
  //    for (i <- 0 to tList.size - 2) {
  //      tList(i) > tList(i + 1) must_== (true)
  //    }
  //    tList(0) > tList( 1) must_== (true) // Otherwise: could not find implicit value for evidence parameter of type org.specs2.execute.AsResult[Unit]
  //  }
  //
  //  "for decompression for compression levels (6,3,1)" >> {
  //    var tList: List[Double] = List()
  //    for (i <- cLevelToTry) {
  //      val tP = Paths.get(baseDir.toString, bagName.slice(0, 3), bagName + s"${i}.zip")
  //      def f() = ucompress(tP.toAbsolutePath,i)
  //      val t = time(f)
  //      println(s"decompression time for compression level $i: ${t} ms")
  //      tList = t :: tList
  //    }
  //    for (i <- 0 to tList.size - 2) {
  //      tList(i) > tList(i + 1) must_== (true)
  //    }
  //    tList(0) > tList( 1) must_== (true) // Otherwise: could not find implicit value for evidence parameter of type org.specs2.execute.AsResult[Unit]
  //  }
  //}
  //  "bag" >> {
  //    "sha is correct" >> {
  //    }
  //  }
}
