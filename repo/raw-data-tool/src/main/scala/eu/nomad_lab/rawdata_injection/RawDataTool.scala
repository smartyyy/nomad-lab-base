/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.rawdata_injection;
import scala.collection.mutable.ListBuffer
import scala.collection.breakOut
import scala.collection.mutable
import com.typesafe.scalalogging.StrictLogging
import eu.{ nomad_lab => lab }
import eu.nomad_lab.repo
import eu.nomad_lab.JsonUtils
import java.nio.file.Path
import java.nio.file.Paths

object RawDataTool extends StrictLogging {

  val usage = """
Usage:
  rawDataTool [--help] [--update-users] [--upload-name upoadName] [--force-open] [--split-file splitFile] [--small-uploads-file duFile] [--base-path base/path/for/duFile] [--no-verify] [--chunk-range fromChunkIndex untilChunkIndex]

command line interface to the raw data injection process
  """
  def main(args: Array[String]): Unit = {
    /*if (args.length == 0) {
      println(usage)
      return
    }*/
    var list: List[String] = args.toList
    var uploadName: Option[String] = None
    var forceOpen: Boolean = false
    var splitFile: Option[String] = None
    var smallUploadsFile: Option[String] = None
    var basePath: Option[Path] = None
    var updateUsers: Boolean = false
    var verify: Boolean = true
    var fromChunkIndex: Int = 0
    var untilChunkIndex: Int = Int.MaxValue
    while (!list.isEmpty) {
      val arg = list.head
      list = list.tail
      arg match {
        case "--help" | "-h" =>
          println(usage)
          return
        case "--upload-name" =>
          if (list.isEmpty) {
            println(s"Error: expected upload name after --upload-name.$usage")
            return
          }
          uploadName = Some(list.head)
          list = list.tail
        case "--force-open" =>
          forceOpen = true
        case "--small-uploads-file" =>
          if (list.isEmpty) {
            println(s"Error: expected a du file after --small-uploads-file.$usage")
            return
          }
          smallUploadsFile = Some(list.head)
          list = list.tail
        case "--base-path" =>
          if (list.isEmpty) {
            println(s"Error: expected a base path after --base-path.$usage")
            return
          }
          basePath = Some(Paths.get(list.head))
          list = list.tail
        case "--no-verify" =>
          verify = false
        case "--split-file" =>
          if (list.isEmpty) {
            println(s"Error: expected a su split file after --split-file.$usage")
            return
          }
          splitFile = Some(list.head)
          list = list.tail
        case "--update-users" =>
          updateUsers = true
        case "--chunk-range" =>
          if (list.isEmpty) {
            println(s"Error: expected a fromChunkIndex after --chunk-range.$usage")
            return
          }
          fromChunkIndex = list.head.toInt
          list = list.tail
          if (list.isEmpty) {
            println(s"Error: expected a toChunkIndex after fromChunkIndex after --chunk-range.$usage")
            return
          }
          untilChunkIndex = list.head.toInt
          list = list.tail
        case _ =>
          println(s"Error: unexpected argument $arg.$usage")
          return
      }
    }

    val ri = lab.rawdata_injection.RepoInjection()
    if (updateUsers)
      ri.transferUsers()

    /* val repoDb = ri.repoDb
    val firstCalcs = repoDb.openCalcFilenamesQuery().orderBy(repo.db.Tables.METADATA.ADDED.asc()).limit(0,10).fetchArray(repo.db.Tables.METADATA.FILENAMES)
    firstCalcs.foreach{ (filenames: Array[Byte]) =>
      repoDb.mainFileFromFilenames(filenames) match {
        case Some(f) => println(s"${f.archiveDir} ${f.uploadName} ${f.relativePath}")
        case None => ()
      }
    }
    println("...")
    val lastCalcs = repoDb.openCalcFilenamesQuery().orderBy(repo.db.Tables.METADATA.ADDED.desc()).limit(0,10).fetchArray(repo.db.Tables.METADATA.FILENAMES)
    lastCalcs.foreach{ (filenames: Array[Byte]) =>
      repoDb.mainFileFromFilenames(filenames) match {
        case Some(f) => println(s"${f.archiveDir} ${f.uploadName} ${f.relativePath}")
        case None => ()
      }
    }

    println("calc_id, checksum, permission, filenames")
    val calculationsForFirstQuery = repoDb.calculationsWithPrefixQuery(repoDb.mainFileFromFilenames(firstCalcs.head).get.uploadName)
    println(calculationsForFirstQuery.getSQL())
    val calcs = calculationsForFirstQuery.fetchLazy()
    while (calcs.hasNext()) {
      val calc = calcs.fetchOne()
      println(s"${calc.value1()} ${calc.value2()} ${if (calc.value3() == 1) "open access" else "restricted "} ${JsonUtils.normalizedStr(repoDb.mainFileFromFilenames(calc.value4()).get.toJValue)}")
    }
    val uploadInfo = ri.analyzeUploadAccess("KU5bgqOcJ5GNm2QTCGu8utbMpZJdzUmUP3xcOgFI")
     */

    splitFile match {
      case Some(path) =>
        val fIn = new java.io.File(path)
        if (!fIn.exists)
          println(s"Cannot find split file at $path .$usage")
        else
          ri.readSplit(
            fIn,
            forceOpen = forceOpen,
            verify = verify,
            fromChunk = fromChunkIndex,
            untilChunk = untilChunkIndex
          )

      case None => ()
    }

    smallUploadsFile match {
      case Some(path) =>
        val fIn = new java.io.File(path)
        if (!fIn.exists)
          println(s"Cannot find du file at $path .$usage")
        else
          ri.readSmallUploads(
            fIn,
            forceOpen = forceOpen,
            basePath = basePath,
            verify = verify
          )
      case None => ()
    }
  }

}
