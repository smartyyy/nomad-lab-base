/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo.queries

import com.sksamuel.elastic4s.http.ElasticDsl._
import com.sksamuel.elastic4s.json4s.ElasticJson4s.Implicits.Json4sHitReader
import com.sksamuel.elastic4s.searches.SearchDefinition
import eu.nomad_lab.elasticsearch.ESManager
import eu.nomad_lab.repo.objects.JsonFormats.formats
import eu.nomad_lab.repo.objects.{ CalculationWrapper, MappableBaseValue }
import eu.nomad_lab.webservice_base.ResponseFormatter.ResponseData
import org.json4s.native.Serialization
import org.{ json4s => jn }

import scala.concurrent.{ ExecutionContext, Future, Promise }

object PidInfo {

  case class CalculationInfo(uris: Seq[String], uploader: Seq[String], uploadDate: Seq[Long])

  case class PidMatch(
      id: String,
      data: Map[String, CalculationInfo]
  ) extends MappableBaseValue {
    override def typeStr = "PID_mapping"
    override def idStr: String = id

    override def toMap: Map[String, Any] = data.map {
      case (pid, entry) => pid -> Map(
        "uris" -> entry.uris,
        "uploader" -> entry.uploader,
        "uploadDate" -> entry.uploadDate
      )
    }
  }
}

class PidInfo(private val es: ESManager)(implicit private val exec: ExecutionContext) {
  import PidInfo._

  def getPIDsForArchiveID(representation: SearchDefinition, archiveID: String): Future[ResponseData[PidMatch]] = {
    import es.connector.metaNameToField

    val promise = Promise[ResponseData[PidMatch]]()

    def iterateOverCalculations(data: Map[String, CalculationInfo], offset: Long): Unit = {
      val baseQuery = representation.size(500).sortByFieldAsc(metaNameToField("repository_calc_id"))
      val results = es.client.execute(baseQuery.searchAfter(Seq(offset)))
      results.onComplete {
        case util.Success(response) =>
          val jV = Json4sHitReader[CalculationWrapper](Serialization, formats,
            manifest[CalculationWrapper])
          val entries = response.to[CalculationWrapper](jV).map(_.section_repository_info)
          val newData = entries.map { entry =>
            val uris = entry.main_file_uri ++: entry.secondary_file_uris
            entry.repository_calc_pid -> CalculationInfo(
              uris = uris.map(x => x.replace(entry.repository_archive_gid.head, archiveID)),
              uploader = Seq(entry.section_uploader_info.uploader_name.getOrElse("unknown")),
              uploadDate = entry.upload_date.toSeq
            )
          }.toMap
          if (newData.isEmpty) {
            promise.success(ResponseData(Seq(PidMatch(archiveID, data)), Map()))
          } else {
            val calc_id_max = entries.map { _.repository_calc_id }.fold(offset)(math.max)
            iterateOverCalculations(data ++ newData, calc_id_max)
          }
        case util.Failure(error) => promise.failure(error)
      }
    }

    iterateOverCalculations(Map(), -1)
    promise.future
  }

}
