/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo

import javax.ws.rs.{ GET, Path, QueryParam }

import akka.event.Logging
import akka.http.scaladsl.model.{ ContentTypes, HttpEntity, HttpResponse, StatusCodes }
import akka.http.scaladsl.server.{ Directives, Route }
import eu.nomad_lab.elasticsearch.{ ApiCallException, ESManager }
import eu.nomad_lab.repo.RequestTransformer.PagingFilter
import eu.nomad_lab.repo.objects.{ CalculationGroupLegacy, CalculationLegacy, DataSetLegacy }
import eu.nomad_lab.repo.queries.{ Calculations, DataSetsAndCitations, TopicsData, Uploads }
import eu.nomad_lab.webservice_base.ResponseFormatter.{ InvalidRequestRejection, ResponseData }
import io.swagger.annotations._

import scala.concurrent.ExecutionContext
import scala.util.{ Failure, Success }

object OldApi {
  def apply(es: ESManager)(implicit exec: ExecutionContext): OldApi = {
    val topicsData = new TopicsData(es)
    val transform = new RequestTransformer(es, topicsData)
    val calculations = new Calculations(es)
    val references = new DataSetsAndCitations(es)
    val uploads = new Uploads(es)
    apply(es, transform, topicsData, calculations, references, uploads)
  }

  protected def apply(es: ESManager, transform: RequestTransformer, topicsData: TopicsData,
    calculations: Calculations, references: DataSetsAndCitations,
    uploads: Uploads)(implicit exec: ExecutionContext): OldApi = {
    new OldApi(es, transform, topicsData, calculations, references, uploads)
  }
}

@Api(
  value = "NOMAD Repository ElasticSearch interface for TopicID-based queries",
  produces = "vnd.api+json"
)
@Path("/old-api")
@ApiResponses(Array(
  new ApiResponse(code = 200, message = "operation successful"),
  new ApiResponse(code = 400, message = "invalid parameters for request"),
  new ApiResponse(code = 405, message = "invalid HTTP method"),
  new ApiResponse(code = 500, message = "Internal server error")
))
class OldApi private (
  private val es: ESManager,
  private val transform: RequestTransformer,
  private val topicsData: TopicsData,
  private val calculations: Calculations,
  private val references: DataSetsAndCitations,
  private val uploads: Uploads
)(implicit private val exec: ExecutionContext)
    extends Directives {
  import es.connector
  import eu.nomad_lab.webservice_base.ResponseFormatter.resolveQueryResult

  def routes: Route = pathPrefix("old-api") {
    concat(
      path("topics") {
        get {
          parameters('tids.?, 'only_selected_atoms.as[Boolean] ? false, 'data_source.?,
            'comment.?) {
            (tIds, onlySelectedAtoms, dataSource, comment) =>
              findPossibleTopics(tIds, onlySelectedAtoms, dataSource, comment)
          }
        }
      },
      path("datasets") {
        get {
          parameters('tids.?, 'only_selected_atoms.as[Boolean] ? false, 'data_source.?,
            'comment.?, 'upload_ids.?, 'num_groups.as[Int] ? 100) {
            (tIds, onlySelectedAtoms, dataSource, comment, uploads, numGroups) =>
              findMatchingDataSets(tIds, onlySelectedAtoms, dataSource, comment, uploads, numGroups)
          }
        }
      },
      path("uploads") {
        parameters('tids.?, 'only_selected_atoms.as[Boolean] ? false, 'data_source.?, 'comment.?,
          'datasets.?, 'group_id.?, 'num_groups.as[Int] ? 100) {
          (tIds, onlySelectedAtoms, dataSource, comment, dataSets, group, numResults) =>
            get {
              findMatchingUploads(tIds, onlySelectedAtoms, dataSource, comment, dataSets,
                group, numResults)
            }
        }
      },
      path("groups") {
        get {
          parameters('tids.?, 'only_selected_atoms.as[Boolean] ? false, 'data_source.?,
            'comment.?, 'datasets.?, 'num_groups.as[Int] ? 100, 'before.?, 'after.?) {
            (tIds, onlySelectedAtoms, dataSource, comment, dataSets, numGroups, before, after) =>
              findMatchingCalculationGroups(tIds, onlySelectedAtoms, dataSource, comment, dataSets,
                numGroups, before, after)
          }
        }
      },
      path("results") {
        parameters('tids.?, 'only_selected_atoms.as[Boolean] ? false, 'data_source.?, 'comment.?,
          'datasets.?, 'group_id.?, 'upload_ids.?, 'limit.as[Int] ? 100, 'before.as[Int].?, 'after.as[Int].?) {
          (tIds, onlySelectedAtoms, dataSource, comment, dataSets, group, uploads, numResults,
          before, after) =>
            get {
              findMatchingCalculations(tIds, onlySelectedAtoms, dataSource, comment, dataSets,
                group, uploads, numResults, before, after)
            }
        }
      },
      path("documents") {
        get {
          parameters('calc_ids.?, 'pids.?) { (calcIds, pIds) => findDocuments(calcIds, pIds) }
        }
      },
      path("facets") {
        get {
          parameters('tids.?, 'only_selected_atoms.as[Boolean] ? false, 'data_source.?,
            'comment.?) {
            (tIds, onlySelectedAtoms, dataSource, comment) =>
              findPossibleTopicsSimple(tIds, onlySelectedAtoms, dataSource, comment)
          }
        }
      }
    )
  }

  @GET @Path("/topics")
  @ApiOperation(
    value = "get all topics that yield a non-empty result when added to the given query"
  )
  @ApiResponses(Array(new ApiResponse(code = 200, message = "operation successful")))
  def findPossibleTopics(
    @ApiParam(value = "Topic IDs used for filtering")@QueryParam("tids") tIds: Option[String],
    @ApiParam(value = "do not match calculations with additional, not specified chemical elements?", defaultValue = "false")@QueryParam("only_selected_atoms") onlySelectedAtoms: Boolean,
    @ApiParam(value = "can be 'all' or '{uploader|coauthor|shared|staging},<userID> to select calculations based on access rights")@QueryParam("data_source") dataSource: Option[String],
    @ApiParam(value = "text filter applied to the calculation's comment")@QueryParam("comment") comment: Option[String]
  ): Route = onComplete {
    val query = transform.transformTIdRequest(tIds, onlySelectedAtoms,
      dataSource, comment, None, None, Seq(), None)
    val representation = query.map(connector.transformToQuery)
    val result = representation.map(topicsData.getFacets)
    result.flatMap(identity)
  }(resolveQueryResult(_))

  @GET @Path("/uploads")
  @ApiOperation(
    value = "get uploads containing calculations matching the given filter criteria"
  )
  @ApiResponses(Array(new ApiResponse(code = 200, message = "operation successful")))
  def findMatchingUploads(
    @ApiParam(value = "Topic IDs used for filtering")@QueryParam("tids") tIds: Option[String],
    @ApiParam(value = "do not match calculations with additional, not specified chemical elements?", defaultValue = "false")@QueryParam("only_selected_atoms") onlySelectedAtoms: Boolean,
    @ApiParam(value = "can be 'all' or '{uploader|coauthor|shared|staging},<userID> to select calculations based on access rights")@QueryParam("data_source") dataSource: Option[String],
    @ApiParam(value = "text filter applied to the calculation's comment")@QueryParam("comment") comment: Option[String],
    @ApiParam(value = "NOMAD repository data set (by ID) to filter for")@QueryParam("datasets") dataSets: Option[String],
    @ApiParam(value = "calculation group checksum to filter for")@QueryParam("group_id") group: Option[String],
    @ApiParam(value = "number of uploads to return")@QueryParam("num_groups") numResults: Int
  ): Route = onComplete {
    val query = transform.transformTIdRequest(tIds, onlySelectedAtoms,
      dataSource, comment, group, dataSets, Seq(), None)
    val representation = query.map(connector.transformToQuery)
    val result = representation.map(uploads.getUploads(_, numResults))
    result.flatMap(identity)
  }(resolveQueryResult(_))

  @GET @Path("/results")
  @ApiOperation(
    value = "get calculations matching the given filter criteria",
    notes = "the results are sorted by calculation ID, you can use the before/after parameters" +
    "to scroll through the results with multiple queries"
  )
  @ApiResponses(Array(new ApiResponse(code = 200, message = "operation successful")))
  def findMatchingCalculations(
    @ApiParam(value = "Topic IDs used for filtering")@QueryParam("tids") tIds: Option[String],
    @ApiParam(value = "do not match calculations with additional, not specified chemical elements?", defaultValue = "false")@QueryParam("only_selected_atoms") onlySelectedAtoms: Boolean,
    @ApiParam(value = "can be 'all' or '{uploader|coauthor|shared|staging},<userID> to select calculations based on access rights")@QueryParam("data_source") dataSource: Option[String],
    @ApiParam(value = "text filter applied to the calculation's comment")@QueryParam("comment") comment: Option[String],
    @ApiParam(value = "NOMAD repository data set (by ID) to filter for")@QueryParam("datasets") dataSets: Option[String],
    @ApiParam(value = "calculation group checksum to filter for")@QueryParam("group_id") group: Option[String],
    @ApiParam(value = "comma-separated list of upload IDs to filter for")@QueryParam("upload_ids") uploads: Option[String],
    @ApiParam(value = "number of calculations to return")@QueryParam("limit") numResults: Int,
    @ApiParam(value = "descending sort by calculation ID, only return calculations before the given ID", `type` = "integer")@QueryParam("before") before: Option[Int],
    @ApiParam(value = "ascending sort by calculation ID, only return calculations after the given ID", `type` = "integer")@QueryParam("after") after: Option[Int]
  ): Route = onComplete {
    val pagingFilter = PagingFilter("repository_calc_id", after, before)
    val uploadIds = uploads.map(_.split(",").map(_.toInt).toSeq).getOrElse(Seq())
    val query = transform.transformTIdRequest(tIds, onlySelectedAtoms,
      dataSource, comment, group, dataSets, uploadIds, Some(pagingFilter))
    val representation = query.map(connector.transformToQuery)
    val result = representation.map(calculations.getResults(_, numResults, pagingFilter))
    result.flatMap(identity).map(x => ResponseData(x.data.map(CalculationLegacy(_)), x.meta))
  }(resolveQueryResult(_))

  @GET @Path("/groups")
  @ApiOperation(
    value = "get calculation groups matching the given filter criteria",
    notes = "Calculation groups are sets of calculations which are equivalent on the subset of " +
    "information shown in the repository search results overview. The results are sorted by " +
    "group checksum, you can use the before/after parameters to scroll through the results with " +
    "multiple queries."
  )
  @ApiResponses(Array(new ApiResponse(code = 200, message = "operation successful")))
  def findMatchingCalculationGroups(
    @ApiParam(value = "Topic IDs used for filtering")@QueryParam("tids") tIds: Option[String],
    @ApiParam(value = "do not match calculations with additional, not specified chemical elements?", defaultValue = "false")@QueryParam("only_selected_atoms") onlySelectedAtoms: Boolean,
    @ApiParam(value = "can be 'all' or '{uploader|coauthor|shared|staging},userID' to select calculations based on access rights")@QueryParam("data_source") dataSource: Option[String],
    @ApiParam(value = "text filter applied to the calculation's comment")@QueryParam("comment") comment: Option[String],
    @ApiParam(value = "NOMAD repository data set (by ID) to filter for")@QueryParam("datasets") dataSets: Option[String],
    @ApiParam(value = "number of groups to return")@QueryParam("num_groups") numGroups: Int,
    @ApiParam(value = "descending sort by grouping checksum, only return groups before the given checksum")@QueryParam("before") before: Option[String],
    @ApiParam(value = "ascending sort by grouping checksum, only return groups after the given checksum")@QueryParam("after") after: Option[String]
  ): Route = onComplete {
    val pagingFilter = PagingFilter("repository_grouping_checksum", after, before)
    val query = transform.transformTIdRequest(tIds, onlySelectedAtoms,
      dataSource, comment, None, dataSets, Seq(), Some(pagingFilter))
    val representation = query.map(connector.transformToQuery)
    val result = representation.map(calculations.getGroups(_, numGroups, pagingFilter))
    result.flatMap(identity).map(x => ResponseData(x.data.map(CalculationGroupLegacy(_)), x.meta))
  }(resolveQueryResult(_))

  @GET @Path("/datasets")
  @ApiOperation(value = "get Data Sets which contain calculations matching the given filter criteria")
  @ApiResponses(Array(new ApiResponse(code = 200, message = "operation successful")))
  def findMatchingDataSets(
    @ApiParam(value = "Topic IDs used for filtering")@QueryParam("tids") tIds: Option[String],
    @ApiParam(value = "do not match calculations with additional, not specified chemical elements?", defaultValue = "false")@QueryParam("only_selected_atoms") onlySelectedAtoms: Boolean,
    @ApiParam(value = "can be 'all' or '{uploader|coauthor|shared|staging},userID' to select calculations based on access rights")@QueryParam("data_source") dataSource: Option[String],
    @ApiParam(value = "text filter applied to the calculation's comment")@QueryParam("comment") comment: Option[String],
    @ApiParam(value = "comma-separated list of upload IDs to filter for")@QueryParam("upload_ids") uploads: Option[String],
    @ApiParam(value = "number of data Sets to return")@QueryParam("num_groups") numGroups: Int
  ): Route = onComplete {
    val uploadIds = uploads.map(_.split(",").map(_.toInt).toSeq).getOrElse(Seq())
    val query = transform.transformTIdRequest(tIds, onlySelectedAtoms,
      dataSource, comment, None, None, uploadIds, None)
    val representation = query.map(connector.transformToQuery)
    val result = representation.map(references.getDataSets(_, numGroups))
    result.flatMap(identity).map(x => ResponseData(x.data.map(DataSetLegacy(_)), x.meta))
  }(resolveQueryResult(_))

  @GET @Path("/documents")
  @ApiOperation(value = "retrieve individual documents via calculation ID or PID")
  @ApiResponses(Array(new ApiResponse(code = 200, message = "operation successful")))
  def findDocuments(
    @ApiParam(value = "comma-separated list of calculation IDs to fetch")@QueryParam("calc_ids") calcIds: Option[String],
    @ApiParam(value = "comma-separated list of calculation PIDs to fetch")@QueryParam("pids") pIds: Option[String]
  ): Route = onComplete {
    val results = calculations.getDocuments(calcIds, pIds)
    results.map(x => ResponseData(x.data.map(CalculationLegacy(_)), x.meta))
  }(resolveQueryResult(_))

  @Deprecated @GET @Path("/facets")
  @ApiOperation(
    value = "get all topics that yield a non-empty result when added to the given query",
    notes = "this call provides the same information as topics with less details and no json:api formatting",
    produces = "json"
  )
  @ApiResponses(Array(new ApiResponse(code = 200, message = "operation successful")))
  def findPossibleTopicsSimple(
    @ApiParam(value = "Topic IDs used for filtering")@QueryParam("tids") tIds: Option[String],
    @ApiParam(value = "do not match calculations with additional, not specified chemical elements?", defaultValue = "false")@QueryParam("only_selected_atoms") onlySelectedAtoms: Boolean,
    @ApiParam(value = "can be 'all' or '{uploader|coauthor|shared|staging},<userID> to select calculations based on access rights")@QueryParam("data_source") dataSource: Option[String],
    @ApiParam(value = "text filter applied to the calculation's comment")@QueryParam("comment") comment: Option[String]
  ): Route = onComplete {
    val query = transform.transformTIdRequest(tIds, onlySelectedAtoms,
      dataSource, comment, None, None, Seq(), None)
    val representation = query.map(connector.transformToQuery)
    val result = representation.map(topicsData.getFacetsSimple)
    result.flatMap(identity)
  } {
    case Success(body) =>
      logRequest("served", Logging.InfoLevel) {
        val entity = HttpEntity(ContentTypes.`application/json`, body)
        complete(HttpResponse(status = StatusCodes.OK, entity = entity))
      }
    case Failure(ex: ApiCallException) => reject(InvalidRequestRejection(ex.getMessage))
    case Failure(ex) => throw ex
  }

}
