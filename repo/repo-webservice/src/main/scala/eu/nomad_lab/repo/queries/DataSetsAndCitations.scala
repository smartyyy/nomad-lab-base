/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo.queries

import com.sksamuel.elastic4s.http.ElasticDsl.{ termsAgg, topHitsAggregation, _ }
import com.sksamuel.elastic4s.searches.SearchDefinition
import com.sksamuel.elastic4s.searches.sort.FieldSortDefinition
import eu.nomad_lab.JsonUtils
import eu.nomad_lab.elasticsearch.ESManager
import eu.nomad_lab.repo.objects.JsonFormats.formats
import eu.nomad_lab.repo.objects._
import eu.nomad_lab.webservice_base.ResponseFormatter.ResponseData
import org.json4s.Extraction.decompose
import org.json4s.JsonAST.{ JArray, JValue }
import org.{ json4s => jn }

import scala.concurrent.{ ExecutionContext, Future }

class DataSetsAndCitations(private val es: ESManager)(implicit private val exec: ExecutionContext) {

  def getDataSets(representation: SearchDefinition, nGroups: Int): Future[ResponseData[DataSetResult]] = {
    import es.connector.metaNameToField

    val query = representation.aggregations(
      termsAgg("datasets", metaNameToField("dataset_calc_id")).size(nGroups.min(1000)).subaggs(
        topHitsAggregation("ref_docs").size(1).sortBy(
          FieldSortDefinition(metaNameToField("repository_calc_id"))
        ),
        termsAgg("access_levels", metaNameToField("repository_access_now"))
      )
    )
    es.client.execute(query).map { response =>
      val aggData = JsonUtils.parseStr(response.aggregationsAsString)
      val buckets = (aggData \ "datasets" \ "buckets").extract[Seq[JValue]]
      val dataSets = buckets.flatMap { dataSet =>
        val id = (dataSet \ "key").extract[Int]
        val count = (dataSet \ "doc_count").extract[Int]
        val sample = (dataSet \ "ref_docs" \ "hits" \ "hits")(0) \ "_source"
        val access = (dataSet \ "access_levels" \ "buckets").extract[JArray].arr.map { level =>
          AccessLevel.withName((level \ "key").extract[String]) -> (level \ "doc_count").extract[Int]
        }.toMap
        val calcDataSets = sample.extract[CalculationWrapper].section_repository_info.
          section_repository_userdata.section_repository_dataset
        calcDataSets.find(_.dataset_calc_id == id).map(x => DataSetResult(x, access, count))
      }
      val agg = response.termsAgg("datasets")
      val meta0 = Map[String, jn.JValue](
        "total_hits" -> decompose(response.totalHits),
        "is_timed_out" -> decompose(response.isTimedOut),
        "is_terminated_early" -> decompose(response.isTerminatedEarly),
        "doc_count_error_upper_bound" -> decompose(agg.docCountErrorUpperBound),
        "sum_other_doc_count" -> decompose(agg.otherDocCount)
      )
      ResponseData(dataSets, meta0)
    }
  }

  def getExternalCitations(representation: SearchDefinition, nGroups: Int): Future[ResponseData[CitationResult]] = {
    import es.connector.metaNameToField

    val query = representation.aggregations(
      //      termsAgg("citations", metaNameToField("citation_repo_id")).size(nGroups.min(1000)).subaggs(
      //        topHitsAggregation("ref_docs").size(1).
      //          sortBy(FieldSortDefinition(metaNameToField("repository_calc_id"))),
      //      termsAgg("access_levels", metaNameToField("repository_access_now"))
      //      )
      //FIXME: the exclusion of Materials Project is a temporary hack until the DB is cleaned up
      termsAgg("citations", metaNameToField("citation_value")).size(nGroups.min(1000)).subaggs(
        topHitsAggregation("ref_docs").size(1).
          sortBy(FieldSortDefinition(metaNameToField("repository_calc_id"))),
        termsAgg("access_levels", metaNameToField("repository_access_now"))
      ).includeExclude("", """https://materialsproject\.org/.*""")
    )
    es.client.execute(query).map { response =>
      val aggData = JsonUtils.parseStr(response.aggregationsAsString)
      val buckets = (aggData \ "citations" \ "buckets").extract[Seq[JValue]]
      val citations = buckets.flatMap { citation =>
        //        val id = (citation \ "key").extract[Int]
        val id = (citation \ "key").extract[String]
        val count = (citation \ "doc_count").extract[Int]
        val sample = (citation \ "ref_docs" \ "hits" \ "hits")(0) \ "_source"
        val access = (citation \ "access_levels" \ "buckets").extract[JArray].arr.map { level =>
          AccessLevel.withName((level \ "key").extract[String]) -> (level \ "doc_count").extract[Int]
        }.toMap
        val citationList = sample.extract[CalculationWrapper].section_repository_info.
          section_repository_userdata.section_citation
        citationList.find(_.citation_value == id).map(x => CitationResult(x, access, count))
        //        citationList.find(_.citation_repo_id == id).map(x => CitationResult(x, access, count))
      }
      val agg = response.termsAgg("citations")
      val meta0 = Map[String, jn.JValue](
        "total_hits" -> decompose(response.totalHits),
        "is_timed_out" -> decompose(response.isTimedOut),
        "is_terminated_early" -> decompose(response.isTerminatedEarly),
        "doc_count_error_upper_bound" -> decompose(agg.docCountErrorUpperBound),
        "sum_other_doc_count" -> decompose(agg.otherDocCount)
      )
      ResponseData(citations, meta0)
    }
  }

  def getDataSetDois(representation: SearchDefinition, nGroups: Int): Future[ResponseData[DataSetDoiResult]] = {
    import es.connector.metaNameToField

    val query = representation.aggregations(
      termsAgg("dataset_dois", metaNameToField("dataset_doi_id")).size(nGroups.min(1000)).subaggs(
        topHitsAggregation("ref_docs").size(1).sortBy(
          FieldSortDefinition(metaNameToField("repository_calc_id"))
        ),
        termsAgg("access_levels", metaNameToField("repository_access_now"))
      )
    )
    es.client.execute(query).map { response =>
      val aggData = JsonUtils.parseStr(response.aggregationsAsString)
      val buckets = (aggData \ "dataset_dois" \ "buckets").children
      val doiEntries = buckets.flatMap { doiHit =>
        val id = (doiHit \ "key").extract[Int]
        val count = (doiHit \ "doc_count").extract[Int]
        val sample = (doiHit \ "ref_docs" \ "hits" \ "hits")(0) \ "_source"
        val access = (doiHit \ "access_levels" \ "buckets").extract[JArray].arr.map { level =>
          AccessLevel.withName((level \ "key").extract[String]) -> (level \ "doc_count").extract[Int]
        }.toMap
        val allDataSets = sample.extract[CalculationWrapper].section_repository_info.
          section_repository_userdata.section_repository_dataset
        val dataSet = allDataSets.find(_.section_dataset_doi.exists(_.dataset_doi_id == id))
        val doi = dataSet.flatMap(_.section_dataset_doi.find(_.dataset_doi_id == id)).get
        dataSet.map(DataSetDoiResult(doi, _, access, count))
      }
      val agg = response.termsAgg("dataset_dois")
      val meta0 = Map[String, jn.JValue](
        "total_hits" -> decompose(response.totalHits),
        "is_timed_out" -> decompose(response.isTimedOut),
        "is_terminated_early" -> decompose(response.isTerminatedEarly),
        "doc_count_error_upper_bound" -> decompose(agg.docCountErrorUpperBound),
        "sum_other_doc_count" -> decompose(agg.otherDocCount)
      )
      ResponseData(doiEntries, meta0)
    }
  }
}
