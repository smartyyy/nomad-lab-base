/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo

import javax.ws.rs.{ GET, Path, QueryParam }

import akka.http.scaladsl.server.{ Directives, Route }
import eu.nomad_lab.elasticsearch.ESManager
import eu.nomad_lab.repo.RequestTransformer.PagingFilter
import eu.nomad_lab.repo.objects.{ CalculationGroupLegacy, CalculationLegacy }
import eu.nomad_lab.repo.queries.{ Calculations, DataSetsAndCitations, TopicsData }
import eu.nomad_lab.webservice_base.ResponseFormatter.ResponseData
import io.swagger.annotations._

import scala.concurrent.{ ExecutionContext, Future }

object NqlSearchApi {
  def apply(es: ESManager)(implicit exec: ExecutionContext): NqlSearchApi = {
    val topicsData = new TopicsData(es)
    val transform = new RequestTransformer(es, topicsData)
    val calculations = new Calculations(es)
    val references = new DataSetsAndCitations(es)
    apply(es, transform, calculations, references)
  }

  protected def apply(es: ESManager, transform: RequestTransformer, calculations: Calculations,
    references: DataSetsAndCitations)(implicit exec: ExecutionContext): NqlSearchApi = {
    new NqlSearchApi(es, transform, calculations, references)
  }
}

@Api(
  value = "NQL (Nomad Query Language) based search API",
  produces = "vnd.api+json"
)
@Path("/search")
@ApiResponses(Array(
  new ApiResponse(code = 200, message = "operation successful"),
  new ApiResponse(code = 400, message = "invalid parameters for request"),
  new ApiResponse(code = 405, message = "invalid HTTP method"),
  new ApiResponse(code = 500, message = "Internal server error")
))
class NqlSearchApi private (
  private val es: ESManager,
  private val transform: RequestTransformer,
  private val calculations: Calculations,
  private val references: DataSetsAndCitations
)(implicit private val exec: ExecutionContext)
    extends Directives {
  import es.connector
  import eu.nomad_lab.webservice_base.ResponseFormatter.resolveQueryResult

  def routes: Route = pathPrefix("search") {
    concat(
      path("calculation_groups_oldformat") {
        get {
          parameters('query.?, 'num_results.as[Int] ? 100, 'before.?, 'after.?) {
            (query, numResults, before, after) =>
              findCalculationGroupsOldFormat(query, numResults, before, after)
          }
        }
      },
      path("calculations_oldformat") {
        get {
          parameters('query.?, 'num_results.as[Int] ? 100, 'before.as[Int].?, 'after.as[Int].?) {
            (query, numResults, before, after) =>
              findCalculationsOldFormat(query, numResults, before, after)
          }
        }
      },
      path("citations") {
        get {
          parameters('query.?, 'num_results.as[Int] ? 100) { (query, numResults) =>
            findCitations(query, numResults)
          }
        }
      },
      path("datasets") {
        get {
          parameters('query.?, 'num_results.as[Int] ? 100) { (query, numResults) =>
            findDataSets(query, numResults)
          }
        }
      },
      path("dataset_dois") {
        get {
          parameters('query.?, 'num_results.as[Int] ? 100) { (query, numResults) =>
            findDataSetDois(query, numResults)
          }
        }
      }
    )
  }

  @GET @Path("/calculations_oldformat")
  @ApiOperation(value = "list calculations matching the given filter (uses an old response format)")
  @ApiResponses(Array(new ApiResponse(code = 200, message = "operation successful")))
  def findCalculationsOldFormat(
    @ApiParam(value = "NQL filter expression")@QueryParam("query") query: Option[String],
    @ApiParam(value = "number of calculations to return")@QueryParam("num_results") numResults: Int,
    @ApiParam(value = "descending sort by calculation ID, only return calculations before the given ID", `type` = "integer")@QueryParam("before") before: Option[Int],
    @ApiParam(value = "ascending sort by calculation ID, only return calculations after the given ID", `type` = "integer")@QueryParam("after") after: Option[Int]
  ): Route = onComplete {
    val pagingFilter = PagingFilter("repository_calc_id", after, before)
    val representation = Future {
      val paging = transform.getPagingFilter(Some(pagingFilter))
      val fullQuery = if (paging.nonEmpty || query.nonEmpty)
        Some(Seq(query, paging).flatten.mkString(" AND "))
      else
        None
      connector.transformToQuery(fullQuery)
    }
    val result = representation.map(calculations.getResults(_, numResults, pagingFilter))
    result.flatMap(identity).map(x => ResponseData(x.data.map(CalculationLegacy(_)), x.meta))
  }(resolveQueryResult(_))

  @GET @Path("/calculation_groups_oldformat")
  @ApiOperation(value = "list calculations groups matching the given filter (uses an old response format)")
  @ApiResponses(Array(new ApiResponse(code = 200, message = "operation successful")))
  def findCalculationGroupsOldFormat(
    @ApiParam(value = "NQL filter expression")@QueryParam("query") query: Option[String],
    @ApiParam(value = "number of calculations to return")@QueryParam("num_results") numResults: Int,
    @ApiParam(value = "descending sort by group checksum, only return groups before the given checksum", `type` = "integer")@QueryParam("before") before: Option[String],
    @ApiParam(value = "ascending sort by group checksum, only return groups after the given checksum", `type` = "integer")@QueryParam("after") after: Option[String]
  ): Route = onComplete {
    val pagingFilter = PagingFilter("repository_grouping_checksum", after, before)
    val representation = Future {
      val paging = transform.getPagingFilter(Some(pagingFilter))
      val fullQuery = if (paging.nonEmpty || query.nonEmpty)
        Some(Seq(query, paging).flatten.mkString(" AND "))
      else
        None
      connector.transformToQuery(fullQuery)
    }
    val result = representation.map(calculations.getGroups(_, numResults, pagingFilter))
    result.flatMap(identity).map(x => ResponseData(x.data.map(CalculationGroupLegacy(_)), x.meta))
  }(resolveQueryResult(_))

  @GET @Path("/citations")
  @ApiOperation(value = "list external citations assigned to calculations matching the given filter")
  @ApiResponses(Array(new ApiResponse(code = 200, message = "operation successful")))
  def findCitations(
    @ApiParam(value = "NQL filter expression")@QueryParam("query") query: Option[String],
    @ApiParam(value = "number of citations to return")@QueryParam("num_results") numResults: Int
  ): Route = onComplete {
    val representation = Future { connector.transformToQuery(query) }
    val result = representation.map(references.getExternalCitations(_, numResults))
    result.flatMap(identity)
  }(resolveQueryResult(_))

  @GET @Path("/datasets")
  @ApiOperation(value = "list data sets which contain calculations matching the given filter")
  @ApiResponses(Array(new ApiResponse(code = 200, message = "operation successful")))
  def findDataSets(
    @ApiParam(value = "NQL filter expression")@QueryParam("query") query: Option[String],
    @ApiParam(value = "number of data sets to return")@QueryParam("num_results") numResults: Int
  ): Route = onComplete {
    val representation = Future { connector.transformToQuery(query) }
    val result = representation.map(references.getDataSets(_, numResults))
    result.flatMap(identity)
  }(resolveQueryResult(_))

  @GET @Path("/dataset_dois")
  @ApiOperation(value = "list DOI-tagged NOMAD data sets with calculations matching the given filter")
  @ApiResponses(Array(new ApiResponse(code = 200, message = "operation successful")))
  def findDataSetDois(
    @ApiParam(value = "NQL filter expression")@QueryParam("query") query: Option[String],
    @ApiParam(value = "number of DOIs to return")@QueryParam("num_results") numResults: Int
  ): Route = onComplete {
    val representation = Future { connector.transformToQuery(query) }
    val result = representation.map(references.getDataSetDois(_, numResults))
    result.flatMap(identity)
  }(resolveQueryResult(_))

}
