/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo

import javax.ws.rs.{ POST, Path, QueryParam }

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.server.{ Directives, Route }
import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.elasticsearch.{ ApiCallException, ESManager }
import eu.nomad_lab.repo.objects.Helpers.CalculationId
import eu.nomad_lab.webservice_base.ResponseFormatter.ResponseData
import io.swagger.annotations._
import spray.json.{ DefaultJsonProtocol, RootJsonFormat }

import scala.concurrent.{ ExecutionContext, Future }
import scala.util.control.NonFatal

object UpdateApi {
  def apply(es: ESManager)(implicit exec: ExecutionContext): UpdateApi = {
    new UpdateApi(es)
  }
}

@Api(
  value = "Update API for reloading calculation data",
  produces = "text"
)
@Path("/repo-update/index")
@ApiResponses(Array(
  new ApiResponse(code = 200, message = "operation successful"),
  new ApiResponse(code = 400, message = "invalid parameters for request"),
  new ApiResponse(code = 405, message = "invalid HTTP method"),
  new ApiResponse(code = 500, message = "Internal server error")
))
class UpdateApi private (private val es: ESManager)(implicit private val exec: ExecutionContext)
    extends Directives with StrictLogging with DefaultJsonProtocol with SprayJsonSupport {
  import eu.nomad_lab.webservice_base.ResponseFormatter.resolveQueryResult
  private implicit val refreshRequestFormat: RootJsonFormat[RefreshRequest] = jsonFormat1(RefreshRequest)
  private implicit val calculationIdFormat: RootJsonFormat[CalculationId] = jsonFormat1(CalculationId)

  private var repoDb: Option[RepoDb] = None

  //try to connect to DBs in the background, if this fails we try to connect again later on demand
  Future { connectToDb() }

  def routes: Route = pathPrefix("repo-update" / "index") {
    path("calculation") {
      post {
        entity(as[RefreshRequest]) { content =>
          updateCalculation(content)
        }
      }
    }
  }

  def repoDB: RepoDb = {
    if (repoDb.isEmpty) connectToDb()
    repoDb.get
  }

  private def connectToDb(): Unit = {
    synchronized(
      if (repoDb.isEmpty) {
        try {
          repoDb = Some(RepoDb())
        } catch {
          case NonFatal(e) =>
            if (repoDb.nonEmpty) {
              try {
                repoDb.get.dbContext.close()
              } catch {
                case NonFatal(_) =>
              }
            }
            throw e
        }
      }
    )
  }

  private def closeDBConnection(): Unit = {
    synchronized(
      try {
        repoDb.foreach(_.dbContext.close)
        repoDb = None
      } catch {
        case NonFatal(e) => logger.error("failed to close DB connection", e)
      }
    )
  }

  @POST @Path("/calculation")
  @ApiOperation(value = "reload the given calculations from the Postgres database")
  @ApiResponses(Array(new ApiResponse(code = 200, message = "operation successful")))
  def updateCalculation(
    @ApiParam(value = "list of calculation IDs to update", required = true) request: RefreshRequest
  ): Route = {
    onComplete {
      try {
        val calcIds = request.calc_ids.map(CalculationId(_))
        if (calcIds.nonEmpty) {
          try {
            insertCalculations(calcIds)
          } catch {
            case NonFatal(_) => try {
              closeDBConnection()
              connectToDb()
              insertCalculations(calcIds)
            } catch {
              case NonFatal(e) =>
                Future.failed(new UnsupportedOperationException("cannot connect to database", e))
            }
          }
        } else {
          Future.failed(ApiCallException("no calculation Ids given"))
        }
      } catch {
        case NonFatal(e) => Future.failed(ApiCallException(s"could not parse calculation Ids: $e"))
      }
    }(resolveQueryResult(_))
  }

  private def insertCalculations(calcIds: Seq[CalculationId]): Future[ResponseData[UpsertResponse]] = {
    val topicInsertion = TopicElasticInserter(repoDB, es,
      indexName = Some(es.settings.typeNameTopics))
    val lastTopic = es.findLastId(es.settings.indexNameTopics, es.settings.typeNameTopics,
      "repository_topic_id")
    val topicsFuture = lastTopic.map(x => topicInsertion.indexTopics(x.toInt))
    val ingestion = new ElasticSearchIngestion(repoDb.get, es, es.settings.indexNameData)
    val results = ingestion.updateCalculationUserDataByCalcId(calcIds)

    //we're zipping with the topics here to ensure that both concurrent updates are finished
    topicsFuture.zip(results).map { x => ResponseData(Seq(x._2._1), x._2._2) }
  }
}

