/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo.objects

import eu.nomad_lab.jsonapi.BaseValue
import eu.nomad_lab.repo.objects.JsonFormats.formats
import org.json4s.Extraction.decompose
import org.{ json4s => jn }

case class CalculationGroup(sample: CalculationWrapper, numDocuments: Int) extends BaseValue {

  override def typeStr: String = "CalculationGroup"

  override def idStr: String = sample.section_repository_info.repository_grouping_checksum

  override def attributes: Map[String, jn.JValue] = Map(
    "num_calculations" -> decompose(numDocuments),
    "section_repository_info" -> decompose(sample.section_repository_info.filteredMap)
  )
}