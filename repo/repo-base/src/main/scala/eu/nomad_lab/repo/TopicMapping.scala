/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo
import eu.nomad_lab.repo.db.{ Tables => REPO }
import org.jooq.impl.DSL
import collection.JavaConverters._
import scala.collection.breakOut
import scala.collection.Traversable

/**
 * Description and classifications of the tags assigned to the calculation.
 *
 * This class maps tags id (tid) to the string based identifiers used in
 * elastic search and viceversa, caching the information of the database.
 */
class TopicMapping(
    repoDb: RepoDb
) {
  var cachedTags: Map[Int, Topic] = Map()
  var userIdToTagCache: Map[Int, Topic] = Map()
  var keyValueToTidCache: Map[(String, String), Topic] = Map()
  var atoms: Set[Topic] = Set()

  /**
   * Internal registration of a tag (to build the key value -> tid map)
   */
  def registerTag(tag: Topic): Unit = {
    cachedTags += tag.repository_topic_id -> tag
    tag.maybeKey match {
      case None => ()
      case Some(key) =>
        if (key == "section_author_info")
          userIdToTagCache += tag.userId -> tag
        else
          keyValueToTidCache += key -> tag.value -> tag
    }
    if (tag.repository_topic_category_id == 10)
      atoms += tag
  }

  /**
   * Makes sure that tags have been loaded at least once
   */
  def ensureLoad(): Unit = {
    if (cachedTags.isEmpty)
      loadAllTags
  }

  /**
   * Loads all tags
   */
  def loadAllTags(): Unit = {
    val tps = repoDb.dbContext.select(
      REPO.TOPICS.TOPIC, REPO.TOPICS.TID, REPO.TOPICS.CID
    ).from(
      REPO.TOPICS
    ).fetch()
    for (r <- tps.asScala) {
      registerTag(Topic(
        r.getValue(REPO.TOPICS.TOPIC),
        r.getValue(REPO.TOPICS.CID).toInt,
        r.getValue(REPO.TOPICS.TID).toInt
      ))
    }
  }

  /**
   * Returns the tag with the given tids
   */
  def tagWithTid(tid: Int): Topic = {
    if (cachedTags.isEmpty)
      loadAllTags
    cachedTags.get(tid) match {
      case Some(t) => t
      case None =>
        val r = repoDb.dbContext.select(
          REPO.TOPICS.TOPIC, REPO.TOPICS.TID, REPO.TOPICS.CID
        ).from(
          REPO.TOPICS
        ).where(
          REPO.TOPICS.TID.equal(new Integer(tid))
        ).fetchOne()
        if (r == null) {
          throw new Exception(s"Could not find tag with tid $tid")
        } else {
          val newTag = Topic(r.getValue(REPO.TOPICS.TOPIC), r.getValue(REPO.TOPICS.CID).toInt, r.getValue(REPO.TOPICS.TID).toInt)
          registerTag(newTag)
          newTag
        }
    }
  }

  /**
   * maps an array of tids (os returned by the db) to a key -> tags map
   *
   * Tags without key are discarded
   */
  def classifyTags(tags: Traversable[Int]): Map[String, Seq[Topic]] = {
    tags.foldLeft(Map[String, Seq[Topic]]()) {
      case (res, n) =>
        val tid = n.toInt
        val tag = tagWithTid(tid)
        tag.maybeKey match {
          case None => res
          case Some(key) =>
            res + (key -> (res.getOrElse(key, Seq()) :+ tag))
        }
    }.map { case (k, v) => k -> v.sortBy(_.value) }
  }

  /**
   * returns the tag corresponding to a user
   */
  def tagWithUserId(userId: Int): Option[Topic] = {
    if (cachedTags.isEmpty)
      loadAllTags
    userIdToTagCache.get(userId) match {
      case Some(tag) => Some(tag)
      case None =>
        val r = repoDb.dbContext.select(
          REPO.TOPICS.TOPIC, REPO.TOPICS.TID, REPO.TOPICS.CID
        ).from(
          REPO.TOPICS
        ).where(
          REPO.TOPICS.TOPIC.like(s"%<sup class=hdn>$userId</sup>")
        ).fetchOne()
        if (r == null) {
          None
        } else {
          val newTag = Topic(r.getValue(REPO.TOPICS.TOPIC), r.getValue(REPO.TOPICS.CID).toInt, r.getValue(REPO.TOPICS.TID).toInt)
          registerTag(newTag)
          Some(newTag)
        }
    }
  }

  /**
   * maps a key, value pair back to tid
   *
   * Does not cover users i.e. key == "section_author_info", but does cover "section_author_info.author_repo_id"
   */
  def tagWithKeyValue(key: String, value: String): Option[Topic] = {
    if (cachedTags.isEmpty)
      loadAllTags
    if (key == "section_author_info.author_repo_id") {
      tagWithUserId(value.toInt)
    } else {
      keyValueToTidCache.get(key -> value) match {
        case Some(tag) => Some(tag)
        case None =>
          val originalName = Topic.tagRestore.getOrElse(value, value)
          val r = repoDb.dbContext.select(REPO.TOPICS.TOPIC, REPO.TOPICS.TID, REPO.TOPICS.CID).from(REPO.TOPICS).where(REPO.TOPICS.TOPIC.equal(DSL.inline(originalName))).fetchOne()
          if (r == null) {
            throw new Exception(s"Could not find topic with name $originalName when resolving $key $value")
          } else {
            val newTag = Topic(r.getValue(REPO.TOPICS.TOPIC), r.getValue(REPO.TOPICS.CID).toInt, r.getValue(REPO.TOPICS.TID).toInt)
            registerTag(newTag)
            Some(newTag)
          }
      }
    }
  }
}
