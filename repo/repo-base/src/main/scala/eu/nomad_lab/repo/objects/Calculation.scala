/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo.objects

import com.sksamuel.elastic4s.http.{ ElasticDsl => ES }
import com.sksamuel.elastic4s.mappings.dynamictemplate.DynamicMapping
import com.sksamuel.elastic4s.mappings.{ FieldDefinition, MappingDefinition }
import eu.nomad_lab.{ Base32, CompactSha, LocalEnv }
import org.{ json4s => jn }

object Calculation {

  val typeName = LocalEnv.defaultConfig.getString("nomad_lab.repo.elastic.typeNameData")
  val sectionName = "section_repository_info"

  /**
   * definition of the field contained in a calculation
   */
  def fieldDefinition: FieldDefinition = {
    ES.objectField(sectionName).fields(
      Seq(
        ES.keywordField("main_file_uri"),
        ES.keywordField("secondary_file_uris"),
        ES.keywordField("repository_filepaths").index(false),
        ES.keywordField("repository_archive_gid"),
        ES.longField("repository_calc_id").stored(true),
        ES.keywordField("repository_calc_pid").stored(true),
        ES.longField("upload_id"),
        ES.dateField("upload_date").format("epoch_millis"),
        ES.keywordField("repository_grouping_checksum"),
        Uploader.fieldDefinition,
        CalculationParserData.fieldDefinition,
        CalculationUserData.fieldDefinition
      )
    )
  }

  /**
   * Mapping (index definition) of a calculation
   */
  def mapping: MappingDefinition = {
    ES.mapping(typeName).fields(fieldDefinition).dynamic(DynamicMapping.Strict)
  }

  def groupingChecksum(pData: CalculationParserData, mData: CalculationUserData): String = {
    val s = CompactSha()
    val entrySplit = " | "
    val elementSplit = " # "
    val entries = Seq(
      pData.repository_chemical_formula.map(_.toString).sorted,
      pData.repository_spacegroup_nr.map(_.toString).sorted,
      pData.repository_basis_set_type.map(_.toString).sorted,
      pData.repository_xc_treatment.map(_.toString).sorted,
      pData.repository_code_version.map(_.toString).sorted,
      Seq(mData.repository_access_now.toString),
      mData.section_citation.map(_.toString).sorted,
      Seq(mData.repository_comment.map(_.toString)).flatten,
      mData.section_author_info.map(_.toString).sorted
    )
    entries.foreach { data =>
      data.sorted.foreach { x => s.updateStr(x); s.updateStr(elementSplit) }
      s.updateStr(entrySplit)
    }
    s.gidStr("g")
  }
}

/**
 * Information on a calculation in the repository
 */
case class Calculation(
    main_file_uri: Option[String], //some repository calculations are ignored in the Archive
    secondary_file_uris: Seq[String],
    repository_filepaths: Seq[String],
    repository_archive_gid: Seq[String], //FIXME: should be a single entry, fix in DBs...
    repository_calc_id: Long,
    repository_calc_pid: String,
    upload_id: Int,
    section_uploader_info: Uploader,
    upload_date: Option[Long], //milliseconds since Epoch, some entries don't have a valid value
    section_repository_parserdata: CalculationParserData,
    section_repository_userdata: CalculationUserData,
    repository_grouping_checksum: String
) extends MappableBaseValue {

  override def typeStr = Calculation.sectionName

  override def idStr = repository_calc_id.toString

  def upload_pid: String = {
    if (upload_id >= 0)
      Base32.b32NrRepository(upload_id)
    else
      ""
  }

  def toMap: Map[String, Any] = {
    Map(
      "main_file_uri" -> main_file_uri,
      "secondary_file_uris" -> secondary_file_uris,
      "repository_filepaths" -> repository_filepaths,
      "repository_archive_gid" -> repository_archive_gid,
      "repository_calc_id" -> repository_calc_id,
      "repository_calc_pid" -> repository_calc_pid,
      "upload_id" -> upload_id,
      "section_uploader_info" -> section_uploader_info.filteredMap,
      "upload_date" -> upload_date,
      "section_repository_parserdata" -> section_repository_parserdata.filteredMap,
      "section_repository_userdata" -> section_repository_userdata.filteredMap,
      "repository_grouping_checksum" -> repository_grouping_checksum
    )
  }
}

case class CalculationWrapper(section_repository_info: Calculation)