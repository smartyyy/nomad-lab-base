package eu.nomad_lab.repo.objects

import eu.nomad_lab.jsonapi.BaseValue
import eu.nomad_lab.repo.objects.AccessLevel.AccessLevel
import org.{ json4s => jn }
import eu.nomad_lab.repo.objects.JsonFormats.formats
import org.json4s.Extraction.decompose

object Upload {
  val sectionName: String = "section_upload_info"
}

case class Upload(
    repo_upload_id: Int,
    section_uploader_info: Uploader,
    repo_upload_date: Long,
    repo_upload_access: Option[AccessLevel] //TODO: not yet stored in the index
) extends MappableBaseValue {
  override def typeStr: String = Upload.sectionName

  override def idStr: String = repo_upload_id.toString

  override def toMap: Map[String, Any] = {
    Map(
      "repo_upload_id" -> repo_upload_id,
      "section_uploader_info" -> section_uploader_info,
      "repo_upload_date" -> repo_upload_date,
      "repo_upload_access" -> repo_upload_access
    )
  }
}

/**
 * This class represents an Upload as a search response, while
 * [[eu.nomad_lab.repo.objects.Upload]] is the representation of a data set as part of the
 * calculation data structure.
 * @param upload the actual Upload
 * @param documentAccessLevels maps access levels to the number of contained calculations
 * @param numDocuments number of calculations belonging to this upload (filtered results only)
 */
case class UploadResult(upload: Upload, documentAccessLevels: Map[AccessLevel, Int],
    numDocuments: Int) extends BaseValue {

  override def typeStr: String = "UploadResult"

  override def idStr: String = upload.repo_upload_id.toString

  override def attributes: Map[String, jn.JValue] = Map(
    "num_calculations" -> decompose(numDocuments),
    "access_levels" -> decompose(documentAccessLevels.map(x => x._1.toString -> x._2)),
    "section_upload_info" -> decompose(upload.filteredMap)
  )
}