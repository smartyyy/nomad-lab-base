/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo

import com.sksamuel.elastic4s.IndexAndType
import com.sksamuel.elastic4s.get.GetDefinition
import com.sksamuel.elastic4s.http.ElasticDsl._
import com.sksamuel.elastic4s.json4s.ElasticJson4s.Implicits.Json4sHitReader
import eu.nomad_lab.elasticsearch.ESManager
import eu.nomad_lab.repo.objects.JsonFormats.formats
import eu.nomad_lab.repo.objects._
import org.json4s.Extraction.decompose
import org.json4s.native.Serialization
import org.{ json4s => jn }

import scala.concurrent.{ ExecutionContext, Future }

class ElasticSearchCalculationFetcher(private val es: ESManager)(implicit exec: ExecutionContext) {

  def getCalculations(documentIds: Seq[String]): Future[(Seq[CalculationWrapper], Map[String, jn.JValue])] = {
    val index = IndexAndType(es.settings.indexNameData, es.settings.typeNameData)
    val request = multiget(documentIds.map(id => GetDefinition(index, id)))
    es.client.execute(request).map { response =>
      val jV = Json4sHitReader[CalculationWrapper](Serialization, formats,
        manifest[CalculationWrapper])
      val docs = response.docs.filter(_.found).map(_.to[CalculationWrapper](jV))
      val meta = Map[String, jn.JValue](
        "total_docs" -> decompose(response.docs.length),
        "docs_found" -> decompose(response.docs.count(_.found)),
        "docs_missing" -> decompose(response.docs.count(!_.found))
      )
      (docs, meta)
    }
  }
}
