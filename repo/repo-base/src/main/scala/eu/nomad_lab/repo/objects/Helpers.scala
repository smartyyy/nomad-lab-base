package eu.nomad_lab.repo.objects

/**
 * This helper objects host a few value classes used to highlight the key identifiers in the NOMAD
 * Repository
 */
object Helpers {

  case class CalculationId(id: Long) extends AnyVal

  case class MainFileUri(uri: String) extends AnyVal
}
