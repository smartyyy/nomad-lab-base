/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.rawdata_injection

import java.io.File

import com.typesafe.scalalogging.StrictLogging
import java.nio.file.Path
import java.nio.file.Paths

object DuTools {

  val usage = """
Usage:
  rawDataTool [--help] [--du-h base/dir/path] [--ls-file path/to/result/of/ls] [--chunk-range fromChunkIndex untilChunkIndex] [--max-depth depth(Max 100)] [--write write results to file name. Default: Skip over existing entries]

command line interface to the raw data injection process
              """

  def main(args: Array[String]): Unit = {
    /*if (args.length == 0) {
      println(usage)
      return
    }*/
    var list: List[String] = args.toList
    var duPath: Option[Path] = None
    var fileToWrite: Option[java.io.File] = None
    var lsFile: Option[java.io.File] = None
    var fromChunkIndex: Int = 0
    var untilChunkIndex: Int = Int.MaxValue
    var maxDepth: Int = 100
    while (!list.isEmpty) {
      val arg = list.head
      list = list.tail
      arg match {
        case "--help" | "-h" =>
          println(usage)
          return
        case "--du-h" =>
          if (list.isEmpty) {
            println(s"Error: expected a base path after --du-h.$usage")
            return
          }
          duPath = Some(Paths.get(list.head))
          list = list.tail
        case "--ls-file" =>
          if (list.isEmpty) {
            println(s"Error: expected path to the result of ls --ls-file. $usage")
            return
          }
          lsFile = Some(new File(list.head))
          list = list.tail
        case "--max-depth" =>
          if (list.isEmpty) {
            println(s"Error: expected a base path after --du-h.$usage")
            return
          }
          val uD = list.head.toInt
          if (maxDepth > uD)
            maxDepth = uD
          list = list.tail
        case "--chunk-range" =>
          if (list.isEmpty) {
            println(s"Error: expected a fromChunkIndex after --chunk-range.$usage")
            return
          }
          fromChunkIndex = list.head.toInt
          list = list.tail
          if (list.isEmpty) {
            println(s"Error: expected a toChunkIndex after fromChunkIndex after --chunk-range.$usage")
            return
          }
          untilChunkIndex = list.head.toInt
          list = list.tail
        case "--write" =>
          if (list.isEmpty) {
            println(s"Error: expected a file name to write to after --write. $usage")
            return
          }
          fileToWrite = Some(Paths.get(list.head).toFile)
          FileSizeSupport.entriesInFile(fileToWrite.get)
          list = list.tail
        case _ =>
          println(s"Error: unexpected argument $arg.$usage")
          return
      }
    }
    duPath match {
      case Some(basePath) =>
        val baseDir = basePath.toFile
        val kb = 1024.0
        println(s"${baseDir.getAbsolutePath}")
        val cFiles: List[String] = lsFile match {
          case Some(f) =>
            import scala.io.Source
            val rLines = Source.fromFile(f).getLines
            if (rLines.isEmpty) { println(s"Error: File $f does not contain any data"); sys.exit() }
            val readLines = rLines.toList
            if (readLines.size < untilChunkIndex)
              readLines.map(fName => basePath.toString + "/" + fName)
            else {
              val lTake = readLines.take(untilChunkIndex)
              lTake.takeRight(untilChunkIndex - fromChunkIndex).map(fName => basePath.toString + "/" + fName)
            }
          case None =>
            val lFiles = baseDir.listFiles
            if (lFiles.nonEmpty) lFiles.map(cf => cf.getAbsolutePath).toList
            else List()
        }
        if (!FileSizeSupport.existingFileSizeEntries.contains(baseDir.getPath) && baseDir.isDirectory && cFiles.nonEmpty) {
          val size = FileSizeSupport.getDuSize(cFiles, 1, maxDepth, fileToWrite) // + FileSizeSupport.sizeOfExistingEntries()
          val (symb, dv) = FileSizeSupport.getAppropriateSizeAndSymbol(size)
          val duString = s"${dv}$symb\t${baseDir}"
          FileSizeSupport.writeSizeToBuffer(duString, fileToWrite)
          FileSizeSupport.writeSizesToDisk(fileToWrite)
        } else if (baseDir.isFile) {
          val size = FileSizeSupport.getDuSize(List(basePath.toString), 1, 1, fileToWrite)
          val (symb, dv) = FileSizeSupport.getAppropriateSizeAndSymbol(size)
          val duString = s"${dv}$symb\t${baseDir}"
          FileSizeSupport.writeSizeToBuffer(duString, fileToWrite)
          FileSizeSupport.writeSizesToDisk(fileToWrite)
        } else
          println("Error: Neither a file or a directory")
      case None => ()
    }
  }

}

