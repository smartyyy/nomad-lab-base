/*
 * This file is generated by jOOQ.
*/
package eu.nomad_lab.repo.db.tables;


import eu.nomad_lab.repo.db.DefaultSchema;
import eu.nomad_lab.repo.db.Keys;
import eu.nomad_lab.repo.db.tables.records.BasisSetsRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class BasisSets extends TableImpl<BasisSetsRecord> {

    private static final long serialVersionUID = 2069010532;

    /**
     * The reference instance of <code>basis_sets</code>
     */
    public static final BasisSets BASIS_SETS = new BasisSets();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<BasisSetsRecord> getRecordType() {
        return BasisSetsRecord.class;
    }

    /**
     * The column <code>basis_sets.type</code>.
     */
    public final TableField<BasisSetsRecord, String> TYPE = createField("type", org.jooq.impl.SQLDataType.VARCHAR.nullable(false), this, "");

    /**
     * The column <code>basis_sets.rgkmax</code>.
     */
    public final TableField<BasisSetsRecord, Double> RGKMAX = createField("rgkmax", org.jooq.impl.SQLDataType.DOUBLE, this, "");

    /**
     * The column <code>basis_sets.lmaxapw</code>.
     */
    public final TableField<BasisSetsRecord, Double> LMAXAPW = createField("lmaxapw", org.jooq.impl.SQLDataType.DOUBLE, this, "");

    /**
     * The column <code>basis_sets.lmaxmat</code>.
     */
    public final TableField<BasisSetsRecord, Double> LMAXMAT = createField("lmaxmat", org.jooq.impl.SQLDataType.DOUBLE, this, "");

    /**
     * The column <code>basis_sets.lmaxvr</code>.
     */
    public final TableField<BasisSetsRecord, Double> LMAXVR = createField("lmaxvr", org.jooq.impl.SQLDataType.DOUBLE, this, "");

    /**
     * The column <code>basis_sets.gmaxvr</code>.
     */
    public final TableField<BasisSetsRecord, Double> GMAXVR = createField("gmaxvr", org.jooq.impl.SQLDataType.DOUBLE, this, "");

    /**
     * The column <code>basis_sets.repr</code>.
     */
    public final TableField<BasisSetsRecord, byte[]> REPR = createField("repr", org.jooq.impl.SQLDataType.BLOB, this, "");

    /**
     * The column <code>basis_sets.calc_id</code>.
     */
    public final TableField<BasisSetsRecord, Integer> CALC_ID = createField("calc_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * Create a <code>basis_sets</code> table reference
     */
    public BasisSets() {
        this("basis_sets", null);
    }

    /**
     * Create an aliased <code>basis_sets</code> table reference
     */
    public BasisSets(String alias) {
        this(alias, BASIS_SETS);
    }

    private BasisSets(String alias, Table<BasisSetsRecord> aliased) {
        this(alias, aliased, null);
    }

    private BasisSets(String alias, Table<BasisSetsRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return DefaultSchema.DEFAULT_SCHEMA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<BasisSetsRecord> getPrimaryKey() {
        return Keys.BASIS_SETS_PK;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<BasisSetsRecord>> getKeys() {
        return Arrays.<UniqueKey<BasisSetsRecord>>asList(Keys.BASIS_SETS_PK);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<BasisSetsRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<BasisSetsRecord, ?>>asList(Keys.BASIS_SETS__BASIS_SETS_CALC_ID_FKEY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BasisSets as(String alias) {
        return new BasisSets(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public BasisSets rename(String name) {
        return new BasisSets(name, null);
    }
}
