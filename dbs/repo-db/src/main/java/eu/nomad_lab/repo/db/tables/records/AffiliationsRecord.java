/*
 * This file is generated by jOOQ.
*/
package eu.nomad_lab.repo.db.tables.records;


import eu.nomad_lab.repo.db.tables.Affiliations;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record4;
import org.jooq.Row4;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class AffiliationsRecord extends UpdatableRecordImpl<AffiliationsRecord> implements Record4<Integer, String, String, String> {

    private static final long serialVersionUID = 1619937530;

    /**
     * Setter for <code>affiliations.a_id</code>.
     */
    public void setAId(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>affiliations.a_id</code>.
     */
    public Integer getAId() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>affiliations.name</code>.
     */
    public void setName(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>affiliations.name</code>.
     */
    public String getName() {
        return (String) get(1);
    }

    /**
     * Setter for <code>affiliations.address</code>.
     */
    public void setAddress(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>affiliations.address</code>.
     */
    public String getAddress() {
        return (String) get(2);
    }

    /**
     * Setter for <code>affiliations.email_domain</code>.
     */
    public void setEmailDomain(String value) {
        set(3, value);
    }

    /**
     * Getter for <code>affiliations.email_domain</code>.
     */
    public String getEmailDomain() {
        return (String) get(3);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record4 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row4<Integer, String, String, String> fieldsRow() {
        return (Row4) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row4<Integer, String, String, String> valuesRow() {
        return (Row4) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return Affiliations.AFFILIATIONS.A_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return Affiliations.AFFILIATIONS.NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return Affiliations.AFFILIATIONS.ADDRESS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field4() {
        return Affiliations.AFFILIATIONS.EMAIL_DOMAIN;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getAId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getAddress();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value4() {
        return getEmailDomain();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AffiliationsRecord value1(Integer value) {
        setAId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AffiliationsRecord value2(String value) {
        setName(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AffiliationsRecord value3(String value) {
        setAddress(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AffiliationsRecord value4(String value) {
        setEmailDomain(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AffiliationsRecord values(Integer value1, String value2, String value3, String value4) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached AffiliationsRecord
     */
    public AffiliationsRecord() {
        super(Affiliations.AFFILIATIONS);
    }

    /**
     * Create a detached, initialised AffiliationsRecord
     */
    public AffiliationsRecord(Integer aId, String name, String address, String emailDomain) {
        super(Affiliations.AFFILIATIONS);

        set(0, aId);
        set(1, name);
        set(2, address);
        set(3, emailDomain);
    }
}
