/*
 * This file is generated by jOOQ.
*/
package eu.nomad_lab.repo.db.tables.records;


import eu.nomad_lab.repo.db.tables.Forces;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record2;
import org.jooq.Row2;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ForcesRecord extends UpdatableRecordImpl<ForcesRecord> implements Record2<byte[], Integer> {

    private static final long serialVersionUID = 464431298;

    /**
     * Setter for <code>forces.values</code>.
     */
    public void setValues(byte... value) {
        set(0, value);
    }

    /**
     * Getter for <code>forces.values</code>.
     */
    public byte[] getValues() {
        return (byte[]) get(0);
    }

    /**
     * Setter for <code>forces.calc_id</code>.
     */
    public void setCalcId(Integer value) {
        set(1, value);
    }

    /**
     * Getter for <code>forces.calc_id</code>.
     */
    public Integer getCalcId() {
        return (Integer) get(1);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record2 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row2<byte[], Integer> fieldsRow() {
        return (Row2) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row2<byte[], Integer> valuesRow() {
        return (Row2) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<byte[]> field1() {
        return Forces.FORCES.VALUES;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field2() {
        return Forces.FORCES.CALC_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public byte[] value1() {
        return getValues();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value2() {
        return getCalcId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ForcesRecord value1(byte... value) {
        setValues(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ForcesRecord value2(Integer value) {
        setCalcId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ForcesRecord values(byte[] value1, Integer value2) {
        value1(value1);
        value2(value2);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached ForcesRecord
     */
    public ForcesRecord() {
        super(Forces.FORCES);
    }

    /**
     * Create a detached, initialised ForcesRecord
     */
    public ForcesRecord(byte[] values, Integer calcId) {
        super(Forces.FORCES);

        set(0, values);
        set(1, calcId);
    }
}
