/*
 * This file is generated by jOOQ.
*/
package eu.nomad_lab.repo.db.tables;


import eu.nomad_lab.repo.db.DefaultSchema;
import eu.nomad_lab.repo.db.Keys;
import eu.nomad_lab.repo.db.tables.records.UserMetadataRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class UserMetadata extends TableImpl<UserMetadataRecord> {

    private static final long serialVersionUID = -438135182;

    /**
     * The reference instance of <code>user_metadata</code>
     */
    public static final UserMetadata USER_METADATA = new UserMetadata();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<UserMetadataRecord> getRecordType() {
        return UserMetadataRecord.class;
    }

    /**
     * The column <code>user_metadata.permission</code>.
     */
    public final TableField<UserMetadataRecord, Integer> PERMISSION = createField("permission", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * The column <code>user_metadata.label</code>.
     */
    public final TableField<UserMetadataRecord, String> LABEL = createField("label", org.jooq.impl.SQLDataType.VARCHAR, this, "");

    /**
     * The column <code>user_metadata.calc_id</code>.
     */
    public final TableField<UserMetadataRecord, Integer> CALC_ID = createField("calc_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * Create a <code>user_metadata</code> table reference
     */
    public UserMetadata() {
        this("user_metadata", null);
    }

    /**
     * Create an aliased <code>user_metadata</code> table reference
     */
    public UserMetadata(String alias) {
        this(alias, USER_METADATA);
    }

    private UserMetadata(String alias, Table<UserMetadataRecord> aliased) {
        this(alias, aliased, null);
    }

    private UserMetadata(String alias, Table<UserMetadataRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return DefaultSchema.DEFAULT_SCHEMA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<UserMetadataRecord> getPrimaryKey() {
        return Keys.USER_METADATA_PK;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<UserMetadataRecord>> getKeys() {
        return Arrays.<UniqueKey<UserMetadataRecord>>asList(Keys.USER_METADATA_PK);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<UserMetadataRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<UserMetadataRecord, ?>>asList(Keys.USER_METADATA__USER_METADATA_CALC_ID_FKEY);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UserMetadata as(String alias) {
        return new UserMetadata(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public UserMetadata rename(String name) {
        return new UserMetadata(name, null);
    }
}
