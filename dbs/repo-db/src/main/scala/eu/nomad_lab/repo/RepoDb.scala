/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.repo

import org.jooq.impl.DSL
import com.typesafe.scalalogging.StrictLogging
import com.typesafe.config.{ Config, ConfigFactory }
import org.json4s.{ JArray, JBool, JDecimal, JDouble, JField, JInt, JNothing, JNull, JObject, JString, JValue }
import org.json4s.native.JsonMethods.{ compact, parse, render }
import eu.nomad_lab.{ JsonUtils, LocalEnv }
import db.Tables._
import org.jooq
import java.sql.{ Connection, DriverManager }

import scala.util.control.NonFatal
import java.nio.charset.StandardCharsets

import scala.collection.breakOut
import org.json4s.DefaultFormats

/**
 * Methods to connect to the local relational DB
 */
object RepoDb extends StrictLogging {
  lazy val driver = Class.forName("org.postgresql.Driver")

  /**
   * Error while trying to extract a filename from the repository
   */
  class FileNameExtractionError(
    msg: String,
    what: Throwable = null
  ) extends Exception(msg, what)

  /**
   * represents a file
   */
  object FileName {
    def apply(archiveDir: String, path: String, isArchive: Boolean = false): FileName = {
      val rPath = path.dropWhile(_ == '/')
      val (uploadName, relativePath) = rPath.indexOf("/") match {
        case -1 =>
          (rPath, "")
        case slashPos =>
          (rPath.substring(0, slashPos), rPath.substring(slashPos + 1))
      }
      new FileName(archiveDir, uploadName, relativePath, isArchive)
    }

    val uploadRe = "^/?([-.A-Za-z0-9_]+)/?$".r
  }

  /**
   * class to represent a file
   */
  class FileName(archiveDir0: String, uploadName0: String, relativePath0: String, val isArchive: Boolean = false) {
    val archiveDir = if (!archiveDir0.endsWith("/"))
      archiveDir0 + "/"
    else
      archiveDir0

    val uploadName = uploadName0 match {
      case FileName.uploadRe(upload) => upload
      case _ => throw new FileNameExtractionError(s"Invalid uploadName: '$uploadName0'")
    }

    val relativePath = relativePath0.dropWhile(_ == '/')

    def fullPath: String = {
      archiveDir + uploadName + "/" + relativePath
    }

    def toJValue: JValue = {
      JObject(
        ("archiveDir" -> JString(archiveDir)) ::
          ("uploadName" -> JString(uploadName)) ::
          ("relativePath" -> JString(relativePath)) :: Nil
      )
    }
  }

  /**
   * CustomField to expose the postgres convert_from function
   *
   * Transforms bytea columns to strings using a given encoding
   */
  class ConvertFrom(
      val baseField: jooq.Field[Array[Byte]],
      val encoding: jooq.Field[String]
  ) extends jooq.impl.CustomField[String](baseField.getName(), jooq.impl.SQLDataType.LONGVARCHAR) {

    override def accept(context: jooq.Context[_]): Unit = {
      context.visit(delegate(context.configuration()));
    }

    def delegate(configuration: jooq.Configuration): jooq.QueryPart = {
      configuration.dialect().family() match {
        case jooq.SQLDialect.POSTGRES =>
          DSL.field("convert_from({0}, {1})", classOf[String], baseField, encoding);
        case _ =>
          throw new UnsupportedOperationException(s"ConvertTo not supported for dialect ${configuration.dialect()} not supported");
      }
    }
  }

  /**
   * Utility method exposing the postgres convert_from function to transform bytea arrays to strings using a given encoding
   */
  def convertFrom(field: jooq.Field[Array[Byte]], encoding: String = "UTF-8"): ConvertFrom = {
    new ConvertFrom(field, DSL.inline(encoding))
  }

  /**
   * Error decoding a repository path
   */
  class PathDecodingError(
    msg: String,
    what: Throwable = null
  ) extends Exception(msg, what)

  /**
   * The settings required to connect to the local relational DB
   */
  class Settings(config: Config) {
    // validate vs. reference.conf
    config.checkValid(ConfigFactory.defaultReference(), "simple-lib")

    val username: String = config.getString("nomad_lab.repo.username")
    val password: String = config.getString("nomad_lab.repo.password")
    val jdbcUrl: String = config.getString("nomad_lab.repo.jdbcUrl")
    val extractedDir: String = config.getString("nomad_lab.repo.extractedDir")

    def toJson: JValue = {
      import org.json4s.JsonDSL._;
      (("username" -> username) ~
        ("password" -> password) ~
        ("jdbcUrl" -> jdbcUrl) ~
        ("extractedDir" -> extractedDir))
    }
  }

  private var privateDefaultSettings: Settings = null

  /**
   * default settings, should be initialized on startup
   */
  def defaultSettings(): Settings = {
    RepoDb.synchronized {
      if (privateDefaultSettings == null) {
        privateDefaultSettings = new Settings(LocalEnv.defaultConfig)
      }
      privateDefaultSettings
    }
  }

  /**
   * sets the default settings
   */
  def defaultSettings_=(newValue: Settings): Unit = {
    RepoDb.synchronized {
      if (privateDefaultSettings != null)
        logger.warn(s"eu.nomad_lab.repo overwriting old settings ${JsonUtils.normalizedStr(privateDefaultSettings.toJson)} with ${JsonUtils.normalizedStr(newValue.toJson)}")
      privateDefaultSettings = newValue
    }
  }

  /**
   * Error connected to the DB (for example connection failed)
   */
  class DBError(msg: String, reason: Throwable) extends Exception(msg, reason) {}

  /**
   * Creates a new connection to the DB
   */
  def newConnection(settings: Settings): Connection = {
    try {
      driver

      DriverManager.getConnection(settings.jdbcUrl, settings.username, settings.password)
    } catch {
      case NonFatal(e) =>
        throw new DBError(s"failed to connect to the db using ${compact(render(settings.toJson))}", e)
    }
  }

  def newConnection(): Connection = newConnection(defaultSettings)

  /**
   * Returns a jooq context connected to the DB
   */
  def newContext(settings: Settings): jooq.DSLContext = {
    val conn = newConnection(settings)
    jooq.impl.DSL.using(conn, jooq.SQLDialect.POSTGRES)
  }

  def apply(settings: Settings = null): RepoDb = {
    val sets = settings match {
      case null =>
        defaultSettings()
      case _ =>
        settings
    }
    new RepoDb(newContext(sets), extractedDir = sets.extractedDir)
  }
}

class RepoDb(val dbContext: jooq.DSLContext, val extractedDir: String) extends StrictLogging {
  import RepoDb.FileNameExtractionError

  /**
   * resolves the path part of a repository file path
   */
  def resolvePath(path: String): RepoDb.FileName = {
    val extractedAndEscape = "$EXTRACTED\\/"
    val extractedNoEscape = "$EXTRACTED/"
    val oldPathAndEscape = "\\/data\\/nomad\\/extracted\\/"
    var isArchive: Boolean = false
    var hasError: Boolean = false
    var errorStr: String = ""

    if (path.startsWith(extractedAndEscape)) {
      RepoDb.FileName(extractedDir, path.drop(extractedAndEscape.length).replaceAll("\\/", "/"))
    } else if (path.startsWith(extractedNoEscape)) {
      val res = path.drop(extractedNoEscape.length)
      RepoDb.FileName(extractedDir, res, isArchive)
    } else if (path.startsWith(oldPathAndEscape)) {
      RepoDb.FileName(extractedDir, path.drop(oldPathAndEscape.length).replaceAll("\\/", "/"))
    } else {
      val errorMsg = s"unexpected prefix in path '$path'"
      logger.warn(errorMsg)
      RepoDb.FileName("/", path.replaceAll("\\/", "/"))
    }
  }

  /**
   * returns a query getting the filensmes of the open calculations
   */
  def openCalcFilenamesQuery(): jooq.SelectConditionStep[jooq.Record1[Array[Byte]]] = {
    val res = dbContext.select(METADATA.FILENAMES).from(METADATA).join(USER_METADATA).on(USER_METADATA.CALC_ID.equal(METADATA.CALC_ID)).
      where(USER_METADATA.PERMISSION.equal(1)).and(METADATA.FILENAMES.isNotNull())
    res
  }

  /**
   * gets files out of the filenames array
   */
  def filesFromFilenames(el: Array[Byte]): Seq[RepoDb.FileName] = {
    val paths: JValue = JsonUtils.parseUtf8(el)
    paths match {
      case JArray(arr) =>
        arr.map {
          case JString(head) =>
            resolvePath(head)
          case value =>
            throw new FileNameExtractionError(s"unexpected non string value $value in array, expected an array of strings")
        }(breakOut)
      case JNothing | JNull => Seq()
      case _ =>
        throw new FileNameExtractionError(s"expected an array of strings, not ${JsonUtils.normalizedStr(paths)}")
    }
  }

  /**
   * gets the main file (first path) out of the filenames array
   */
  def mainFileFromFilenames(el: Array[Byte]): Option[RepoDb.FileName] = {
    val paths: JValue = JsonUtils.parseUtf8(el)
    paths match {
      case JArray(arr) =>
        arr match {
          case JString(head) :: _ =>
            Some(resolvePath(head))
          case Nil =>
            None
          case value :: _ =>
            throw new FileNameExtractionError(s"unexpected value $value")
        }
      case JNothing | JNull => None
      case _ =>
        throw new FileNameExtractionError("expected a main file")
    }
  }

  /**
   * returns a query getting the calculations with a given prefix
   *
   * returns calc_id, checksum, permission, filenames
   */
  def calculationsWithPrefixQuery(prefix: String): jooq.ResultQuery[jooq.Record4[Integer, String, Integer, Array[Byte]]] = {
    dbContext.select(
      METADATA.CALC_ID,
      CALCULATIONS.CHECKSUM,
      USER_METADATA.PERMISSION,
      METADATA.FILENAMES
    ).from(
      METADATA.
        join(USER_METADATA).on(METADATA.CALC_ID.equal(USER_METADATA.CALC_ID)).
        join(CALCULATIONS).on(CALCULATIONS.CALC_ID.equal(METADATA.CALC_ID))
    ).where(
        METADATA.FILENAMES.isNotNull().
          and(
            RepoDb.convertFrom(METADATA.FILENAMES, "UTF-8").like(DSL.inline("[\"$EXTRACTED\\\\/" + prefix.replaceAll("/", "\\\\/") + "%")).
              or(RepoDb.convertFrom(METADATA.FILENAMES, "UTF-8").like(DSL.inline("[\"$EXTRACTED/" + prefix + "%"))).
              or(RepoDb.convertFrom(METADATA.FILENAMES, "UTF-8").like(DSL.inline("\"\\\\/data\\\\/nomad\\\\/extracted\\\\/" + prefix.replaceAll("/", "\\\\/") + "%")))
          )
      )
  }
}
