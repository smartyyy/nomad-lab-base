/*
 * This file is generated by jOOQ.
*/
package eu.nomad_lab.rawdata.db.tables;


import eu.nomad_lab.rawdata.db.DefaultSchema;
import eu.nomad_lab.rawdata.db.Keys;
import eu.nomad_lab.rawdata.db.tables.records.AuthorAuthorGroupRecord;

import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.ForeignKey;
import org.jooq.Schema;
import org.jooq.Table;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import org.jooq.impl.TableImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class AuthorAuthorGroup extends TableImpl<AuthorAuthorGroupRecord> {

    private static final long serialVersionUID = -1332299381;

    /**
     * The reference instance of <code>author_author_group</code>
     */
    public static final AuthorAuthorGroup AUTHOR_AUTHOR_GROUP = new AuthorAuthorGroup();

    /**
     * The class holding records for this type
     */
    @Override
    public Class<AuthorAuthorGroupRecord> getRecordType() {
        return AuthorAuthorGroupRecord.class;
    }

    /**
     * The column <code>author_author_group.author_id</code>.
     */
    public final TableField<AuthorAuthorGroupRecord, Integer> AUTHOR_ID = createField("author_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>author_author_group.author_group_id</code>.
     */
    public final TableField<AuthorAuthorGroupRecord, Integer> AUTHOR_GROUP_ID = createField("author_group_id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this, "");

    /**
     * The column <code>author_author_group.author_seq</code>.
     */
    public final TableField<AuthorAuthorGroupRecord, Integer> AUTHOR_SEQ = createField("author_seq", org.jooq.impl.SQLDataType.INTEGER, this, "");

    /**
     * Create a <code>author_author_group</code> table reference
     */
    public AuthorAuthorGroup() {
        this("author_author_group", null);
    }

    /**
     * Create an aliased <code>author_author_group</code> table reference
     */
    public AuthorAuthorGroup(String alias) {
        this(alias, AUTHOR_AUTHOR_GROUP);
    }

    private AuthorAuthorGroup(String alias, Table<AuthorAuthorGroupRecord> aliased) {
        this(alias, aliased, null);
    }

    private AuthorAuthorGroup(String alias, Table<AuthorAuthorGroupRecord> aliased, Field<?>[] parameters) {
        super(alias, null, aliased, parameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Schema getSchema() {
        return DefaultSchema.DEFAULT_SCHEMA;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UniqueKey<AuthorAuthorGroupRecord> getPrimaryKey() {
        return Keys.PK_AUTHOR_AUTHOR_GROUP;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<UniqueKey<AuthorAuthorGroupRecord>> getKeys() {
        return Arrays.<UniqueKey<AuthorAuthorGroupRecord>>asList(Keys.PK_AUTHOR_AUTHOR_GROUP);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ForeignKey<AuthorAuthorGroupRecord, ?>> getReferences() {
        return Arrays.<ForeignKey<AuthorAuthorGroupRecord, ?>>asList(Keys.AUTHOR_AUTHOR_GROUP__FK_AUTHOR_AUTHOR_GROUP_AUTHOR_ID, Keys.AUTHOR_AUTHOR_GROUP__FK_AUTHOR_AUTHOR_GROUP_AUTHOR_GROUP_ID);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AuthorAuthorGroup as(String alias) {
        return new AuthorAuthorGroup(alias, this);
    }

    /**
     * Rename this table
     */
    @Override
    public AuthorAuthorGroup rename(String name) {
        return new AuthorAuthorGroup(name, null);
    }
}
