/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.rawdata
import eu.nomad_lab.ref.RawDataRef
import eu.nomad_lab.CompactSha
import java.nio.file.Paths
import collection.JavaConverters._

/**
 * Mapper that caches all raw data archives, and keeps the mapping original directory -> raw data archive
 *
 * can recover the nomad reference to a calculation from its original path
 */
class RawDataMapper(
    rawDataDb: RawDataDb
) {
  var rawData: Map[String, RawDataInfo] = Map()
  var pathTree: RPathTree = RPathNode(Map(), Nil)
  var lastRawDataId: Int = -1

  def updateRawData(): Unit = {
    rawDataDb.loopOnRawData(rawDataIdMin = Some(lastRawDataId)) { (rawDataId, rInfo) =>
      lastRawDataId = rawDataId
      addRawDataInfo(rInfo)
    }
  }

  def addRawDataInfo(newData: RawDataInfo): Unit = {
    rawData.get(newData.gid) match {
      case Some(d) =>
        if (d != newData)
          throw new Exception(s"multiple values archive gid ${newData.gid}: $newData vs $d")
      case None =>
        rawData += newData.gid -> newData
        pathTree = pathTree.insertRawDataInfo(newData)
    }
  }

  def rawDataForRepoPath(path: Seq[String]): List[RawDataInfo] = {
    pathTree.resolvePath(path)
  }

  def nmdUriForRepoPath(path: String): List[RawDataRef] = {
    val pathSeq: Seq[String] = (for (p <- Paths.get(path).iterator.asScala) yield p.toString).toSeq
    rawDataForRepoPath(pathSeq).map { r: RawDataInfo =>
      val basePath: Seq[String] = (for (p <- Paths.get(r.base_path).iterator.asScala) yield p.toString).toSeq
      for ((p, i) <- basePath.zipWithIndex) {
        if (pathSeq(i) != p)
          throw new Exception(s"Internal error rawData $r has mismatchig ${basePath.mkString("/")} for path $path")
      }
      val mainFileUri = RawDataRef(r.gid, "data" +: pathSeq.drop(basePath.length))
      mainFileUri
    }
  }
}
