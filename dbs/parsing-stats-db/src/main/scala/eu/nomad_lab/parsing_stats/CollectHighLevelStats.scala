/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.parsing_stats

import eu.nomad_lab.parsing_stats.db.{ Tables => STATS }
import scala.collection
import org.json4s.{ JValue, JArray, JObject, JString, JInt }
import eu.nomad_lab.JsonSupport.formats
import org.jooq.impl.DSL
import scala.collection.JavaConverters._
import scala.collection.breakOut
import eu.nomad_lab.meta

object HighLevelStats {
  /**
   *  Exception during the collection of the high level statistics
   */
  class StatsException(
    msg: String,
    what: Throwable = null
  ) extends Exception(msg, what)

  /**
   * Defines which kinds of stats are collected
   */
  case class CollectionParam(
    restrictToRootNamespaces: Option[Set[String]] = None,
    restrictToParsersNamed: Option[Set[String]] = None,
    restrictToParsersWithId: Option[Set[String]] = None,
    restrictToLastNVersions: Option[Int] = None,
    skipUnknownVersion: Boolean = false
  )

  /**
   * Statistics of a single parser (and root namespace)
   */
  class StatsEntry(
      val rootNamespace: String,
      val parserName: String,
      val parserId: String,
      val stats0: collection.Map[String, Long] = Map(),
      var nArchives: Int = 0,
      var nMainFilesDetected: Long = 0,
      var nMainFilesSucessfullyParsed: Long = 0
  ) {
    val stats = collection.mutable.Map[String, Long]()
    stats ++= stats0

    def toJValue: JValue = JObject(
      ("rootNamespace" -> JString(rootNamespace)) ::
        ("parserName" -> JString(parserName)) ::
        ("parserId" -> JString(parserId)) ::
        ("stats" -> (JObject(
          stats.map {
            case (s, l) =>
              s -> JInt(l)
          }(breakOut): List[(String, JInt)]
        ))) ::
          ("nArchives" -> JInt(nArchives)) ::
          ("nMainFilesDetected" -> JInt(nMainFilesDetected)) ::
          ("nMainFilesSucessfullyParsed" -> JInt(nMainFilesSucessfullyParsed)) ::
          ("nRuns" -> JInt(nRuns)) ::
          ("nGeometries" -> JInt(nGeometries)) ::
          ("nForces" -> JInt(nForces)) ::
          ("nPartialCharges" -> JInt(nPartialCharges)) ::
          ("nBands" -> JInt(nBands)) ::
          ("nEigenvalues" -> JInt(nEigenvalues)) ::
          ("nCommonMetaInfoTypes" -> JInt(nCommonMetaInfoTypes)) ::
          ("nMetaInfoTypes" -> JInt(nMetaInfoTypes)) ::
          ("nCommonValues" -> JInt(nCommonValues)) ::
          ("nValues" -> JInt(nValues)) :: Nil
    )

    def nRuns: Long = stats.getOrElse("section_run", 0: Long)

    def nMetaInfoTypes: Long = stats.size.toLong

    def nValues: Long = stats.foldLeft(0: Long)(_ + _._2)

    def nSingleConfiguration: Long = stats.getOrElse("section_single_configuration_calculation", 0: Long)

    def nGeometries: Long = {
      stats.getOrElse("atom_position", 0: Long)
    }

    def nForces: Long = {
      stats.getOrElse("atom_forces", 0: Long)
    }

    def nPartialCharges: Long = stats.getOrElse("section_atomic_multipoles", 0: Long)

    def nBands: Long = stats.getOrElse("section_k_band", 0: Long)

    def nEigenvalues: Long = stats.getOrElse("section_eigenvalues_group", 0: Long)

    def nCommonValues: Long = {
      val commonNames = meta.KnownMetaInfoEnvs.common.allNames.toSet
      stats.foldLeft(0: Long) { (vAll: Long, item: Tuple2[String, Long]) =>
        if (commonNames(item._1))
          item._2 + vAll
        else
          vAll
      }
    }

    def nCommonMetaInfoTypes: Long = {
      val commonNames = meta.KnownMetaInfoEnvs.common.allNames.toSet
      stats.keys.filter(commonNames).size.toLong
    }

    def mergeArchiveJsonStats(newStats: JValue): Unit = {
      val newMap = newStats.extract[Map[String, Long]]
      for ((k, v) <- newMap)
        stats += (k -> (stats.getOrElse(k, 0: Long) + v))
    }
  }
}

class HighLevelStats(
    statsDb: ParsingStatsDb,
    params: HighLevelStats.CollectionParam
) {
  import HighLevelStats.StatsException
  import HighLevelStats.StatsEntry

  val stats = collection.mutable.Map[String, Seq[StatsEntry]]()

  def collectAll(): Unit = {
    val allRootNamespaces = statsDb.dbContext.select(
      STATS.ROOT_NAMESPACE.ROOT_NAMESPACE_NAME
    ).from(
      STATS.ROOT_NAMESPACE
    ).fetch(STATS.ROOT_NAMESPACE.ROOT_NAMESPACE_NAME)
    if (allRootNamespaces == null)
      return
    val selectedRootNamespaces = params.restrictToRootNamespaces match {
      case Some(namespaces) =>
        allRootNamespaces.asScala.filter(namespaces.contains(_))
      case None =>
        allRootNamespaces.asScala
    }
    for (rootNamespace <- selectedRootNamespaces) {
      val rootNamespaceId = statsDb.rootNamespaceIdFetch(rootNamespace) match {
        case Some(nId) => nId
        case None => throw new StatsException(s"Internal exception getting rootNamespace $rootNamespace")
      }
      val parserIds = statsDb.dbContext.select(
        STATS.PARSER.PARSER_IDENTIFIER
      ).from(
        STATS.CALCULATION_PARSER.
          join(STATS.PARSER).
          on(STATS.PARSER.PARSER_ID.equal(STATS.CALCULATION_PARSER.PARSER_ID))
      ).where(
          STATS.CALCULATION_PARSER.ROOT_NAMESPACE_ID.equal(new Integer(rootNamespaceId))
        ).groupBy(
            STATS.PARSER.PARSER_IDENTIFIER
          ).fetch(STATS.PARSER.PARSER_IDENTIFIER)
      if (parserIds != null) {
        val parsers = parserIds.asScala.filter { (parserId: String) =>
          val parser = statsDb.parserVersionFetch(parserId).get
          (params.restrictToParsersNamed match {
            case Some(names) => names.contains(parser.parserName)
            case None => true
          }) && (params.restrictToParsersWithId match {
            case Some(pIds) => pIds.contains(parser.parserId)
            case None => true
          }) && (!params.skipUnknownVersion || parser.parserId != (parser.parserName + "-unknown"))
        }.sorted
        params.restrictToLastNVersions match {
          case None => ()
          case Some(n) => throw new StatsException(s"restrictToLastNVersions not yet implemented")
        }
        val parserStats = parsers.map { (parserId: String) =>
          collectSingle(rootNamespace, parserId)
        }
        stats += (rootNamespace -> parserStats.toSeq)
      }
    }
  }

  def collectSingle(rootNamespace: String, parserId: String): StatsEntry = {
    val rootNamespaceId = statsDb.rootNamespaceIdFetch(rootNamespace) match {
      case None =>
        throw new StatsException(s"could not get rootNamespace $rootNamespace")
      case Some(rId) => rId
    }
    val parser = statsDb.parserVersionFetch(parserId) match {
      case None =>
        throw new StatsException(s"could not get parser with id $parserId")
      case Some(p) => p
    }
    val nMainFilesDetected = statsDb.dbContext.selectCount().
      from(
        STATS.CALCULATION_PARSER.
          join(STATS.CALCULATION_ASSIGNEMENT).
          on(STATS.CALCULATION_ASSIGNEMENT.CALCULATION_PARSER_ID.equal(
            STATS.CALCULATION_PARSER.CALCULATION_PARSER_ID
          ))
      ).where(
          STATS.CALCULATION_PARSER.ROOT_NAMESPACE_ID.equal(rootNamespaceId).
            and(STATS.CALCULATION_PARSER.PARSER_ID.equal(parser.parserDbId))
        ).fetchOne().value1()

    val nMainFilesSucessfullyParsed = statsDb.dbContext.selectCount().
      from(
        STATS.CALCULATION_PARSER.
          join(STATS.CALCULATION_PARSING).
          on(STATS.CALCULATION_PARSING.CALCULATION_PARSER_ID.equal(
            STATS.CALCULATION_PARSER.CALCULATION_PARSER_ID
          ))
      ).where(
          STATS.CALCULATION_PARSER.ROOT_NAMESPACE_ID.equal(rootNamespaceId).
            and(STATS.CALCULATION_PARSER.PARSER_ID.equal(parser.parserDbId)).
            and(STATS.CALCULATION_PARSING.PARSE_RESULT.equal(statsDb.parseResultToDbEnum(eu.nomad_lab.parsers.ParseResult.ParseSuccess)))
        ).fetchOne().value1()

    val res = new StatsEntry(
      rootNamespace,
      parser.parserName,
      parserId,
      nMainFilesDetected = nMainFilesDetected.toLong,
      nMainFilesSucessfullyParsed = nMainFilesSucessfullyParsed.toLong
    )

    val statsToCollect = statsDb.dbContext.select(
      STATS.RAW_DATA_PARSER_STATS.META_STATS
    ).from(
      STATS.RAW_DATA_PARSER_STATS
    ).where(
      STATS.RAW_DATA_PARSER_STATS.ROOT_NAMESPACE_ID.equal(rootNamespaceId).
        and(STATS.RAW_DATA_PARSER_STATS.PARSER_ID.equal(parser.parserDbId))
    ).fetchLazy()
    while (statsToCollect.hasNext()) {
      val toAdd = statsToCollect.fetchOne()
      res.mergeArchiveJsonStats(toAdd.getValue(STATS.RAW_DATA_PARSER_STATS.META_STATS))
      res.nArchives += 1
    }

    res
  }

  def writeHtml(w: java.io.Writer): Unit = {
    w.write("<html><head><title>Parser Statistics</title></head><body>")
    for ((rootNamespace, nStats) <- stats) {
      w.write(s"""<h1>$rootNamespace</h1>""")
      val byParser = nStats // .groupBy(statEntry => statsDb.parserVersionFetch(statEntry.parserName))
      //for ((pName, stats) <- byParser) {
      // w.write(s"""<h2>$pName</h2>""
      w.write(s"""
<table border = "1">
<tr>
<th>parserId</th><th>raw data archives</th><th>files detected</th><th>successfully parsed</th><th> runs</th><th># single configuration calculations</th><th> geometries</th><th>partial charges</th><th>eigenvalues</th><th>bands</th><th>#common meta info</th><th>#meta info</th><th># common values</th><th># values</th></tr>
""")
      val stats = byParser
      for (stat <- stats)
        w.write(s"""<tr>
<td>${stat.parserId}</td>
<td>${stat.nArchives}</td>
<td>${stat.nMainFilesDetected}</td>
<td>${stat.nMainFilesSucessfullyParsed}</td>
<td>${stat.nRuns}</td>
<td>${stat.nSingleConfiguration}</td>
<td>${stat.nGeometries}</td>
<td>${stat.nPartialCharges}</td>
<td>${stat.nEigenvalues}</td>
<td>${stat.nBands}</td>
<td>${stat.nCommonMetaInfoTypes}</td>
<td>${stat.nMetaInfoTypes}</td>
<td>${stat.nCommonValues}</td>
<td>${stat.nValues}</td></tr>
""")

      w.write("</table>")

      w.write(s"""
</body>
</html>""")
    }
  }

  def toJValue: JValue = {
    JObject(
      stats.map {
        case (rootNamespace, stats) =>
          (rootNamespace -> JArray(stats.map(_.toJValue)(breakOut): List[JValue]))
      }(breakOut): List[(String, JArray)]
    )
  }

}
