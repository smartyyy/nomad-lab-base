/*
   Copyright 2016-2017 The NOMAD Developers Group

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
package eu.nomad_lab.parsing_stats
import org.jooq.impl.DSL
import org.json4s.{ JNothing, JNull, JBool, JDouble, JDecimal, JInt, JString, JArray, JObject, JValue, JField }
import org.jooq;
import eu.{ nomad_lab => lab }
import eu.nomad_lab.parsers.ParseResult
import scala.util.control.NonFatal
import scala.collection.mutable
import scala.collection.breakOut
import com.typesafe.config.Config
import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.parsing_stats.db.{ Tables => STATS }
import eu.nomad_lab.parsing_stats.db.enums.ParseResultEnum
import eu.nomad_lab.db_support.DbObject
import eu.nomad_lab.parsing_queue.TreeParser

/**
 * The settings required to connect to the parsing stats DB
 */
class ParsingStatsSettings(
  config: Config
) extends DbObject.DbSettings(
  "nomad_lab.parsing_stats",
  config
)

/**
 * Methods to connect to the parsing stats DB
 */
object ParsingStatsDb extends DbObject[ParsingStatsSettings](
  (config: Config) => new ParsingStatsSettings(config),
  Seq("parsing-stats-db/migrations")
) {

  /**
   * Returns a new context connected to the parsing stats database
   * using the default settings
   */
  def apply(checkDb: Boolean = true): ParsingStatsDb = {
    apply(defaultSettings(), checkDb)
  }

  /**
   * Returns a new context connected to the parsing stats database
   * using the given settings
   */
  def apply(settings: ParsingStatsSettings, checkDb: Boolean): ParsingStatsDb = {
    new ParsingStatsDb(newContext(settings, checkDb))
  }

  /**
   * Exception with the db handling
   */
  class ParsingStatsDbException(
    msg: String,
    what: Throwable = null
  ) extends Exception(msg, what)

  /**
   * describes a parser
   */
  case class ParserVersion(
    val parserDbId: Int,
    val parserName: String,
    val parserId: String,
    val parserInfo: JValue
  )

  /**
   * Identifies a raw data
   */
  case class RawDataInfo(
    val treeUri: String,
    val rawDataId: Int
  )

  /**
   * information when a tree has been fully parsed
   */
  case class TreeInfo(
    val nSuccessful: Int,
    val nFailed: Int,
    val cumulativeParsingSeconds: Double
  )

  /**
   * Identifies a calculation
   */
  case class CalculationInfo(
    val calculationId: Long,
    val calculationGid: String,
    val calculationUri: String,
    val rawDataId: Option[Int]
  )

  /**
   * Identifies a calculation and a parser (and a root namespace)
   */
  case class CalculationParser(
    val calculationParserId: Long,
    val calculationInfo: CalculationInfo,
    val parserVersion: ParserVersion,
    val rootNamespaceId: Int,
    val rootNamespace: String
  )

}

class ParsingStatsDb(
    val dbContext: jooq.DSLContext
) extends StrictLogging {
  import ParsingStatsDb.ParsingStatsDbException
  import ParsingStatsDb.ParserVersion
  import ParsingStatsDb.CalculationInfo
  import ParsingStatsDb.CalculationParser
  import ParsingStatsDb.TreeInfo

  val cachedParsers = mutable.Map[String, ParserVersion]()
  val cachedNamespaces = mutable.Map[String, Int]()

  /**
   * Fetches from the db root_namespace_id for the given root_namespace_name if available
   */
  def rootNamespaceIdFetch(rootNamespaceName: String): Option[Int] = {
    cachedNamespaces.get(rootNamespaceName) match {
      case Some(rName) => Some(rName)
      case None =>
        val res = dbContext.select(
          STATS.ROOT_NAMESPACE.ROOT_NAMESPACE_ID
        ).from(
          STATS.ROOT_NAMESPACE
        ).where(
          STATS.ROOT_NAMESPACE.ROOT_NAMESPACE_NAME.equal(rootNamespaceName)
        ).fetchOne(STATS.ROOT_NAMESPACE.ROOT_NAMESPACE_ID)
        if (res == null)
          None
        else
          Some(res)
    }
  }

  /**
   * Fetches the root_namespace_id if available, or creates a new one and returns it
   */
  def rootNamespaceIdFetchOrInsert(rootNamespaceName: String): Int = {
    rootNamespaceIdFetch(rootNamespaceName) match {
      case Some(dataId) => dataId
      case None =>
        var lastExc: Option[Exception] = None
        val newId = try {
          dbContext.insertInto(STATS.ROOT_NAMESPACE).
            set(STATS.ROOT_NAMESPACE.ROOT_NAMESPACE_NAME, rootNamespaceName).
            returning(STATS.ROOT_NAMESPACE.ROOT_NAMESPACE_ID).
            fetchOne()
        } catch {
          case e: RuntimeException =>
            lastExc = Some(e)
            null
        }
        if (newId == null) {
          rootNamespaceIdFetch(rootNamespaceName) match {
            case Some(dataId) => dataId
            case None =>
              throw new ParsingStatsDbException(s"failed to insert root namespace $rootNamespaceName", lastExc.getOrElse(null))
          }
        } else {
          newId.getValue(STATS.ROOT_NAMESPACE.ROOT_NAMESPACE_ID)
        }
    }
  }

  /**
   * Fetches a ParserVersion object with the given parserId if available
   */
  def parserVersionFetch(parserIdentifier: String): Option[ParserVersion] = {
    cachedParsers.get(parserIdentifier) match {
      case Some(parser) =>
        Some(parser)
      case None =>
        val dbParser = dbContext.select(
          STATS.PARSER.PARSER_ID,
          STATS.PARSER.PARSER_NAME,
          STATS.PARSER.PARSER_INFO
        ).from(
          STATS.PARSER
        ).where(
          STATS.PARSER.PARSER_IDENTIFIER.equal(DSL.inline(parserIdentifier))
        ).fetchOne()
        if (dbParser == null) {
          None
        } else {
          Some(ParserVersion(
            parserDbId = dbParser.getValue(STATS.PARSER.PARSER_ID),
            parserName = dbParser.getValue(STATS.PARSER.PARSER_NAME),
            parserId = parserIdentifier,
            parserInfo = dbParser.getValue(STATS.PARSER.PARSER_INFO)
          ))
        }
    }
  }

  /**
   * Fetches the parserVersion with the given parseId if available (ignoring parserInfo and parserName)
   * if not available a new parserVersion with the given parserName and parserInfo is created and inserted in the DB
   */
  def parserVersionFetchOrInsert(parserName: String, parserIdentifier: String, parserInfo: JValue): ParserVersion = {
    parserVersionFetch(parserIdentifier) match {
      case Some(parser) => parser
      case None =>
        var insertExc: Option[Exception] = None
        val newId = try {
          dbContext.insertInto(STATS.PARSER).
            set(STATS.PARSER.PARSER_IDENTIFIER, parserIdentifier).
            set(STATS.PARSER.PARSER_NAME, parserName).
            set(STATS.PARSER.PARSER_INFO, parserInfo).
            returning(STATS.PARSER.PARSER_ID).
            fetchOne()
        } catch {
          case e: RuntimeException =>
            insertExc = Some(e)
            null
        }
        if (newId == null) {
          parserVersionFetch(parserIdentifier) match {
            case Some(parser) => parser
            case None =>
              throw new ParsingStatsDbException(s"could not add parser $parserName, $parserIdentifier, ${lab.JsonSupport.writeStr(parserInfo)}", insertExc.getOrElse(null))
          }
        } else {
          ParserVersion(
            parserDbId = newId.getValue(STATS.PARSER.PARSER_ID),
            parserName = parserName,
            parserId = parserIdentifier,
            parserInfo = parserInfo
          )
        }
    }
  }

  /**
   * Fetches the db raw_data_id for the given rawDataGid if available
   */
  def rawDataIdFetch(treeUri: String): Option[Int] = {
    val res = dbContext.select(
      STATS.RAW_DATA.RAW_DATA_ID
    ).from(
      STATS.RAW_DATA
    ).where(
      STATS.RAW_DATA.RAW_DATA_GID.equal(treeUri)
    ).fetchOne(STATS.RAW_DATA.RAW_DATA_ID)
    if (res == null)
      None
    else
      Some(res)
  }

  /**
   * Fetches the raw_data_id if available, or creates a new one and returns it
   */
  def rawDataIdFetchOrInsert(treeUri: String): Int = {
    rawDataIdFetch(treeUri) match {
      case Some(dataId) => dataId
      case None =>
        var lastExc: Option[Exception] = None
        val newId = try {
          dbContext.insertInto(STATS.RAW_DATA).
            set(STATS.RAW_DATA.RAW_DATA_GID, treeUri).
            returning(STATS.RAW_DATA.RAW_DATA_ID).
            fetchOne()
        } catch {
          case e: RuntimeException =>
            lastExc = Some(e)
            null
        }
        if (newId == null) {
          rawDataIdFetch(treeUri) match {
            case Some(dataId) => dataId
            case None =>
              throw new ParsingStatsDbException(s"failed to insert raw data $treeUri", lastExc.getOrElse(null))
          }
        } else {
          newId.getValue(STATS.RAW_DATA.RAW_DATA_ID)
        }
    }
  }

  /**
   * Fetches the raw_data_id if available, or creates a new one.
   *
   * Updates the
   */
  def treeScanStart(rootNamespace: String, treeUri: String): Int = {
    rawDataIdFetch(treeUri) match {
      case Some(dataId) => dataId
      case None =>
        var lastExc: Option[Exception] = None
        val newId = try {
          dbContext.insertInto(STATS.RAW_DATA).
            set(STATS.RAW_DATA.RAW_DATA_GID, treeUri).
            returning(STATS.RAW_DATA.RAW_DATA_ID).
            fetchOne()
        } catch {
          case e: RuntimeException =>
            lastExc = Some(e)
            null
        }
        if (newId == null) {
          rawDataIdFetch(treeUri) match {
            case Some(dataId) => dataId
            case None =>
              throw new ParsingStatsDbException(s"failed to insert raw data $treeUri", lastExc.getOrElse(null))
          }
        } else {
          newId.getValue(STATS.RAW_DATA.RAW_DATA_ID)
        }
    }
  }

  val rawDataUriRe = """nmd://(R[-_a-zA-Z0-9]{28})(?:/.*)?$""".r

  def extractRawDataGid(uri: String): Option[String] = {
    uri match {
      case rawDataUriRe(gid) => Some(gid)
      case _ => None
    }
  }

  /**
   * Fetches the calculation info corresponding to the given calculationGid if available
   */
  def calculationInfoFetch(calculationGid: String): Option[CalculationInfo] = {
    val dbValue = dbContext.select(
      STATS.CALCULATION.CALCULATION_ID,
      STATS.CALCULATION.CALCULATION_GID,
      STATS.CALCULATION.CALCULATION_URI,
      STATS.CALCULATION.RAW_DATA_ID
    ).from(
      STATS.CALCULATION
    ).where(
      STATS.CALCULATION.CALCULATION_GID.equal(DSL.inline(calculationGid))
    ).fetchOne()
    if (dbValue == null) {
      None
    } else {
      Some(CalculationInfo(
        calculationId = dbValue.getValue(STATS.CALCULATION.CALCULATION_ID),
        calculationGid = dbValue.getValue(STATS.CALCULATION.CALCULATION_GID),
        calculationUri = dbValue.getValue(STATS.CALCULATION.CALCULATION_URI),
        rawDataId = dbValue.getValue(STATS.CALCULATION.RAW_DATA_ID) match {
          case null => None
          case v => Some(v + 0)
        }
      ))
    }
  }

  def calculationInfoFetchOrInsert(calculationGid: String, calculationUri: String): CalculationInfo = {
    calculationInfoFetch(calculationGid) match {
      case Some(calc) => calc
      case None =>
        val rawDataId = extractRawDataGid(calculationUri) match {
          case Some(gid) => Some(rawDataIdFetchOrInsert(gid))
          case None => None
        }
        var lastExc: Option[Exception] = None
        val newId = try {
          dbContext.insertInto(STATS.CALCULATION).
            set(STATS.CALCULATION.CALCULATION_GID, calculationGid).
            set(STATS.CALCULATION.CALCULATION_URI, calculationUri).
            set(STATS.CALCULATION.RAW_DATA_ID, rawDataId match {
              case Some(v) => new Integer(v)
              case None => null
            }).
            returning(STATS.CALCULATION.CALCULATION_ID).
            fetchOne()
        } catch {
          case e: RuntimeException =>
            lastExc = Some(e)
            null
        }
        if (newId == null) {
          calculationInfoFetch(calculationGid) match {
            case Some(calc) => calc
            case None =>
              throw new ParsingStatsDbException(s"Error inserting $calculationGid ($calculationUri)", lastExc.getOrElse(null))
          }
        } else {
          CalculationInfo(
            calculationId = newId.getValue(STATS.CALCULATION.CALCULATION_ID),
            calculationGid = calculationGid,
            calculationUri = calculationUri,
            rawDataId = rawDataId
          )
        }
    }
  }

  def calculationParserFetch(rootNamespace: String, calculationGid: String, parserIdentifier: String): Option[CalculationParser] = {
    rootNamespaceIdFetch(rootNamespace) match {
      case Some(rootNamespaceId) =>
        parserVersionFetch(parserIdentifier) match {
          case Some(parser) =>
            val dbData = dbContext.select(
              STATS.CALCULATION.CALCULATION_ID,
              STATS.CALCULATION.CALCULATION_URI,
              STATS.CALCULATION.RAW_DATA_ID,
              STATS.CALCULATION_PARSER.CALCULATION_PARSER_ID
            ).from(
              STATS.CALCULATION.
                join(STATS.CALCULATION_PARSER).
                on(STATS.CALCULATION.CALCULATION_ID.equal(STATS.CALCULATION_PARSER.CALCULATION_ID))
            ).where(
                STATS.CALCULATION.CALCULATION_GID.equal(DSL.inline(calculationGid)).
                  and(STATS.CALCULATION_PARSER.PARSER_ID.equal(new Integer(parser.parserDbId))).
                  and(STATS.CALCULATION_PARSER.ROOT_NAMESPACE_ID.equal(new java.lang.Integer(rootNamespaceId)))
              ).fetchOne()
            if (dbData != null) {
              Some(CalculationParser(
                calculationParserId = dbData.getValue(STATS.CALCULATION_PARSER.CALCULATION_PARSER_ID),
                calculationInfo = CalculationInfo(
                  calculationId = dbData.getValue(STATS.CALCULATION.CALCULATION_ID),
                  calculationGid = calculationGid,
                  calculationUri = dbData.getValue(STATS.CALCULATION.CALCULATION_URI),
                  rawDataId = dbData.getValue(STATS.CALCULATION.RAW_DATA_ID) match {
                    case null => None
                    case rId => Some(rId)
                  }
                ),
                parserVersion = parser,
                rootNamespaceId = rootNamespaceId,
                rootNamespace = rootNamespace
              ))
            } else {
              None
            }
          case None => None
        }
      case None => None
    }
  }

  def calculationParserFetchOrInsert(
    rootNamespace: String,
    calculationGid: String,
    calculationUri: String,
    parserName: String,
    parserIdentifier: String,
    parserInfo: JValue
  ): CalculationParser = {
    calculationParserFetch(rootNamespace, calculationGid, parserIdentifier) match {
      case Some(calcP) => calcP
      case None =>
        val rootNamespaceId = rootNamespaceIdFetchOrInsert(rootNamespace)
        val parser = parserVersionFetchOrInsert(parserName, parserIdentifier, parserInfo)
        val calculation = calculationInfoFetchOrInsert(calculationGid, calculationUri)
        var lastExc: Option[Exception] = None
        val newId = try {
          dbContext.insertInto(STATS.CALCULATION_PARSER).
            set(STATS.CALCULATION_PARSER.ROOT_NAMESPACE_ID, new Integer(rootNamespaceId)).
            set(STATS.CALCULATION_PARSER.PARSER_ID, new Integer(parser.parserDbId)).
            set(STATS.CALCULATION_PARSER.CALCULATION_ID, new java.lang.Long(calculation.calculationId)).
            returning(STATS.CALCULATION_PARSER.CALCULATION_PARSER_ID).
            fetchOne()
        } catch {
          case e: RuntimeException =>
            lastExc = Some(e)
            null
        }
        newId match {
          case null =>
            calculationParserFetch(rootNamespace, calculationGid, parserIdentifier) match {
              case Some(calcP) => calcP
              case None =>
                throw new ParsingStatsDbException(s"Error inserting CalculationParser $calculationGid ($calculationUri), $parserIdentifier in namespace $rootNamespace", lastExc.getOrElse(null))
            }
          case cpId =>
            CalculationParser(
              calculationParserId = cpId.getValue(STATS.CALCULATION_PARSER.CALCULATION_PARSER_ID),
              calculationInfo = calculation,
              parserVersion = parser,
              rootNamespaceId = rootNamespaceId,
              rootNamespace = rootNamespace
            )
        }
    }
  }

  def addCalculationAssignement(assignement: TreeParser.ParserAssociation): Unit = {
    val candidateParser = assignement.candidateParsers.head
    val parserInfo = candidateParser.parser.parserInfo
    val parserIdentifier = candidateParser.parser.parserIdentifier
    val calculationParser: CalculationParser = calculationParserFetchOrInsert(
      rootNamespace = assignement.rootNamespace,
      calculationGid = TreeParser.calculateCalculationGid(assignement.mainFileUri),
      calculationUri = assignement.mainFileUri,
      parserName = assignement.candidateParsers.head.parserName,
      parserIdentifier = parserIdentifier,
      parserInfo = parserInfo
    )
    val matchPriority = candidateParser.parserMatch.matchPriority
    val weakMatch = candidateParser.parserMatch.weakMatch
    val assignementInfo = candidateParser.parserMatch.extraInfo
    val calculation = calculationParser.calculationInfo
    val parser = calculationParser.parserVersion
    val rootNamespace = assignement.rootNamespace
    val oldValue = dbContext.select(
      STATS.CALCULATION_ASSIGNEMENT.MATCH_PRIORITY,
      STATS.CALCULATION_ASSIGNEMENT.WEAKMATCH,
      STATS.CALCULATION_ASSIGNEMENT.ASSIGNEMENT_INFO
    ).from(
      STATS.CALCULATION_ASSIGNEMENT
    ).where(
      STATS.CALCULATION_ASSIGNEMENT.CALCULATION_PARSER_ID.equal(new java.lang.Long(calculationParser.calculationParserId))
    ).fetchOne()
    if (oldValue == null) {
      logger.debug(s"Inserting assignement for parser:${lab.JsonSupport.writeStr(parser)}, calculation:${lab.JsonSupport.writeStr(calculation)} in namespace $rootNamespace")
      dbContext.insertInto(STATS.CALCULATION_ASSIGNEMENT).
        set(STATS.CALCULATION_ASSIGNEMENT.CALCULATION_PARSER_ID, new java.lang.Long(calculationParser.calculationParserId)).
        set(STATS.CALCULATION_ASSIGNEMENT.MATCH_PRIORITY, new Integer(matchPriority)).
        set(STATS.CALCULATION_ASSIGNEMENT.WEAKMATCH, new java.lang.Boolean(weakMatch)).
        set(STATS.CALCULATION_ASSIGNEMENT.ASSIGNEMENT_INFO, assignementInfo).
        execute()
    } else {
      val oldMatchPriority = oldValue.getValue(STATS.CALCULATION_ASSIGNEMENT.MATCH_PRIORITY)
      val oldWeakMatch = oldValue.getValue(STATS.CALCULATION_ASSIGNEMENT.WEAKMATCH)
      val oldAssignementInfo = lab.JsonUtils.normalizedStr(oldValue.getValue(STATS.CALCULATION_ASSIGNEMENT.ASSIGNEMENT_INFO))
      if (oldMatchPriority != matchPriority ||
        oldWeakMatch != weakMatch ||
        oldAssignementInfo != lab.JsonUtils.normalizedStr(assignementInfo)) {
        logger.warn(s"Replacing parser assignement in namespace $rootNamespace for parser:${lab.JsonSupport.writeStr(parser)}, calculation:${lab.JsonSupport.writeStr(calculation)}; matchPriority:$oldMatchPriority ->${matchPriority}, weakMatch: $oldWeakMatch -> ${weakMatch}, assignementInfo: $oldAssignementInfo -> ${lab.JsonSupport.writeStr(assignementInfo)}")
        dbContext.update(STATS.CALCULATION_ASSIGNEMENT).
          set(STATS.CALCULATION_ASSIGNEMENT.MATCH_PRIORITY, new Integer(matchPriority)).
          set(STATS.CALCULATION_ASSIGNEMENT.WEAKMATCH, new java.lang.Boolean(weakMatch)).
          set(STATS.CALCULATION_ASSIGNEMENT.ASSIGNEMENT_INFO, assignementInfo).
          where(
            STATS.CALCULATION_ASSIGNEMENT.CALCULATION_PARSER_ID.equal(calculationParser.calculationParserId)
          ).execute()
      } else {
        logger.debug(s"Ignoring already present assignement for parser:${lab.JsonSupport.writeStr(parser)}, calculation:${lab.JsonSupport.writeStr(calculation)} in namespace $rootNamespace")
      }
    }
  }

  def parseResultToDbEnum(parseResult: ParseResult.Value): ParseResultEnum = {
    parseResult match {
      case ParseResult.ParseSuccess => ParseResultEnum.ParseSuccess
      case ParseResult.ParseSkipped => ParseResultEnum.ParseSkipped
      case ParseResult.ParseWithWarnings => ParseResultEnum.ParseWithWarnings
      case ParseResult.ParseFailure => ParseResultEnum.ParseFailure
    }
  }

  def parseResultFromDbEnum(parseResult: ParseResultEnum): ParseResult.Value = {
    parseResult match {
      case ParseResultEnum.ParseSuccess => ParseResult.ParseSuccess
      case ParseResultEnum.ParseSkipped => ParseResult.ParseSkipped
      case ParseResultEnum.ParseWithWarnings => ParseResult.ParseWithWarnings
      case ParseResultEnum.ParseFailure => ParseResult.ParseFailure
    }
  }

  def setParsingResult(parseResult: lab.QueueMessage.CalculationParserResult): Unit = {
    val calculationParser: CalculationParser = calculationParserFetchOrInsert(
      rootNamespace = parseResult.rootNamespace,
      calculationGid = parseResult.calculationGid,
      calculationUri = parseResult.parseRequest.mainFileUri,
      parserName = parseResult.parseRequest.parserName,
      parserIdentifier = parseResult.parserId,
      parserInfo = parseResult.parserInfo
    )
    val parser = calculationParser.parserVersion
    val calculation = calculationParser.calculationInfo
    val rootNamespace = parseResult.rootNamespace
    val oldValue = dbContext.select(
      STATS.CALCULATION_PARSING.PARSE_RESULT,
      STATS.CALCULATION_PARSING.META_STATS,
      STATS.CALCULATION_PARSING.ERROR_MSG,
      STATS.CALCULATION_PARSING.PARSING_TIME_SEC
    ).from(
      STATS.CALCULATION_PARSING
    ).where(
      STATS.CALCULATION_PARSING.CALCULATION_PARSER_ID.equal(new java.lang.Long(calculationParser.calculationParserId))
    ).fetchOne()
    if (oldValue == null) {
      logger.debug(s"Inserting parse result for parser:${lab.JsonSupport.writeStr(parser)}, calculation:${lab.JsonSupport.writeStr(calculation)} in namespace $rootNamespace")
      dbContext.insertInto(STATS.CALCULATION_PARSING).
        set(STATS.CALCULATION_PARSING.CALCULATION_PARSER_ID, new java.lang.Long(calculationParser.calculationParserId)).
        set(STATS.CALCULATION_PARSING.PARSE_RESULT, parseResultToDbEnum(parseResult.parseResult)).
        set(STATS.CALCULATION_PARSING.ERROR_MSG, parseResult.errorMessage.getOrElse(null)).
        set(STATS.CALCULATION_PARSING.PARSING_TIME_SEC, new java.lang.Double(parseResult.parsingTimeSec)).
        execute()
    } else {
      val oldParseResult = oldValue.getValue(STATS.CALCULATION_PARSING.PARSE_RESULT)
      val oldErrorMessage = oldValue.getValue(STATS.CALCULATION_PARSING.ERROR_MSG)
      val oldParsingTimeSec = oldValue.getValue(STATS.CALCULATION_PARSING.PARSING_TIME_SEC)
      if (oldParseResult != parseResult.parseResult.toString() ||
        oldErrorMessage != parseResult.errorMessage ||
        oldParsingTimeSec != parseResult.parsingTimeSec) {
        if (oldParseResult != null || oldErrorMessage != null || oldParsingTimeSec != null)
          logger.warn(s"Replacing parse result in namespace $rootNamespace for parser:${lab.JsonSupport.writeStr(parser)}, calculation:${lab.JsonSupport.writeStr(calculation)}; parseResult:$oldParseResult ->${parseResult.parseResult}, errorMessage: $oldErrorMessage -> ${parseResult.errorMessage}, parsingTimeSec: $oldParsingTimeSec -> ${parseResult.parsingTimeSec}")
        else
          logger.debug(s"Inserting parse result for parser:${lab.JsonSupport.writeStr(parser)}, calculation:${lab.JsonSupport.writeStr(calculation)} in namespace $rootNamespace")
        dbContext.update(STATS.CALCULATION_PARSING).
          set(STATS.CALCULATION_PARSING.PARSE_RESULT, parseResultToDbEnum(parseResult.parseResult)).
          set(STATS.CALCULATION_PARSING.ERROR_MSG, parseResult.errorMessage.getOrElse(null)).
          set(STATS.CALCULATION_PARSING.PARSING_TIME_SEC, new java.lang.Double(parseResult.parsingTimeSec)).
          where(
            STATS.CALCULATION_PARSING.CALCULATION_PARSER_ID.equal(new java.lang.Long(calculationParser.calculationParserId))
          ).execute()
      } else {
        logger.debug(s"Ignoring already present parse result in namespace $rootNamespace for parser:${lab.JsonSupport.writeStr(parser)}, calculation:${lab.JsonSupport.writeStr(calculation)}")
      }
    }
  }

  def updateParsingStats(stats: lab.parsing_queue.CalculationParser.StatsValues): Unit = {
    val calculationParser: CalculationParser = calculationParserFetchOrInsert(
      rootNamespace = stats.rootNamespace,
      calculationGid = stats.calculationGid,
      calculationUri = stats.mainFileUri,
      parserName = stats.parserName,
      parserIdentifier = stats.parserId,
      parserInfo = stats.parserInfo
    )
    val parser = calculationParser.parserVersion
    val calculation = calculationParser.calculationInfo
    val rootNamespace = stats.rootNamespace
    val change = mutable.Map[String, Long]()
    val maxCalcUpdateRetries = 3
    var hasChanges: Boolean = false
    val calcUpdateFailure = ParsingStatsDb.failureForTransaction(dbContext, maxCalcUpdateRetries) {
      (conf: org.jooq.Configuration, iTry: Int) =>
        val oldValue = DSL.using(conf).select(
          STATS.CALCULATION_PARSING.META_STATS
        ).from(
          STATS.CALCULATION_PARSING
        ).where(
          STATS.CALCULATION_PARSING.CALCULATION_PARSER_ID.equal(new java.lang.Long(calculationParser.calculationParserId))
        ).fetchOne()
        val newStats = JObject(stats.parsingStats.map {
          case (k, v) =>
            k -> JInt(v)
        }(breakOut): List[(String, JInt)])
        if (oldValue != null) {
          val oldStats = oldValue.getValue(STATS.CALCULATION_PARSING.META_STATS)
          val oldStatsStr = lab.JsonUtils.normalizedStr(oldStats)
          val newStatsStr = lab.JsonUtils.normalizedStr(newStats)
          if (oldStatsStr != newStatsStr) {
            hasChanges = true
            if (oldStats != JNothing)
              logger.debug(s"Updating stats of namespace $rootNamespace ($iTry) for parser:${lab.JsonSupport.writeStr(parser)}, calculation:${lab.JsonSupport.writeStr(calculation)}; oldStats: $oldStatsStr, newStats: $newStatsStr")
            else
              logger.debug(s"Inserting stats of namespace $rootNamespace ($iTry) for parser:${lab.JsonSupport.writeStr(parser)}, calculation:${lab.JsonSupport.writeStr(calculation)}")
            // update change if needed
            calculation.rawDataId match {
              case Some(rawId) =>
                oldStats match {
                  case JNothing => ()
                  case JObject(obj) =>
                    change.clear()
                    obj.foreach {
                      case (k, JInt(v)) =>
                        change += (k -> -v.toLong)
                      case (k, v) =>
                        logger.warn(s"Ignoring unexpected value for key $k in old stats during stats update ($iTry) of namespace $rootNamespace for parser:${lab.JsonSupport.writeStr(parser)}, calculation:${lab.JsonSupport.writeStr(calculation)}, got ${lab.JsonUtils.normalizedStr(v)}")
                    }
                  case el =>
                    logger.warn(s"Ignoring unexpected old stats in stats update ($iTry) for namespace $rootNamespace, parser:${lab.JsonSupport.writeStr(parser)}, calculation:${lab.JsonSupport.writeStr(calculation)}, expected a dictionary but got ${lab.JsonUtils.normalizedStr(el)}")
                }
              case None => ()
            }
            DSL.using(conf).update(
              STATS.CALCULATION_PARSING
            ).set(
              STATS.CALCULATION_PARSING.META_STATS, newStats
            ).where(
              STATS.CALCULATION_PARSING.CALCULATION_PARSER_ID.equal(new java.lang.Long(calculationParser.calculationParserId))
            ).execute()
          } else {
            hasChanges = false
            logger.debug(s"No stats change ($iTry) for namespace $rootNamespace parser:${lab.JsonSupport.writeStr(parser)}, calculation:${lab.JsonSupport.writeStr(calculation)}")
          }
        } else {
          hasChanges = true
          DSL.using(conf).insertInto(
            STATS.CALCULATION_PARSING
          ).set(
            STATS.CALCULATION_PARSING.META_STATS, newStats
          ).set(
            STATS.CALCULATION_PARSING.CALCULATION_PARSER_ID, new java.lang.Long(calculationParser.calculationParserId)
          ).execute()
          logger.debug(s"Inserted stats ($iTry) for namespace $rootNamespace parser:${lab.JsonSupport.writeStr(parser)}, calculation:${lab.JsonSupport.writeStr(calculation)}")
        }
    }
    calcUpdateFailure match {
      case Some(e) =>
        logger.warn(s"Error updating stats for namespace $rootNamespace parser:${lab.JsonSupport.writeStr(parser)}, calculation:${lab.JsonSupport.writeStr(calculation)}", e)
      case None =>
        logger.debug(s"Successful stats update for namespace $rootNamespace parser:${lab.JsonSupport.writeStr(parser)}, calculation:${lab.JsonSupport.writeStr(calculation)}")
    }

    // update raw data stats if needed
    if (!hasChanges)
      return ()
    calculation.rawDataId match {
      case Some(rawId) =>
        stats.parsingStats.foreach {
          case (k, v) =>
            change += (k -> (change.getOrElse(k, 0: Long) + v))
        }
        var maxRawDataUpdateTries = 10
        val rawDataUpdateFailure = ParsingStatsDb.failureForTransaction(dbContext, maxRawDataUpdateTries) {
          (conf: org.jooq.Configuration, iTry: Int) =>
            // update raw data values in a single transaction
            // get old value, and merge with change
            val res = mutable.Map[String, Long]()
            res ++= change
            val oldRawDataStats = DSL.using(conf).select(
              STATS.RAW_DATA_PARSER_STATS.META_STATS
            ).from(
              STATS.RAW_DATA_PARSER_STATS
            ).where(
              STATS.RAW_DATA_PARSER_STATS.PARSER_ID.equal(new Integer(parser.parserDbId)).
                and(STATS.RAW_DATA_PARSER_STATS.ROOT_NAMESPACE_ID.equal(new Integer(calculationParser.rootNamespaceId))).
                and(STATS.RAW_DATA_PARSER_STATS.RAW_DATA_ID.equal(new Integer(rawId)))
            ).fetchOne()
            if (oldRawDataStats != null) {
              val oldRawStats = oldRawDataStats.getValue(STATS.RAW_DATA_PARSER_STATS.META_STATS)
              oldRawStats match {
                case JNothing => ()
                case JObject(obj) =>
                  obj.foreach {
                    case (k, JInt(v)) =>
                      res += (k -> (res.getOrElse(k, 0: Long) + v.toLong))
                    case (k, v) =>
                      logger.warn(s"Ignoring unexpected value for key $k in raw data stats during raw data update for parser:${lab.JsonSupport.writeStr(parser)}, calculation:${lab.JsonSupport.writeStr(calculation)}, got ${lab.JsonUtils.normalizedStr(v)}")
                  }
                case el =>
                  logger.warn(s"Ignoring unexpected raw data stats in raw data update parser:${lab.JsonSupport.writeStr(parser)}, calculation:${lab.JsonSupport.writeStr(calculation)}, expected a dictionary but got ${lab.JsonUtils.normalizedStr(el)}")
              }
              DSL.using(conf).update(
                STATS.RAW_DATA_PARSER_STATS
              ).set(
                STATS.RAW_DATA_PARSER_STATS.META_STATS, JObject(
                (res.map {
                  case (k, v) => (k -> (if (v == 0) JNothing else JInt(v)))
                }(breakOut)): List[(String, JValue)]
              )
              ).where(
                STATS.RAW_DATA_PARSER_STATS.PARSER_ID.equal(new Integer(parser.parserDbId)).
                  and(STATS.RAW_DATA_PARSER_STATS.ROOT_NAMESPACE_ID.equal(new Integer(calculationParser.rootNamespaceId))).
                  and(STATS.RAW_DATA_PARSER_STATS.RAW_DATA_ID.equal(new Integer(rawId)))
              ).execute()
              logger.debug(s"updated raw data stats in try $iTry with values from namespace: $rootNamespace, parser:${lab.JsonSupport.writeStr(parser)}, calculation:${lab.JsonSupport.writeStr(calculation)}")
            } else {
              DSL.using(conf).insertInto(
                STATS.RAW_DATA_PARSER_STATS
              ).set(
                STATS.RAW_DATA_PARSER_STATS.META_STATS, JObject(
                res.map {
                  case (k, v) => k -> (if (v == 0) JNothing else JInt(v))
                }(breakOut): List[(String, JValue)]
              )
              ).set(
                STATS.RAW_DATA_PARSER_STATS.PARSER_ID, new Integer(parser.parserDbId)
              ).set(
                STATS.RAW_DATA_PARSER_STATS.ROOT_NAMESPACE_ID, new Integer(calculationParser.rootNamespaceId)
              ).set(
                STATS.RAW_DATA_PARSER_STATS.RAW_DATA_ID, new Integer(rawId)
              ).execute()
              logger.debug(s"initialized raw data stats with values from namespace: $rootNamespace, parser:${lab.JsonSupport.writeStr(parser)}, calculation:${lab.JsonSupport.writeStr(calculation)}")
            }
        }
        rawDataUpdateFailure match {
          case Some(exc) =>
            logger.warn(s"Could not update raw stats for namespace: $rootNamespace, parser:${lab.JsonSupport.writeStr(parser)}, calculation:${lab.JsonSupport.writeStr(calculation)} due to exception $exc")
          case None =>
            logger.debug(s"Updated raw stats for namespace: $rootNamespace, parser:${lab.JsonSupport.writeStr(parser)}, calculation:${lab.JsonSupport.writeStr(calculation)}")
        }
      case None => ()
    }
  }

  /**
   * register the start of a scan of the given raw data
   */
  def registerTreeScanStart(rootNamespace: String, treeUri: String): Unit = {
    val rawDataId = rawDataIdFetchOrInsert(treeUri)
    val namespaceId = rootNamespaceIdFetchOrInsert(rootNamespace)
    val oldValue = dbContext.select(
      STATS.TREE_SCAN.TREE_SCAN_ID,
      STATS.TREE_SCAN.LAST_PARSE_START
    ).from(
      STATS.TREE_SCAN
    ).where(
      STATS.TREE_SCAN.RAW_DATA_ID.equal(new Integer(rawDataId)).
        and(STATS.TREE_SCAN.ROOT_NAMESPACE_ID.equal(new Integer(namespaceId)))
    ).fetchOne()
    val now = new java.sql.Timestamp((new java.util.Date).getTime())
    if (oldValue == null) {
      dbContext.insertInto(
        STATS.TREE_SCAN
      ).set(
        STATS.TREE_SCAN.RAW_DATA_ID, new Integer(rawDataId)
      ).set(
        STATS.TREE_SCAN.ROOT_NAMESPACE_ID, new Integer(namespaceId)
      ).set(
        STATS.TREE_SCAN.LAST_PARSE_START, now
      ).execute()
    } else {
      dbContext.update(
        STATS.TREE_SCAN
      ).set(
        STATS.TREE_SCAN.LAST_PARSE_START, now
      ).where(
        STATS.TREE_SCAN.RAW_DATA_ID.equal(new Integer(rawDataId)).
          and(STATS.TREE_SCAN.ROOT_NAMESPACE_ID.equal(new Integer(namespaceId)))
      ).execute()
    }
  }

  /**
   * register the end of a scan of the given raw data
   */
  def registerTreeScanEnd(rootNamespace: String, treeUri: String): Unit = {
    val rawDataId = rawDataIdFetchOrInsert(treeUri)
    val namespaceId = rootNamespaceIdFetchOrInsert(rootNamespace)
    val oldValue = dbContext.select(
      STATS.TREE_SCAN.TREE_SCAN_ID,
      STATS.TREE_SCAN.LAST_PARSE_END
    ).from(
      STATS.TREE_SCAN
    ).where(
      STATS.TREE_SCAN.RAW_DATA_ID.equal(new Integer(rawDataId)).
        and(STATS.TREE_SCAN.ROOT_NAMESPACE_ID.equal(new Integer(namespaceId)))
    ).fetchOne()
    val now = new java.sql.Timestamp((new java.util.Date).getTime())
    if (oldValue == null) {
      dbContext.insertInto(
        STATS.TREE_SCAN
      ).set(
        STATS.TREE_SCAN.RAW_DATA_ID, new Integer(rawDataId)
      ).set(
        STATS.TREE_SCAN.ROOT_NAMESPACE_ID, new Integer(namespaceId)
      ).set(
        STATS.TREE_SCAN.LAST_PARSE_END, now
      ).execute()
    } else {
      dbContext.update(
        STATS.TREE_SCAN
      ).set(
        STATS.TREE_SCAN.LAST_PARSE_END, now
      ).where(
        STATS.TREE_SCAN.RAW_DATA_ID.equal(new Integer(rawDataId)).
          and(STATS.TREE_SCAN.ROOT_NAMESPACE_ID.equal(new Integer(namespaceId)))
      ).execute()
    }
  }

  /**
   * called after a tree has been fully parsed with some stats
   */
  def registerTreeProcessed(rootNamespace: String, treeUri: String, parserVersion: ParserVersion, treeInfo: TreeInfo): Unit = {
    val rawDataId = rawDataIdFetchOrInsert(treeUri)
    val namespaceId = rootNamespaceIdFetchOrInsert(rootNamespace)
    val parserDbId = parserVersionFetchOrInsert(parserVersion.parserName, parserVersion.parserId, parserVersion.parserInfo)
    /*
      val oldValue = dbContext.select(
        STATS.TREE_SCAN.TREE_SCAN_ID,
        STATS.TREE_SCAN.LAST_PARSE_END
      ).from(
        STATS.TREE_SCAN
      ).where(
        STATS.TREE_SCAN.RAW_DATA_ID.equal(new Integer(rawDataId)).
          and(STATS.TREE_SCAN.ROOT_NAMESPACE_ID.equal(new Integer(namespaceId)))
      ).fetchOne()
      val now = new java.sql.Timestamp((new java.util.Date).getTime())
      if (oldValue == null) {
        dbContext.insertInto(
          STATS.TREE_SCAN
        ).set(
          STATS.TREE_SCAN.RAW_DATA_ID, new Integer(rawDataId)
        ).set(
          STATS.TREE_SCAN.ROOT_NAMESPACE_ID, new Integer(namespaceId)
        ).set(
          STATS.TREE_SCAN.LAST_PARSE_END, now
        ).execute()
      } else {
        dbContext.update(
          STATS.TREE_SCAN
        ).set(
          STATS.TREE_SCAN.LAST_PARSE_END, now
        ).where(
          STATS.TREE_SCAN.RAW_DATA_ID.equal(new Integer(rawDataId)).
            and(STATS.TREE_SCAN.ROOT_NAMESPACE_ID.equal(new Integer(namespaceId)))
        ).execute()
      }
     */
  }
}
