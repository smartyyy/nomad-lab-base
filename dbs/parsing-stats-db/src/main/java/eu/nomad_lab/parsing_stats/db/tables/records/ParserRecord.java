/*
 * This file is generated by jOOQ.
*/
package eu.nomad_lab.parsing_stats.db.tables.records;


import eu.nomad_lab.parsing_stats.db.tables.Parser;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record4;
import org.jooq.Row4;
import org.jooq.impl.UpdatableRecordImpl;
import org.json4s.JsonAST.JValue;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class ParserRecord extends UpdatableRecordImpl<ParserRecord> implements Record4<Integer, String, String, JValue> {

    private static final long serialVersionUID = -1432030790;

    /**
     * Setter for <code>parser.parser_id</code>.
     */
    public void setParserId(Integer value) {
        set(0, value);
    }

    /**
     * Getter for <code>parser.parser_id</code>.
     */
    public Integer getParserId() {
        return (Integer) get(0);
    }

    /**
     * Setter for <code>parser.parser_identifier</code>.
     */
    public void setParserIdentifier(String value) {
        set(1, value);
    }

    /**
     * Getter for <code>parser.parser_identifier</code>.
     */
    public String getParserIdentifier() {
        return (String) get(1);
    }

    /**
     * Setter for <code>parser.parser_name</code>.
     */
    public void setParserName(String value) {
        set(2, value);
    }

    /**
     * Getter for <code>parser.parser_name</code>.
     */
    public String getParserName() {
        return (String) get(2);
    }

    /**
     * Setter for <code>parser.parser_info</code>.
     */
    public void setParserInfo(JValue value) {
        set(3, value);
    }

    /**
     * Getter for <code>parser.parser_info</code>.
     */
    public JValue getParserInfo() {
        return (JValue) get(3);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record4 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row4<Integer, String, String, JValue> fieldsRow() {
        return (Row4) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row4<Integer, String, String, JValue> valuesRow() {
        return (Row4) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return Parser.PARSER.PARSER_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return Parser.PARSER.PARSER_IDENTIFIER;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return Parser.PARSER.PARSER_NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<JValue> field4() {
        return Parser.PARSER.PARSER_INFO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getParserId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getParserIdentifier();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getParserName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JValue value4() {
        return getParserInfo();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ParserRecord value1(Integer value) {
        setParserId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ParserRecord value2(String value) {
        setParserIdentifier(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ParserRecord value3(String value) {
        setParserName(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ParserRecord value4(JValue value) {
        setParserInfo(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ParserRecord values(Integer value1, String value2, String value3, JValue value4) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached ParserRecord
     */
    public ParserRecord() {
        super(Parser.PARSER);
    }

    /**
     * Create a detached, initialised ParserRecord
     */
    public ParserRecord(Integer parserId, String parserIdentifier, String parserName, JValue parserInfo) {
        super(Parser.PARSER);

        set(0, parserId);
        set(1, parserIdentifier);
        set(2, parserName);
        set(3, parserInfo);
    }
}
