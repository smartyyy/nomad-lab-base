/*
 * This file is generated by jOOQ.
*/
package eu.nomad_lab.parsing_stats.db.tables.records;


import eu.nomad_lab.parsing_stats.db.tables.CalculationParser;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record4;
import org.jooq.Row4;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.9.3"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class CalculationParserRecord extends UpdatableRecordImpl<CalculationParserRecord> implements Record4<Long, Integer, Long, Integer> {

    private static final long serialVersionUID = -903155887;

    /**
     * Setter for <code>calculation_parser.calculation_parser_id</code>.
     */
    public void setCalculationParserId(Long value) {
        set(0, value);
    }

    /**
     * Getter for <code>calculation_parser.calculation_parser_id</code>.
     */
    public Long getCalculationParserId() {
        return (Long) get(0);
    }

    /**
     * Setter for <code>calculation_parser.parser_id</code>.
     */
    public void setParserId(Integer value) {
        set(1, value);
    }

    /**
     * Getter for <code>calculation_parser.parser_id</code>.
     */
    public Integer getParserId() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>calculation_parser.calculation_id</code>.
     */
    public void setCalculationId(Long value) {
        set(2, value);
    }

    /**
     * Getter for <code>calculation_parser.calculation_id</code>.
     */
    public Long getCalculationId() {
        return (Long) get(2);
    }

    /**
     * Setter for <code>calculation_parser.root_namespace_id</code>.
     */
    public void setRootNamespaceId(Integer value) {
        set(3, value);
    }

    /**
     * Getter for <code>calculation_parser.root_namespace_id</code>.
     */
    public Integer getRootNamespaceId() {
        return (Integer) get(3);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Long> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record4 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row4<Long, Integer, Long, Integer> fieldsRow() {
        return (Row4) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row4<Long, Integer, Long, Integer> valuesRow() {
        return (Row4) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field1() {
        return CalculationParser.CALCULATION_PARSER.CALCULATION_PARSER_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field2() {
        return CalculationParser.CALCULATION_PARSER.PARSER_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Long> field3() {
        return CalculationParser.CALCULATION_PARSER.CALCULATION_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field4() {
        return CalculationParser.CALCULATION_PARSER.ROOT_NAMESPACE_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value1() {
        return getCalculationParserId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value2() {
        return getParserId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long value3() {
        return getCalculationId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value4() {
        return getRootNamespaceId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CalculationParserRecord value1(Long value) {
        setCalculationParserId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CalculationParserRecord value2(Integer value) {
        setParserId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CalculationParserRecord value3(Long value) {
        setCalculationId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CalculationParserRecord value4(Integer value) {
        setRootNamespaceId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CalculationParserRecord values(Long value1, Integer value2, Long value3, Integer value4) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached CalculationParserRecord
     */
    public CalculationParserRecord() {
        super(CalculationParser.CALCULATION_PARSER);
    }

    /**
     * Create a detached, initialised CalculationParserRecord
     */
    public CalculationParserRecord(Long calculationParserId, Integer parserId, Long calculationId, Integer rootNamespaceId) {
        super(CalculationParser.CALCULATION_PARSER);

        set(0, calculationParserId);
        set(1, parserId);
        set(2, calculationId);
        set(3, rootNamespaceId);
    }
}
