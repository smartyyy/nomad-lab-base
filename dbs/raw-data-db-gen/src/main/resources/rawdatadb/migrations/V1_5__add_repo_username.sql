-- alter table upload
--   drop column repo_username;

alter table author
  add column repo_username varchar(80), 
  add constraint u_author_repo_username unique (repo_username);
