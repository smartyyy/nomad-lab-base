-- -- add indexes to foreign constraints to improve the querying

drop index if exists idx_chunk_piece_upload_chunk_id;

drop index if exists idx_upload_chunk_upload_id;
drop index if exists idx_upload_chunk_author_group_id;
drop index if exists idx_upload_chunk_citation_group_id;

drop index if exists idx_upload_author_id;

drop index if exists idx_raw_data_upload_chunk_id;
drop index if exists idx_raw_data_author_group_id;
drop index if exists idx_raw_data_citation_group_id;

--

create index idx_chunk_piece_upload_chunk_id on chunk_piece(upload_chunk_id);

create index idx_upload_chunk_upload_id on upload_chunk(upload_id);
create index idx_upload_chunk_author_group_id on upload_chunk(author_group_id);
create index idx_upload_chunk_citation_group_id on upload_chunk(citation_group_id);

create index idx_upload_author_id on upload(author_id);

create index idx_raw_data_upload_chunk_id on raw_data(upload_chunk_id);
create index idx_raw_data_author_group_id on raw_data(author_group_id);
create index idx_raw_data_citation_group_id on raw_data(citation_group_id);
