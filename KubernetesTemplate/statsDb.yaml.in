apiVersion: v1
kind: Service
metadata:
  name: parsingstatsdb
  namespace: default
  labels:
    component: parsingstatsdb
spec:
  type: NodePort
  ports:
    - port: 5432
      name: postgres-port
      targetPort: 5432
      protocol: TCP
  selector:
    app: parsingstatsdb
---
apiVersion: v1
kind: ReplicationController
metadata:
  name: parsingstatsdb-controller
  namespace: default
  labels:
    component: parsingstatsdb
spec:
  replicas: 1
  template:
    metadata:
      labels:
        app: postgres
        component: parsingstatsdb
    spec:
      containers:
      - image: postgres:9.5
        name: parsingstatsdb
        ports:
          - containerPort: 5432
            name: postgres-port
            protocol: TCP
        volumeMounts:
        - mountPath: /var/lib/postgresql/data
          name: postgres-stats-db
          readOnly: true
        env:
          - name: POSTGRES_USER
            value: "${nomad_lab.parsing_stats.username}"
          - name: POSTGRES_PASSWORD
            value: "${nomad_lab.parsing_stats.password}"
      volumes:
      - name: postgres-stats-db
        hostPath:
          path: /nomad/nomadlab/parsingstats/default/dbdata
---
