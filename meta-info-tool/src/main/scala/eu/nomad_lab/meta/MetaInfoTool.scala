/*
   Copyright 2016-2018 Arvid Conrad Ihrig, Fawzi Roberto Mohamed
                       Fritz-Haber-Institut der Max-Planck-Gesellschaft

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */

package eu.nomad_lab.meta

import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.nio.file.StandardCopyOption
import java.time.ZonedDateTime

import com.typesafe.scalalogging.StrictLogging
import scala.util.control.NonFatal
import scala.util.matching.Regex
import eu.nomad_lab.LocalEnv
import eu.nomad_lab.JsonSupport.formats

object MetaInfoTool extends StrictLogging {

  def main(args: Array[String]): Unit = {
    if (args.length == 0) {
      println(usage)
      return
    }
    var list: List[String] = args.toList
    if (list.isEmpty) {
      println(usage)
      return
    }
    val cmd = list.head
    list = list.tail
    cmd match {
      case "--help" | "-h" =>
        println(usage)
      case "convert-old" => convertOldCommand(list)
      case "reformat" => reformatCommand(list)
      case command =>
        println(s"invalid command $command")
        println(usage)
    }
  }

  private val usage = """
      |Usage:
      |metaInfoTool [--help] <command>
      |
      |  Available commands:
      |
      |  convert-old: Converts the meta info from the old format to the new
      |  reformat: Reformats the "new" metainfo
      |  explode: explodes a dictionary to a directory
      |  implode: implodes a directory to a dictionary
      |
      |  Run archiveTool <command> [--help] to view information about the
      |  options for each command.""".stripMargin

  private val usage_convert_old = """metaInfoTool convert-old
      |    [--to=<dir>] [--no-overwrite] <file1> [<file2>...]
      |
      |  Converts a meta info from the old format to the new one.""".stripMargin

  private val usage_reformat = """metaInfoTool reformat <file1> [<file2>...]
      |
      |  Reformats the meta info dictionaries <file1> ..."""

  def convertOldCommand(args: List[String]): Unit = {
    var list: List[String] = args
    val toDirRe = "^--to-dir=(.*)$".r
    var toDir: Path = Paths.get(".")
    var files: Seq[String] = Seq()
    var overwrite: Boolean = true
    while (list.nonEmpty) {
      val arg = list.head
      list = list.tail
      arg match {
        case "--help" | "-h" =>
          println(usage_convert_old)
          return
        case toDirRe(r) => toDir = Paths.get(r)
        case "--no-overwrite" => overwrite = false
        case f => files = files :+ f
      }
    }
    for (p <- files) {
      val inP = Paths.get(p)
      val outP = toDir.resolve(inP.getFileName())
      if (Files.exists(outP) && Files.isSameFile(inP, outP))
        throw new Exception("Same in & out file, $inP vs $outP, refusing to run...")
      val newEnv = SimpleMetaInfoEnv.fromFilePath(
        filePath = p,
        dependencyResolver = new RelativeDependencyResolver()
      )
      val dict = newEnv.toMetaDictionary()
      dict.toPath(outP)
    }
  }

  def reformatCommand(args: List[String]): Unit = {
    val files = args
    for (p <- files) {
      val inP = Paths.get(p)
      val outP = Files.createTempFile(inP.getParent, inP.getFileName.toString.slice(0, 5), ".tmp", LocalEnv.filePermissionsAttributes)
      val dict = MetaDictionary.fromFilePath(inP)
      dict.toPath(outP, overwrite = true, checkName = false)
      Files.move(inP, inP.getParent.resolve(inP.getFileName.toString + ".bk"),
        StandardCopyOption.REPLACE_EXISTING)
      Files.move(outP, inP, StandardCopyOption.REPLACE_EXISTING)
    }

  }

}
