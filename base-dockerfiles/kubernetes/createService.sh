sed -e "s/{{user}}/${1}/g;" service.yaml.in > service.yaml
curl -XPOST $KUBERNETES_MASTER/api/v1/namespaces/${1}/services -d@service.yaml
