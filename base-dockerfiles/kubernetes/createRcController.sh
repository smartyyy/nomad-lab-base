sed -e "s/{{user}}/${1}/g;" rc-controller.yaml.in > rc-controller.yaml
curl -XPOST $KUBERNETES_MASTER/api/v1/namespaces/${1}/replicationcontrollers -d@rc-controller.yaml
