#!/bin/bash
# Gets and installs hdf5 (with java components) and netcdf (and JNA)
# all stuff gets installed in linux-native/lib, and is copyed over to sbt lib
# directory (../lib)
#
# developed on ununtu 14.04, but should be useful as starting point also for
# other environments

myDir="$(dirname $0)"
if [[ "${myDir:0:1}" != "/" ]]; then
    if [[ "$myDir" == "." ]]; then
        # cosmetically more pleasing :)
        baseDir="$(pwd)"
    else
        baseDir="$(pwd)/$myDir"
    fi
else
    baseDir="$myDir"
fi

cd "$baseDir"

# install up to date cmake
if ! [[ -e "cmake-3.11.1-Linux-x86_64.tar.gz" ]] ; then
    wget https://cmake.org/files/v3.11/cmake-3.11.1-Linux-x86_64.tar.gz
fi
if [[ -e "cmake-3.11.1-Linux-x86_64.tar.gz" && ! -e "cmake-3.11.1-Linux-x86_64" ]] ; then
    tar xzf cmake-3.11.1-Linux-x86_64.tar.gz
fi
if [[ -e "cmake-3.11.1-Linux-x86_64/bin" ]] ; then
    export PATH=$baseDir/cmake-3.11.1-Linux-x86_64/bin:$PATH
fi

# setup java
if [[ -z "$JAVA_HOME" ]] ; then
    export JAVA_HOME="$(update-java-alternatives -l | head -1 | tr -s ' ' | cut -d ' ' -f 3)"
fi

# get hdf5 source
if ! [[ -e "CMake-hdf5-1.10.2.tar.gz" ]] ; then
    wget https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-1.10/hdf5-1.10.2/src/CMake-hdf5-1.10.2.tar.gz
fi
if [[ -e "CMake-hdf5-1.10.2.tar.gz" && ! -e "CMake-hdf5-1.10.2" ]] ; then
    tar xzf CMake-hdf5-1.10.2.tar.gz
fi
# compile hdf5
if [[ -e "CMake-hdf5-1.10.2/hdf5-1.10.2" && ! -e "CMake-hdf5-1.10.2/build/bin" ]] ; then
    cd CMake-hdf5-1.10.2
    patch HDF5options.cmake < ../HDF5options.patch
    ./build-unix.sh
    cd ..
fi
# install libs
if [[ -e "CMake-hdf5-1.10.2/build/_CPack_Packages/Linux/TGZ/HDF5-1.10.2-Linux/HDF_Group/HDF5/1.10.2/lib" ]] ; then
    mkdir -p hdf5-support/lib
    for f in jarhdf5-1.10.2.jar libhdf5_java.so libhdf5_java.so.100.2.0 libhdf5_java.so.101 libhdf5.settings ; do
        cp CMake-hdf5-1.10.2/build/_CPack_Packages/Linux/TGZ/HDF5-1.10.2-Linux/HDF_Group/HDF5/1.10.2/lib/$f hdf5-support/lib
    done
fi
