package eu.nomad_lab.webservice_base

import akka.actor.ActorSystem
import akka.event.Logging
import akka.http.scaladsl.model.{ ContentTypes, HttpEntity, HttpResponse, StatusCodes }
import akka.http.scaladsl.server.{ PathMatcher, RejectionHandler, Route }
import com.typesafe.config.Config
import com.typesafe.scalalogging.StrictLogging
import eu.nomad_lab.meta.MetaInfoEnv
import akka.http.scaladsl.server.Directives._
import com.github.swagger.akka.SwaggerHttpService
import com.github.swagger.akka.model.Info
import eu.nomad_lab.LocalEnv
import eu.nomad_lab.webservice_base.ResponseFormatter.ErrorFormatter
import io.swagger.models.Scheme

import scala.concurrent.ExecutionContext

/**
 * common implementation shared by NOMAD webservices, provides optional API endpoints
 * for an integrated Swagger API documentation
 */
trait Webservice extends StrictLogging {

  //abstract data to be provided by the implementation
  implicit def system: ActorSystem
  implicit def executionContext: ExecutionContext
  def serviceRoute: Route
  val config: Config
  val metaData: MetaInfoEnv
  val apiDocumentationClasses: Set[Class[_]]
  val apiDescription: String
  val apiVersion: String
  val apiTitle: String
  val skipDefinitions: Seq[String]

  lazy val log = Logging(system, classOf[Webservice])

  implicit def rejectionHandler: RejectionHandler = ResponseFormatter.RejectionFormatter

  private def swaggerDocsRoute(protocol: String, hostName: String, rootPath: String): Route = {
    pathPrefix("docs") {
      concat(
        path("index.html") {
          val source = getClass.getResourceAsStream("/swagger/index.html")
          val content = try
            scala.io.Source.fromInputStream(source).getLines.mkString("\n")
          finally
            source.close()
          val normalizedPath = rootPath.stripPrefix("/").stripSuffix("/") + "/"
          val path = s"$protocol$hostName/$normalizedPath"
          val response = LocalEnv.makeReplacements(Map("docPath" -> path), content)
          complete(HttpResponse(
            status = StatusCodes.OK,
            entity = HttpEntity(ContentTypes.`text/html(UTF-8)`, response)
          ))
        },
        getFromResourceDirectory("swagger")
      )
    }
  }

  /**
   * constructs the full Akka Http route for this service
   * @param serviceBaseName the service name, will be prepended to all API endpoint paths
   * @param rootPath optional global path prefix, will be prepended to the service base name
   * @param includeSwagger include endpoints for swagger documentation?
   * @return the route structure of the service
   */
  def fullRoutes(serviceBaseName: String, rootPath: Seq[String], includeSwagger: Boolean,
    hostName: String, protocol: io.swagger.models.Scheme): Route = {
    val path = rootPath.foldRight(PathMatcher(serviceBaseName))((x, y) => x / y)
    if (includeSwagger) {
      val pathString = rootPath.foldRight(serviceBaseName)((x, y) => s"$x/$y").stripSuffix("/")
      val docService = new SwaggerHttpService {
        override val apiClasses: Set[Class[_]] = apiDocumentationClasses
        override val schemes = List(protocol, Scheme.HTTPS, Scheme.HTTP).distinct
        override val host = hostName
        override val basePath = pathString
        override val apiDocsPath = "api-docs"
        override val unwantedDefinitions = skipDefinitions
        override val info = Info(
          description = apiDescription, version = apiVersion, title = apiTitle
        )
      }

      handleExceptions(new ErrorFormatter(log)) {
        logRequest("incoming", Logging.InfoLevel) {
          pathPrefix(path) {
            concat(
              serviceRoute,
              swaggerDocsRoute(s"${protocol.toValue}://", hostName, pathString),
              docService.routes
            )
          }
        }
      }
    } else {
      handleExceptions(new ErrorFormatter(log)) {
        logRequest("incoming", Logging.InfoLevel) {
          pathPrefix(path) {
            serviceRoute
          }
        }
      }
    }
  }

}
